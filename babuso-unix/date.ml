open! Babuso_lib.Import
open Ptime

type t = { time : Ptime.t; tz : int }
(** [time] is a POSIX timestamp, hence UTC, the [tz] is the time-zone of the
    date. *)

let opt_exn msg o = Option.value_exn ~message:(Fmt.str "Date: %s" msg) o

let pp_debug ppf { time; tz } =
  Fmt.pf ppf "{time: %s; tz: %d}" (Ptime.to_rfc3339 time) tz

let now ?(tz_s = 0) () =
  let time = Unix.gettimeofday () |> Ptime.of_float_s |> opt_exn "now failed" in
  { time; tz = tz_s }

module Local = struct
  let _default_tz_s = ref 0

  let update_tz () =
    let open Unix in
    let now = gettimeofday () in
    let local = localtime now |> mktime |> fst in
    let gm = gmtime now |> mktime |> fst in
    let tz_seconds = local -. gm in
    _default_tz_s := Float.to_int tz_seconds

  let () = update_tz ()

  let default_tz_s () =
    update_tz ();
    !_default_tz_s

  let now () = now ~tz_s:(default_tz_s ()) ()
end

let to_short_string ?(with_tz = true) { time; tz } =
  let (y, m, d), ((hh, mm, ss), tz) = to_date_time ~tz_offset_s:tz time in
  let ms = frac_s time |> Span.to_float_s in
  Fmt.str "%04d%02d%02d-%02d%02d%02d.%03d%s" y m d hh mm ss
    (ms *. 1000. |> Float.to_int)
    (if with_tz then
     Fmt.str "%s%02d%02d"
       (if tz >= 0 then "+" else "-")
       (abs tz / 60 / 60)
       (abs tz / 60 % 60)
    else "")
