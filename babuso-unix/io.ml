open! Babuso_lib.Import

module Raw = struct
  let series_name = "Babuso" (* for now, only one *)

  let command_to_string_list cmd =
    let open Heavy_shell.Heavy_series in
    let series = init series_name in
    (command series cmd)#output_lines
end

module Shell = struct
  type t = Default

  let default () = Default
  let with_fake_capability (_ctxt : < shell : t ; .. >) f = f

  let command_to_string_list ctxt =
    with_fake_capability ctxt (fun s -> Raw.command_to_string_list s)
end

module File = struct
  type file_io = File_io

  let default () = File_io
  let with_fake_capability (_ctxt : < file_io : file_io ; .. >) f = f
  let file_exists ctxt = with_fake_capability ctxt Caml.Sys.file_exists

  let read_lines ctxt =
    with_fake_capability ctxt (fun p ->
        let open Caml in
        let o = open_in p in
        let r = ref [] in
        try
          while true do
            r := input_line o :: !r
          done;
          assert false
        with _ ->
          close_in o;
          List.rev !r)

  let write_lines ctxt =
    with_fake_capability ctxt (fun p l ->
        let o = Caml.open_out p in
        List.iter l ~f:(Caml.Printf.fprintf o "%s\n");
        Caml.close_out o)

  let make_path ctxt =
    with_fake_capability ctxt (fun p ->
        let the_path = Fmt.str "%s" (Path.quote p) in
        let md = "mkdir -p " ^ the_path in
        let (_ : string list) = Raw.command_to_string_list md in
        ())
end

module Clock = struct
  type t = Clock

  let default () = Clock
  let with_fake_capability (_ctxt : < clock : t ; .. >) f = f
  let sleep ctxt = with_fake_capability ctxt (fun f -> Unix.sleepf f)
end
