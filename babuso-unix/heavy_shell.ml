open! Babuso_lib
open! Babuso_lib.Import
open Babuso_error

module Heavy_series = struct
  type t = { temp_dir_path : string; mutable index : int }

  let make_dir_f fmt_str path =
    let cmd = Fmt.str fmt_str path in
    match Caml.Sys.command cmd with
    | 0 -> ()
    | _other ->
        raise
          (Babuso_exn
             ( Io_error
                 { msg = Fmt.str "Error creating directory, command = %S" cmd },
               Some "Heavy_shell.Heavy_series.make_dir" ))

  let init name =
    let temp_dir_path =
      Fmt.str "/tmp/%s/%s" name (Date.now () |> Date.to_short_string)
    in
    make_dir_f "mkdir -p %s" (Path.quote temp_dir_path);
    { temp_dir_path; index = 0 }

  let fresh_tmp series readable =
    let open Path in
    let path =
      series.temp_dir_path // Fmt.str "%04d-%s" series.index readable
    in
    make_dir_f "mkdir -p %s" (Path.quote path);
    series.index <- series.index + 1;
    path

  let read_lines path =
    let open Caml in
    let o = open_in path in
    let r = ref [] in
    try
      while true do
        r := input_line o :: !r
      done;
      assert false
    with _ ->
      close_in o;
      List.rev !r

  let command series cmd =
    let readable =
      let sanitize =
        String.map ~f:(function
          | ('a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '_' | '+' | '@') as ok
            ->
              ok
          | _ -> '_')
      in
      match String.length cmd with
      | l when l < 20 -> sanitize cmd
      | _ ->
          Fmt.str "%s...%s"
            (sanitize (String.subo cmd ~len:10))
            (sanitize (String.suffix cmd 10))
    in
    let tmp_dir = fresh_tmp series readable in
    let out = Path.(tmp_dir // "out") in
    let err = Path.(tmp_dir // "err") in
    let full_cmd =
      Fmt.str "{ %s ; } > %s 2> %s" cmd (Path.quote out) (Path.quote err)
    in
    match Caml.Sys.command full_cmd with
    | 0 ->
        object
          method out_path = out
          method err_path = err
          method output_lines = read_lines out
        end
    | other ->
        let std_err = read_lines err in
        let err, tag =
          ( Command_line_error
              {
                command = cmd;
                cmd_type = Unknown;
                exit_code = other;
                std_out = read_lines out;
                std_err;
              },
            Some "Heavy_shell.Heavy_series.command" )
        in
        raise (Babuso_exn (err, tag))
end
