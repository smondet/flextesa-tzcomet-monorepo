include Base

module Smart_ml = struct
  let line_no = []
  let unline f = f ~line_no

  let init () =
    Stdlib.Printexc.register_printer (function
      | SmartML.Basics.SmartExcept errs ->
          Some
            (Fmt.str "smartml-error: %a"
               (Fmt.list SmartML.Basics.pp_smart_except)
               errs)
      | _ -> None);
    ()

  let () = init ()
end
