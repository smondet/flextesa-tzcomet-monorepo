open! Import

let default_type_checking_config = SmartML.Config.default

type untyped_entrypoint =
  ( SmartML.Basics.Untyped.command,
    SmartML.Type.t,
    SmartML.Basics.untyped )
  SmartML.Basics.entry_point

let contract :
    ?michelson_simplification:bool ->
    ?type_checking_simplify:bool ->
    ?storage_record:(string * SmartML.Type.t * SmartML.Expr.t) list ->
    untyped_entrypoint list ->
    [> `Code of string ] * [> `Storage_fields_order of string list ] =
 fun ?(michelson_simplification = true)
     ?(type_checking_simplify = default_type_checking_config.simplify)
     ?(storage_record = []) entrypoints ->
  let open SmartML in
  let (storage : Expr.t), (storage_fields_order : string list) =
    let open Construct in
    let field (f, _, _) = f in
    match storage_record with
    | [] -> (E.constant L.unit, [])
    | [ one ] ->
        ( E.typed_comb_record [ one; ("dummy", T.unit, E.constant L.unit) ],
          [ field one; "dummy" ] )
    | more -> (E.typed_comb_record more, List.map ~f:field more)
  in
  let type_checked =
    let open Basics in
    let entry_points_layout =
      let open Utils_pure.Binary_tree in
      right_comb
        (List.map entrypoints ~f:(fun (ep : _ entry_point) ->
             Layout.{ source = ep.channel; target = ep.channel }))
    in
    let contract =
      let flags =
        if michelson_simplification then (* the default *) []
        else [ Config.(Bool_Flag (Simplify, false)) ]
      in
      build_contract ~storage ~entry_points_layout entrypoints ~flags
    in
    let config = Config.{ default with simplify = type_checking_simplify } in
    Tools.Checker.check_contract config contract
  in
  let instance =
    SmartML.Basics.
      {
        template = type_checked;
        state =
          {
            balance = Utils_pure.Bigint.of_int 100;
            storage = None;
            baker = None;
            lazy_entry_points = [];
            metadata = [];
          };
      }
  in
  let compiled =
    Tools.Compiler.compile_instance ~scenario_vars:Utils.String.Map.empty
      instance
  in
  let concrete = Tools.Michelson.display_tcontract compiled in
  (`Code concrete, `Storage_fields_order storage_fields_order)
