open! Import
open Wallet_data

module Attempts = struct
  module Tzid = Tezai_base58_digest.Identifier

  let check f s =
    let () = f s in
    s

  let check_dec f s =
    let (_ : string) = f s in
    s

  let kt1 s = `Kt1 (check Tzid.Kt1_address.check s)
  let friend_tz check_f s = `Friend_tz (check check_f s)
  let friend_pk check_f s = `Friend_pk (check_dec check_f s)

  let tz_address =
    [
      friend_tz Tzid.Ed25519.Public_key_hash.check;
      friend_tz Tzid.Secp256k1.Public_key_hash.check;
      friend_tz Tzid.P256.Public_key_hash.check;
    ]

  let any_address = kt1 :: tz_address

  let process ~message attempts s =
    let open Drafting.Render.Env in
    match
      List.find_map attempts ~f:(fun f -> try Some (f s) with _ -> None)
    with
    | Some s -> return s
    | None -> failwith "%s" message
end

let uri_or_address s =
  let open Attempts in
  let uri_path scheme f s =
    match String.split s ~on:':' with
    | [ s; p ] when String.equal s scheme -> f p
    | _ -> assert false
  in
  let unencrypted (module S : Tzid.Signer) =
    uri_path "unencrypted" (fun sk ->
        let (_ : string) = S.Secret_key.decode sk in
        `Unencrypted ("unencrypted:" ^ sk))
  in
  let ledger s =
    let u = Uri.of_string s in
    match Uri.scheme u with Some "ledger" -> `Ledger s | _ -> assert false
  in
  let attempts =
    any_address
    @ [
        friend_pk Tzid.Ed25519.Public_key.decode;
        friend_pk Tzid.Secp256k1.Public_key.decode;
        friend_pk Tzid.P256.Public_key.decode;
        unencrypted (module Tzid.Ed25519);
        unencrypted (module Tzid.Secp256k1);
        unencrypted (module Tzid.P256);
        ledger;
      ]
  in
  process attempts s ~message:"This does not look like an URI or an Address."

let address s =
  let open Attempts in
  process any_address s ~message:"This does not look like a Tezos Address."

let address_to_string s =
  let open Drafting.Render.Env in
  address s >>= function `Friend_tz tz -> return tz | `Kt1 k -> return k

let address_to_michelson s =
  let open Drafting.Render.Env in
  let ret s = return (Michelson.C.string s) in
  address s >>= function `Friend_tz tz -> ret tz | `Kt1 k -> ret k

let signature s =
  let open Drafting.Render.Env in
  let module S = Tezai_base58_digest.Identifier.Generic_signer.Signature in
  try
    return
      (let (_ : S.t) = S.of_base58 s in
       s
        : Signature.t)
  with _ -> failwith "This is not a valid Base58 signature."

let any_z s =
  let open Drafting.Render.Env in
  match Z.of_string s with
  | _ when String.is_empty s ->
      failwith "The empty string is not a valid integer."
  | z -> return z
  | exception _ -> failwith "%S is not a proper integer." s

let positive_z s =
  let open Drafting.Render.Env in
  match Z.of_string s with
  | _ when String.is_empty s ->
      failwith "The empty string is not a valid natural."
  | z when Z.leq Z.zero z -> return z
  | _ -> failwith "%S is not a proper positive integer." s
  | exception _ -> failwith "%S is not a proper positive integer." s

let parse_amount s =
  let open Drafting.Render.Env in
  try
    let convert_float s =
      match Float.of_string s with
      | f when Float.(0. < f) -> f
      | _ -> Fmt.failwith "%S is not a proper positive number." s
      | exception _ -> Fmt.failwith "%S is not a proper number." s
    in
    let convert_int s =
      match Z.of_string s with
      | z when Z.leq Z.zero z -> z
      | _ -> Fmt.failwith "%S is not a proper positive integer." s
      | exception _ -> Fmt.failwith "%S is not a proper positive integer." s
    in
    let follow = "follow the format '<amount> [<unit>]'" in
    let v =
      match
        String.split ~on:' ' s
        |> List.filter_map ~f:(fun s ->
               match String.strip s with "" -> None | o -> Some o)
      with
      | [] -> Fmt.failwith "Should %s." follow
      | [ one ] -> `Tez (convert_float one)
      | [ one; two ] ->
          let u =
            match String.lowercase two with
            | "tz" | "tez" -> `Tez (convert_float one)
            | "mutez" -> `Mutez (convert_int one)
            | "usd" -> `Usd (convert_float one)
            | "eur" -> `Eur (convert_float one)
            | _ ->
                Fmt.failwith
                  "%S is not a proper unit, use tz, Tez, mutez, USD, EUR, \
                   nothing (= ꜩ)."
                  two
          in
          u
      | _ -> Fmt.failwith "%S does not %s" s follow
    in
    return v
  with Failure s -> failwith "%s" s

let amount_to_mutez parsed rates =
  let open Drafting.Render.Env in
  let rates () =
    match rates with
    | Some r -> return r
    | None -> failwith "Conversion rates are not available."
  in
  let of_tez t = return (Float.(t * 1_000_000.) |> Z.of_float) in
  match parsed with
  | `Mutez m -> return m
  | `Tez t -> of_tez t
  | `Usd m ->
      let/ rt = rates () in
      Float.(m / Wallet_data.Exchange_rates.usd rt) |> of_tez
  | `Eur m ->
      let/ rt = rates () in
      Float.(m / Wallet_data.Exchange_rates.eur rt) |> of_tez

let parse_amount_to_mutez s rates =
  let open Drafting.Render.Env in
  let/ united = parse_amount s in
  amount_to_mutez united rates

(** Checks that it parses as a Tezos public-key and returns the corresponding
    public key hash. *)
let public_key pk =
  let open Drafting.Render.Env in
  Tezai_base58_digest.Identifier.Generic_signer.(
    match Public_key.of_base58 pk with
    | pkgen ->
        return
          (object
             method pk = pk

             method pkh =
               Public_key_hash.of_public_key pkgen |> Public_key_hash.to_base58
          end)
    | exception e -> failwith "Failed to parse public key: %a" Exn.pp e)

let just_public_key s =
  let open Drafting.Render.Env in
  let/ o = public_key s in
  return o#pk

let threshold th ~public_keys =
  let open Drafting.Render.Env in
  match (Z.of_string th, List.length public_keys) with
  | exception _ -> failwith "The threshold must be a positive integer."
  | _ when String.is_empty th ->
      failwith "The threshold must be a positive integer."
  | n, _ when Z.lt n Z.zero ->
      failwith "The threshold must be a positive integer."
  | z, _ when Z.equal z Z.zero ->
      warntext (return z) "Technically OK, but that makes an unusable contract."
  | th, l when Z.gt th (Z.of_int l) ->
      failwith
        "You want the threshold to be lower or equal to the number of public \
         keys (%d here)."
        l
  | n, _ -> return n

let seconds_of_string name =
  let open Drafting.Render.Env in
  let with_warning = function
    | some when Z.lt some (Z.of_int 3600) ->
        return some
          ~warnings:
            [
              Prosaic_message.(
                inline_textf "Time-span %s seems too small to be useful." name);
            ]
    | some -> return some
  in
  let with_multiplier multiplier s =
    let/ z = positive_z s in
    with_warning Z.(z * of_int multiplier)
  in
  function
  | "" -> failwith "A Time-span %s cannot be empty" name
  | more -> (
      match
        String.split more ~on:' ' |> List.map ~f:String.strip
        |> List.filter ~f:String.(( <> ) "")
      with
      | [ one ] -> with_multiplier 1 one
      | [ one; unit ] -> begin
          let/ m =
            match String.lowercase unit with
            | "minutes" | "min" -> return 60
            | "hours" | "h" -> return (60 * 60)
            | "days" | "d" -> return (24 * 60 * 60)
            | "weeks" | "w" -> return (7 * 24 * 60 * 60)
            | "months" -> return (30 * 24 * 60 * 60)
            | other -> failwith "For %s: cannot understand unit: %S." name other
          in
          with_multiplier m one
        end
      | _ -> failwith "Cannot understand timespan %s: %S." name more)

let prosaic_validation_exn r =
  match r with
  | Ok o -> o
  | Error { Drafting.Render.errors; _ } ->
      Failure.raise
        Docpp.(
          text "Validation failure:"
          +++ itemize (List.map errors ~f:(fun m -> Prosaic_message.to_pp m)))

module Operation = struct
  module With_signatures = struct
    type t =
      | None_required
      | Not_yet
      | Multisig of {
          public_keys : Public_key.t list;
          threshold : Z.t;
          hash : Signature.Draft.Data_hash.t;
        }
    [@@deriving sexp, variants, equal]

    let make_hash ~destination ~parameters ~counter =
      Signature.Draft.Data_hash.digest_sexp
        Sexp.(
          List
            [
              Atom destination;
              Variable.Map.sexp_of_t parameters;
              Z.sexp_of_t counter;
            ])
  end

  let amount_mutez ~rates am = parse_amount_to_mutez am rates

  let source_account_id ?(is_just_gas = false) =
    let open Drafting.Render.Env in
    function
    | "" ->
        failwith "Missing %s account"
          (if is_just_gas then "Gas-Wallet" else "Source")
    | more -> return more

  let pk_collection ~valid pks =
    let open Drafting.Render.Env in
    let/ pks = pks in
    List.fold pks ~init:(return []) ~f:(fun prevm s ->
        let/ p = prevm in
        if valid then
          let/ pk = just_public_key s in
          return (pk :: p)
        else return (s :: p))

  let delegate_option dv =
    let open Drafting.Render.Env in
    of_var dv >>= function
    | "none" -> return Michelson.C.e_none
    | other when String.is_prefix other ~prefix:"tz" ->
        let/ mich = address_to_michelson other in
        return Michelson.C.(e_some mich)
    | other -> failwith "Wrong address-or-none for delegate: %S" other

  let lambda_unit_operations ~amount_mutez =
    let open Drafting.Render.Env in
    let open Michelson.C in
    let open Lambda_unit_operations.Draft in
    let contract_call ?entrypoint ~contract_type ~amount_z ~parameter address =
      [
        prim "PUSH" [ prim "address" []; string address ];
        prim
          ~annotations:
            (Option.value_map ~default:[] entrypoint ~f:(fun x -> [ x ]))
          "CONTRACT" [ contract_type ];
        assert_some;
        push t_mutez amount_z;
        push contract_type parameter;
        prim "TRANSFER_TOKENS" [];
      ]
    in
    let op =
      let cons l = [ seq l; prim "CONS" [] ] in
      function
      | Transfer { destination; amount } ->
          let/ amount_z = amount_mutez amount
          and/ dest = bind_ref destination ~f:address_to_string in
          return
            (cons
               (contract_call dest ~contract_type:t_unit ~amount_z
                  ~parameter:e_unit))
      | Delegation { delegate } ->
          delegate_option delegate >>= fun delegate ->
          return
            (cons
               [
                 push (t_option t_key_hash) delegate
                 (* ( match delegate with
                    | "none" -> e_none
                    | s -> e_some (string s) ) *);
                 prim "SET_DELEGATE" [];
               ])
      | Fa2_transfer
          { token_id_address; token_id_number; amount; source; destination } ->
          let/ address = bind_ref token_id_address ~f:address_to_string
          and/ tokid = bind_ref ~f:positive_z token_id_number
          and/ am = bind_ref ~f:positive_z amount
          and/ src = bind_ref ~f:address_to_michelson source
          and/ dest = bind_ref ~f:address_to_michelson destination in
          let entrypoint = "%transfer" in
          let contract_type =
            t_list
              (t_pair
                 [
                   t_address;
                   t_list (t_pair [ t_address; t_pair [ t_nat; t_nat ] ]);
                 ])
          in
          let parameter =
            seq [ seq [ src; seq [ seq [ dest; int tokid; int am ] ] ] ]
          in
          let amount_z = int Z.zero in
          return
            (cons
               (contract_call address ~entrypoint ~contract_type ~amount_z
                  ~parameter))
    in
    function
    | Structured ops ->
        map_reduce_collection ops ~map:op
        *! (collection_to_list ops >>= function
            | [] ->
                warntext (return ())
                  "The lambda is empty, hence valid but useless."
            | _ -> return ())
        >>= fun michops ->
        let all =
          prim "DROP" [] :: prim "NIL" [ prim "operation" [] ] :: michops
        in
        return (Lambda_unit_operations.michelson (seq all))

  let parameter ~rates param =
    let module T = Variable.Type in
    let open Drafting.Render.Env in
    let open Operation.Draft.Parameter_draft in
    begin
      let/ value =
        match param.v with
        | Address av ->
            let/ s = of_var av in
            let/ k = address_to_string s in
            return (Variable.Value.address k)
        | Amount av ->
            let/ am = of_var av in
            let/ z = amount_mutez ~rates am in
            return (Variable.Value.mutez z)
        | Of_type (T.Nat, sv) ->
            let/ str = of_var sv in
            let/ z = positive_z str in
            return (Variable.Value.nat z)
        | Of_type (T.Time_span, sv) ->
            let/ str = of_var sv in
            let/ s = seconds_of_string param.name str in
            return (Variable.Value.time_span s)
        | Of_type (t, v) ->
            let/ str = of_var v in
            let/ value =
              try return (Variable.Value.t_of_sexp (Sexplib.Sexp.of_string str))
              with e ->
                failwith
                  "Complex parameter: %a validation not implemented + \
                   s-expression failure: %S → %a"
                  Sexp.pp_hum (T.sexp_of_t t) str Exn.pp e
            in
            return value
              ~warnings:
                [
                  Prosaic_message.inline_textf
                    "Complex parameter: %a validation not implemented: %S"
                    Sexp.pp_hum (T.sexp_of_t t) str;
                ]
      in
      return (param.name, value)
    end

  let draft ~rates ~with_signatures draft :
      Operation.Specification.t Drafting.Render.Env.t =
    let open Wallet_data in
    let open Operation in
    let open Drafting.Render.Env in
    let no_signatures () =
      match with_signatures with
      | With_signatures.None_required | With_signatures.Not_yet -> return ()
      | With_signatures.Multisig _ -> failwith "Maybe a BUG?"
    in
    let signatures sl =
      match with_signatures with
      | With_signatures.None_required ->
          failwith "Call_generic_multisig + None_required = BUG"
      | With_signatures.Not_yet ->
          return (Variable.Value.signature_option_list [])
      | With_signatures.Multisig { public_keys; threshold; hash } ->
          let/ sl = map_reduce_collection sl ~map:(fun s -> return [ s ]) in
          let sol =
            Signature.Draft.As_list.to_signature_list sl ~hash ~keys:public_keys
          in
          let count =
            List.fold sol ~init:0 ~f:(fun prev -> function
              | None -> prev | Some _ -> prev + 1)
          in
          if Z.(lt (of_int count) threshold) then
            failwith "Not enough signatures: %d out of %a" count Z.pp_print
              threshold
          else return (Variable.Value.signature_option_list sol)
    in
    match draft with
    | Draft.Simple_transfer { source_account; destination_address; amount } ->
        let/ () = no_signatures ()
        and/ source = bind_ref source_account ~f:source_account_id
        and/ destination =
          bind_ref destination_address ~f:(fun addr -> address_to_string addr)
        and/ amount = bind_ref amount ~f:(amount_mutez ~rates) in
        return
          (Specification.transfer ~source ~destination ~amount
             ~entrypoint:"default" ~parameters:[])
    | Call_generic_multisig_update_keys
        {
          contract_address;
          gas_wallet;
          threshold = thshld;
          public_keys = pks;
          signatures = sigs;
        } ->
        let/ source =
          bind_ref ~f:(source_account_id ~is_just_gas:true) gas_wallet
        and/ pks = pk_collection ~valid:true (of_var pks)
        and/ thrs =
          let/ th = of_var thshld
          and/ public_keys = pk_collection ~valid:false (of_var pks) in
          let/ thrs = threshold th ~public_keys in
          return (Variable.Value.nat thrs)
        and/ sigs = signatures sigs in
        return
          (Specification.transfer ~source ~destination:contract_address
             ~amount:Z.zero ~entrypoint:"main/update_keys"
             ~parameters:
               [
                 ("threshold", thrs);
                 ("keys", Variable.Value.key_list pks);
                 ("signatures", sigs);
               ])
    | Call_generic_multisig_main
        { contract_address; gas_wallet; lambda; signatures = sigs } ->
        let source_account addr_ref =
          bind_ref addr_ref ~f:(source_account_id ~is_just_gas:true)
        in
        let lambda_unit_ops r =
          let/ draft = of_var r in
          lambda_unit_operations draft ~amount_mutez:(fun av ->
              let/ am = of_var av in
              let/ z = amount_mutez ~rates am in
              return (Michelson.C.int z))
        in
        let/ source = source_account gas_wallet
        and/ lambda = lambda_unit_ops lambda
        and/ sigs = signatures sigs in
        return
          (Specification.transfer ~source ~destination:contract_address
             ~amount:Z.zero ~entrypoint:"main/lambda"
             ~parameters:
               [
                 ( "lambda",
                   Variable.Value.lambda_unit_to_operation_list
                     Lambda_unit_operations.(lambda |> to_michelson) );
                 ("signatures", sigs);
               ])
    | Call_custom_contract_entrypoint
        { contract_address; entrypoint; gas_wallet; parameters } ->
        let/ source =
          bind_ref ~f:(source_account_id ~is_just_gas:true) gas_wallet
        and/ parameters = map_list parameters ~f:(parameter ~rates) in
        return
          (Specification.transfer ~source ~destination:contract_address
             ~amount:Z.zero ~entrypoint ~parameters)
    | Call_foreign_contract_entrypoint
        { contract_address; entrypoint; gas_wallet; parameters } ->
        let/ source =
          bind_ref ~f:(source_account_id ~is_just_gas:true) gas_wallet
        and/ parameters = map_list parameters ~f:(parameter ~rates) in
        return
          (Specification.transfer ~source ~destination:contract_address
             ~amount:Z.zero ~entrypoint ~parameters)
end

module Account = struct
  let michelson_annotation_string ?(default_forbidden = false) ~what s =
    let open Drafting.Render.Env in
    let l = String.to_list s in
    match
      List.filteri l ~f:(fun ith -> function
        | ('0' .. '9' | '_') when ith = 0 -> true
        | 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' -> false
        | _ -> true)
    with
    | [] when default_forbidden && String.equal s "default" ->
        failwith
          "%S is not valid, you cannot use the special word “default” when the \
           contract is expected to be able to receive funds. "
          s
    | [] when String.is_empty s -> failwith "A %s name cannot be empty." what
    | [] when String.length s > 31 ->
        failwith
          "%S is too long for a %s name, the maximum for Tezos is 31 \
           characters."
          s what
    | [] -> return ()
    | some ->
        failwith
          "%S is not a valid %s, use only alphanumeric characters the first \
           one being always a letter,%s %s not OK."
          s what
          (String.concat ~sep:"," (List.map some ~f:(Fmt.str " %C")))
          (if List.length some = 1 then "is" else "are")

  let fresh_entrypoint_variable current_list ~name ~subname ~right =
    (* [~right] is a [Value.t] in draft-validation and a [Type.t] variable
       collection. *)
    let rec find_name n c =
      let this = n ^ if c = 0 then "" else Fmt.str "_%d" (c + 1) in
      if List.Assoc.mem current_list this ~equal:String.equal then
        find_name n (c + 1)
      else this
    in
    let full_name = find_name (Fmt.str "%s_%s" name subname) 0 in
    (full_name, (full_name, right) :: current_list)

  let timer_variable_names_of_timer name =
    (name ^ "_timespan", name ^ "_last_accessed")

  let team_variable_names_of_team name =
    (name ^ "_counter", name ^ "_threshold", name ^ "_keys")

  let variables_of_custom_contract ~capabilities ~roles ~timers ~spending_limits
      ~teams =
    let open Drafting.Render.Env in
    let open Account.Draft in
    let make id name t = (id, (name, t)) in
    let/ rols =
      map_reduce_collection roles ~map:(fun s ->
          let/ n = of_var s.Role.public_name in
          return [ make s.Role.id n Variable.Type.address ])
    and/ slims =
      map_reduce_collection spending_limits ~map:(fun s ->
          let/ n = of_var s.Spending_limit.public_name in
          return [ make s.Spending_limit.id n Variable.Type.mutez ])
    and/ tims =
      map_reduce_collection timers ~map:(fun s ->
          let/ n = of_var s.Timer.public_name in
          let ts, la = timer_variable_names_of_timer n in
          return
            [
              make s.Timer.id ts Variable.Type.time_span;
              make (Timer_id.next s.Timer.id) la Variable.Type.timestamp_option;
            ])
    and/ teams =
      map_reduce_collection teams ~map:(fun s ->
          let/ n = of_var s.Team.public_name in
          let c, t, k = team_variable_names_of_team n in
          let id1 = s.Team.id in
          let id2 = Team_id.next s.Team.id in
          let id3 = Team_id.next id2 in
          return
            [
              make id1 c Variable.Type.nat;
              make id2 t Variable.Type.nat;
              make id3 k Variable.Type.weighted_key_list;
            ])
    and/ caps =
      map_reduce_collection capabilities ~map:(fun s ->
          let rec control =
            (* For now this basically an exhaustive/expanded version
               of return []: *)
            let open Capability.Control in
            function
            | Anyone | Sender_is _ | Timer_expires _ | Team_acknowledges _ ->
                return []
            | Or more | And more ->
                let/ l = collection_to_list more in
                List.fold l ~init:(return []) ~f:(fun prevm c ->
                    let/ prev = prevm and/ v = of_var c in
                    let/ l =
                      match v with None -> return prev | Some c -> control c
                    in
                    return (prev @ l))
          in
          let actions _ep_name actcol =
            let action_variables variables = function
              | Capability.Action.Do_nothing | Capability.Action.Set_delegate
              | Capability.Action.Reset_timer _
              | Capability.Action.Set_variable _ | Capability.Action.Transfer _
                ->
                  return variables
            in
            let/ actions = collection_to_list actcol in
            List.fold actions ~init:(return []) ~f:(fun prevm va ->
                let/ prev_variables = prevm and/ action = of_var va in
                let/ newvars = action_variables prev_variables action in
                return newvars)
          in
          let/ c =
            of_var s.Capability.control >>= function
            | None -> return []
            | Some c -> control c
          and/ a =
            let/ ep_name = of_var s.Capability.name in
            actions ep_name s.Capability.actions
          in
          return (c @ a))
    in
    return (rols @ slims @ tims @ teams @ caps)

  let draft ~rates ~address_of_account_id ~public_key_of_account_id content =
    let open Drafting.Render.Env in
    let open Account.Draft in
    match content with
    | From_address_or_uri uri ->
        let/ uri_string = of_var uri in
        let/ uri = uri_or_address uri_string in
        return (`Uri uri)
    | Generic_multisig { gas_wallet; threshold = th; public_keys = pks } ->
        let/ gas_wallet =
          bind_ref ~f:(Operation.source_account_id ~is_just_gas:true) gas_wallet
        and/ keys = Operation.pk_collection ~valid:true (of_var pks)
        and/ threshold =
          let/ th = of_var th
          and/ public_keys =
            Operation.pk_collection ~valid:false (of_var pks)
          in
          threshold th ~public_keys
        in
        return
          (`Generic_multisig
            (object
               method threshold = threshold
               method gas_wallet = gas_wallet
               method keys = keys
            end))
    | Custom_contract
        {
          gas_wallet;
          has_default_do_nothing;
          capabilities;
          roles;
          timers;
          spending_limits;
          teams;
        } -> (
        let check_non_duplicate_collection col ~projection ~what =
          let/ names =
            map_reduce_collection col ~map:(fun s ->
                let/ n = of_var (projection s) in
                return [ n ])
          in
          match List.find_all_dups ~compare:String.compare names with
          | [] -> return ()
          | more ->
              failwith "There are duplicate %s: %a." what
                Fmt.Dump.(list string)
                more
        in
        let get_role id =
          map_reduce_collection roles ~map:(fun s ->
              if Role_id.equal id s.Role.id then return [ s ] else return [])
          >>= function
          | [ s ] ->
              let/ name = of_var s.Role.public_name
              and/ account_id = of_var s.Role.initial_value in
              return
                (object
                   method variable_name = name
                   method account_id = account_id
                end)
          | _ -> failwith "Role not found: %a" Internal_id.pp id
        in
        let role_variable_name id =
          let/ r = get_role id in
          return r#variable_name
        in
        let spending_limit_variable_name id =
          map_reduce_collection spending_limits ~map:(fun s ->
              if Spending_limit_id.equal id s.Spending_limit.id then
                return [ s.Spending_limit.public_name ]
              else return [])
          >>= function
          | [ one ] -> of_var one
          | _ -> failwith "Variable name not found: %a" Internal_id.pp id
        in
        let not_empty_account_id what id =
          if String.is_empty id then failwith "%s must be filled-in." what
          else return ()
        in
        let roles_to_variables () =
          map_reduce_collection roles ~map:(fun s ->
              let/ name = of_var s.Role.public_name
              and/ account_id = of_var s.Role.initial_value in
              let/ addr = catch (fun () -> address_of_account_id account_id)
              and/ () =
                not_empty_account_id "Initial account for the role" account_id
              and/ () = michelson_annotation_string name ~what:"role name" in
              return [ (name, Variable.Value.address addr) ])
        in
        let timer_variable_names id =
          map_reduce_collection timers ~map:(fun s ->
              if Timer_id.equal id s.Timer.id then
                return [ s.Timer.public_name ]
              else return [])
          >>= function
          | [ one ] ->
              let/ name = of_var one in
              return (timer_variable_names_of_timer name)
          | _ -> failwith "Variable name not found: %a" Internal_id.pp id
        in
        let expand_team one =
          let/ name = of_var one.Team.public_name
          and/ init = of_var one.Team.initial_value in
          let/ initkeys =
            let open Team_draft in
            match init with
            | Percentages { keys } ->
                map_reduce_collection keys ~map:(fun (rolev, weightv) ->
                    let/ pk =
                      let/ role_id_o = of_var rolev in
                      let/ role_id =
                        somef role_id_o "Role missing in team %S." name
                      in
                      let/ role = get_role role_id in
                      catch (fun () -> public_key_of_account_id role#account_id)
                    and/ w = of_var weightv in
                    return [ (Z.of_int w, pk) ])
          in
          let counter_name, threshold_name, keys_name =
            team_variable_names_of_team name
          in
          let variables =
            [
              (keys_name, Variable.Value.weighted_key_list initkeys);
              (threshold_name, Variable.Value.nat (Z.of_int 100));
              (counter_name, Variable.Value.nat (Z.of_int 0));
            ]
          in
          return
            (object
               method keys_name = keys_name
               method threshold_name = threshold_name
               method counter_name = counter_name
               method variables = variables
            end)
        in
        let get_team id =
          map_reduce_collection teams ~map:(fun s ->
              if Team_id.equal id s.Team.id then return [ s ] else return [])
          >>= function
          | [ one ] -> expand_team one
          | _ -> failwith "Variable name not found: %a" Internal_id.pp id
        in
        let/ vars =
          variables_of_custom_contract ~capabilities ~roles ~timers
            ~spending_limits ~teams
        in
        let caps ~default_do_nothing () =
          let rec control name c =
            let/ controlopt = of_var c in
            somef controlopt "Control for capability %S is not set." name
            >>= function
            | Capability.Control.Anyone -> return Control.anyone
            | Capability.Control.Sender_is v ->
                let/ sender_role_id = of_var v in
                let/ sender_role_id =
                  somef sender_role_id
                    "Role for control “Sender-is” in %S is missing." name
                in
                let/ v = role_variable_name sender_role_id in
                return (Control.sender_is (Constant_or_variable.variable v))
            | Capability.Control.Timer_expires v ->
                let/ timer_role_id = of_var v in
                let/ timer_role_id =
                  somef timer_role_id
                    "Timer for control “Timer-expires” in %S is missing." name
                in
                let/ timespan, last_activity =
                  timer_variable_names timer_role_id
                in
                return
                  (Control.when_inactive
                     ~timespan:(Constant_or_variable.variable timespan)
                     ~last_activity)
            | Capability.Control.Team_acknowledges vo ->
                let/ team_id_opt = of_var vo in
                let/ team_id =
                  somef team_id_opt
                    "Team for control “Team-consensus” in %S is missing." name
                in
                let/ team = get_team team_id in
                let keys = Constant_or_variable.variable team#keys_name in
                let threshold =
                  Constant_or_variable.variable team#threshold_name
                in
                return
                  (Control.weighted_multisig ~counter:team#counter_name ~keys
                     ~threshold)
                (* failwith "Capability.Control.Team_acknowledges: not implemented" *)
            | Capability.Control.Or col ->
                let/ props =
                  map_reduce_collection col ~map:(fun vopt ->
                      let/ r = (control name) vopt in
                      return [ r ])
                in
                return (Control.or_ props)
            | Capability.Control.And col ->
                let/ props =
                  map_reduce_collection col ~map:(fun vopt ->
                      let/ r = (control name) vopt in
                      return [ r ])
                in
                return (Control.and_ props)
            (* | _ -> not_implemented "%s.control" name *)
          in
          let actions ep_name actcol =
            let action_to_abilities variables = function
              | Capability.Action.Do_nothing ->
                  return ([ Ability.do_nothing ], variables)
              | Capability.Action.Set_variable v ->
                  let/ ido = of_var v in
                  let/ id =
                    somef ido
                      "In entrypoint “%s” set-variable missing variable."
                      ep_name
                  in
                  let vto = List.Assoc.find vars id ~equal:Internal_id.equal in
                  let/ vn, vt =
                    somef vto "In entrypoint “%s” cannot find variable: %a"
                      ep_name Internal_id.pp id
                  in
                  return
                    ( [ Ability.update_variables [ (vn, vt) ] ],
                      (vn, vt) :: variables )
              | Capability.Action.Reset_timer timer_id ->
                  let/ timer_id_o = of_var timer_id in
                  let/ timer_id =
                    somef timer_id_o
                      "Timer for action “Reset-Timer” in %S is missing." ep_name
                  in
                  let/ _, last_activity = timer_variable_names timer_id in
                  return
                    ( [ Ability.reset_activity_timer last_activity ],
                      (last_activity, Variable.Type.time_span) :: variables )
              | Capability.Action.Transfer { amount_limit; destination } ->
                  let amount () =
                    let/ al = of_var amount_limit in
                    match al with
                    | None -> return (`No_limit, variables)
                    | Some aid ->
                        let/ name = spending_limit_variable_name aid in
                        return
                          ( `Limit (Constant_or_variable.variable name),
                            (name, Variable.Type.mutez) :: variables )
                  in
                  let destination () =
                    of_var destination >>= function
                    | None -> return (`Anyone, variables)
                    | Some role_id ->
                        let/ v = role_variable_name role_id in
                        return
                          ( `Address (Constant_or_variable.variable v),
                            (v, Variable.Type.address) :: variables )
                  in
                  let/ amount, variables_amount = amount ()
                  and/ destination, variables_dest = destination () in
                  return
                    ( [ Ability.transfer_mutez ~amount ~destination ],
                      variables_amount @ variables_dest @ variables )
              | Capability.Action.Set_delegate ->
                  return ([ Ability.set_delegate ], variables)
            in
            let/ actions = collection_to_list actcol in
            let/ l, v =
              List.fold actions
                ~init:(return ([], []))
                ~f:(fun prevm va ->
                  let/ prev_actions, prev_variables = prevm
                  and/ action = of_var va in
                  let/ abs, newvars =
                    action_to_abilities prev_variables action
                  in
                  return (prev_actions @ abs, newvars))
            in
            return (Ability.sequence l, v)
          in
          map_reduce_collection capabilities ~map:(fun s ->
              let/ name = of_var s.Capability.name in
              let/ control = control name s.Capability.control
              and/ ability, variables = actions name s.Capability.actions
              and/ () =
                michelson_annotation_string name ~what:"capability name"
                  ~default_forbidden:default_do_nothing
              in
              return [ (Entrypoint.make name ~control ~ability, variables) ])
        in
        let variables_set_by_capabilities () =
          map_reduce_collection capabilities ~map:(fun s ->
              map_reduce_collection s.Capability.actions ~map:(fun v ->
                  of_var v >>= function
                  | Capability.Action.Set_variable ov -> (
                      of_var ov >>= function
                      | None -> return []
                      | Some vid -> return [ vid ])
                  | Capability.Action.Reset_timer ov -> (
                      of_var ov >>= function
                      | None -> return []
                      | Some vid -> return [ Timer_id.next vid ])
                  | (_ : Capability.Action.t) -> return []))
        in
        let check_variables_set_by_capabilities () =
          let/ ids = variables_set_by_capabilities () in
          List.fold vars ~init:(return ()) ~f:(fun prevm (vid, (vn, _)) ->
              let/ () = prevm in
              if List.mem ids vid ~equal:Internal_id.equal then return ()
              else
                return ()
                  ~warnings:
                    [
                      Prosaic_message.inline_textf
                        "There is no entrypoint setting variable %s." vn;
                    ])
        in
        let timers_to_variables () =
          map_reduce_collection timers ~map:(fun s ->
              let/ name = of_var s.Timer.public_name
              and/ time = of_var s.Timer.initial_value in
              let/ seconds = seconds_of_string name time
              and/ () = michelson_annotation_string name ~what:"timer name" in
              let ts, la = timer_variable_names_of_timer name in
              return
                [
                  (ts, Variable.Value.time_span seconds);
                  (la, Variable.Value.timestamp_option None);
                ])
        in
        let spending_limits_to_variables () =
          map_reduce_collection spending_limits ~map:(fun s ->
              let/ name = of_var s.Spending_limit.public_name
              and/ limit = of_var s.Spending_limit.initial_value in
              let/ mutez = Operation.amount_mutez ~rates limit
              and/ () = michelson_annotation_string name ~what:"timer name" in
              return [ (name, Variable.Value.mutez mutez) ])
        in
        let teams_to_variables () =
          map_reduce_collection teams ~map:(fun s ->
              let/ team = expand_team s in
              return team#variables)
        in
        let/ default_do_nothing = of_var has_default_do_nothing in
        let/ gas_wallet =
          bind_ref ~f:(Operation.source_account_id ~is_just_gas:true) gas_wallet
        and/ eps_and_vars = caps ~default_do_nothing ()
        and/ roles_init = roles_to_variables ()
        and/ timers_init = timers_to_variables ()
        and/ spending_limits_init = spending_limits_to_variables ()
        and/ teams_inits = teams_to_variables ()
        and/ () =
          check_non_duplicate_collection roles ~projection:Role.public_name
            ~what:"role-names"
        and/ () =
          check_non_duplicate_collection capabilities
            ~projection:Capability.name ~what:"capability-names"
        and/ () = check_variables_set_by_capabilities () in
        let eps, ep_vars = List.unzip eps_and_vars in
        let variables_touched_by_actions = List.concat ep_vars in
        let entrypoints =
          if default_do_nothing then
            Entrypoint.make "default" ~control:Control.anyone
              ~ability:Ability.do_nothing
            :: eps
          else eps
        in
        let initialization, leftover =
          List.sort ~compare:Poly.compare
            (roles_init @ spending_limits_init @ timers_init
           @ teams_inits (* @ List.concat ep_vars *))
          |> List.partition_tf ~f:(fun (name, _) ->
                 List.exists vars ~f:(fun (_, (n, _)) -> String.equal n name))
        in
        let unitialized_but_used =
          List.filter_map variables_touched_by_actions ~f:(fun (n, _) ->
              if
                List.exists initialization ~f:(fun (nn, _) -> String.equal n nn)
              then None
              else Some n)
        in
        match unitialized_but_used with
        | [] ->
            return
              ~warnings:
                (List.map leftover ~f:(fun (name, v) ->
                     Prosaic_message.inline_textf
                       "Variable %S (= %s) is not used at all." name
                       (Variable.Value.to_michelson v |> Michelson.to_concrete)))
              (`Custom_contract
                (object
                   method gas_wallet = gas_wallet
                   method can_receive_funds = default_do_nothing
                   method entrypoints = entrypoints
                   method initialization = Variable.Map.of_list initialization
                end))
        | more ->
            failwith "Variables %a are used but not initialized."
              Fmt.Dump.(list string)
              more)
end
