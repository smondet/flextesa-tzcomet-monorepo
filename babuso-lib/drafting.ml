open! Import

module Reference = struct
  type 'a t = 'a Lwd.var

  let create v = Lwd.var v
  let set = Lwd.set
  let get = Lwd.peek
  let equal eq a b = eq (get a) (get b)
  let compare cmp a b = cmp (get a) (get b)
  let sexp_of_t f s = f (get s)
  let t_of_sexp o s = create (o s)
end

module Collection = struct
  type 'a t = 'a Lwd_table.t

  open Lwd_table

  let create = make
  let append = append'

  let of_list l =
    let self = create () in
    List.iter l ~f:(append self);
    self

  let get = get
  let set = set

  let fold t ~init ~f =
    let rec go acc = function
      | None -> acc
      | Some row -> (
          match get row with None -> acc | Some x -> go (f acc x) (next row))
    in
    go init (first t)

  let iteri_rows t ~f =
    let rec go count = function
      | None -> ()
      | Some s ->
          let prepare_next = next s in
          f count s;
          go (count + 1) prepare_next
    in
    go 0 (first t)

  let find_mapi t ~f =
    let rec go count = function
      | None -> None
      | Some s -> (
          let prepare_next = next s in
          match
            f count (get s |> Option.value_exn ~message:"find_mapi-get")
          with
          | Some s -> Some s
          | None -> go (count + 1) prepare_next)
    in
    go 0 (first t)

  let compare cmp a b =
    let rec go = function
      | None, None -> 0
      | Some _, None -> -1
      | None, Some _ -> 1
      | Some rowa, Some rowb -> begin
          match Option.compare cmp (get rowa) (get rowb) with
          | 0 -> go (next rowa, next rowb)
          | other -> other
        end
    in
    go (first a, first b)

  let equal eq a b =
    let cmp a b = if eq a b then 0 else 1 in
    compare cmp a b = 0

  let sexp_of_t f self =
    let l = fold self ~init:[] ~f:(fun p x -> f x :: p) in
    Sexp.List (List.rev l)

  let t_of_sexp o = function
    | Sexp.List l ->
        let self = create () in
        List.iter l ~f:(fun sexp -> append' self (o sexp));
        self
    | Atom a -> Fmt.failwith "Collection.of_sexp: unexpected atom: %S" a
end

module Render = struct
  type message = Prosaic_message.t [@@deriving sexp]

  (** We {i artificially} make the ['a ok] type incompatible to make sure {!Env}
      functions are only used within {!env} (or {!sample_env}). *)
  module Make_fresh_types () = struct
    type 'a ok = { v : 'a; [@main] warnings : message list }
    [@@deriving make, fields]

    type error = { warnings : message list; errors : message list }
    [@@deriving make, fields, sexp]

    type 'a t = ('a ok, error) Result.t Lwd.t

    let is_ok self =
      Lwd.map self ~f:(function Ok _ -> true | Error _ -> false)
  end

  module Outside = Make_fresh_types ()
  module In_env = Make_fresh_types ()
  include Outside

  let env f : _ t =
    Lwd.map (f ()) ~f:(function
      | Ok { In_env.v; warnings } -> Ok (make_ok v ~warnings)
      | Error { In_env.errors; warnings } ->
          Error (make_error ~errors ~warnings ()))

  let sample_env ?(on_warnings = fun (_ : message list) -> ()) f =
    env f |> Lwd.observe |> Lwd.quick_sample
    |> Result.map ~f:(fun { v; warnings } ->
           on_warnings warnings;
           v)

  module Env = struct
    include In_env

    let ok ?(warnings = []) v = Ok { v; warnings }
    let return ?warnings o : _ t = Lwd.return (ok ?warnings o)
    let error ?(warnings = []) errors = Error { warnings; errors }
    let fail e : _ t = Lwd.return (error [ e ])
    let of_var v : _ t = Lwd.get v |> Lwd.map ~f:ok

    let bind (o : _ t) ~f : _ t =
      Lwd.bind o ~f:(function
        | Ok o ->
            Lwd.map (f o.v) ~f:(function
              | Ok { v; warnings } -> Ok { v; warnings = o.warnings @ warnings }
              | Error { warnings; errors } ->
                  Error { warnings = o.warnings @ warnings; errors })
        | Error e -> Lwd.return (Error e))

    let bind_ref v ~f = bind (of_var v) ~f:(fun x -> f x)

    let map ?(warn = []) (o : _ t) ~f : _ t =
      Lwd.map o ~f:(function
        | Ok x -> ok (f x.v) ~warnings:(x.warnings @ warn)
        | Error _ as e -> e)

    let ignore_result o = map o ~f:(fun _ -> ())

    let failwith fmt =
      Fmt.kstr (fun s -> fail Prosaic_message.(text s |> inline)) fmt

    let warntext o fmt =
      Fmt.kstr
        (fun s -> map o ~f:Fn.id ~warn:[ Prosaic_message.(text s |> inline) ])
        fmt

    let of_result_message = function
      | Ok o -> return o
      | Error msg -> failwith "%s" msg

    let of_result = function Ok o -> return o | Error msg -> fail msg
    let catch f = try return (f ()) with e -> failwith "%a" Exn.pp e
    let ( >>= ) o f = bind o ~f
    let some ~error = function None -> fail error | Some s -> return s

    let somef o fmt =
      Fmt.kstr (fun s -> some ~error:Prosaic_message.(text s |> inline) o) fmt

    let ( ** ) (a : 'left t) (b : 'right t) : ('left * 'right) t =
      Lwd.bind (Lwd.pair a b) ~f:(fun (ar, br) ->
          Lwd.return
            (match (ar, br) with
            | Ok a, Ok b ->
                Ok { v = (a.v, b.v); warnings = a.warnings @ b.warnings }
            | Ok a, Error b ->
                Error { errors = b.errors; warnings = a.warnings @ b.warnings }
            | Error a, Ok b ->
                Error { errors = a.errors; warnings = a.warnings @ b.warnings }
            | Error a, Error b ->
                Error
                  {
                    errors = a.errors @ b.errors;
                    warnings = a.warnings @ b.warnings;
                  }))

    let ( *! ) (a : 'left t) (b : unit t) : 'left t =
      a ** b >>= fun (a, ()) -> return a

    let ( let/ ) = ( >>= )
    let ( and/ ) = ( ** )
    let not_implemented s = Fmt.kstr (failwith "not-implemented: %s") s

    let map_reduce_collection col ~(map : _ -> 'a list t) =
      Lwd_table.map_reduce
        (fun _ x -> map x)
        (return [], fun a b -> a ** b >>= fun (a, b) -> return (a @ b))
        col
      |> Lwd.join

    let collection_to_list col =
      map_reduce_collection ~map:(fun x -> return [ x ]) col

    let map_list l ~f : _ t =
      let tab = Lwd_table.make () in
      List.iter l ~f:(fun x -> Lwd_table.append' tab x);
      map_reduce_collection tab ~map:(fun x -> f x >>= fun x -> return [ x ])
  end
end
