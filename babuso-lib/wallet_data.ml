open! Import

module Bytes_rep = struct
  type t = As_hex of string  (** without the 0x *)
  [@@deriving sexp, equal, variants, compare]

  let of_hex s = As_hex s
  let empty = of_hex ""

  let of_binary s =
    let (`Hex h) = Hex.of_string s in
    As_hex h

  let of_binary_bytes s =
    let (`Hex h) = Hex.of_bytes s in
    As_hex h

  let to_binary (As_hex x) = Hex.to_string (`Hex x)
  let to_zero_x = function As_hex h -> Fmt.str "0x%s" h

  let to_zero_x_summary ?(limit = 40) ?(chunk = 12) blob =
    let b = to_zero_x blob in
    if String.length b < limit then b
    else
      let len = chunk in
      String.sub b ~pos:0 ~len ^ "…"
      ^ String.sub b ~pos:(String.length b - len) ~len

  let to_hex = function As_hex h -> h
  let to_pp h = Docpp.verbatim (to_zero_x h)
  let to_json = function As_hex h -> `String h
  let append a b = match (a, b) with As_hex ha, As_hex hb -> As_hex (ha ^ hb)

  let size = function
    | As_hex h ->
        let lh = String.length h in
        assert (lh % 2 = 0);
        lh / 2

  let hex_dump (As_hex h) = Hex.hexdump_s (`Hex h)
end

module B58 = struct
  type t = string [@@deriving sexp, equal, compare]
end

module Address = struct
  type t = B58.t [@@deriving sexp, equal, compare]
end

module Public_key = struct
  type t = B58.t [@@deriving sexp, equal, compare]
end

module Public_key_hash = struct
  type t = B58.t [@@deriving sexp, equal, compare]
end

module Michelson_expression = struct
  type t = Untyped of Michelson.t [@@deriving sexp, equal, variants, compare]

  let of_michelson m = untyped m
  let to_michelson (Untyped u) = u
end

module Timestamp = struct
  type t = Seconds of Z.t [@@deriving sexp, equal, compare, variants]
end

module Signature = struct
  type t = B58.t [@@deriving sexp, equal, compare]

  let to_bytes s =
    Tezai_base58_digest.Identifier.Generic_signer.Signature.(
      of_base58 s |> to_bytes)
    |> Bytes_rep.of_binary

  (* module Full = struct
       type t = {public_key: Public_key.t; blob: Bytes_rep.t; signature: t}
       [@@deriving sexp, equal]
     end *)

  module Draft = struct
    open Drafting

    module Data_hash = struct
      type t = H of string [@@deriving sexp, equal, compare]

      let digest_sexp s = H (Sexp.to_string_mach s |> Caml.Digest.string)
    end

    type hash = Data_hash.t [@@deriving sexp, equal, compare]
    type sig_draft = t option [@@deriving sexp, equal, compare]
    type sig_req = hash * Public_key.t [@@deriving sexp, equal, compare]

    type t = (sig_req * sig_draft) Collection.t
    [@@deriving sexp, equal, compare]

    (* let of_blob t b = List.Assoc.find t ~equal:Bytes_rep.equal b *)

    let add_or_replace (self : t) ~hash ~pk s =
      let found = ref false in
      Collection.iteri_rows self ~f:(fun _ row ->
          match Collection.get row with
          | Some (req, _) when equal_sig_req req (hash, pk) ->
              found := true;
              Collection.set row (req, Some s)
          | None | Some _ -> ());
      if !found then () else Collection.append self ((hash, pk), Some s)

    module As_list = struct
      type t = (sig_req * sig_draft) List.t [@@deriving sexp, equal, compare]

      let get (self : t) ~hash ~pk =
        List.find_map self ~f:(fun (req, s) ->
            if equal_sig_req req (hash, pk) then s else None)

      let to_signature_list self ~hash ~keys =
        List.map keys ~f:(fun pk -> get self ~hash ~pk)
    end
  end
end

module Mutez = struct
  include Z

  let to_tez_string mu =
    assert (Z.compare mu Z.zero >= 0);
    (mu / of_int 1_000_000 |> to_string)
    ^
    match mu mod of_int 1_000_000 |> to_int with
    | 0 -> ""
    | more -> Fmt.str ".%06d" more
end

module Michelson_type = struct
  type t = Lambda_unit_operations [@@deriving sexp, equal, compare, variants]
end

module Lambda_unit_operations = struct
  type t = Michelson of Michelson.t [@@deriving sexp, equal, variants]

  let to_michelson = function Michelson m -> m

  type lambda_t = t

  module Draft = struct
    open Drafting

    type op =
      | Transfer of {
          destination : string Reference.t;
          amount : string Reference.t;
        }
      | Delegation of { delegate : string Reference.t }
      | Fa2_transfer of {
          token_id_address : string Reference.t;
          token_id_number : string Reference.t;
          amount : string Reference.t;
          source : string Reference.t;
          destination : string Reference.t;
        }
    [@@deriving sexp, equal, variants, compare]

    type t = Structured of op Collection.t
    [@@deriving sexp, equal, variants, compare]

    let create () = structured (Collection.create ())

    let empty_transfer () =
      transfer ~destination:(Reference.create "") ~amount:(Reference.create "")

    let empty_delegation () = delegation ~delegate:(Reference.create "")

    let empty_fa2_transfer () =
      fa2_transfer ~token_id_address:(Reference.create "")
        ~token_id_number:(Reference.create "") ~amount:(Reference.create "")
        ~source:(Reference.create "") ~destination:(Reference.create "")

    let add_empty_transfer = function
      | Structured ops -> Collection.append ops (empty_transfer ())

    let add_empty_delegation = function
      | Structured ops -> Collection.append ops (empty_delegation ())

    let add_empty_fa2_transfer = function
      | Structured ops -> Collection.append ops (empty_fa2_transfer ())
  end
end

module Variable = struct
  module Name = struct
    type t = string [@@deriving sexp, equal]
  end

  let unique_name used_names name =
    let rec find_name n c =
      let this = n ^ if c = 0 then "" else Fmt.str "_%d" (c + 1) in
      match List.find used_names ~f:(fun s -> String.equal s this) with
      | Some _ -> find_name n (c + 1)
      | None -> this
    in
    find_name name 0

  module Type = struct
    type t =
      | Mutez
      | Address
      | Nat
      | Key_list
      | Weighted_key_list
      | Pkh_option
      | Bytes
      | Signature_option_list
      | Lambda_unit_to_operation_list
      | Time_span
      | Timestamp_option
      | Unit
      | Arbitrary of Michelson_expression.t
    [@@deriving sexp, variants, compare, equal]

    let of_michelson =
      let open Tezai_michelson.Untyped.M in
      let of_name = function
        | "nat" -> nat
        | "mutez" -> mutez
        | "bytes" -> bytes
        (* | "string" -> string *)
        | "address" -> address
        (* | "bool" -> bool *)
        (* | "timestamp" -> timestamp *)
        | "unit" -> unit
        | _ -> assert false
      in
      let open Michelson_expression in
      function
      | Prim (_, name, [], annot) as t ->
          ((try of_name name with _ -> arbitrary (of_michelson t)), annot)
      | Prim (_, _, _, annot) as other -> (arbitrary (of_michelson other), annot)
      | other -> (arbitrary (of_michelson other), [])

    let to_michelson =
      let open Tezai_michelson.Untyped.C in
      function
      | Mutez -> t_mutez
      | Address -> prim "address" []
      | Nat | Time_span -> t_nat
      | Weighted_key_list -> t_list (t_pair [ t_nat; prim "key" [] ])
      | Key_list -> t_list (prim "key" [])
      | Pkh_option -> t_option t_key_hash
      | Bytes -> prim "bytes" []
      | Signature_option_list -> t_list (t_option (prim "signature" []))
      | Lambda_unit_to_operation_list -> t_lambda t_unit (t_list t_operation)
      | Timestamp_option -> t_option (prim "timestamp" [])
      | Unit -> t_unit
      | Arbitrary me -> Michelson_expression.to_michelson me

    let pp ppf self = Fmt.pf ppf "%a" Sexp.pp_hum (sexp_of_t self)
  end

  module Value = struct
    type t =
      | Address of Address.t
      | Mutez of Z.t
      | Nat of Z.t
      | Key_list of Public_key.t list
      | Weighted_key_list of (Z.t * Public_key.t) list
      | Unit
      | Some_pkh of Public_key_hash.t
      | Signature_option_list of Signature.t option list
      | Bytes of string
      | None_pkh
      | Lambda_unit_to_operation_list of Michelson.t
      | Time_span of Z.t
      | String of string
      | Timestamp_option of Timestamp.t option
      | Arbitrary_typed of {
          t : Michelson_expression.t;
          v : Michelson_expression.t;
        }
    [@@deriving sexp, variants, equal]

    let to_type =
      let module T = Type in
      function
      | Unit -> T.Unit
      | None_pkh | Some_pkh _ -> T.Pkh_option
      | Address _ -> T.Address
      | Bytes _ -> T.Bytes
      | Nat _ -> T.Nat
      | Mutez _ -> T.Mutez
      | Lambda_unit_to_operation_list _ -> T.Lambda_unit_to_operation_list
      | Time_span _ -> T.time_span
      | String _ -> assert false
      | Timestamp_option _ -> T.Timestamp_option
      | Signature_option_list _ -> T.Signature_option_list
      | Key_list _ -> T.Key_list
      | Arbitrary_typed a -> T.arbitrary a.t
      | Weighted_key_list _ -> T.Weighted_key_list

    let to_michelson =
      let open Tezai_michelson.Untyped.C in
      function
      | Unit -> e_unit
      | None_pkh -> e_none
      | Some_pkh a -> e_some (string a)
      | Address a -> string a
      | Bytes s -> bytes (Bytes.of_string s)
      | Lambda_unit_to_operation_list e -> e
      | Nat n | Mutez n | Time_span n -> int n
      | String s -> string s
      | Timestamp_option ts -> (
          match ts with Some (Seconds t) -> e_some (int t) | None -> e_none)
      | Signature_option_list sol ->
          seq_map
            ~f:(fun so ->
              match so with None -> e_none | Some s -> e_some (string s))
            sol
      | Key_list kl -> seq_map ~f:string kl
      | Arbitrary_typed a -> Michelson_expression.to_michelson a.v
      | Weighted_key_list wkl ->
          seq_map ~f:(fun (z, k) -> e_pair (int z) (string k)) wkl

    let of_michelson (t : Type.t) expr =
      let open Tezai_michelson.Untyped.M in
      let get_int = function
        | Int (_, z) -> z
        | other ->
            Failure.raise_textf "Parsing Michelson '%a', looking for type Int."
              Tezai_michelson.Untyped.pp other
      in
      let get_string_or_bytes of_bytes = function
        | String (_, s) -> s
        | Bytes (_, b) -> of_bytes b
        | other ->
            Failure.raise_textf
              "Parsing Michelson '%a', looking for bytes or string."
              Tezai_michelson.Untyped.pp other
      in
      let get_address =
        get_string_or_bytes (fun b ->
            Tezai_base58_digest.Identifier.Address.(of_bytes b |> to_base58))
      in
      let get_list = function
        | Seq (_, l) -> l
        | other ->
            Failure.raise_textf
              "Parsing Michelson '%a', looking for a sequence."
              Tezai_michelson.Untyped.pp other
      in
      let get_option = function
        | Prim (_, "None", _, _) -> None
        | Prim (_, "Some", [ one ], _) -> Some one
        | other ->
            Failure.raise_textf "Parsing Michelson '%a', looking for an option."
              Tezai_michelson.Untyped.pp other
      in
      let get_pair = function
        | Prim (_, "Pair", [ one; two ], _) -> (one, two)
        | other ->
            Failure.raise_textf "Parsing Michelson '%a', looking for a pair."
              Tezai_michelson.Untyped.pp other
      in
      let check_prim prim = function
        | Prim (_, p, [], _) when String.equal p prim -> ()
        | other ->
            Failure.raise_textf
              "Parsing Michelson '%a', looking for primitive %S."
              Tezai_michelson.Untyped.pp other prim
      in
      match t with
      | Type.Mutez -> get_int expr |> mutez
      | Type.Nat -> get_int expr |> nat
      | Type.Time_span -> get_int expr |> time_span
      | Type.Address -> get_address expr |> address
      | Type.Key_list ->
          let keys = get_list expr in
          List.map keys
            ~f:
              (get_string_or_bytes (fun b ->
                   Tezai_base58_digest.Identifier.Generic_signer.Public_key.(
                     of_bytes b |> to_base58)))
          |> key_list
      | Type.Weighted_key_list ->
          let keys = get_list expr in
          List.map keys ~f:(fun wkpair ->
              let w, k = get_pair wkpair in
              ( get_int w,
                get_string_or_bytes
                  (fun b ->
                    Tezai_base58_digest.Identifier.Generic_signer.Public_key.(
                      of_bytes b |> to_base58))
                  k ))
          |> weighted_key_list
      | Type.Bytes -> get_string_or_bytes Bytes.to_string expr |> bytes
      | Type.Timestamp_option ->
          get_option expr
          |> Option.map ~f:(fun e -> get_int e |> Timestamp.seconds)
          |> timestamp_option
      | Type.Unit ->
          check_prim "Unit" expr;
          unit
      | Type.Arbitrary a ->
          Arbitrary_typed { t = a; v = Michelson_expression.of_michelson expr }
      | Type.Pkh_option | Type.Signature_option_list
      | Type.Lambda_unit_to_operation_list ->
          assert false

    module Q = struct
      let nat = function
        | Nat n -> n
        | other -> Fmt.failwith "Not a Nat: %a" Sexp.pp_hum (sexp_of_t other)

      let key_list = function
        | Key_list n -> n
        | other ->
            Fmt.failwith "Not a Key_list: %a" Sexp.pp_hum (sexp_of_t other)
    end
  end

  module Map = struct
    type t = (Name.t * Value.t) list [@@deriving sexp, equal]

    let empty : t = []
    let of_list = Fn.id

    let get_exn self = function
      | "dummy" -> Value.unit
      | k -> (
          try List.Assoc.find_exn self k ~equal:String.equal
          with _ -> Failure.raise_textf "Cannot find value for variable %S" k)
  end

  module Record = struct
    type t = Comb of (Name.t * Value.t) list
    [@@deriving sexp, variants, equal]

    let empty = Comb []

    let of_map map ~structure : t =
      let f one = (one, Map.get_exn map one) in
      match structure with
      | [] -> Comb []
      | [ one ] -> Comb [ f one ]
      | more -> Comb (List.map ~f more)

    let to_michelson self =
      let open Tezai_michelson.Untyped.C in
      let f (_, one) = Value.to_michelson one in
      match self with
      | Comb [] -> e_unit
      | Comb [ one ] -> f one
      | Comb more -> seq_map ~f more

    let get (Comb record) field =
      match List.Assoc.find record field ~equal:String.equal with
      | Some s -> s
      | None -> Fmt.failwith "Cannot find value for field %S" field

    let update_from_normalized_michelson (Comb record) expr =
      let open Tezai_michelson.Untyped in
      let open M in
      let rec go record expr =
        match record with
        | [] -> []
        | (this_k, this_v) :: next -> (
            match expr with
            | Prim (_, "Pair", [ mich; next_expr ], _) ->
                let t = Value.to_type this_v in
                let newv = Value.of_michelson t mich in
                (this_k, newv) :: go next next_expr
            | other ->
                let t = Value.to_type this_v in
                let newv = Value.of_michelson t other in
                assert (List.is_empty next);
                [ (this_k, newv) ])
      in
      go record expr |> comb

    let of_normalized_michelson_type_and_expression ~t ~v =
      let open Tezai_michelson.Untyped in
      let open M in
      let get_field_name l =
        match List.find_map ~f:(String.chop_prefix ~prefix:"%") l with
        | Some s -> s
        | None -> ""
      in
      let rec go t v =
        match (t, v) with
        | Prim (_, "pair", [ lt; rt ], _), Prim (_, "Pair", [ le; re ], _) ->
            let t, annot = Type.of_michelson lt in
            let v = Value.of_michelson t le in
            (get_field_name annot, v) :: go rt re
        | _, _ ->
            let t, annot = Type.of_michelson t in
            let v = Value.of_michelson t v in
            [ (get_field_name annot, v) ]
      in
      comb (go t v)
  end

  module Parameter_type = struct
    type t = {
      structure : [ `Comb ]; [@default `Comb]
      fields : (string * Type.t) list; [@main]
    }
    [@@deriving sexp, fields, equal, make]

    let comb_of_normalized_michelson t =
      let open Tezai_michelson.Untyped in
      let open M in
      let get_field_name l =
        List.find_map ~f:(String.chop_prefix ~prefix:"%") l
        |> Option.value ~default:""
      in
      let rec go t =
        match t with
        | Prim (_, "pair", [ lt; rt ], _) ->
            let t, annot = Type.of_michelson lt in
            (get_field_name annot, t) :: go rt
        | _ ->
            let t, annot = Type.of_michelson t in
            [ (get_field_name annot, t) ]
      in
      make (go t)

    let get_comb self =
      assert (Poly.(self.structure = `Comb));
      self.fields
  end
end

module Constant_or_variable = struct
  type 'a t = Constant of 'a | Variable of Variable.Name.t
  [@@deriving sexp, variants, equal]

  let collect_variables t = function
    | Constant _ -> []
    | Variable v -> [ (v, t) ]
end

module Control = struct
  type t =
    | Anyone
    | Weighted_multisig of {
        counter : Variable.Name.t;
        keys : (Z.t * Public_key.t) list Constant_or_variable.t;
        threshold : Z.t Constant_or_variable.t;
      }
    | Sender_is of Address.t Constant_or_variable.t
    | When_inactive of {
        last_activity : Variable.Name.t;
        timespan : Z.t Constant_or_variable.t;
      }
    | And of t list
    | Or of t list
  [@@deriving sexp, variants, equal]

  let rec collect_variables = function
    | Weighted_multisig { counter; threshold; keys } ->
        (counter, Variable.Type.nat)
        :: Constant_or_variable.collect_variables
             Variable.Type.weighted_key_list keys
        @ Constant_or_variable.collect_variables Variable.Type.nat threshold
    | Sender_is s ->
        Constant_or_variable.collect_variables Variable.Type.address s
    | When_inactive { timespan; last_activity } ->
        Constant_or_variable.collect_variables Variable.Type.nat timespan
        @ [ (last_activity, Variable.Type.timestamp_option) ]
    | And l | Or l -> List.concat_map l ~f:collect_variables
    | Anyone -> []

  let key_param_id n = Fmt.str "param_id_%d" n

  let collect_parameters = function
    | Weighted_multisig _ ->
        [ ("signatures", Variable.Type.Signature_option_list) ]
    | _ -> []
end

module Ability = struct
  type t =
    | Call_operations_lambda
    | Transfer_mutez of {
        amount : [ `No_limit | `Limit of Mutez.t Constant_or_variable.t ];
        destination :
          [ `Anyone | `Address of Address.t Constant_or_variable.t | `Sender ];
      }
    | Set_delegate
    | Reset_activity_timer of Variable.Name.t
    | Sequence of t list
    | Vote
    | Do_nothing
    | Boomerang
    | Update_variables of (Variable.Name.t * Variable.Type.t) list
    | Generic_multisig_main
  [@@deriving sexp, variants, equal]

  let rec collect_variables = function
    | Transfer_mutez { amount; destination } -> (
        (match amount with
        | `No_limit -> []
        | `Limit l ->
            Constant_or_variable.collect_variables Variable.Type.mutez l)
        @
        match destination with
        | `Anyone | `Sender -> []
        | `Address add ->
            Constant_or_variable.collect_variables Variable.Type.address add)
    | Reset_activity_timer _ -> []
    | Sequence l -> List.concat_map l ~f:collect_variables
    | Call_operations_lambda | Set_delegate | Vote | Do_nothing | Boomerang
    | Update_variables _ | Generic_multisig_main ->
        []

  let rec collect_parameters = function
    | Set_delegate -> [ ("delegate", Variable.Type.pkh_option) ]
    | Transfer_mutez { amount = _; destination } -> (
        [ ("amount", Variable.Type.mutez) ]
        @
        match destination with
        | `Anyone -> [ ("destination", Variable.Type.address) ]
        | _ -> [])
    | Sequence l ->
        let ps =
          List.fold l ~init:[] ~f:(fun acc x -> collect_parameters x @ acc)
        in
        let foldf :
            (string * Variable.Type.t) list ->
            string * Variable.Type.t ->
            (string * Variable.Type.t) list =
         fun acc (s, t) ->
          let new_name = Variable.unique_name (List.map acc ~f:fst) s in
          (new_name, t) :: acc
        in
        List.fold ps ~init:[] ~f:foldf
    | Boomerang | Do_nothing -> []
    | Update_variables vars -> vars
    | Reset_activity_timer _ -> []
    | Generic_multisig_main | Vote | Call_operations_lambda -> assert false
end

module Entrypoint = struct
  type t = { name : string; [@main] control : Control.t; ability : Ability.t }
  [@@deriving sexp, fields, make, equal]

  let cmp_fst : string * 'a -> string * 'b -> int =
   fun (x, _) (y, _) -> String.compare x y

  let collect_parameters { control; ability; _ } =
    let controls = Control.collect_parameters control in
    let abilities = Ability.collect_parameters ability in
    List.sort (controls @ abilities) ~compare:cmp_fst

  let collect_variables { control; ability; _ } =
    let controls = Control.collect_variables control in
    let abilities = Ability.collect_variables ability in
    List.sort (controls @ abilities) ~compare:cmp_fst
end

module Foreign_contract = struct
  type t = {
    entrypoints : (string * Michelson.t) list;
    storage_type : Michelson.t; [@default Michelson.C.t_unit]
    storage_content : Michelson.t; [@default Michelson.C.e_unit]
  }
  [@@deriving sexp, fields, make, equal, compare]
end

module Smart_contract = struct
  type t =
    | Custom of {
        entrypoints : Entrypoint.t list (* ; fully_recognized: bool *);
        can_receive_funds : bool;
        variables : Variable.Record.t;
      }
    | Foreign of Foreign_contract.t
    | Generic_multisig of { variables : Variable.Record.t }
  [@@deriving sexp, variants, equal]

  let make ?(can_receive_funds = false) ?(variables = Variable.Record.empty)
      entrypoints =
    Custom { entrypoints; can_receive_funds; variables }

  let collect_variables self =
    match self with
    | Generic_multisig _ ->
        let open Variable.Type in
        [ ("counter", nat); ("threshold", nat); ("keys", key_list) ]
    | Custom { entrypoints; _ } ->
        List.concat_map entrypoints ~f:(fun ep ->
            Entrypoint.collect_variables ep)
        |> List.dedup_and_sort ~compare:Poly.compare
    | Foreign _ -> []
end

module Originated_contract = struct
  type t = { address : Address.t; [@main] contract : Smart_contract.t }
  [@@deriving sexp, fields, make, equal]
end

module Key_pair = struct
  type backend =
    | Ledger of { uri : string }
    | Unencrypted of { private_uri : string }
  [@@deriving sexp, variants, equal]

  type t = {
    public_key_hash : Public_key_hash.t;
    public_key : Public_key.t;
    backend : backend; [@main]
  }
  [@@deriving sexp, fields, make, equal]

  let octez_client_sk_uri = function
    | Ledger { uri } -> uri
    | Unencrypted { private_uri } -> private_uri
end

module Friend = struct
  type t = {
    public_key_hash : Public_key_hash.t;
    public_key : Public_key.t option;
  }
  [@@deriving sexp, fields, make, equal]
end

module Human_prose = struct
  type t = Raw_markdown of string [@@deriving sexp, variants, compare, equal]
end

module Account = struct
  module Draft = struct
    open Drafting

    module Internal_id = struct
      type t = V of string * int [@@deriving sexp, variants, equal, compare]

      let fresh () = v (Fresh_id.make ()) 0
      let next (V (s, ith)) = V (s, ith + 1)

      let pp ppf (V (s, ith)) =
        Fmt.pf ppf "{Internal:%s%s}" s
          (if ith = 0 then "" else Fmt.str "/%d" ith)
    end

    module Role_id = Internal_id
    module Timer_id = Internal_id
    module Spending_limit_id = Internal_id
    module Team_id = Internal_id

    module Capability = struct
      module Control = struct
        type t =
          | Anyone
          | Sender_is of Role_id.t Option.t Reference.t
          | Or of t Option.t Reference.t Collection.t
          | And of t Option.t Reference.t Collection.t
          | Timer_expires of Timer_id.t Option.t Reference.t
          | Team_acknowledges of Team_id.t Option.t Reference.t
        [@@deriving sexp, variants, equal, compare]
      end

      module Action = struct
        type t =
          | Transfer of {
              amount_limit : Spending_limit_id.t Option.t Reference.t;
              destination : Role_id.t Option.t Reference.t;
            }
          | Set_delegate
          | Set_variable of Internal_id.t Option.t Reference.t
          | Reset_timer of Timer_id.t Option.t Reference.t
          | Do_nothing
        [@@deriving sexp, variants, equal, compare]

        let empty_transfer () =
          transfer ~amount_limit:(Reference.create None)
            ~destination:(Reference.create None)
      end

      type t = {
        name : string Reference.t;
        control : Control.t Option.t Reference.t;
        actions : Action.t Reference.t Collection.t;
      }
      [@@deriving sexp, make, fields, equal, compare]

      let create_empty () =
        make ~name:(Reference.create "") ~control:(Reference.create None)
          ~actions:(Collection.create ())
    end

    module Meta_variable (Value_type : sig
      type t [@@deriving sexp, equal, compare]

      val empty : unit -> t
    end) =
    struct
      type t = {
        id : Internal_id.t;
        public_name : string Reference.t;
        initial_value : Value_type.t Reference.t;
      }
      [@@deriving sexp, fields, equal, compare]

      let create_empty () =
        {
          id = Internal_id.fresh ();
          public_name = Reference.create "";
          initial_value = Reference.create (Value_type.empty ());
        }
    end

    module Value_string = struct
      include String

      let empty () = ""
    end

    module Role = Meta_variable (Value_string)
    module Timer = Meta_variable (Value_string)
    module Spending_limit = Meta_variable (Value_string)

    module Team_draft = struct
      type t =
        | Percentages of {
            keys : (Role_id.t option Reference.t * int Reference.t) Collection.t;
          }
      [@@deriving sexp, variants, equal, compare]

      let empty () = Percentages { keys = Collection.create () }
    end

    module Team = Meta_variable (Team_draft)

    type t =
      | From_address_or_uri of string Reference.t
      | Generic_multisig of {
          gas_wallet : string Reference.t;
          threshold : string Reference.t;
          public_keys : string list Reference.t;
        }
      | Custom_contract of {
          gas_wallet : string Reference.t;
          has_default_do_nothing : bool Reference.t;
          capabilities : Capability.t Collection.t;
          roles : Role.t Collection.t;
          timers : Timer.t Collection.t;
          spending_limits : Spending_limit.t Collection.t;
          teams : Team.t Collection.t;
        }
    [@@deriving sexp, variants, compare, equal]

    let create_from_address_or_uri ?(uri = "") () : t =
      from_address_or_uri (Reference.create uri)

    let create_generic_multisig () : t =
      generic_multisig ~gas_wallet:(Reference.create "")
        ~threshold:(Reference.create "") ~public_keys:(Reference.create [])

    let create_custom_contract () : t =
      custom_contract ~gas_wallet:(Reference.create "")
        ~capabilities:(Collection.create ())
        ~has_default_do_nothing:(Reference.create true)
        ~roles:(Collection.create ()) ~timers:(Collection.create ())
        ~spending_limits:(Collection.create ()) ~teams:(Collection.create ())
    (* ~threshold:(Reference.create "")  *)
  end

  module Status = struct
    type t =
      | Originated_contract of Originated_contract.t
      | Key_pair of Key_pair.t
      | Friend of Friend.t
      | Contract_to_originate of {
          operation : string;
          contract : Originated_contract.t option;
        }
      | Contract_failed_to_originate of {
          operation : string;
          contract : Originated_contract.t option;
        }
      | Draft of Draft.t
    [@@deriving sexp, variants, equal]
  end

  type t = {
    id : string;
    display_name : string;
    balance : Z.t option;
    state : Status.t; [@main]
    history : Status.t list; [@default []]
    comments : Human_prose.t option;
  }
  [@@deriving sexp, fields, make, equal]

  let get_address { state; _ } =
    let open Status in
    match state with
    | Originated_contract { address; _ } -> Some address
    | Key_pair { public_key_hash; _ } -> Some public_key_hash
    | Friend { public_key_hash; _ } -> Some public_key_hash
    | Contract_to_originate _ -> None
    | Contract_failed_to_originate _ -> None
    | Draft _ -> None

  let get_public_key { state; _ } =
    let open Status in
    match state with
    | Originated_contract _ -> None
    | Key_pair { public_key; _ } -> Some public_key
    | Friend { public_key; _ } -> public_key
    | Contract_failed_to_originate _ -> None
    | Contract_to_originate _ -> None
    | Draft _ -> None

  let get_secret_key { state; _ } =
    let open Status in
    match state with
    | Key_pair { backend; _ } -> Some backend
    | Originated_contract _ -> None
    | Friend _ -> None
    | Contract_to_originate _ -> None
    | Contract_failed_to_originate _ -> None
    | Draft _ -> None

  let can_be_gas_wallet = function
    | { state = Status.Key_pair _; balance = Some bal; _ } -> Z.gt bal Z.zero
    | _ -> false

  let can_sign = function
    | { state = Status.Key_pair _; _ } -> true
    | _ -> false

  let is_draft acc = match acc.state with Draft _ -> true | _ -> false

  let multisig_parameters = function
    | {
        state =
          Status.Originated_contract
            Originated_contract.
              { contract = Smart_contract.Generic_multisig { variables }; _ };
        _;
      } ->
        let counter =
          Variable.Record.get variables "counter" |> Variable.Value.Q.nat
        in
        let threshold =
          Variable.Record.get variables "threshold" |> Variable.Value.Q.nat
        in
        let keys =
          Variable.Record.get variables "keys" |> Variable.Value.Q.key_list
        in
        object
          method counter = counter
          method threshold = threshold
          method keys = keys
        end
    | _ -> assert false

  let custom_contract_entrypoint ~entrypoint = function
    | {
        state =
          Status.Originated_contract
            Originated_contract.
              {
                contract = Smart_contract.Custom { entrypoints; variables; _ };
                _;
              };
        _;
      } ->
        let entrypoint =
          match
            List.find entrypoints ~f:(fun Entrypoint.{ name; _ } ->
                String.equal name entrypoint)
          with
          | Some s -> s
          | None -> failwith "Entrypoint not found"
        in
        object
          method entrypoint = entrypoint
          method variables = variables
        end
    | _ -> failwith "Not a custom contract"

  let foreign_contract_entrypoint ~entrypoint = function
    | {
        state =
          Status.Originated_contract
            Originated_contract.
              { contract = Smart_contract.Foreign foreign_contract; _ };
        _;
      } ->
        let open Foreign_contract in
        let entrypoint_type =
          match
            List.find foreign_contract.entrypoints ~f:(fun (name, _) ->
                String.equal name entrypoint)
          with
          | Some (_, t) -> t
          | None -> failwith "Entrypoint not found"
        in
        object
          method entrypoint_type = entrypoint_type
          method foreign_contract = foreign_contract
        end
    | _ -> failwith "Not a custom contract"
end

module Mempool_section = struct
  type t =
    | Applied
    | Refused
    | Outdated
    | Branch_refused
    | Branch_delayed
    | Unprocessed
  [@@deriving sexp, equal, variants]

  let is_applied = function Applied -> true | _ -> false
end

module Mempool_error = struct
  type t = Unknown of Json.t [@@deriving sexp, equal, variants, compare]

  let of_json = unknown
end

module Tezos_operation_error = struct
  module Kind = struct
    type t =
      | Empty_transaction
      | Non_existing_contract of string
      | Balance_too_low of { contract : string; balance : Z.t; amount : Z.t }
      | Storage_exhausted
      | Ill_typed_michelson of {
          expected_type : Michelson.t;
          expression : Michelson.t;
        }
      | Syntax_error_in_michelson of {
          expected_from_protocol : string;
          expression : Michelson.t;
        }
      | Contract_runtime_error of { address : string }
      | Contract_hit_failwith of { location : int; message : Michelson.t }
      | Unknown of Json.t
    [@@deriving sexp, variants, equal]

    let of_error_json one =
      let open Json.Q in
      let suffixes = ref [] in
      let with_suffix s f = suffixes := (s, f) :: !suffixes in
      with_suffix "contract.empty_transaction" (fun _ -> empty_transaction);
      with_suffix "contract.non_existing_contract" (fun one ->
          non_existing_contract (string_field ~k:"contract" one));
      with_suffix "storage_exhausted.operation" (fun _ -> storage_exhausted);
      with_suffix "contract.balance_too_low" (fun one ->
          balance_too_low
            ~contract:(string_field ~k:"contract" one)
            ~balance:(z_field ~k:"balance" one)
            ~amount:(z_field ~k:"amount" one));
      let michelfield ~k j = field ~k j |> Michelson.of_json in
      with_suffix "michelson_v1.ill_typed_data" (fun one ->
          ill_typed_michelson
            ~expected_type:(michelfield ~k:"expected_type" one)
            ~expression:(michelfield ~k:"ill_typed_expression" one));
      with_suffix "michelson_v1.invalid_constant" (fun one ->
          ill_typed_michelson
            ~expected_type:(michelfield ~k:"expected_type" one)
            ~expression:(michelfield ~k:"wrong_expression" one));
      with_suffix "michelson_v1.invalid_syntactic_constant" (fun one ->
          syntax_error_in_michelson
            ~expected_from_protocol:(string_field ~k:"expected_form" one)
            ~expression:(michelfield ~k:"wrong_expression" one));
      with_suffix "michelson_v1.runtime_error" (fun one ->
          contract_runtime_error
            ~address:(string_field ~k:"contract_handle" one));
      with_suffix "michelson_v1.script_rejected" (fun one ->
          contract_hit_failwith
            ~location:(int_field one ~k:"location")
            ~message:(michelfield one ~k:"with"));
      try
        let id = string_field ~k:"id" one in
        let _, make =
          List.find_exn !suffixes ~f:(fun (suffix, _) ->
              String.is_suffix id ~suffix)
        in
        make one
      with _ -> unknown one
  end

  type t = { kind : Kind.t; raw : Json.t [@main] }
  [@@deriving sexp, fields, equal, make]

  let of_errors_json json =
    let open Json.Q in
    List.map (list json) ~f:(fun obj ->
        let kind = Kind.of_error_json obj in
        make json ~kind)
end

module Simulation_result = struct
  module Balance_update = struct
    type t =
      | Account of { address : string; change : Z.t }
      | Burn of { category : string; change : Z.t }
    [@@deriving sexp, variants, equal]
  end

  module Operation_result = struct
    type t = {
      status : [ `Applied | `Failed | `Backtracked ];
      errors : Tezos_operation_error.t list;
      balance_updates : Balance_update.t list;
      consumed_milligas : Z.t option;
      storage_size : Z.t option;
      allocated_destination_contract : bool option;
      paid_storage_size_diff : Z.t option;
      originated_contracts : string list;
      internal_operations : t list;
    }
    [@@deriving sexp, fields, make, equal]

    let rec total_milligas self =
      let init = Option.value self.consumed_milligas ~default:Z.zero in
      List.fold self.internal_operations ~init ~f:(fun gas op ->
          Z.add gas (total_milligas op))
  end

  type t = { operations : Operation_result.t list; [@main] raw : Json.t }
  [@@deriving sexp, equal, make, fields]

  let originated_contracts (self : t) =
    List.concat_map self.operations ~f:Operation_result.originated_contracts

  let total_milligas self =
    List.fold ~init:Z.zero self.operations ~f:(fun gas op ->
        Z.add gas (Operation_result.total_milligas op))

  let total_paid_storage_diff (self : t) =
    List.fold self.operations ~init:Z.zero ~f:(fun p o ->
        Z.add p
          (Operation_result.paid_storage_size_diff o
          |> Option.value ~default:Z.zero))

  let all_applied (self : t) =
    List.for_all self.operations ~f:(function
      | { Operation_result.status = `Applied; _ } -> true
      | _ -> false)

  let all_errors self =
    List.concat_map self.operations ~f:Operation_result.errors
end

(** For now we keep the weird name because we link with Flextesa which already
    owns the {!Tezos_protocol} name. *)
module Tzrotocol = struct
  module Kind = struct
    type t = Ithaca | Jakarta | Alpha
    [@@deriving sexp, variants, compare, equal]

    let canonical_hash = function
      | Jakarta -> "PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY"
      | Ithaca -> "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A"
      | Alpha -> "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK"

    let all = [ Jakarta; Ithaca; Alpha ]
    let all_with_hashes = List.map all ~f:(fun k -> (k, canonical_hash k))

    let name = function
      | Ithaca -> "Ithaca"
      | Jakarta -> "Jakarta"
      | Alpha -> "Alpha"
  end

  type t = { kind : Kind.t; [@main] hash : string option }
  [@@deriving sexp, make, equal, compare, fields]

  let of_hash hash =
    let kind =
      List.find_map Kind.all_with_hashes ~f:(function
        | k, h when String.equal h hash -> Some k
        | _ -> None)
      |> Option.value ~default:Kind.ithaca
    in
    make ~hash kind
end

module Tezos_operation = struct
  type t =
    | Transfer of {
        source : Account.t;
        amount : Z.t;
        destination : Address.t;
        entrypoint : string;
        parameters : Michelson.t option;
      }
    | Origination of {
        source : Account.t;
        balance : Z.t;
        code : Michelson.t;
        storage_initialization : Michelson.t;
      }
    | Reveal of { account : Account.t }
  [@@deriving sexp, variants, equal]

  let source = function
    | Transfer { source; _ } -> source
    | Origination { source; _ } -> source
    | Reveal { account; _ } -> account
end

module Tezos_batch = struct
  type op = {
    specification : Tezos_operation.t; [@main]
    fee : Z.t; [@default Z.zero]
    gas_limit : Z.t; [@default Z.of_int 1_040_000]
    storage_limit : Z.t; [@default Z.of_int 60_000]
    counter : Z.t;
  }
  [@@deriving sexp, fields, make, equal]

  let fake_signature =
    "edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q"

  type t = {
    branch : string;
    protocol : Tzrotocol.t;
    signature : Signature.t; [@default fake_signature]
    chain_id : string;
    operations : op list; [@main]
  }
  [@@deriving sexp, fields, make, equal]

  let unbatch self =
    List.map self.operations ~f:(fun op -> { self with operations = [ op ] })

  let to_json format batch =
    let open Json.C in
    let open Tezos_operation in
    let z z = string (Z.to_string z) in
    let account_required msg f c =
      match f c with
      | Some s -> s
      | None ->
          Failure.raise
            Docpp.(
              textf "Missing %s from account:" msg
              +++ sexpable Account.sexp_of_t c)
    in
    let op { specification; counter; fee; gas_limit; storage_limit } =
      match specification with
      | Origination { source; balance; code; storage_initialization } ->
          ("kind" --> string "origination")
          @@@ "source"
              --> string (account_required "address" Account.get_address source)
          @@@ ("fee" --> z fee)
          @@@ ("counter" --> z counter)
          @@@ ("gas_limit" --> z gas_limit)
          @@@ ("storage_limit" --> z storage_limit)
          @@@ ("balance" --> z balance)
          @@@ "script"
              --> (("code" --> Michelson.to_json code)
                  @@@ ("storage" --> Michelson.to_json storage_initialization))
      | Transfer { source; amount; destination; entrypoint; parameters } ->
          ("kind" --> string "transaction")
          @@@ "source"
              --> string (account_required "address" Account.get_address source)
          @@@ ("fee" --> z fee)
          @@@ ("counter" --> z counter)
          @@@ ("gas_limit" --> z gas_limit)
          @@@ ("storage_limit" --> z storage_limit)
          @@@ ("amount" --> z amount)
          @@@ ("destination" --> string destination)
          @@@ "parameters"
              --> (("entrypoint" --> string entrypoint)
                  @@@ "value"
                      -->
                      match parameters with
                      | None -> Michelson.to_json (Michelson.C.prim "Unit" [])
                      | Some p -> Michelson.to_json p)
      | Reveal { account } ->
          ("kind" --> string "reveal")
          @@@ "source"
              --> string
                    (account_required "address" Account.get_address account)
          @@@ ("fee" --> z fee)
          @@@ ("counter" --> z counter)
          @@@ ("gas_limit" --> z gas_limit)
          @@@ ("storage_limit" --> z storage_limit)
          @@@ "public_key"
              --> string
                    (account_required "public-key" Account.get_public_key
                       account)
    in
    let operation_chunk ~with_sig =
      ("branch" --> string batch.branch)
      @@@ ("contents" --> a (List.map batch.operations ~f:op))
      @@@ if with_sig then "signature" --> string batch.signature else obj []
    in
    match format with
    | `For_simulation ->
        ("operation" --> operation_chunk ~with_sig:true)
        @@@ ("chain_id" --> string batch.chain_id)
    | `For_forge -> operation_chunk ~with_sig:false
end

module Simulation_failure = struct
  type t = {
    which : [ `Computing_fees | `Final ];
    simulation : Simulation_result.t;
    batch : Tezos_batch.t;
  }
  [@@deriving fields, make, sexp, equal]

  let to_pp e = Docpp.(text "Simulation-failure:" +++ sexpable sexp_of_t e)

  exception E of t

  let () =
    Exception.register_pp (function E e -> Some (to_pp e) | _ -> None);
    Caml.Printexc.register_printer (function
      | E e -> Some (Fmt.str "%a" Docpp.to_fmt (to_pp e))
      | _ -> None)

  let raise which ~simulation ~batch = raise (E { which; simulation; batch })

  let kinds_of_exn = function
    | E { simulation; _ } ->
        List.concat_map
          ~f:(fun op ->
            List.map
              (Simulation_result.Operation_result.errors op)
              ~f:Tezos_operation_error.kind)
          (Simulation_result.operations simulation)
    | _ -> []

  let exn_has_kind e ~f = List.exists (kinds_of_exn e) ~f
end

module Operation = struct
  type id = string [@@deriving sexp, equal]

  module Draft = struct
    open Drafting

    module Parameter_draft = struct
      type v =
        | Address of string Reference.t
        | Amount of string Reference.t
        | Of_type of Variable.Type.t * string Reference.t
      [@@deriving sexp, variants, compare, equal]

      type t = { name : string; v : v [@main] }
      [@@deriving sexp, fields, compare, equal, make]

      let of_parameter (name, ty) =
        make ~name
          (match ty with
          | Variable.Type.Address -> address (Reference.create "")
          | Variable.Type.Mutez -> amount (Reference.create "")
          | _ -> of_type ty (Reference.create ""))
    end

    type t =
      | Simple_transfer of {
          source_account : string Reference.t;
          destination_address : string Reference.t;
          amount : string Reference.t;
        }
      | Call_generic_multisig_update_keys of {
          contract_address : Address.t;
          gas_wallet : string Reference.t;
          threshold : string Reference.t;
          public_keys : string list Reference.t;
          signatures : Signature.Draft.t;
        }
      | Call_generic_multisig_main of {
          contract_address : Address.t;
          gas_wallet : string Reference.t;
          lambda : Lambda_unit_operations.Draft.t Reference.t;
          signatures : Signature.Draft.t;
        }
      | Call_custom_contract_entrypoint of {
          contract_address : Address.t;
          entrypoint : string;
          gas_wallet : string Reference.t;
          parameters : Parameter_draft.t List.t;
        }
      | Call_foreign_contract_entrypoint of {
          contract_address : Address.t;
          entrypoint : string;
          gas_wallet : string Reference.t;
          parameters : Parameter_draft.t List.t;
        }
    [@@deriving sexp, variants, compare, equal]

    let create_call_generic_multisig_update_keys ?threshold ?(public_keys = [])
        ~address () =
      call_generic_multisig_update_keys ~gas_wallet:(Reference.create "")
        ~threshold:
          (Reference.create
             (Option.value_map threshold ~f:Z.to_string ~default:""))
        ~public_keys:(Reference.create public_keys)
        ~contract_address:address ~signatures:(Collection.create ())

    let create_call_generic_multisig_main ~address () =
      call_generic_multisig_main ~gas_wallet:(Reference.create "")
        ~lambda:(Lambda_unit_operations.Draft.create () |> Reference.create)
        ~contract_address:address ~signatures:(Collection.create ())

    let create_simple_transfer ?(source = "") ?(destination = "") ?(amount = "")
        () =
      simple_transfer ~source_account:(Reference.create source)
        ~destination_address:(Reference.create destination)
        ~amount:(Reference.create amount)

    let create_call_custom_contract_entrypoint ~address ~entrypoint ~parameters
        () =
      call_custom_contract_entrypoint ~contract_address:address ~entrypoint
        ~gas_wallet:(Reference.create "")
        ~parameters:(List.map parameters ~f:Parameter_draft.of_parameter)

    let create_call_foreign_contract_entrypoint ~address ~entrypoint ~parameters
        () =
      call_foreign_contract_entrypoint ~contract_address:address ~entrypoint
        ~gas_wallet:(Reference.create "")
        ~parameters:(List.map parameters ~f:Parameter_draft.of_parameter)

    let destination_address = function
      | Simple_transfer { destination_address; _ } ->
          Some (Reference.get destination_address)
      | Call_generic_multisig_main { contract_address; _ }
      | Call_generic_multisig_update_keys { contract_address; _ }
      | Call_foreign_contract_entrypoint { contract_address; _ }
      | Call_custom_contract_entrypoint { contract_address; _ } ->
          Some contract_address
  end

  module Specification = struct
    type t =
      | Origination of {
          account : string;
          specification : Smart_contract.t;
          initialization : Variable.Map.t;
          gas_wallet : string;
        }
      | Transfer of {
          source : string;
          destination : Address.t;
          amount : Mutez.t;
          entrypoint : string;
          parameters : (string * Variable.Value.t) list;
        }
      | Draft of Draft.t
    [@@deriving sexp, variants, equal]
  end

  module Error = struct
    type t =
      | Stuck_in_the_mempool of
          Mempool_section.t * Mempool_error.t list * [ `Expired | `Non_expired ]
      | Operation_lost
      | Simulation_failure of Simulation_failure.t
      | Unknown of Sexp.t
    [@@deriving sexp, variants, equal]
  end

  module Status = struct
    type tezos_operation = {
      operation_hash : string;
      base_block_hash : string;
      inclusion_block_hashes : (string * bool) list;
    }
    [@@deriving sexp, fields, make, equal]

    type t =
      | Paused
      | Ordered
      | Work_in_progress of {
          op : tezos_operation option;
          simulation_result : Simulation_result.t option;
          mempool_section : Mempool_section.t option;
          mempool_errors : Mempool_error.t list;
        }
      | Success of { op : tezos_operation }
      | Failed of Error.t
    [@@deriving sexp, variants, equal]

    (** We shall slowly get rid of this function: *)
    let failed_sexpable f x = Failed (Error.unknown (f x))
  end

  type t = {
    id : id;
    order : Specification.t;
    status : Status.t;
    status_history : (float * Status.t) list;
    last_update : float;
    comments : Human_prose.t option;
  }
  [@@deriving sexp, fields, make, equal]

  let same_id a b = equal_id a.id b.id
  let different_id a b = not (same_id a b)

  let compare_by_update a b =
    if same_id a b then 0
    else match Float.compare a.last_update b.last_update with 0 -> -1 | n -> n

  let is_draft op = match op.order with Draft _ -> true | _ -> false

  let with_status op status =
    let status_history =
      match op.status_history with
      | (d, one) :: more when Status.equal one op.status -> (d, one) :: more
      | any_other -> (op.last_update, op.status) :: any_other
    in
    { op with status; status_history }

  let with_status_work_in_progress ?simulation_result ?tezos_operation
      ?(mempool_errors = []) ?mempool_section op =
    let tezos_operation, mempool_section, simulation_result, mempool_errors =
      match op.status with
      | Status.Work_in_progress
          {
            op = tzop;
            mempool_section = ms;
            simulation_result = sr;
            mempool_errors = me;
          } ->
          ( Option.first_some tezos_operation tzop,
            Option.first_some mempool_section ms,
            Option.first_some simulation_result sr,
            List.dedup_and_sort ~compare:Mempool_error.compare
              (mempool_errors @ me) )
      | _ ->
          (tezos_operation, mempool_section, simulation_result, mempool_errors)
    in
    with_status op
      (Status.work_in_progress ~mempool_section ~op:tezos_operation
         ~mempool_errors ~simulation_result)

  let with_status_error op err = with_status op (Status.failed err)

  let with_status_error_sexpable op f x =
    with_status op (Status.failed_sexpable f x)

  let with_status_success op ~tezos_operation =
    with_status op (Status.success ~op:tezos_operation)

  let new_draft content =
    let id = Fresh_id.make () in
    let order = Specification.Draft content in
    let status = Status.paused in
    let last_update = Unix.gettimeofday () in
    make ~id ~order ~status ~last_update ()

  let is_old_and_uninteresting op ~now =
    let old_minutes = 10 in
    match op.status with
    | Status.Success _ | Failed _ ->
        let open Float in
        last_update op + (of_int old_minutes * 60.) < now
    | Paused | Ordered | Work_in_progress _ -> false

  let get_source_account_id op =
    let open Specification in
    match op.order with
    | Origination { gas_wallet; _ } -> Some gas_wallet
    | Transfer { source; _ } -> Some source
    | Draft draft -> (
        match draft with
        | Simple_transfer _ | Call_custom_contract_entrypoint _
        | Call_generic_multisig_main _ | Call_generic_multisig_update_keys _
        | Call_foreign_contract_entrypoint _ ->
            None)

  let get_destination_address op =
    let open Specification in
    match op.order with
    | Origination _ -> None
    | Transfer { destination; _ } -> Some destination
    | Draft draft -> Draft.destination_address draft

  let get_amount op =
    let open Specification in
    match op.order with
    | Origination _ -> None
    | Transfer { amount; _ } -> Some amount
    | Draft draft -> (
        match draft with
        | Call_generic_multisig_main _ | Call_generic_multisig_update_keys _ ->
            None
        | Simple_transfer _ | Call_custom_contract_entrypoint _
        | Call_foreign_contract_entrypoint _ ->
            None)
end

module Signing_request = struct
  module V0 = struct
    type metadata =
      [ `From_address of Address.t
      | `Comment of string * Human_prose.t
      | `Operation_draft of Operation.Draft.t ]
    [@@deriving sexp, variants, compare, equal]

    type t = {
      blob : Bytes_rep.t; [@main]
      public_key : Public_key.t;
      metadata : metadata list;
    }
    [@@deriving sexp, fields, compare, equal, make]
  end

  include V0

  type t = Signing_request_v0 of V0.t
  [@@deriving sexp, variants, compare, equal]

  let make ?metadata ~public_key blob =
    Signing_request_v0 (make ?metadata ~public_key blob)

  let of_v0 f self = Variants.map self ~signing_request_v0:(fun _ -> f)
  let blob self = of_v0 V0.blob self
  let public_key = of_v0 V0.public_key
  let metadata = of_v0 V0.metadata
end

module Currency = struct
  type t = [ `USD | `EUR | `BTC | `ETH ]
  [@@deriving sexp, variants, compare, equal]

  let to_symbol = function
    | `USD -> "USD"
    | `EUR -> "EUR"
    | `BTC -> "milliBTC"
    | `ETH -> "milliETH"

  let to_unit = function
    | `USD -> (1., "$")
    | `EUR -> (1., "€")
    | `BTC -> (1000., "m₿")
    | `ETH -> (1000., "mΞ")
end

module Exchange_rates = struct
  type t = {
    timestamp : float;
    btc : float;
    eur : float;
    usd : float;
    eth : float;
  }
  [@@deriving sexp, fields]

  let get_currency self (c : Currency.t) =
    match c with
    | `BTC -> self.btc
    | `USD -> self.usd
    | `ETH -> self.eth
    | `EUR -> self.eur
end

module Tezos_node = struct
  type t = { id : string; base_url : string [@main] }
  [@@deriving sexp, compare, equal, make, fields]

  let make_new base_url = make ~id:(Fresh_id.make ()) base_url

  module Status = struct
    type problem =
      [ `Timeouts_observed
      | `Wrong_chain_id of string * string
      | `No_chain_id_consensus
      | `Head_is_behind ]
    [@@deriving sexp, compare, equal, variants]

    type t = {
      health : [ `Fine | `Unknown | `Injured | `Dead ]; [@default `Unknown]
      problems : problem list;
      head : (int * string) option;
      chain_id : string option;
      version : string option;
    }
    [@@deriving make, sexp, compare, equal, fields]

    let add_problem self (p : problem) =
      let health =
        let hurt () =
          match self.health with
          | `Fine | `Unknown | `Injured -> `Injured
          | `Dead -> `Dead
        in
        match p with
        | `Timeouts_observed -> hurt ()
        | `Wrong_chain_id _ -> `Dead
        | `Head_is_behind -> hurt ()
        | `No_chain_id_consensus -> `Dead
      in
      {
        self with
        problems =
          List.dedup_and_sort ~compare:compare_problem (p :: self.problems);
        health;
      }
  end
end

module Network = struct
  type t = Mainnet | Ghostnet | Kathmandunet | Sandbox of string
  [@@deriving variants, compare, equal, sexp]

  let of_chain_id = function
    | "NetXdQprcVkpaWU" -> Mainnet
    | "NetXi2ZagzEsXbZ" -> Kathmandunet
    | "NetXnHfVqm9iesp" -> Ghostnet
    | s -> Sandbox s

  let all_public_chain_ids =
    [ "NetXdQprcVkpaWU"; "NetXi2ZagzEsXbZ"; "NetXnHfVqm9iesp" ]
end

module Network_information = struct
  type t = {
    nodes : (Tezos_node.t * Tezos_node.Status.t) list;
    network : Network.t option;
  }
  [@@deriving fields, compare, equal, sexp, make]

  let fine_nodes { nodes; _ } =
    List.filter_map nodes ~f:(function
      | _, { health = `Fine; head = Some (l, h); version = Some v; _ } ->
          Some (l, h, v)
      | _ -> None)
end

module Ledger_nano = struct
  module Device = struct
    type t = { animals : string; wallet_app_version : string option }
    [@@deriving sexp, equal, compare, fields, make]
  end

  module Curve_derivation = struct
    type t = Bip25519 | Ed25519 | Secp256k1 | P256
    [@@deriving sexp, equal, compare, variants]

    let all = [ Bip25519; Ed25519; Secp256k1; P256 ]

    let to_path = function
      | Bip25519 -> "bip25519"
      | Ed25519 -> "ed25519"
      | Secp256k1 -> "secp256k1"
      | P256 -> "P-256"
  end
end

module Uri_analysis = struct
  module Fact = struct
    type t =
      | Balance of Z.t
      | Address of Address.t
      | Public_key of Public_key.t
    [@@deriving sexp, equal, compare, variants]
  end

  type t = {
    input : string; [@main]
    valid : bool; [@default true]
    facts : Fact.t list;
  }
  [@@deriving sexp, equal, compare, make, fields]
end
