open! Import
open Wallet_data

module Message = struct
  module Up = struct
    type t =
      | List_connected_ledgers
      | Import_account of {
          name : string;
          kind :
            [ `Ledger_uri of string
            | `Unencrypted_uri of string
            | `Address of string ];
          comments : Human_prose.t option;
        }
      | Save_account_draft of {
          id : string;
          display_name : string;
          content : Account.Draft.t;
          comments : Human_prose.t option;
          glorify : bool;
        }
      | Create_generic_multisig_account of {
          name : string;
          keys : string list;
          threshold : int;
          gas_wallet : string;
          comments : Human_prose.t option;
        }
      | Create_smart_contract of {
          name : string;
          gas_wallet : string;
          entrypoints : Entrypoint.t list;
          initialization : Variable.Map.t;
          comments : Human_prose.t option;
        }
      | Save_operation_draft of Operation.t
      | Delete_operation_draft of { id : string }
      | Glorify_operation_draft of Operation.t
      | Run_operation_draft_simulation of { draft : Operation.Draft.t }
      | Run_account_draft_simulation of { draft : Account.Draft.t }
      | Order_operation of {
          specification : Operation.Specification.t;
          comments : Human_prose.t option;
        }
      | Make_blob_for_signature of {
          target_contract_id : string;
          entrypoint : string;
          parameters : (string * Variable.Value.t) list;
        }
      | Sign_blob of { account : string; blob : Bytes_rep.t }
      | Check_signature of {
          public_key : Public_key.t;
          blob : Bytes_rep.t;
          signature : Signature.t;
        }
      | Delete_account of string
      | Update_comments of { id : string; comments : Human_prose.t option }
      | Update_display_name of { id : string; name : string }
      | Get_configuration
      | Update_configuration of Wallet_configuration.t
      | Get_exchange_rates
      | All_accounts
      | All_operations
      | Get_events of float
      | Get_network_information
      | Generate_key_pair of {
          kind : [ `Unencrypted ];
          curve : [ `Ed25519 | `Secp256k1 | `P256 ];
        }
      | Analyze_uri_or_address of string
      | Guess_michelson_expression_type of Michelson.t
    [@@deriving sexp, variants, equal]
  end

  module Down = struct
    module Alert = struct
      type content =
        | Ledger_wants_human_intervention of {
            ledger_id : string;
            ledger_name : string;
            reason : string;
          }
        | Account_is_being_created of { id : string; display_name : string }
      [@@deriving sexp, variants]

      type t = { id : string; content : content }
      [@@deriving sexp, fields, make]
    end

    module Async_failure = struct
      type t = string [@@deriving sexp, equal]
    end

    type event =
      | Please_just_reload_all of string
      | New_exchange_rates of Exchange_rates.t
      | Fresh_network_information of Network_information.t
      | Alert_on of Alert.t
      | Alert_off of { id : string }
      | Configuration_update of Wallet_configuration.t
      | Async_failure_event of Async_failure.t
    [@@deriving sexp, variants]

    type t =
      | Ledgers of Ledger_nano.Device.t list
      | Account_list of Account.t list
      | Operation_list of Operation.t list
      | Events of event list * float
      | Exchange_rates of Exchange_rates.t option
      | Failure_list of Async_failure.t list
      | Configuration of Wallet_configuration.t
      | Blob of Bytes_rep.t
      | Signature of string
      | Network_information of Network_information.t
      | Simulation_result of Simulation_result.t * Tezos_batch.t
      | Key_pair of {
          public_key : string;
          public_key_hash : string;
          secret_key_uri : string;
        }
      | Uri_or_address_analysis of Uri_analysis.t
      | Michelson_typing of Michelson_type.t Option.t
      | Done
    [@@deriving sexp, variants]
  end
end
