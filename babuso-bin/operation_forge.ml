open! Import
open Babuso_lib
open Wallet_data

module Contract_call = struct
  let parameter parameters s =
    List.Assoc.find parameters s ~equal:String.equal |> function
    | Some s -> Variable.Value.to_michelson s
    | None -> Fmt.failwith "Missing parameter: %S" s

  let variable variables s =
    Variable.(Record.get variables s |> Value.to_michelson)

  module Generic_multisig = struct
    let update_keys_payload _ ~parameters =
      let open Tezai_michelson.Untyped.C in
      e_right
        (e_pair
           (parameter parameters "threshold")
           (parameter parameters "keys"))

    let lambda_payload _ ~parameters =
      let open Tezai_michelson.Untyped.C in
      e_left (parameter parameters "lambda")

    let thing_to_pack_and_sign ~chain_id ~parameters ~variables ~address payload
        =
      let open Tezai_michelson.Untyped.C in
      ignore parameters;
      e_pair
        (e_pair (string chain_id) (string address))
        (e_pair (variable variables "counter") payload)

    let arg ~payload ~parameters ~variables =
      let open Tezai_michelson.Untyped.C in
      let signature_opts = parameter parameters "signatures" in
      e_pair (e_pair (variable variables "counter") payload) signature_opts
  end
end
