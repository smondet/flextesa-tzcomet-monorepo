open! Import
open Babuso_lib
open! Wallet_data
open Babuso_error

module Flextesa_key_pairs = struct
  module Crypto = Tezai_tz1_crypto.Signer

  let sk seed = Crypto.Secret_key.of_seed seed
  let pk seed = sk seed |> Crypto.Public_key.of_secret_key
  let pkh seed = pk seed |> Crypto.Public_key_hash.of_public_key
  let pubkey n = pk n |> Crypto.Public_key.to_base58
  let pubkey_hash n = pkh n |> Crypto.Public_key_hash.to_base58
  let private_key n = "unencrypted:" ^ Crypto.Secret_key.to_base58 (sk n)
end

module Fake = struct
  let fake_animals = "crouching-tiger-hidden-dragon"

  let fake_ledger uri =
    let seed = Caml.Digest.(string uri |> to_hex) in
    let behavior =
      let make ?(fail_show = false) ?(fail_import = false) ?fail_sign
          ?(delay = 2.) () =
        object
          method fail_import = fail_import
          method fail_sign = fail_sign
          method fail_show = fail_show
          method delay = delay
        end
      in
      let delay = function
        | [] -> 2.
        | one :: _ -> ( try Float.of_string one with _ -> 2.)
      in
      match
        Uri.of_string uri |> Uri.path |> String.split ~on:'/' |> List.rev
      with
      | "42h" :: _more -> make ~fail_import:true ~delay:(delay _more) ()
      | "420h" :: nth :: _more ->
          let fail_sign = try Int.of_string nth with _ -> 0 in
          make ~fail_sign () ~delay:(delay _more)
      | "4242h" :: _more -> make ~fail_show:true ~delay:(delay _more) ()
      | "51h" :: _more -> make () ~delay:(delay _more)
      | _ -> make ()
    in
    object
      method uri = uri
      method behavior = behavior
      method pkh = Flextesa_key_pairs.pubkey_hash seed
      method pk = Flextesa_key_pairs.pubkey seed
      method sk = Flextesa_key_pairs.private_key seed
    end

  let _accounts : [ `Fake_ledger of string * string * int ref ] list ref =
    ref []
  (* This is not saved to disk; may not be in sync' with what the
     octez client knows. *)

  let command ctxt ~real_one args =
    let logs = ref [] in
    let log p = logs := p :: !logs in
    let uri_is_fake uri = String.is_prefix ~prefix:"ledger://crouching" uri in
    Exn.protect
      ~f:(fun () ->
        match args with
        | [ "list"; "connected"; "ledgers" ] ->
            Io.Clock.sleep ctxt 1.;
            real_one args
            @ [
                Fmt.str "## Ledger `%s`" fake_animals;
                "Found a Tezos Wallet 4.2.42 (git-description: ";
                "more garbage";
              ]
        | "import" :: "secret" :: "key" :: "fail" :: _ ->
            Io.Clock.sleep ctxt 1.;
            log Docpp.(text "adding failure");
            Fmt.failwith "Fake failure!"
        | "import" :: "secret" :: "key" :: name :: uri :: _ when uri_is_fake uri
          ->
            let ledger = fake_ledger uri in
            Io.Clock.sleep ctxt ledger#behavior#delay;
            log
              Docpp.(
                text "Intercepting fake import:"
                +++ itemize
                      [
                        text "Replacing:" +++ verbatim uri;
                        text "With:" +++ verbatim ledger#sk;
                      ]);
            _accounts := `Fake_ledger (name, uri, ref 0) :: !_accounts;
            (* accounts := `Ledger (name, ledger) :: !accounts ; *)
            if ledger#behavior#fail_import then
              Failure.raise_textf "Ledger %S fails at import on purpose." uri;
            real_one [ "import"; "secret"; "key"; name; ledger#sk; "--force" ]
        | [ "sign"; "bytes"; bytes; "for"; uri ] when uri_is_fake uri ->
            log Docpp.(text "Intercepting fake sign:" +++ verbatim uri);
            let ledger = fake_ledger uri in
            Io.Clock.sleep ctxt ledger#behavior#delay;
            let counter =
              match
                List.find_map !_accounts ~f:(function
                  | `Fake_ledger (_, u, c) when String.equal u uri -> Some c
                  | _ -> None)
              with
              | Some c -> c
              | None ->
                  let c = ref 0 in
                  _accounts := `Fake_ledger ("", uri, c) :: !_accounts;
                  c
            in
            Int.incr counter;
            begin
              match ledger#behavior#fail_sign with
              | Some count when count = !counter ->
                  Failure.raise_textf
                    "Ledger %S fails at %dth signature on purpose." uri count
              | None | Some _ -> ()
            end;
            real_one [ "sign"; "bytes"; bytes; "for"; ledger#sk ]
        | [ "show"; "ledger"; uri ] when uri_is_fake uri ->
            log Docpp.(text "Intercepting fake show-ledger:" +++ verbatim uri);
            let ledger = fake_ledger uri in
            Io.Clock.sleep ctxt ledger#behavior#delay;
            if ledger#behavior#fail_show then
              Failure.raise_textf "Ledger %S fails to be shown on purpose." uri;
            let props =
              [
                ("Application", "This is a Fake LEdger from Babuso");
                ("Curve", "`Backticks`");
                ("Public Key", ledger#pk);
                ("Public Key Hash", ledger#pkh);
              ]
            in
            Fmt.str "Found ledger corresponding to %s:" uri
            :: List.map props ~f:(fun (prop, v) -> Fmt.str "* %s: %s" prop v)
        | other -> real_one other)
      ~finally:(fun () ->
        dbgp
          Docpp.(
            text "Fake tezos client:"
            +++ itemize
                  [
                    text "Args:" +++ itemize (List.map args ~f:(textf "%S"));
                    text "State:"
                    +++ itemize
                          (List.map !_accounts ~f:(function
                               | `Fake_ledger (n, u, c) ->
                               textf "Fake-ledger %S →" n
                               +++ verbatim u +++ textf "(%d signatures)" !c));
                    text "Logs:" +++ itemize (List.rev_map !logs ~f:Fn.id);
                  ]))
end

type t = {
  executable : string;
  data_dir : string;
  faking_it : bool; [@default false]
  mutable initialized : bool; [@default false]
}
[@@deriving sexp, make, fields]

let get ctxt : t = ctxt#octez_client

module Low_level = struct
  let command ctxt self args =
    let real_one args =
      let quoted =
        String.concat ~sep:" "
          (List.map ~f:Path.quote
             ([
                self.executable;
                "--base-dir";
                self.data_dir;
                "--protocol";
                "PtJakart2xVj";
              ]
             @ args))
      in
      Io.Shell.command_to_string_list ctxt
        ("TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=\"Y\" " ^ quoted)
    in
    dbgf "octez-client %a\n  command: %a" Sexp.pp (sexp_of_t self)
      Fmt.Dump.(list string)
      args;
    if self.faking_it then Fake.command ctxt args ~real_one else real_one args

  let ensure_initialized ctxt =
    let client = get ctxt in
    match client.initialized with
    | true -> ()
    | false ->
        Io.File.make_path ctxt client.data_dir;
        let (_ : string list) =
          command ctxt client
            [ "--endpoint"; "http://localhost:0"; "config"; "update" ]
        in
        client.initialized <- true;
        ()
end

let run_command ctxt args =
  Low_level.ensure_initialized ctxt;
  Low_level.command ctxt (get ctxt) args

let import_secret_key ctxt id uri =
  let lines =
    run_command ctxt [ "import"; "secret"; "key"; id; uri; "--force" ]
  in
  dbgf "Import result: %a" Fmt.Dump.(list string) lines;
  let lines = run_command ctxt [ "show"; "address"; id ] in
  dbgf "Show result: %a" Fmt.Dump.(list string) lines;
  let public_key_hash, public_key =
    match List.map lines ~f:(fun l -> String.split ~on:' ' l) with
    | [ [ "Hash:"; h ]; [ "Public"; "Key:"; p ] ] -> (h, p)
    | _ ->
        let msg =
          Fmt.str "Failed to parse public-key(-hash): %a"
            Fmt.Dump.(list string)
            lines
        in
        raise
          (Babuso_exn (General_error msg, Some "Babuso.Service.ui_protocol"))
  in
  object
    method id = id
    method pkh = public_key_hash
    method pk = public_key
  end

let sign_bytes ctxt bytes account =
  let output =
    run_command ctxt
      [ "sign"; "bytes"; Bytes_rep.to_zero_x bytes; "for"; account ]
  in
  match List.map ~f:(String.split ~on:' ') output with
  | [ [ "Signature:"; b58 ] ] -> b58
  | _ ->
      Failure.raise
        Docpp.(
          text "Failed to parse octez-client output"
          +++ itemize
                [
                  text "Bytes:" +++ Bytes_rep.to_pp bytes;
                  text "Account:" +++ verbatim account;
                  text "Output:" +++ verbatim_lines output;
                ])

let check_signature ctxt ~public_key ~signature ~blob =
  let ensure_name =
    run_command ctxt
      [
        "import";
        "public";
        "key";
        public_key;
        "unencrypted:" ^ public_key;
        "--force";
      ]
  in
  dbgp
    Docpp.(
      textf "Imported PK %S as itself." public_key
      +++ verbatim_lines ensure_name);
  try
    let (_ : string list) =
      run_command ctxt
        [
          "check";
          "that";
          "bytes";
          Bytes_rep.to_zero_x blob;
          "were";
          "signed";
          "by";
          public_key;
          "to";
          "produce";
          signature;
        ]
    in
    `Checks_out
  with _ -> `No "Octez-client check failed"

let list_connected_ledgers ctxt =
  let lines = run_command ctxt [ "list"; "connected"; "ledgers" ] in
  let parsed =
    List.fold lines ~init:([], []) ~f:(fun (cur, all) line ->
        let words = String.split_on_chars line ~on:[ ' '; '`' ] in
        match words with
        | "##" :: "Ledger" :: "" :: new_one :: _ ->
            (* match String.chop_prefix line ~prefix:"## Ledger `" with
               | Some new_one -> *)
            ([ String.chop_suffix_if_exists new_one ~suffix:"`" ], cur :: all)
        | "Found" :: "a" :: "Tezos" :: "Wallet" :: version :: _ ->
            (version :: cur, all)
        | _ -> (cur, all))
    |> fun (last, all) ->
    List.filter_map (last :: all) ~f:(function
      | [] -> None
      | [ animals ] -> Some (Ledger_nano.Device.make ~animals ())
      | [ version; animals ] ->
          Some (Ledger_nano.Device.make ~animals ~wallet_app_version:version ())
      | _ ->
          let msg =
            Fmt.str "Bug parsing ledgers: %a" Fmt.Dump.(list string) lines
          in
          raise
            (Babuso_exn (General_error msg, Some "Babuso.Service.ui_protocol")))
  in
  parsed

let show_address ctxt id =
  let lines = run_command ctxt [ "show"; "address"; id; "-S" ] in
  match
    List.map
      ~f:(fun l -> String.split ~on:' ' l |> List.map ~f:String.strip)
      lines
  with
  | [ [ "Hash:"; pkh ]; [ "Public"; "Key:"; pk ]; [ "Secret"; "Key:"; sk ] ] ->
      object
        method pkh = pkh
        method pk = pk
        method sk = sk
      end
  | _ ->
      Failure.raise
        Docpp.(
          textf "Failed to parse output of show address %S -S" id
          +++ verbatim_lines lines)

let gen_key ctxt ~curve =
  let id = Fresh_id.make () in
  let lines =
    run_command ctxt
      [
        "gen";
        "keys";
        id;
        "--sig";
        (match curve with
        | `Ed25519 -> "ed25519"
        | `Secp256k1 -> "secp256k1"
        | `P256 -> "p256");
        "--force";
      ]
  in
  assert (List.is_empty lines);
  show_address ctxt id

let get_public_part ctxt uri =
  match Uri.of_string uri |> Uri.scheme with
  | Some "unencrypted" ->
      let id = Fresh_id.make () in
      let (_ : string list) =
        run_command ctxt [ "import"; "secret"; "key"; id; uri; "--force" ]
      in
      (show_address ctxt id :> < pk : string ; pkh : string >)
  | Some "ledger" ->
      let lines = run_command ctxt [ "show"; "ledger"; uri ] in
      let interesting =
        List.filter_map lines ~f:(fun line ->
            match String.chop_prefix line ~prefix:"* Public Key Hash: " with
            | Some pkh -> Some (`Pkh pkh)
            | None -> (
                match String.chop_prefix line ~prefix:"* Public Key: " with
                | Some pk -> Some (`Pk pk)
                | None -> None))
      in
      let pk, pkh =
        match interesting with
        | [ `Pkh pkh; `Pk pk ] | [ `Pk pk; `Pkh pkh ] -> (pk, pkh)
        | _ ->
            Failure.raise_textf "not implemented: %a"
              Fmt.Dump.(list string)
              lines
      in
      object
        method pk = pk
        method pkh = pkh
      end
  | None | Some _ ->
      Failure.raise_textf "Getting public-part of %S is not supported" uri
