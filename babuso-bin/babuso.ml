open Import
open Babuso_lib
open Wallet_data

let start_server ?(debug = false) ?addr ctxt () =
  let (Wallet_configuration.{ port; max_connections; _ } as config) =
    Backend_state.configuration ctxt
  in
  Update_loop.start ctxt;
  let get_configured_nodes () =
    Backend_state.configuration ctxt |> Wallet_configuration.nodes
  in
  Monitor_network.start_update_loop ~get_configured_nodes ctxt;
  (* We send the initial configuration as an update; from None to Some: *)
  Events.configuration_update ctxt config;
  dbgf "Starting server :%d (max: %d)" port max_connections;
  Httpd._enable_debug debug;
  let server = Httpd.create ?addr ~port ~max_connections () in
  Tiny_httpd_camlzip.setup ~compress_above:1024 ~buf_size:(1024 * 1024) server;
  Httpd.set_top_handler server (fun req ->
      Httpd.respond_404 ~req Docpp.(text "Unkonwn request path."));
  Httpd.add_route_handler ~meth:`GET server
    Httpd.Route.(rest_of_path)
    (fun path req ->
      Httpd.handle_exceptions ~req (fun () ->
          (* let header = Httpd.header_argument query in *)
          match Uri.pct_decode path with
          | "" | "index" | "index.html" -> Service.index ctxt
          | "main-client.js" -> Service.main_js ctxt
          | "bootstrap.css" -> Service.bootstrap_css ctxt
          | "please/die" -> Caml.exit 0
          | path -> Failure.raise ~code:500 Docpp.(textf "Wrong path: %S" path)));
  Httpd.add_route_handler ~meth:`POST server
    Httpd.Route.(exact "ui-protocol" @/ return)
    (fun req ->
      Httpd.handle_exceptions ~req (fun () ->
          Service.ui_protocol ctxt ~body:(Httpd.Request.body req)));
  Fmt.pr "listening on http://%s:%d\n%!" (Httpd.addr server) (Httpd.port server);
  match Httpd.run server with Ok () -> () | Error e -> raise e

let start_server_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun ctxt addr () -> start_server ?addr ctxt ())
    $ Backend_state.loading_cmdliner_term ()
    $ Arg.(
        value
          (opt (some string) None
             (info [ "listen-address" ]
                ~doc:"Set the listen address of the HTTP server.")))
    $ const ()
  in
  (term, Cmd.info "start-server" ~doc:"Start the webserver.")

let octez_client_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun ctxt args ->
        let res = Octez_client.run_command ctxt args in
        List.iter ~f:(Fmt.pr "%s\n%!") res)
    $ Backend_state.loading_cmdliner_term ()
    $ Arg.(value (pos_all string [] (info [] ~doc:"ARGUMENTS")))
  in
  (term, Cmd.info "octez-client" ~doc:"Call commands with the default client.")

module Query = struct
  type context = Backend_state.full_context

  let up_message_examples ctxt =
    let open Protocol.Message.Up in
    let examples = ref [] in
    let ex name v = examples := (name, v) :: !examples in
    ex "account-draft"
      (save_account_draft ~id:(Fresh_id.make ()) ~display_name:"Draft-00"
         ~content:Account.Draft.(create_generic_multisig ())
         ~comments:
           (Human_prose.raw_markdown "Some **Markdown** here" |> Option.some)
         ~glorify:true);
    ex "update-configuration"
      (update_configuration (Backend_state.configuration ctxt));
    !examples

  let post ctxt s =
    let url =
      Fmt.str "http://localhost:%d/ui-protocol"
        (Backend_state.configuration ctxt |> Wallet_configuration.port)
    in
    `Sexp_string (Http_client.call ~meth:(`Post s) ctxt url)

  let post_message ctxt msg =
    post ctxt (Sexp.to_string_hum (Protocol.Message.Up.sexp_of_t msg))

  let commands :
      (string
      * string
      * (context ->
        string list ->
        [ `Sexp_string of string | `Text_list of string list ]))
      list
      ref =
    ref []

  let command name help f = commands := (name, help, f) :: !commands

  let one_arg cmd = function
    | [ one ] -> one
    | [] -> Failure.raise Docpp.(textf "Command %s requires one argument" cmd)
    | more ->
        Failure.raise
          Docpp.(
            textf "Command %s requires exacly one argument, %d were given:" cmd
              (List.length more)
            +++ concat_map ~sep:comma more ~f:verbatim)

  let () =
    command "help" "Show help about the available queries." (fun _ctxt _args ->
        let res =
          List.rev_map !commands ~f:(fun (n, h, _) -> Fmt.str "* %S: %s" n h)
        in
        `Text_list res)

  let () =
    command "raw" "Send a raw protocol message (S-Expression)."
      (fun ctxt args -> post ctxt (one_arg "raw" args))

  let () =
    command "list-examples" "List the available examples." (fun ctxt _args ->
        `Text_list (List.map ~f:fst (up_message_examples ctxt)))

  let () =
    command "edit-example" "Edit an example message (with $EDITOR) and sent it."
      (fun ctxt args ->
        let substring = one_arg "edit-example" args in
        let example =
          match
            List.filter_map (up_message_examples ctxt) ~f:(fun (n, v) ->
                if String.is_substring n ~substring then Some v else None)
          with
          | [] ->
              Failure.raise
                Docpp.(textf "Found no example with pattern %S" substring)
          | [ one ] -> one
          | _ ->
              Failure.raise
                Docpp.(
                  textf "Found too many examples with pattern %S" substring)
        in
        let tmp = Path.temp_file "babuso-query" "message.scm" in
        Io.File.write_lines ctxt tmp
          [ Sexp.to_string_hum (Protocol.Message.Up.sexp_of_t example) ];
        let (_ : string list) =
          Fmt.kstr
            (Io.Shell.command_to_string_list ctxt)
            "\"$EDITOR\" %s" (Path.quote tmp)
        in
        post ctxt (Io.File.read_lines ctxt tmp |> String.concat ~sep:"\n"))

  let command_fail cmd pp =
    Failure.raise Docpp.(textf "Command %S failed:" cmd +++ pp)

  let command_failf cmd fmt =
    Fmt.kstr (fun s -> command_fail cmd (Docpp.text s)) fmt

  let () =
    let name = "uri-account" in
    command name "Add an URI-Account (display name + URI)." (fun ctxt args ->
        let display_name, acc_uri =
          match args with
          | [] -> command_failf name "Missing aguments, need 1 or 2."
          | [ one ] ->
              (Fmt.str "CLI-Account-%s" Caml.Digest.(string one |> to_hex), one)
          | [ one; two ] -> (one, two)
          | _ -> command_failf name "Too many arguments, need 1 or 1"
        in
        let msg =
          Protocol.Message.Up.save_account_draft ~id:(Fresh_id.make ())
            ~comments:None ~display_name ~glorify:true
            ~content:Account.Draft.(create_from_address_or_uri ~uri:acc_uri ())
        in
        post_message ctxt msg)

  let command () =
    let open Cmdliner in
    let open Term in
    let term =
      const (fun ctxt output_format cmd args ->
          let answer =
            match
              List.filter !commands ~f:(fun (n, _, _) ->
                  String.is_prefix n ~prefix:cmd)
            with
            | [] ->
                Failure.raise
                  Docpp.(textf "Could not find a command starting with: %s" cmd)
            | [ (_, _, f) ] -> f ctxt args
            | more ->
                Failure.raise
                  Docpp.(
                    textf "Too many commands start with %S:" cmd
                    +++ concat_map ~sep:comma
                          ~f:(fun (n, _, _) -> verbatim n)
                          more)
          in
          begin
            match (output_format, answer) with
            | `Raw, `Sexp_string s -> Fmt.pr "%s\n%!" s
            | (`Raw | `Guess), `Text_list s ->
                Fmt.pr "%s\n%!" (String.concat ~sep:"\n" s)
            | (`Sexp | `Guess), `Sexp_string s ->
                Fmt.pr "%a\n%!" Sexp.pp_hum (Sexplib.Sexp.of_string s)
            | `Sexp, `Text_list s ->
                Fmt.pr "%a\n%!" Sexp.pp_hum
                  (Sexp.List (List.map ~f:(fun s -> Sexp.Atom s) s))
          end;
          ())
      $ Backend_state.loading_cmdliner_term ()
      $ Arg.(
          let opts = [ ("raw", `Raw); ("sexp", `Sexp); ("quess", `Guess) ] in
          let doc =
            Fmt.str "The output format, one of %s."
              (List.map opts ~f:(fun (k, _) -> Fmt.str "%S" k)
              |> String.concat ~sep:", ")
          in
          value (opt (enum opts) `Guess (info [ "format" ] ~doc)))
      $ Arg.(required (pos 0 (some string) None (info [] ~doc:"COMMAND")))
      $ Arg.(value (pos_right 0 string [] (info [] ~doc:"ARGUMENTS")))
    in
    (term, Cmd.info "query" ~doc:"Query the backend.")
end

let configure_command () =
  let open Cmdliner in
  let open Term in
  let term =
    const (fun (data_dir, configuration) () ->
        let ctxt =
          object
            method file_io = Io.File.default ()
          end
        in
        Backend_state.save_configuration ctxt data_dir configuration;
        ())
    $ Backend_state.configuration_cmdliner_term ()
    $ const ()
  in
  (term, Cmd.info "configure" ~doc:"Write the configuration.")

let main () =
  let open Cmdliner in
  let version = "0.0.0" in
  Caml.exit
    (Failure.run_or_die (fun () ->
         Cmd.eval ~catch:false
           (Cmd.group
              (Cmd.info "babuso" ~version ~doc:"Bank, But Self-Operated")
              (List.map
                 ~f:(fun (t, i) -> Cmd.v i t)
                 ([
                    start_server_command ();
                    configure_command ();
                    octez_client_command ();
                    Query.command ();
                    Mockup_tests.command ();
                  ]
                 @ Backend_tests.commands ())))))

let () =
  Debug_app.on := true;
  main ()
