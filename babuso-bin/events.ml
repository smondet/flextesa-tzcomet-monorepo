open! Import
open Babuso_lib
open Wallet_data
module Ev = Protocol.Message.Down

type event = { timestamp : float; content : Ev.event }
type t = { mutable all : event list; mutex : Mutex.t; condition : Condition.t }

let create () =
  { all = []; mutex = Mutex.create (); condition = Condition.create () }

let get ctxt : t = ctxt#events

let add ctxt events =
  let self = get ctxt in
  Mutex.lock self.mutex;
  let timestamp = Unix.gettimeofday () in
  List.iter events ~f:(fun content ->
      let filter_out_old =
        let open Ev in
        let make f =
          List.filter ~f:(fun { timestamp = _; content } -> not (f content))
        in
        match content with
        | New_exchange_rates _ ->
            make (function New_exchange_rates _ -> true | _ -> false)
        | Fresh_network_information _ ->
            make (function New_exchange_rates _ -> true | _ -> false)
        | Configuration_update _ ->
            make (function Configuration_update _ -> true | _ -> false)
        | Please_just_reload_all _ ->
            make (function Please_just_reload_all _ -> true | _ -> false)
        | Alert_on _ | Alert_off _ | Async_failure_event _ -> Fmt.id
      in
      self.all <- { timestamp; content } :: filter_out_old self.all);
  Condition.broadcast self.condition;
  Mutex.unlock self.mutex;
  ()

let get ctxt ~since =
  let self = get ctxt in
  Mutex.lock self.mutex;
  let interesting_event since ev = Float.(ev.timestamp - since > 0.) in
  let there_are_events () =
    match self.all with
    | ev :: _ when interesting_event since ev -> true
    | _ -> false
  in
  while not (there_are_events ()) do
    Condition.wait self.condition self.mutex
  done;
  let to_return = List.take_while self.all ~f:(interesting_event since) in
  let to_keep =
    let since = Unix.gettimeofday () -. 300. in
    List.take_while self.all ~f:(interesting_event since)
  in
  dbgf "Events GC: keeping %d events" (List.length to_keep);
  self.all <- to_keep;
  (* let all = self.all in
     self.all <- [] ; *)
  Mutex.unlock self.mutex;
  to_return

let add_generic ctxt fmt =
  Fmt.kstr (fun s -> add ctxt [ Ev.Please_just_reload_all s ]) fmt

let account_added ctxt (acc : Account.t) =
  add_generic ctxt "account_added %s" acc.id

let account_removed ctxt (id : string) =
  add_generic ctxt "account_removed %s" id

let account_changed ctxt (id : string) =
  add_generic ctxt "account_changed %s" id

let operation_added ctxt (acc : Operation.t) =
  add_generic ctxt "operation_added %s" acc.id

let operation_removed ctxt (id : string) =
  add_generic ctxt "operation_removed %s" id

let operation_changed ctxt (id : string) =
  add_generic ctxt "operation_changed %s" id

let new_exchange_rates ctxt rates = add ctxt [ Ev.New_exchange_rates rates ]
let new_async_failure ctxt failure = add ctxt [ Ev.Async_failure_event failure ]

let add_alert ctxt content =
  let id = Fresh_id.make () in
  add ctxt [ Ev.alert_on (Ev.Alert.make ~id ~content) ];
  id

let remove_alert ctxt id = add ctxt [ Ev.alert_off ~id ]

let configuration_update ctxt config =
  add ctxt [ Ev.configuration_update config ]
