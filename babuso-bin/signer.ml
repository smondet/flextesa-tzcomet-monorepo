open! Import
open Babuso_lib
open Wallet_data

type t = { mode : [ `Octez_client ] [@default `Octez_client] }
[@@deriving sexp, equal, fields, make]

let _get ctxt : t = ctxt#signer

module Watermark = struct
  let operation = Bytes_rep.of_hex "03"
end

let with_potential_alert ctxt ~account_id ~uri ~reason ~f =
  let finally =
    match Uri.scheme (Uri.of_string uri) with
    | Some "unencrypted" -> Fn.id
    | Some "ledger" ->
        let alert_id =
          Events.add_alert ctxt
            (Events.Ev.Alert.ledger_wants_human_intervention
               ~ledger_id:account_id ~ledger_name:uri ~reason)
        in
        fun () -> Events.remove_alert ctxt alert_id
    | _ ->
        Failure.raise
          Docpp.(
            text "Cannot protect SK-URI, unrecognizable scheme:"
            +++ verbatim uri)
  in
  Exn.protect ~f ~finally

let process_secret_key_uri ctxt ~account_id uri =
  let _self = _get ctxt in
  let id = Caml.Digest.(string uri |> to_hex) in
  let open Key_pair in
  let backend =
    match Uri.scheme (Uri.of_string uri) with
    | Some "unencrypted" -> unencrypted ~private_uri:uri
    | Some "ledger" -> ledger ~uri
    | _ ->
        Failure.raise
          Docpp.(
            textf "Cannot import URI for %s, unrecognizable scheme:" account_id
            +++ verbatim uri)
  in
  with_potential_alert ctxt ~account_id ~uri
    ~reason:"Please check and write down the address ON THE LEDGER itself."
    ~f:(fun () ->
      let kp =
        try Octez_client.import_secret_key ctxt id uri
        with e ->
          Failure.raise
            Docpp.(
              textf "Importing URI %s failed" uri +++ sexpable Exn.sexp_of_t e)
      in
      make ~public_key:kp#pk ~public_key_hash:kp#pkh backend)

let sign_bytes ctxt account ?watermark bytes : Signature.t =
  let _self = _get ctxt in
  let backend =
    Account.get_secret_key account
    |> Failure.of_none
         ~f:
           Docpp.(
             fun () ->
               text
                 "Signer cannot sign because there is no secret key backend \
                  for account:"
               +++ sexpable Account.sexp_of_t account)
  in
  let with_wm s =
    match watermark with None -> s | Some w -> Bytes_rep.append w s
  in
  let uri = Key_pair.octez_client_sk_uri backend in
  with_potential_alert ctxt ~account_id:account.id ~uri
    ~reason:"Accept the signature." ~f:(fun () ->
      Octez_client.sign_bytes ctxt (with_wm bytes) uri)

let check_signature ctxt ~public_key ~signature ~blob =
  let _self = _get ctxt in
  Octez_client.check_signature ctxt ~public_key ~signature ~blob

let generate_key_pair ctxt ~curve =
  let _self = _get ctxt in
  Octez_client.gen_key ctxt ~curve

let identify_uri ctxt uri =
  let _self = _get ctxt in
  Octez_client.get_public_part ctxt uri
