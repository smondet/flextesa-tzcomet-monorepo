open Import
open Babuso_lib
open Babuso_error
open Wallet_data

let balance ctxt ~address =
  let balance = Backend_state.RPC.get_balance ctxt ~address in
  balance

let manager_key ctxt ~address = Backend_state.RPC.get_manager_key ctxt ~address

let generic_multisig_counter_threshold_keys ctxt ~address =
  let storage = Backend_state.RPC.storage ctxt ~address in
  dbgf "Storage: %a" Tezai_michelson.Untyped.pp storage;
  let open Tezai_michelson.Untyped.M in
  match storage with
  | Prim
      ( _,
        "Pair",
        [ Int (_, c); Prim (_, "Pair", [ Int (_, thr); Seq (_, keys) ], _) ],
        _ ) ->
      ( c,
        thr,
        List.map keys ~f:(function
          | Bytes (_, s) ->
              Tezai_base58_digest.Identifier.Generic_signer.Public_key.(
                of_bytes s |> to_base58)
          | String (_, s) -> s
          | _ -> assert false) )
  | Prim _ | Int (_, _) | String (_, _) | Bytes (_, _) | Seq (_, _) ->
      assert false

let generic_multisig_variables ctxt ~address =
  let counter, threshold, keys =
    generic_multisig_counter_threshold_keys ctxt ~address
  in
  Variable.(
    Record.comb
      [
        ("counter", Value.nat counter);
        ("threshold", Value.nat threshold);
        ("keys", Value.key_list keys);
      ])

let generic_multisig ctxt ~address =
  let variables = generic_multisig_variables ctxt ~address in
  let contract = Smart_contract.generic_multisig ~variables in
  Originated_contract.make address ~contract

let update_custom_contract_variables ctxt ~address ~variables =
  let raw_storage = Backend_state.RPC.storage ctxt ~address in
  let normalized =
    Tezai_contract_metadata_manipulation.Micheline_helpers.normalize_combs
      ~primitive:"Pair" raw_storage
  in
  dbgf "update_custom_contract_variables(%s)\nStorage: %a, Normalized: %a"
    address Tezai_michelson.Untyped.pp raw_storage Tezai_michelson.Untyped.pp
    normalized;
  let variables =
    Wallet_data.Variable.Record.update_from_normalized_michelson variables
      normalized
  in
  dbgf "update_custom_contract_variables Variables: %a" Sexp.pp_hum
    (Variable.Record.sexp_of_t variables);
  variables

let forein_contract ctxt ~address =
  let entrypoints = Backend_state.RPC.entrypoints ctxt ~address in
  let storage_content =
    Backend_state.RPC.storage ctxt ~address
    |> Tezai_contract_metadata_manipulation.Micheline_helpers.normalize_combs
         ~primitive:"Pair"
  in
  let script = Backend_state.RPC.script ctxt ~address in
  Foreign_contract.make ~entrypoints ~storage_type:script#storage_type
    ~storage_content ()

let any_kt1 ctxt ~address =
  Originated_contract.make address (* ~variables:Variable.Record.empty *)
    ~contract:(Smart_contract.foreign (forein_contract ctxt ~address))

let identify_address ctxt ~address =
  match String.prefix address 3 with
  | "KT1" -> (
      let attempts = [ generic_multisig; any_kt1 ] in
      match
        List.find_map attempts ~f:(fun attempt ->
            match attempt ctxt ~address with
            | backend -> Some backend
            | exception e ->
                dbgf "Attempt Failed: %a" Exn.pp e;
                None)
      with
      | Some s -> Account.Status.originated_contract s
      | None ->
          let msg = Fmt.str "Cannot identify address: %S" address in
          raise
            (Babuso_exn
               (General_error msg, Some "Babuso.Indexer.identify_address")))
  | "tz1" | "tz2" | "tz3" ->
      Account.Status.friend (Friend.make ~public_key_hash:address ())
  | _ ->
      let msg = Fmt.str "Cannot identify address: %S" address in
      raise
        (Babuso_exn (General_error msg, Some "Babuso.Indexer.identify_address"))

let analyze_uri_result ctxt parsed =
  let module F = Uri_analysis.Fact in
  let facts = ref [] in
  let fact f = facts := f :: !facts in
  let balance address = fact (balance ctxt ~address |> F.balance) in
  let maybe f x = try f x with _ -> () in
  begin
    match parsed with
    | `Friend_tz address | `Kt1 address -> maybe balance address
    | `Friend_pk pk ->
        let address =
          Tezai_base58_digest.Identifier.Generic_signer.(
            Public_key_hash.of_public_key (Public_key.of_base58 pk)
            |> Public_key_hash.to_base58)
        in
        fact (F.address address);
        maybe balance address
    | `Unencrypted uri | `Ledger uri ->
        let identified = Signer.identify_uri ctxt uri in
        fact (F.address identified#pkh);
        fact (F.public_key identified#pk);
        maybe balance identified#pkh
  end;
  List.rev !facts

let guess_michelson_expression_type ctxt expression =
  let open Michelson_type in
  let attempts =
    Michelson.C.
      [ (t_lambda t_unit (t_list t_operation), lambda_unit_operations) ]
  in
  List.find_map attempts ~f:(fun (t, meta_type) ->
      if Backend_state.RPC.typecheck_data ctxt ~data:expression ~t then
        Some meta_type
      else None)
