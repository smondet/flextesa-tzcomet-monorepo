open! Import
open! Babuso_lib.Wallet_data
open! SmartML
open Tezai_smartml_generation
open! Construct

let example_expr_of_type =
  let open Variable.Type in
  let open Big_int in
  function
  | Mutez -> E.constant (L.mutez zero_big_int)
  | Address -> L.address "KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton" |> E.constant
  | Nat | Time_span -> L.nat (big_int_of_int 42) |> E.constant
  | Weighted_key_list -> E.build_list ~elems:[]
  | Key_list -> E.build_list ~elems:[]
  | Pkh_option -> E.none
  | Bytes -> L.bytes "" |> E.constant
  | Signature_option_list -> E.build_list ~elems:[]
  | Timestamp_option -> E.none
  | Unit -> L.unit |> E.constant
  | Lambda_unit_to_operation_list | Arbitrary _ -> assert false

let compile_variable_type =
  let open Variable.Type in
  function
  | Mutez -> T.mutez
  | Address -> T.address
  | Nat | Time_span -> T.nat
  | Weighted_key_list -> T.list (T.pair T.nat T.key)
  | Key_list -> T.list T.key
  | Pkh_option -> T.option T.key_hash
  | Bytes -> T.bytes
  | Signature_option_list -> T.list (T.option T.signature)
  | Lambda_unit_to_operation_list -> T.lambda T.unit (T.list T.operation)
  | Timestamp_option -> T.option T.timestamp
  | Unit -> T.unit
  | Arbitrary me ->
      Failure.raise_textf "Arbitrary Michelson type %a not supported."
        Michelson.pp
        (Michelson_expression.to_michelson me)

module Storage = struct
  type t = {
    field : string;
    typ : SmartML.Type.t;
    vtype : Variable.Type.t;
    value_expr : Expr.t;
  }
  [@@deriving fields, make]

  let build field vtype =
    make ~field ~vtype
      ~typ:(compile_variable_type vtype)
      ~value_expr:(example_expr_of_type vtype)

  let make = `Dont_use_make_any_more
end

module Parameters = struct
  type t = { field : string; vtype : Variable.Type.t }
  [@@deriving fields, make, equal]

  let to_expr p = E.(parameter ~field:p.field ())
end

let add_param_request_checking_type storage_requests parameter_requests ~t_equal
    v =
  (* Check for type mismatch in the list of storage requests *)
  match
    List.find storage_requests ~f:(fun x ->
        String.equal x.Storage.field v.Parameters.field)
  with
  | Some r when not (t_equal r.vtype v.vtype) ->
      Failure.raise
        Docpp.(
          textf "BUG: %s field type mismatch:" v.field
          +++ itemize
                [
                  text "Storage:" +++ sexpable Variable.Type.sexp_of_t r.vtype;
                  text "Parameter:" +++ sexpable Variable.Type.sexp_of_t v.vtype;
                ])
  | _ -> (
      (* Check for type mismatch in the list of param requests *)
      match
        List.find !parameter_requests ~f:(fun x ->
            String.equal x.Parameters.field v.field)
      with
      | None -> parameter_requests := v :: !parameter_requests
      | Some r when t_equal (Parameters.vtype r) (Parameters.vtype v) -> ()
      | Some _ ->
          Failure.raise_textf "BUG: %s field type mismatch: Parameter/Parameter"
            v.field)

let add_parameter_request storage_requests parameter_requests req =
  add_param_request_checking_type storage_requests parameter_requests
    ~t_equal:Variable.Type.equal req

let add_unique_parameter_request storage_requests parameter_requests req =
  let unique_names =
    List.map !parameter_requests ~f:(fun pr -> pr.Parameters.field)
  in
  let unique_name = Variable.unique_name unique_names req.Parameters.field in
  add_parameter_request storage_requests parameter_requests
    { req with field = unique_name };
  unique_name

let add_storage_request_checking_type ~t_equal ~vt_equal storage_reqs param_reqs
    v =
  (* Check for type mismatch in the list of parameter requests *)
  match
    List.find param_reqs ~f:(fun x ->
        String.equal x.Parameters.field v.Storage.field)
  with
  | Some r when not (vt_equal r.vtype v.vtype) ->
      Failure.raise_textf "BUG: %s field type mismatch: Storage/Parameter"
        v.field
  | _ -> (
      (* Check for type mismatch in the list of storage requests *)
      match
        List.find !storage_reqs ~f:(fun x ->
            String.equal x.Storage.field v.field)
      with
      | None -> storage_reqs := v :: !storage_reqs
      | Some r when t_equal (Storage.typ r) (Storage.typ v) -> ()
      | Some _ ->
          Failure.raise_textf "BUG: %s field type mismatch: Storage"
            (Storage.field v))

let add_storage_request storage_reqs param_reqs req =
  add_storage_request_checking_type ~t_equal:T.equal
    ~vt_equal:Variable.Type.equal storage_reqs param_reqs req

let const_or_var_storage storage_requests parameter_requests cov
    ~make_expression ~vt =
  let open Constant_or_variable in
  match cov with
  | Constant m -> make_expression m
  | Variable varname ->
      add_storage_request storage_requests !parameter_requests
        (Storage.build varname vt);
      E.storage ~field:varname ()

let not_implemented fmt control ability =
  Fmt.kstr
    (fun s ->
      Failure.raise
        Docpp.(
          text "NOT IMPLEMENTED:" +++ text s
          +++ itemize
                [
                  text "Control" +++ sexpable Control.sexp_of_t control;
                  text "Ability" +++ sexpable Ability.sexp_of_t ability;
                ]))
    fmt

let process_action storage_requests parameter_requests control ability =
  let open Ability in
  let module VT = Variable.Type in
  let rec loop storage_requests parameter_requests control ability =
    match ability with
    | Do_nothing -> []
    | Boomerang -> [ I.send_mutez ~destination:E.sender E.amount ]
    | Set_delegate ->
        let unique =
          add_unique_parameter_request !storage_requests parameter_requests
            (Parameters.make ~field:"delegate" ~vtype:VT.pkh_option)
        in
        [ I.set_delegate E.(parameter ~field:unique ()) ]
    | Update_variables vars ->
        let foldf (name, vtype) acc =
          add_parameter_request !storage_requests parameter_requests
            (Parameters.make ~field:name ~vtype);
          add_storage_request storage_requests !parameter_requests
            (Storage.build name vtype);
          let field_exp = E.(storage ~field:name ()) in
          let val_exp = E.optionally_get_field E.(parameter ()) (Some name) in
          let cmd = I.update_variable ~field_exp ~val_exp in
          cmd :: acc
        in
        List.fold_right vars ~init:[] ~f:foldf
    | Sequence xs ->
        let res =
          List.concat_map xs ~f:(fun x ->
              loop storage_requests parameter_requests control x)
        in
        res
    | Reset_activity_timer var ->
        let field_exp = E.(storage ~field:var ()) in
        [ I.update_variable ~field_exp ~val_exp:(E.some E.now) ]
    | Transfer_mutez { amount; destination } ->
        let destination_expr =
          match destination with
          | `Anyone ->
              let unique =
                add_unique_parameter_request !storage_requests
                  parameter_requests
                  (Parameters.make ~field:"destination" ~vtype:VT.address)
              in
              E.(parameter ~field:unique ())
          | `Sender -> E.sender
          | `Address cov ->
              let get_address =
                const_or_var_storage storage_requests parameter_requests cov
                  ~make_expression:(fun s -> E.constant (L.address s))
                  ~vt:VT.address
              in
              get_address
        in
        let unique =
          add_unique_parameter_request !storage_requests parameter_requests
            (Parameters.make ~field:"amount" ~vtype:VT.mutez)
        in
        let amount_expr = E.(parameter ~field:unique ()) in
        let check_amount =
          match amount with
          | `No_limit -> []
          | `Limit amount_cov ->
              let make_expression z =
                E.constant
                  (L.mutez (Z.to_string z |> Big_int.big_int_of_string))
              in
              [
                I.verify
                  E.(
                    amount_expr
                    <== const_or_var_storage storage_requests parameter_requests
                          amount_cov ~make_expression ~vt:VT.mutez);
              ]
        in
        check_amount
        @ [ I.send_mutez ~destination:destination_expr amount_expr ]
    | _ -> not_implemented "ability" control ability
  in
  loop storage_requests parameter_requests control ability

let check_parameters parameter_requests ep =
  (* This is a check that should always succeed unless there is a bug:
      disagreement between this generator and
      `Entrypoint.collect_parameters`. *)
  let other_source = Entrypoint.collect_parameters ep in
  let assoc =
    List.map parameter_requests ~f:(fun req ->
        (req.Parameters.field, req.vtype))
  in
  List.iter2 other_source assoc ~f:(fun (namel, typel) (namer, typer) ->
      if String.equal namel namer && Variable.Type.equal typel typer then ()
      else
        Failure.raise_textf "BUG: entrypoint parameters mismatch: %s Vs %s (%a)"
          namel namer Sexp.pp
          Entrypoint.(sexp_of_t ep))
  |> List.Or_unequal_lengths.(
       function
       | Ok () -> ()
       | Unequal_lengths ->
           Failure.raise_textf "BUG: entrypoint mismatch length: %a Vs %a -- %a"
             Fmt.Dump.(list string)
             (List.map other_source ~f:fst)
             Fmt.Dump.(list string)
             (List.map assoc ~f:fst) Sexp.pp
             Entrypoint.(sexp_of_t ep))

let process_check storage_requests parameter_requests control =
  let rec to_check_expr_and_command ctrl =
    let open Control in
    match ctrl with
    | Anyone -> ([], None)
    | Sender_is sender_spec ->
        let compare_to =
          const_or_var_storage storage_requests parameter_requests sender_spec
            ~make_expression:(fun s -> E.constant (L.address s))
            ~vt:Variable.Type.address
        in
        ([ E.(sender === compare_to) ], None)
    | When_inactive { timespan; last_activity } ->
        add_storage_request storage_requests !parameter_requests
          (Storage.build last_activity Variable.Type.timestamp_option);
        let tspan_exp =
          const_or_var_storage storage_requests parameter_requests timespan
            ~make_expression:(fun n -> E.constant (L.nat_of_z n))
            ~vt:Variable.Type.time_span
        in
        let last_active_exp = E.storage ~field:last_activity () in
        ( [
            E.(
              E.not_eq last_active_exp E.none
              &&& (add_seconds
                     ~timestamp:(open_some last_active_exp)
                     ~nat:tspan_exp
                  <== now));
          ],
          None )
    | Weighted_multisig { counter; threshold; keys } ->
        let payload =
          let param_list =
            List.rev
              (List.map !parameter_requests ~f:(fun pr ->
                   E.(parameter ~field:pr.field) ()))
          in
          let rec loop : Expr.t list -> Expr.t = function
            | [ a ] -> a
            | [ a; b ] -> E.tuple [ b; a ]
            | a :: b :: zs -> E.tuple [ loop zs; E.tuple [ b; a ] ]
            | other ->
                Failure.raise_textf "Unexpected list in the payload: %s"
                  (Expr.show (E.build_list ~elems:other))
          in
          let make_pairs : Expr.t list -> Expr.t option = function
            | [] -> None
            | other -> Some (loop other)
          in
          make_pairs param_list
        in
        let threshold_expr =
          const_or_var_storage storage_requests parameter_requests threshold
            ~make_expression:(fun z -> E.constant (L.nat_of_z z))
            ~vt:Nat
        in
        let keys_to_tuples ks =
          let to_tuple (z, pk) =
            E.tuple [ E.constant (L.nat_of_z z); E.constant (L.key pk) ]
          in
          List.map ~f:(fun pair -> to_tuple pair) ks
        in
        let make_keys_expression const_keys =
          let tuples = keys_to_tuples const_keys in
          E.build_list ~elems:tuples
        in
        let keys_expr =
          const_or_var_storage storage_requests parameter_requests keys
            ~make_expression:make_keys_expression
            ~vt:Variable.Type.Weighted_key_list
        in
        add_parameter_request !storage_requests parameter_requests
          (Parameters.make ~field:"signatures" ~vtype:Signature_option_list);
        add_storage_request storage_requests !parameter_requests
          (Storage.build counter Nat);
        let counter_expr = E.(storage ~field:counter ()) in
        let check_threshold =
          let param_sigs = E.(parameter ~field:"signatures" ()) in
          let param_sig = "param_sig" in
          let param_sig_var = E.variable param_sig in
          let total = "total" in
          let local_total =
            I.define_local total (E.constant (L.nat_of_z (Z.of_int 0))) true
          in
          let total_expr = E.local total in
          let current_wt_key = "current_wt_key" in
          let current_wt_key_var = E.variable current_wt_key in
          let bytes_to_sign =
            match payload with
            | Some pl ->
                E.(
                  pack
                    E.(
                      tuple
                        [
                          E.chain_id;
                          tuple [ self_address; tuple [ counter_expr; pl ] ];
                        ]))
            | None ->
                E.(
                  pack
                    E.(
                      tuple [ E.chain_id; tuple [ self_address; counter_expr ] ]))
          in
          let inner_body_cmd =
            I.if_then_else
              E.(
                not_eq param_sig_var none
                &&& check_signature
                      (second current_wt_key_var)
                      (open_some param_sig_var) bytes_to_sign)
              I.(
                update_variable ~field_exp:total_expr
                  ~val_exp:E.(add total_expr (first current_wt_key_var)))
              (* TODO: would like to get rid of these unnecessary update_variables in various *)
              (* else clauses - need to find a no-op that typechecks *)
              I.(
                update_variable ~field_exp:total_expr
                  ~val_exp:(E.variable total))
          in
          let for_body_cmd =
            (* inner for loop *)
            I.for_loop param_sig param_sigs inner_body_cmd
          in
          let for_cmd = I.for_loop current_wt_key keys_expr for_body_cmd in
          let update_counter =
            I.if_then_else
              E.(threshold_expr <== total_expr)
              I.(
                update_variable ~field_exp:counter_expr
                  ~val_exp:
                    E.(add counter_expr (constant (L.nat_of_z (Z.of_int 1)))))
              I.(
                update_variable ~field_exp:counter_expr
                  ~val_exp:
                    E.(add counter_expr (constant (L.nat_of_z (Z.of_int 0)))))
          in
          let check_total =
            I.verify E.(threshold_expr <== total_expr) ~failwith:total_expr
          in
          I.seq [ local_total; for_cmd; update_counter; check_total ]
        in
        ([], Some check_threshold)
    | And l ->
        List.fold_right ~init:([], None) l ~f:(fun b (acc, _) ->
            let exps, cmd = to_check_expr_and_command b in
            match cmd with
            | None -> (exps @ acc, None)
            | Some _ ->
                let err_str =
                  "Weighted multisig cannot be combined with 'and' expressions"
                in
                Failure.raise_textf "%s" err_str)
    | Or l ->
        let (exprss, _) : Expr.t list list * Command.t option =
          match l with
          | [] -> ([ [ E.constant (L.bool false) ] ], None)
          | exp :: exps ->
              let initial =
                let first_exps, first_c = to_check_expr_and_command exp in
                match first_c with
                | None -> ([ first_exps ], None)
                | Some _ ->
                    let err_str =
                      "Weighted multisig cannot be combined with 'and' \
                       expressions"
                    in
                    Failure.raise_textf "%s" err_str
              in
              List.fold_right ~init:initial exps ~f:(fun b (acc, _) ->
                  let es, c = to_check_expr_and_command b in
                  match c with
                  | None -> (es :: acc, None)
                  | Some _ ->
                      let err_str =
                        "Weighted multisig cannot be combined with 'and' \
                         expressions"
                      in
                      Failure.raise_textf "%s" err_str)
        in
        let expr =
          match exprss with
          | [] -> [ E.constant (L.bool false) ]
          | one :: more ->
              [
                List.fold ~init:(E.and_l one) more ~f:(fun a b ->
                    E.(a ||| and_l b));
              ]
        in
        (expr, None)
  in
  let exps, cmd = to_check_expr_and_command control in
  let verified = List.map exps ~f:I.verify in
  match cmd with Some c -> verified @ [ c ] | None -> verified

let make_contract entrypoints =
  let ep_compare { Entrypoint.name = n1; _ } { Entrypoint.name = n2; _ } =
    String.compare n1 n2
  in
  let sorted_eps = List.rev (List.sort entrypoints ~compare:ep_compare) in
  let compile_contract sorted_eps (reqs : Storage.t list) =
    let storage_record =
      List.map reqs ~f:(fun req -> (req.field, req.typ, req.value_expr))
    in
    Compile.contract sorted_eps ~storage_record
  in
  let storage_requests = ref [] in
  let entrypts =
    List.map sorted_eps ~f:(fun ({ Entrypoint.name; control; ability } as ep) ->
        let parameter_requests = ref [] in
        let param_req_cmp : Parameters.t -> Parameters.t -> int =
         fun x y -> String.compare x.field y.field
        in
        let do_action =
          process_action storage_requests parameter_requests control ability
        in
        (* Sorting the params for do_check makes weighted multisig easier to understand  *)
        parameter_requests :=
          List.sort !parameter_requests ~compare:param_req_cmp;
        let do_check =
          process_check storage_requests parameter_requests control
        in
        let sorted_params =
          List.sort !parameter_requests ~compare:param_req_cmp
        in
        let parameter =
          match sorted_params with
          | [] -> T.unit
          | more ->
              let assoc =
                List.map more ~f:(fun req -> (req.field, req.vtype))
              in
              let zz =
                T.comb_record (List.Assoc.map assoc ~f:compile_variable_type)
              in
              zz
        in
        let new_entrypoint =
          (* Note: do_action must happen before check_parameters *)
          check_parameters sorted_params ep;
          Construct.entry_point name ~parameter (do_check @ do_action)
        in
        new_entrypoint)
  in
  let storage_req_cmp : Storage.t -> Storage.t -> int =
   fun x y -> String.compare x.field y.field
  in
  let sorted_storage = List.sort !storage_requests ~compare:storage_req_cmp in
  compile_contract entrypts sorted_storage

let _valid_examples () =
  let alice_pkh = "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" in
  let all = ref [] in
  let add name t = all := (name, t) :: !all in
  let open Ability in
  let open Control in
  let open Entrypoint in
  let open Constant_or_variable in
  let default_transfer = make "default" ~control:anyone ~ability:do_nothing in
  add "dead-end" [ default_transfer ];
  add "double-dead-end"
    [ default_transfer; make "redefault" ~control:anyone ~ability:do_nothing ];
  add "boomerang-with-delegation"
    [
      make "default" ~control:anyone ~ability:boomerang;
      make "set_delegate"
        ~control:(sender_is (constant alice_pkh))
        ~ability:set_delegate;
    ];
  add "admin-transfer-with-limit"
    [
      default_transfer;
      make "admin_transfer"
        ~control:(sender_is (variable "administrator"))
        ~ability:
          (transfer_mutez
             ~amount:(`Limit (constant (Z.of_int 42_042_042)))
             ~destination:`Anyone);
    ];
  List.rev !all
