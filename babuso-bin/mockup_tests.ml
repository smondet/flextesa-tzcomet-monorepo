open! Import

module Another_heavy_shell = struct
  let command ctxt argv =
    let tmpout = Path.temp_file "heavy" "-out.txt" in
    let tmperr = Path.temp_file "heavy" "-err.txt" in
    let command =
      Fmt.str "%s > %s 2> %s"
        (String.concat ~sep:" " (List.map ~f:Path.quote argv))
        (Path.quote tmpout) (Path.quote tmperr)
    in
    (* Caml.Format.eprintf "%s\n" command ; *)
    let succeeds =
      try
        let should_be_empty = Io.Raw.command_to_string_list command in
        assert (List.is_empty should_be_empty);
        true
      with _ -> false
    in
    let out = lazy (Io.File.read_lines ctxt tmpout) in
    let err = lazy (Io.File.read_lines ctxt tmperr) in
    object
      method success = succeeds
      method command = command
      method outfile = tmpout
      method errfile = tmperr
      method out = Lazy.force out
      method err = Lazy.force err
    end
end

module Mockup_client = struct
  type t = {
    octez_client : string option;
    base_dir : string;
    mutable inits : int; [@default 0]
    mutable originations : int; [@default 0]
    mutable calls : int; [@default 0]
  }
  [@@deriving sexp, make, fields]

  let get ctxt : t = ctxt#mockup_client

  let reset_stats ctxt =
    let self = get ctxt in
    self.inits <- 0;
    self.originations <- 0;
    self.calls <- 0;
    ()

  let command ctxt args =
    let self = get ctxt in
    let exec = Option.value self.octez_client ~default:"octez-client" in
    let res =
      let argv =
        exec :: "--base-dir" :: self.base_dir :: "--mode" :: "mockup" :: args
      in
      Another_heavy_shell.command ctxt argv
    in
    res

  let succeed ctxt args =
    let res = command ctxt args in
    if res#success then res
    else
      Failure.raise
        Docpp.(
          textf "Mockup command failed:"
          +++ itemize
                [
                  text "Command:"
                  +++ Fmt.kstr text "<mockup> %a"
                        Fmt.(list ~sep:sp Dump.string)
                        args;
                  text "STDERR-File:" +++ verbatim res#errfile;
                  text "STDOUT-File:" +++ verbatim res#outfile;
                ]
          +++ verbatim_lines
                ~on_line:(Fmt.kstr verbatim "ERR> %s")
                (List.take (List.rev res#err) 10 |> List.rev))

  let succeed_unit ctxt args =
    let (_ : < .. >) = succeed ctxt args in
    ()

  let init ctxt =
    let self = get ctxt in
    let (_ : string list) =
      Fmt.kstr Io.Raw.command_to_string_list "rm -fr %s"
        (Path.quote self.base_dir)
    in
    self.inits <- self.inits + 1;
    succeed_unit ctxt
      [
        "--protocol";
        "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK";
        "create";
        "mockup";
      ]

  let address_of_contract_name ctxt name =
    let res = succeed ctxt [ "show"; "known"; "contract"; name ] in
    String.concat ~sep:"" res#out

  let delegate_of_contract ctxt addr =
    let cmd = succeed ctxt [ "get"; "delegate"; "for"; addr ] in
    match cmd#out with
    | [ "none" ] -> None
    | [ some ] ->
        Some
          (Option.value_exn ~message:"delegate_of_contract-some"
             (List.hd (String.split ~on:' ' some)))
    | _ -> assert false

  let tez_balance_of_contract ctxt addr =
    let cmd = succeed ctxt [ "get"; "balance"; "for"; addr ] in
    match cmd#out with
    | [ some ] ->
        Float.of_string
          (Option.value_exn ~message:"tez_balance_of_contract-some"
             (List.hd (String.split ~on:' ' some)))
    | _ -> assert false

  let opt_to_arg cmd_opt arg =
    (function None -> [] | Some s -> [ cmd_opt; s ]) arg

  let originate ?(amount = "0") ?(sender = "bootstrap1") ?init ctxt ~name ~path
      =
    let self = get ctxt in
    self.originations <- self.originations + 1;
    succeed_unit ctxt
      ([
         "originate";
         "contract";
         name;
         "transferring";
         amount;
         "from";
         sender;
         "running";
         path;
       ]
      @ opt_to_arg "--init" init @ [ "--burn-cap"; "10" ])

  let call ?(amount = "0") ?entrypoint ?arg ?burn_cap ctxt ~caller ~contract =
    let self = get ctxt in
    self.calls <- self.calls + 1;
    let argv =
      [ "transfer"; amount; "from"; caller; "to"; contract ]
      @ opt_to_arg "--entrypoint" entrypoint
      @ opt_to_arg "--arg" arg
      @ opt_to_arg "--burn-cap" burn_cap
    in
    succeed_unit ctxt argv

  let contract_entrypoint ~entrypoint ctxt ~contract =
    let self = get ctxt in
    self.calls <- self.calls + 1;
    let argv =
      [
        "get";
        "contract";
        "entrypoint";
        "type";
        "of";
        entrypoint;
        "for";
        contract (*; "--normalize-types"*);
      ]
    in
    let res = succeed ctxt argv in
    String.concat ~sep:"" res#out

  let run_script ?(amount = 0) ?(balance = 0) ?(now = 1) ?entrypoint ctxt
      ~contract ~caller ~storage ~params =
    let self = get ctxt in
    self.calls <- self.calls + 1;
    let argv =
      [
        "run";
        "script";
        contract;
        "on";
        "storage";
        storage;
        "and";
        "input";
        params;
        "--now";
        Int.to_string now;
        "--amount";
        Int.to_string amount;
        "--balance";
        Int.to_string balance;
        "--source";
        caller;
      ]
      (* ; "--payer"; addr *)
      (* ; "--self-address"; addr  *)
      @ opt_to_arg "--entrypoint" entrypoint
    in
    succeed_unit ctxt argv

  let account_of_seed seed =
    let open Flextesa.Tezos_protocol.Account in
    let flx = of_name seed in
    object
      method name = seed
      method pkh = pubkey_hash flx
      method pk = pubkey flx
      method sk = private_key flx
    end

  let typecheck ctxt ~path = succeed_unit ctxt [ "typecheck"; "script"; path ]
end

module Scenario = struct
  type ctxt =
    < root : string
    ; file_io : Io.File.file_io
    ; mockup_client : Mockup_client.t >

  type t = { name : string; run : ctxt -> unit } [@@deriving fields]

  let root ctxt : string = ctxt#root

  let assert_failure _ctxt msg f =
    let should_fail =
      try
        f ();
        true
      with _ -> false
    in
    if should_fail then
      Failure.raise
        Docpp.(textf "Function %S was expected to fail but didn't." msg)

  let annotate _ctxt ppdoc f =
    try f () with
    | Failure.F { code; doc } ->
        Failure.raise ~code Docpp.(ppdoc ++ newline ++ doc)
    | e -> Failure.raise Docpp.(ppdoc ++ newline ++ verbatim (Exn.to_string e))

  let assert_condition _ctxt msg = function
    | true -> ()
    | false -> Failure.raise Docpp.(text "Assertion failed:" +++ verbatim msg)

  let assert_equal ctxt msg fmt a b =
    assert_condition ctxt
      (Fmt.str "%s ( %a == %a )" msg fmt a fmt b)
      (String.equal (Fmt.str "%a" fmt a) (Fmt.str "%a" fmt b))

  module All = struct
    let _v : t list ref = ref []
    let add t = _v := t :: !_v
    let names _ = List.rev_map !_v ~f:name

    let run ?(filter = fun _ -> true) ctxt ~root =
      let all = List.rev !_v in
      let results =
        let name_column =
          List.fold ~init:0 (names ctxt) ~f:(fun m s -> max m (String.length s))
        in
        List.filter_mapi all ~f:(fun idx -> function
          | scen when not (filter scen) -> None
          | { name; run } -> (
              Fmt.epr "|%s%s|%!" name
                (String.make (name_column - String.length name) '.');
              let start = Unix.gettimeofday () in
              Mockup_client.reset_stats ctxt;
              let stats () =
                let mc = Mockup_client.get ctxt in
                let pf f =
                  let l, r =
                    let p = Float.modf f in
                    Float.Parts.(integral p, fractional p)
                  in
                  Fmt.str "% 3d.%03d" (Float.to_int l)
                    Float.(r * 1000. |> to_int)
                in
                Fmt.str "t: %ss|O: %- 2d|T: %- 3d |"
                  (Unix.gettimeofday () -. start |> pf)
                  (Mockup_client.originations mc)
                  (Mockup_client.calls mc)
              in
              let ctxt =
                object
                  method root = Path.(root // Fmt.str "%03d-%s" idx name)
                  method file_io = ctxt#file_io
                  method mockup_client = ctxt#mockup_client
                end
              in
              try
                run ctxt;
                Fmt.epr "%s OK \\o/\n%!" (stats ());
                None
              with e ->
                Fmt.epr "%s FAILED :(\n%!" (stats ());
                Some (name, e)))
      in
      match results with
      | [] -> ()
      | more ->
          Fmt.epr "\n# Some tests failed:\n%!";
          List.iter more ~f:(fun (name, exn) ->
              Fmt.epr "## Test `%s` Failed\n\n%!" name;
              Fmt.epr "@[%a@]\n\n%!" Exn.pp exn);
          Caml.exit 7
  end

  let add name run = All.add { name; run }
end

module Signing = struct
  open Babuso_lib.Wallet_data

  let hash_data ctxt ~data_to_hash ~of_type =
    let res =
      Mockup_client.succeed ctxt
        [ "hash"; "data"; data_to_hash; "of"; "type"; of_type ]
    in
    let res_out = List.hd_exn res#out in
    let cleaned =
      match String.chop_prefix res_out ~prefix:"Raw packed data: " with
      | Some s -> s
      | None -> res_out
    in
    cleaned

  let get_chain_id ctxt =
    let open Tezai_base58_digest.Identifier in
    let lines =
      Mockup_client.succeed ctxt [ "rpc"; "get"; "/chains/main/chain_id" ]
    in
    let res = String.concat ~sep:"" lines#out in
    let result = String.strip ~drop:(fun ch -> phys_equal ch '"') res in
    let decoded = Chain_id.decode result in
    "0x" ^ Hex.show (Hex.of_string decoded)

  let weighted_multisig_payload : Variable.Value.t list -> string =
   fun vals ->
    let strs =
      List.fold_right vals ~init:[] ~f:(fun v acc ->
          let m = Variable.Value.to_michelson v in
          Michelson.to_concrete m :: acc)
    in
    String.concat ~sep:"; " strs

  let pour_concrete param_types =
    let type_strs =
      List.map param_types ~f:(fun pt ->
          Variable.Type.to_michelson pt |> Michelson.to_concrete)
    in
    let type_str = String.concat ~sep:" " type_strs in
    Fmt.str "(pair chain_id address nat %s)" type_str

  let weighted_multisig_data ~chain_id ~contract_addr ~counter ~payload =
    if String.is_empty payload then
      Fmt.str "{%s; %S; %d}" chain_id contract_addr (Z.to_int counter)
    else
      Fmt.str "{%s; %S; %d; %s}" chain_id contract_addr (Z.to_int counter)
        payload

  let sign_bytes ctxt bytes account =
    let res =
      Mockup_client.succeed ctxt [ "sign"; "bytes"; bytes; "for"; account ]
    in
    match List.map ~f:(String.split ~on:' ') res#out with
    | [ [ "Signature:"; b58 ] ] -> b58
    | _ ->
        Failure.raise
          Docpp.(
            text "Mockup_client - Failure signing bytes"
            +++ itemize
                  [
                    text "Bytes:" +++ verbatim bytes;
                    text "Account:" +++ verbatim account;
                    text "Output:" +++ verbatim_lines res#out;
                  ])

  let sign_multisig :
      Scenario.ctxt ->
      < sk : string ; .. > ->
      params:(Variable.Type.t * Variable.Value.t) list ->
      string ->
      test_counter:Z.t ->
      string =
   fun ctxt signer ~params contract_addr ~test_counter ->
    let payload =
      weighted_multisig_payload (List.map params ~f:(fun pair -> snd pair))
    in
    let chain_id = get_chain_id ctxt in
    let data_to_hash =
      weighted_multisig_data ~chain_id ~contract_addr ~counter:test_counter
        ~payload
    in
    let concrete = pour_concrete (List.map params ~f:(fun pair -> fst pair)) in
    let bytes = hash_data ctxt ~data_to_hash ~of_type:concrete in
    sign_bytes ctxt bytes signer#sk
end

open Babuso_lib.Wallet_data

module Contracts = struct
  let as_file ctxt ~code =
    let root = Scenario.root ctxt in
    let dir = Path.(root // "michelson-contracts") in
    let path =
      Path.(dir // Fmt.str "%s.tz" Caml.Digest.(string code |> to_hex))
    in
    Io.File.make_path ctxt dir;
    Io.File.write_lines ctxt path [ code ];
    path

  let compile _ eps =
    let `Code mich, `Storage_fields_order _ = Law_office.make_contract eps in
    mich

  let compile_to_file ctxt eps = as_file ctxt ~code:(compile ctxt eps)
end

module Tests = struct
  let default_transfer : Entrypoint.t =
    Entrypoint.make "default" ~control:Control.anyone
      ~ability:Ability.do_nothing

  let _test_the_test =
    match Caml.Sys.getenv "test_the_mockup_test" with
    | "true" | "on" | "yes" ->
        Scenario.add "test-the-mockup-tests-by-failing" (fun ctxt ->
            Scenario.annotate ctxt
              Docpp.(text "Calling a contract that does not actually exist!")
              (fun () ->
                Mockup_client.call ctxt ~caller:"bootstrap1"
                  ~contract:"KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton"));
        Scenario.add "test-the-mockup-tests-by-failing-to-fail" (fun ctxt ->
            Scenario.assert_failure ctxt "doing-nothing" (fun () -> ()));
        ()
    | _ | (exception _) -> ()

  let _dead_end =
    Scenario.add "dead-end" @@ fun ctxt ->
    let path = Contracts.compile_to_file ctxt [ default_transfer ] in
    Mockup_client.typecheck ctxt ~path;
    let name = "dead-end-0" in
    Mockup_client.originate ~name ~path ctxt;
    (* let origination =
       test_operations mockup_client
         [`Succ ("Originate dead-end", originate_args ~name ~path ())] in *)
    let addr = Mockup_client.address_of_contract_name ctxt name in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr;
    Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr;
    ()

  let _double_dead_end_redefault =
    Scenario.add "double-dead-end" @@ fun ctxt ->
    let redefault = "redefault" in
    let ep =
      [
        default_transfer;
        Entrypoint.make redefault ~control:Control.anyone
          ~ability:Ability.do_nothing;
      ]
    in
    let path = Contracts.compile_to_file ctxt ep in
    Mockup_client.typecheck ctxt ~path;
    let name = "double-dead-end-0" in
    Mockup_client.originate ~name ~path ctxt;
    let addr = Mockup_client.address_of_contract_name ctxt name in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr;
    Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
      ~entrypoint:redefault;
    ()

  let boot addr = (addr, Constant_or_variable.constant addr)
  let boot1, b1 = boot "tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
  let boot2, b2 = boot "tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN"
  let boot3, b3 = boot "tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU"
  let boot4, b4 = boot "tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv"
  let boot5, b5 = boot "tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv"

  let _boomerang_with_delegation =
    let name = "boomerang-with-delegation" in
    Scenario.add name @@ fun ctxt ->
    let set_del = "set_delegate" in
    let path =
      Contracts.compile_to_file ctxt
        [
          Entrypoint.make "default" ~control:Control.anyone
            ~ability:Ability.boomerang;
          Entrypoint.make set_del
            ~control:(Control.sender_is (Constant_or_variable.constant boot1))
            ~ability:Ability.set_delegate;
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    Mockup_client.originate ~name ~path ctxt;
    let addr = Mockup_client.address_of_contract_name ctxt name in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr ~amount:"10";
    Scenario.assert_condition ctxt "no delegate at first"
      (Option.is_none @@ Mockup_client.delegate_of_contract ctxt addr);
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:set_del ~arg:(Fmt.str "Some %S" boot2);
    Scenario.assert_equal ctxt "delegate is now boot2"
      Fmt.Dump.(option string)
      (Some boot2)
      (Mockup_client.delegate_of_contract ctxt addr);
    Scenario.assert_failure ctxt "Call-set-delegate-from-non-admin" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:set_del ~arg:"None");
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:set_del ~arg:"None";
    Scenario.assert_condition ctxt "delegate is back to the None"
      (Option.is_none @@ Mockup_client.delegate_of_contract ctxt addr);
    ()

  let _admin_transfer_with_limit =
    let name = "admin-transfer-with-limit" in
    Scenario.add name @@ fun ctxt ->
    let admin_transfer = "admin_transfer" in
    let spending_limit = 42_042_042 in
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make admin_transfer
            ~control:
              (Control.sender_is
                 (Constant_or_variable.variable "administrator"))
            ~ability:
              Ability.(
                transfer_mutez
                  ~amount:
                    (`Limit
                      (Constant_or_variable.constant (Z.of_int spending_limit)))
                  ~destination:`Anyone);
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    let init =
      (* bootstrap1 address *)
      Fmt.str "(Pair %S Unit)" boot1
    in
    Mockup_client.originate ~name ~path ctxt ~init ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    let check_bal msg bal =
      Scenario.assert_equal ctxt msg Fmt.float bal
        (Mockup_client.tez_balance_of_contract ctxt addr)
    in
    check_bal "initial balanace" 100.;
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr ~amount:"10";
    check_bal "b1 adds 10" 110.;
    let params1 =
      (* bootstrap2 address and mutez ammount equal to the transfer limit. *)
      Fmt.str "(Pair %d %S)" spending_limit boot2
    in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:admin_transfer ~arg:params1;
    check_bal "b1 sends spending-limit"
      (110. -. (Float.of_int spending_limit /. 1_000_000.));
    Scenario.assert_failure ctxt "admin-transfer-from-bootstrap2" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:admin_transfer ~arg:params1);
    check_bal "b2 does not change balance"
      (110. -. (Float.of_int spending_limit /. 1_000_000.));
    let params2 =
      (* bootstrap2 address and mutez ammount exceeding the transfer limit *)
      (* Fmt.str "(Pair %S %d)" boot2 (spending_limit + 1) in *)
      Fmt.str "(Pair %d %S)" (spending_limit + 1) boot2
    in
    Scenario.assert_failure ctxt "admin-transfer-exceeding-limit" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
          ~entrypoint:admin_transfer ~arg:params2);
    check_bal "b1 > limit does not change balance"
      (110. -. (Float.of_int spending_limit /. 1_000_000.));
    ()

  (* A test isolating a single 'or' condition *)
  (* This test is repeated in the more complicated test that *)
  (* follows (as "e02-b1_or_b2-called-by-bootstrap3-ko") *)
  let _regresion_or =
    let name = "regression_or" in
    let burn_cap = 10 in
    Scenario.add name @@ fun ctxt ->
    let or_transfer = "or_transfer" in
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make or_transfer
            ~control:
              Control.(
                or_
                  [
                    sender_is (Constant_or_variable.constant boot1);
                    sender_is (Constant_or_variable.constant boot2);
                  ])
            ~ability:
              Ability.(transfer_mutez ~amount:`No_limit ~destination:`Anyone);
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    Mockup_client.originate ~name ~path ctxt ~init:"Unit" ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    let params1 = Fmt.str "(Pair %d %S)" 10 boot2 in
    Scenario.assert_failure ctxt "can't call b1-or-b2 from b3" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap3" ~contract:addr
          ~entrypoint:or_transfer ~arg:params1
          ~burn_cap:(Int.to_string burn_cap));
    ()

  let _ands_and_ors =
    let name = "ands-and-ors-for-sender" in
    Scenario.add name @@ fun ctxt ->
    let eps = ref [] in
    let meta_ep name control tests = eps := (name, control, tests) :: !eps in
    let ok_from b = `From (`Ok, b) in
    let ok_from_b n = Fmt.kstr ok_from "bootstrap%d" n in
    let ko_from b = `From (`Ko, b) in
    let ko_from_b n = Fmt.kstr ko_from "bootstrap%d" n in
    meta_ep "one_sender" Control.(sender_is b1) [ ok_from_b 1; ko_from_b 2 ];
    meta_ep "and_3_same_sender"
      Control.(and_ [ sender_is b1; sender_is b1; sender_is b1 ])
      [ ok_from_b 1; ko_from_b 2 ];
    meta_ep "b1_or_b2"
      Control.(or_ [ sender_is b1; sender_is b2 ])
      [ ok_from_b 1; ok_from_b 2; ko_from_b 3; ko_from_b 4 ];
    meta_ep "b1_or_b2_or_b3"
      Control.(or_ [ sender_is b1; sender_is b2; sender_is b3 ])
      [ ok_from_b 1; ok_from_b 2; ok_from_b 3; ko_from_b 4; ko_from_b 5 ];
    meta_ep "b1_or_b2_and_b2_or_b3"
      Control.(
        and_
          [
            or_ [ sender_is b1; sender_is b2 ];
            or_ [ sender_is b2; sender_is b3 ];
          ])
      [ ko_from_b 1; ok_from_b 2; ko_from_b 3; ko_from_b 4; ko_from_b 5 ];
    meta_ep "b1_or_b2_and_b2_or_b3_and_b2_or_b4"
      Control.(
        and_
          [
            or_ [ sender_is b1; sender_is b2 ];
            or_ [ sender_is b2; sender_is b3 ];
            or_ [ sender_is b2; sender_is b4 ];
          ])
      [ ko_from_b 1; ok_from_b 2; ko_from_b 3; ko_from_b 4; ko_from_b 5 ];
    meta_ep "or_of_many_ands"
      Control.(
        or_
          [
            and_ [ sender_is b1; sender_is b2 ];
            and_ [ sender_is b2; sender_is b3 ];
            and_ [ sender_is b2; sender_is b4 ];
          ])
      [ ko_from_b 1; ko_from_b 2; ko_from_b 3; ko_from_b 4; ko_from_b 5 ];
    meta_ep "or_of_other_ands"
      Control.(
        or_
          [
            and_ [ sender_is b1; sender_is b1 ];
            and_ [ sender_is b2 ];
            and_ [ sender_is b2; sender_is b4 ];
            and_ [ sender_is b5 ];
          ])
      [ ok_from_b 1; ok_from_b 2; ko_from_b 3; ko_from_b 4; ok_from_b 5 ];
    meta_ep "and_of_or_of_and"
      Control.(
        and_
          [
            or_ [ sender_is b2; and_ [ sender_is b1; sender_is b1 ] ];
            sender_is b1;
          ])
      [ ok_from_b 1; ko_from_b 2; ko_from_b 3 ];
    let path =
      let make_ep name control =
        Entrypoint.make name ~control
          ~ability:
            Ability.(transfer_mutez ~amount:`No_limit ~destination:`Anyone)
      in
      Contracts.compile_to_file ctxt
        (List.mapi (List.rev !eps) ~f:(fun ith (_, control, _) ->
             (* Initially was using the `name` field but quickly went
                above the 31 characters limit. *)
             (* dbgf "%s %d" n ith; *)
             make_ep (Fmt.str "e%02d" ith) control))
    in
    Mockup_client.originate ~name ~path ctxt ~init:"Unit" ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    List.iteri (List.rev !eps) ~f:(fun ith (name, _, tests) ->
        List.iter tests ~f:(function `From (expect, caller) ->
            (let call () =
               Mockup_client.call ctxt ~caller ~contract:addr ~amount:"0"
                 ~entrypoint:(Fmt.str "e%02d" ith)
                   (* ~arg:(Fmt.str "(Pair %S 1)" boot1) in *)
                 ~arg:(Fmt.str "(Pair 1 %S)" boot1)
             in
             let annotation =
               Fmt.str "e%02d-%s-called-by-%s-%s" ith name caller
                 (match expect with `Ok -> "ok" | `Ko -> "ko")
             in
             match expect with
             | `Ok -> Scenario.annotate ctxt Docpp.(text annotation) call
             | `Ko -> Scenario.assert_failure ctxt annotation call)));
    ()

  let _two_entrypts_one_var =
    let name = "two-entrypoints-one-variable" in
    let admin = "administrator" in
    let set_del = "set_delegate" in
    let admin_transfer = "admin_transfer" in
    let spending_limit = 42_042_042 in
    let ten_tz = 10000000 in
    Scenario.add name @@ fun ctxt ->
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make set_del
            ~control:(Control.sender_is (Constant_or_variable.variable admin))
            ~ability:Ability.set_delegate;
          Entrypoint.make admin_transfer
            ~control:(Control.sender_is (Constant_or_variable.variable admin))
            ~ability:
              Ability.(
                transfer_mutez
                  ~amount:
                    (`Limit
                      (Constant_or_variable.constant (Z.of_int spending_limit)))
                  ~destination:`Anyone);
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    let init =
      (* bootstrap1 address == "Administrator" *)
      Fmt.str "(Pair %S Unit)" boot1
    in
    Mockup_client.originate ~name ~path ~init ctxt ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    Scenario.assert_condition ctxt "no delegate at first"
      (Option.is_none @@ Mockup_client.delegate_of_contract ctxt addr);
    (* boostrap 2 cannot set delegate *)
    Scenario.assert_failure ctxt "Call-set-delegate-from-non-admin" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:set_del ~arg:(Fmt.str "Some %S" boot3));
    (* bootstrap 1 can set delegate *)
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:set_del ~arg:(Fmt.str "Some %S" boot3);
    Scenario.assert_equal ctxt "delegate is now boot3"
      Fmt.Dump.(option string)
      (Some boot3)
      (Mockup_client.delegate_of_contract ctxt addr);
    let check_bal msg bal =
      Scenario.assert_equal ctxt msg Fmt.float bal
        (Mockup_client.tez_balance_of_contract ctxt addr)
    in
    check_bal "initial balanace" 100.;
    (* boostrap 2 cannot transfer (bootstrap2 != admin) *)
    let transfer_params1 = Fmt.str "(Pair %d %S)" ten_tz boot3 in
    Scenario.assert_failure ctxt "non-admin-transfer" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:admin_transfer ~arg:transfer_params1);
    check_bal "transfer by b2, should not change balance" 100.;
    (* bootstrap 2 can transfer (bootstrap1 == admin) *)
    let transfer_params2 = Fmt.str "(Pair %d %S)" ten_tz boot3 in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:admin_transfer ~arg:transfer_params2;
    check_bal "transfer by b1 changes balance" (100. -. 10.);
    ()

  (* (\* Use the same variable for the Sender_is control and *\) *)
  (* (\* for the destination of a transfer *\) *)
  let _two_purpose_one_var =
    let name = "two-purpose-one-variable" in
    let admin = "administrator" in
    let from_admin = "transfer_from_admin_only" in
    let to_address = "transfer_to_address" in
    let spending_limit = 42_042_042 in
    let ten_tz = 10000000 in
    Scenario.add name @@ fun ctxt ->
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make from_admin
            ~control:(Control.sender_is (Constant_or_variable.variable admin))
            ~ability:
              Ability.(
                transfer_mutez
                  ~amount:
                    (`Limit
                      (Constant_or_variable.constant (Z.of_int spending_limit)))
                  ~destination:`Anyone);
          Entrypoint.make to_address ~control:Control.Anyone
            ~ability:
              Ability.(
                transfer_mutez
                  ~amount:
                    (`Limit
                      (Constant_or_variable.constant (Z.of_int spending_limit)))
                  ~destination:(`Address (Constant_or_variable.variable admin)));
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    let init =
      (* bootstrap1 address == "Administrator" *)
      Fmt.str "(Pair %S Unit)" boot1
    in
    Mockup_client.originate ~name ~path ~init ctxt ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    let check_bal msg bal =
      Scenario.assert_equal ctxt msg Fmt.float bal
        (Mockup_client.tez_balance_of_contract ctxt addr)
    in
    check_bal "initial balanace" 100.;
    let transfer_params1 = Fmt.str "(Pair %d %S)" ten_tz boot3 in
    (* b2 cannot send via from_admin *)
    Scenario.assert_failure ctxt "non admin transfer" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:from_admin ~arg:transfer_params1);
    check_bal "transfer by b2, should not change balance" 100.;
    (* b1 can send to b3 via from_admin *)
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:from_admin ~arg:transfer_params1;
    check_bal "transfer by b1 changes balance" (100. -. 10.);
    (* b1 can receive Tez via the admin variable using to_address *)
    let b1_balance = Mockup_client.tez_balance_of_contract ctxt boot1 in
    let transfer_params2 = Fmt.str "%d" ten_tz in
    Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
      ~entrypoint:to_address ~arg:transfer_params2;
    let b1_new_balance = Mockup_client.tez_balance_of_contract ctxt boot1 in
    Scenario.assert_equal ctxt "transfer adds to b1 balance" Fmt.float
      b1_new_balance (b1_balance +. 10.);
    ()

  (* The 'admin' variable is shared between: *)
  (* 'change_admin', 'admin_transfer, and 'to_admin' *)
  (* The admin variable is changed during the test *)
  let _shared_var_with_updates =
    let open Babuso_lib.Wallet_data.Variable in
    let name = "shared-var-with-updates" in
    let admin = "admin" in
    let change_admin = "change_admin" in
    let admin_transfer = "admin_transfer" in
    let to_admin = "to_admin" in
    let spending_limit = 42_042_042 in
    Scenario.add name @@ fun ctxt ->
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make change_admin
            ~control:(Control.sender_is (Constant_or_variable.variable admin))
            ~ability:(Update_variables [ (admin, Type.Address) ]);
          Entrypoint.make admin_transfer
            ~control:(Control.sender_is (Constant_or_variable.variable admin))
            ~ability:
              Ability.(
                transfer_mutez
                  ~amount:
                    (`Limit
                      (Constant_or_variable.Constant (Z.of_int spending_limit)))
                  ~destination:`Anyone);
          Entrypoint.make to_admin ~control:Control.Anyone
            ~ability:
              Ability.(
                transfer_mutez
                  ~amount:
                    (`Limit
                      (Constant_or_variable.Constant (Z.of_int spending_limit)))
                  ~destination:(`Address (Constant_or_variable.variable admin)));
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    let init = Fmt.str "(Pair %S Unit)" boot1 in
    Mockup_client.originate ~name ~path ~init ctxt ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    let check_bal ?(address = addr) msg bal =
      Scenario.assert_equal ctxt msg Fmt.float bal
        (Mockup_client.tez_balance_of_contract ctxt address)
    in
    check_bal "initial balanace" 100.;
    (* B1 (admin) receives Tez from to_admin *)
    let b1_bal = Mockup_client.tez_balance_of_contract ctxt boot1 in
    let params1 = Fmt.str "%d" 1_000_000 in
    Mockup_client.call ctxt ~caller:"bootstrap3" ~contract:addr
      ~entrypoint:to_admin ~arg:params1;
    check_bal "to_admin transfers to b1 (admin)" ~address:boot1 (b1_bal -. 1.);
    (* b2 (non-admin) cannot do admin transfer *)
    let new_bal = Mockup_client.tez_balance_of_contract ctxt addr in
    let params2 = Fmt.str "(Pair %d %S)" 500_000 boot4 in
    Scenario.assert_failure ctxt "non-admin-transfer" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:admin_transfer ~arg:params2);
    check_bal "transfer by b2 (non-admin), should not change balance" new_bal;
    (* b3 (non-admin) also cannot do admin transfer *)
    let params3 = Fmt.str "(Pair %d %S)" 500_000 boot4 in
    Scenario.assert_failure ctxt "non-admin-transfer-2" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap3" ~contract:addr
          ~entrypoint:admin_transfer ~arg:params3);
    check_bal "transfer by b3 (non-admin), should not change balance" new_bal;
    (* b2 (non-admin) cannot change the admin to b3 *)
    (* i.e. b3 can still not do admin transfer *)
    let burn_cap = 10 in
    let params_4 = Fmt.str "%S" boot3 in
    Scenario.assert_failure ctxt "non-admin-var-change" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:change_admin ~arg:params_4
          ~burn_cap:(Int.to_string burn_cap));
    Scenario.assert_failure ctxt "non-admin-transfer-3" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap3" ~contract:addr
          ~entrypoint:admin_transfer ~arg:params3);
    check_bal "transfer by b3 (non-admin), should not change balance" new_bal;
    (* Change the admin to b2 *)
    let params_5 = Fmt.str "%S" boot2 in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:change_admin ~arg:params_5 ~burn_cap:(Int.to_string burn_cap);
    (* b2 now can do admin transfer *)
    let test_amt = 2_000 in
    let params6 = Fmt.str "(Pair %d %S)" test_amt boot4 in
    Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
      ~entrypoint:admin_transfer ~arg:params6;
    check_bal "b2 can now can do admin transfer"
      (new_bal -. (Float.of_int test_amt /. 1_000_000.));
    (* b2 is now the destination of to_admin *)
    let b2_bal = Mockup_client.tez_balance_of_contract ctxt boot2 in
    let to_b2_amt = 500_000 in
    let params7 = Fmt.str "%d" to_b2_amt in
    Mockup_client.call ctxt ~caller:"bootstrap3" ~contract:addr
      ~entrypoint:to_admin ~arg:params7;
    let new_b2_bal = b2_bal -. (Float.of_int to_b2_amt /. 1_000_000.) in
    check_bal "b2 is now the dest. of to_admin" ~address:boot2 new_b2_bal;
    (* b2 can now change the admin to b3  *)
    (* i.e., b3 can now do admin transfer *)
    let params8 = Fmt.str "%S" boot3 in
    Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
      ~entrypoint:change_admin ~arg:params8 ~burn_cap:(Int.to_string burn_cap);
    let cur_bal = Mockup_client.tez_balance_of_contract ctxt addr in
    let params9 = Fmt.str "(Pair %d %S)" test_amt boot4 in
    Mockup_client.call ctxt ~caller:"bootstrap3" ~contract:addr
      ~entrypoint:admin_transfer ~arg:params9;
    let new_bal = cur_bal -. (Float.of_int test_amt /. 1_000_000.) in
    check_bal "b3 can now can do admin transfer" new_bal;
    ()

  (* Exercise the Update_variables ability *)
  let change_variables =
    let open Babuso_lib.Wallet_data.Variable in
    let name = "change-variables" in
    let administrator = "administrator" in
    let user_limit = "user_limit" in
    let change_variables = "change_variables" in
    let admin_transfer = "admin_transfer" in
    let transfer_with_limit = "tranfer_with_limit" in
    let admin_spending_limit = 42_042_042 in
    Scenario.add name @@ fun ctxt ->
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make admin_transfer
            ~control:
              (Control.sender_is (Constant_or_variable.variable administrator))
            ~ability:
              Ability.(
                transfer_mutez
                  ~amount:
                    (`Limit
                      (Constant_or_variable.constant
                         (Z.of_int admin_spending_limit)))
                  ~destination:`Anyone);
          Entrypoint.make change_variables
            ~control:
              (Control.sender_is (Constant_or_variable.variable administrator))
            ~ability:
              (Update_variables
                 [ (administrator, Type.Address); (user_limit, Type.Mutez) ]);
          Entrypoint.make transfer_with_limit ~control:Control.Anyone
            ~ability:
              Ability.(
                transfer_mutez
                  ~amount:(`Limit (Constant_or_variable.variable user_limit))
                  ~destination:`Anyone);
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    let init_user_limit = 1_000_000 in
    let init_admin = boot1 in
    let init =
      (* ["administrator"; "user_limit"] *)
      Fmt.str "(Pair %S %d)" init_admin init_user_limit
    in
    Mockup_client.originate ~name ~path ~init ctxt ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    let check_bal msg bal =
      Scenario.assert_equal ctxt msg Fmt.float bal
        (Mockup_client.tez_balance_of_contract ctxt addr)
    in
    check_bal "initial balanace" 100.;
    (* b2 (non-admin) cannot do admin transfer *)
    let before_params1 = Fmt.str "(Pair %d %S)" 500_000 boot4 in
    Scenario.assert_failure ctxt "non-admin-transfer" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:admin_transfer ~arg:before_params1);
    check_bal "transfer by b2 (non-admin), should not change balance" 100.;
    (* b1 cannot send more than limit via 'transfer_with_limit' *)
    let over_user_limit = 1_100_000 in
    let before_params2 = Fmt.str "(Pair %d %S)" over_user_limit boot4 in
    Scenario.assert_failure ctxt "transfer-exceeding-limit" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
          ~entrypoint:transfer_with_limit ~arg:before_params2);
    check_bal "b1 > limit does not change balance" 100.;
    (* change vars: b2 is now admin, limit is now 'new_user_limit' *)
    let new_admin = boot2 in
    let new_user_limit = 1_500_000 in
    let burn_cap = 10 in
    let update_var_params = Fmt.str "(Pair %S %d)" new_admin new_user_limit in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:change_variables ~arg:update_var_params
      ~burn_cap:(Int.to_string burn_cap);
    let bal2 = 100. -. (Float.of_int burn_cap /. 1_000_000.) in
    check_bal "correct balance after change variables" bal2;
    (* b2 (now the admin) can do admin transfer *)
    let test_amt = 2_000 in
    let after_params1 = Fmt.str "(Pair %d %S)" test_amt boot4 in
    Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
      ~entrypoint:admin_transfer ~arg:after_params1;
    let bal3 = bal2 -. (Float.of_int test_amt /. 1_000_000.) in
    check_bal "b2 is now the admin can do admin transfer" bal3;
    (* b3 can send more than the original limit via 'transfer_with_limit' *)
    let after_params2 = Fmt.str "(Pair %d %S)" over_user_limit boot4 in
    Mockup_client.call ctxt ~caller:"bootstrap3" ~contract:addr
      ~entrypoint:transfer_with_limit ~arg:after_params2;
    check_bal "b3 sends more than original spending-limit"
      (bal3 -. (Float.of_int over_user_limit /. 1_000_000.));
    ()

  let _param_storage_mismatch =
    let open Babuso_lib.Wallet_data.Variable in
    let name = "param-storage-mismatch" in
    let admin = "admin" in
    let admin_limit = "admin_limit" in
    let change_admin_limit = "change_admin" in
    let admin_transfer = "admin_transfer" in
    let _admin_spending_limit = 1_000_000 in
    Scenario.add name @@ fun ctxt ->
    Scenario.assert_failure ctxt "param storage mismatch" (fun () ->
        let _path =
          Contracts.compile_to_file ctxt
            [
              default_transfer;
              Entrypoint.make admin_transfer
                ~control:
                  (Control.sender_is (Constant_or_variable.variable admin))
                ~ability:
                  Ability.(
                    transfer_mutez
                      ~amount:
                        (`Limit (Constant_or_variable.Variable admin_limit))
                      ~destination:`Anyone);
              Entrypoint.make change_admin_limit
                ~control:
                  (Control.sender_is (Constant_or_variable.variable admin))
                  (* Type mismatch: *)
                ~ability:(Update_variables [ (admin_limit, Type.Address) ]);
            ]
        in
        ())

  let _when_inactive =
    let open Babuso_lib.Wallet_data.Variable in
    let name = "when_inactive" in
    let admin_transfer = "admin_transfer" in
    let proxy_admin_change = "proxy_admin_change" in
    let admin = "admin" in
    let admin_proxy = "admin_proxy" in
    let admin_limit = 1_000_000 in
    let one_year = 60 * 60 * 24 * 365 in
    let timespan_val = Z.of_int one_year in
    let last_activity = "last_activity" in
    Scenario.add name @@ fun ctxt ->
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make admin_transfer
            ~control:(Control.sender_is (Constant_or_variable.variable admin))
            ~ability:
              (Ability.Sequence
                 [
                   Ability.(
                     transfer_mutez
                       ~amount:
                         (`Limit
                           (Constant_or_variable.constant (Z.of_int admin_limit)))
                       ~destination:`Anyone);
                   Ability.(reset_activity_timer last_activity);
                 ]);
          Entrypoint.make proxy_admin_change
            ~control:
              (Control.and_
                 [
                   Control.sender_is (Constant_or_variable.variable admin_proxy);
                   Control.when_inactive
                     ~timespan:(Constant_or_variable.constant timespan_val)
                     ~last_activity;
                 ])
            ~ability:(Update_variables [ (admin, Type.Address) ]);
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    let open Tezai_smartml_generation in
    let open! Construct in
    let init = Fmt.str "(Pair %S %S None)" boot1 boot2 in
    let initial_bal = 100. in
    Mockup_client.originate ~name ~path ~init ctxt ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    let check_bal msg bal =
      Scenario.assert_equal ctxt msg Fmt.float bal
        (Mockup_client.tez_balance_of_contract ctxt addr)
    in
    check_bal "initial balanace" initial_bal;
    (* b1 can do admin_transfer *)
    (* This operation resets the inactivity timer *)
    let amt_sent = 500_000 in
    let burn_cap = 10 in
    let params1 = Fmt.str "(Pair %d %S)" amt_sent boot3 in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:admin_transfer ~arg:params1 ~burn_cap:(Int.to_string burn_cap);
    let new_bal = initial_bal -. (Float.of_int amt_sent /. 1_000_000.) in
    check_bal "b1 can do admin transfer" new_bal;
    (* b2 cannot do admin transfer *)
    Scenario.assert_failure ctxt "change-admin-too-early" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:admin_transfer ~arg:params1
          ~burn_cap:(Int.to_string burn_cap));
    check_bal "b1 cannot do admin transfer" new_bal;
    (* Note that for the 'run script' tests below - these are stateless, return *)
    (* balances are hard coded, and so the precise results of transfers are not *)
    (* being verified by these tests  *)
    (* b2 (admin-proxy) cannot do proxy_admin_change before timespan has elapsed *)
    let script_bal = 100 in
    let script_now1 = 1 in
    (* approx inactivity reset time *)
    let script_storage =
      Fmt.str "(Pair  %S %S (Some %d))" boot1 boot2 script_now1
    in
    let script_params = Fmt.str "%S" boot2 in
    Scenario.assert_failure ctxt "change-admin-too-early" (fun () ->
        Mockup_client.run_script ctxt ~caller:boot2 ~contract:path
          ~entrypoint:proxy_admin_change ~storage:script_storage
          ~params:script_params ~balance:script_bal);
    (* simulate waiting for the timespan to pass *)
    (* b2 can now make himself admin via proxy_admin_change *)
    let script_now2 = script_now1 + one_year in
    Mockup_client.run_script ctxt ~caller:boot2 ~contract:path
      ~entrypoint:proxy_admin_change ~storage:script_storage
      ~params:script_params ~now:script_now2 ~balance:script_bal;
    (* b2 can now do admin transfer *)
    (* these two are just arbitrary values *)
    let script_amt_sent = 500_000 in
    let script_bal2 = 95 in
    (* Storage as after proxy_admin_change: *)
    let script_storage2 =
      (* Fmt.str "(Pair (Some %d) (Pair %S %S))" script_now1 boot2 boot2 in *)
      Fmt.str "(Pair %S (Pair %S (Some %d)))" boot2 boot2 script_now1
    in
    let script_params2 = Fmt.str "(Pair %d %S)" script_amt_sent boot3 in
    Mockup_client.run_script ctxt ~caller:boot2 ~contract:path
      ~entrypoint:admin_transfer ~storage:script_storage2 ~params:script_params2
      ~now:script_now2 ~balance:script_bal2;
    ()

  let _weighted_multisig =
    let open Babuso_lib.Wallet_data.Variable in
    let name = "weighted_msig" in
    let admin_transfer = "admin_transfer" in
    let proxy_admin_change = "proxy_admin_change" in
    let signed_transfer = "signed_transfer" in
    let admin = "admin" in
    let admin_limit = 1_000_000 in
    let initial_admin = boot1 in
    let counter_var = "counter" in
    let counter_var_2 = "counter_2" in
    let mark = Mockup_client.account_of_seed "mark" in
    let phil = Mockup_client.account_of_seed "phil" in
    let seb = Mockup_client.account_of_seed "seb" in
    (* In this example, success requires: (Mark & Seb), (Phil & Seb), or (Mark & Phil & Seb) *)
    (* failing cases: (Mark & Phil), or any single singer *)
    let threshold = 51 in
    let mark_contrib = Z.of_int 25 in
    let phil_contrib = Z.of_int 25 in
    let seb_contrib = Z.of_int 26 in
    Scenario.add name @@ fun ctxt ->
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make admin_transfer
            ~control:(Control.sender_is (Constant_or_variable.variable admin))
            ~ability:
              (Ability.transfer_mutez
                 ~amount:
                   (`Limit
                     (Constant_or_variable.constant (Z.of_int admin_limit)))
                 ~destination:`Anyone);
          Entrypoint.make proxy_admin_change
            ~control:
              (Control.weighted_multisig ~counter:counter_var
                 ~threshold:(Constant_or_variable.constant (Z.of_int threshold))
                 ~keys:
                   (Constant_or_variable.constant
                      [
                        (mark_contrib, mark#pk);
                        (phil_contrib, phil#pk);
                        (seb_contrib, seb#pk);
                      ]))
            ~ability:(Ability.Update_variables [ (admin, Type.Address) ]);
          Entrypoint.make signed_transfer
            ~control:
              (Control.weighted_multisig ~counter:counter_var_2
                 ~threshold:(Constant_or_variable.constant (Z.of_int threshold))
                 ~keys:
                   (Constant_or_variable.constant
                      [
                        (mark_contrib, mark#pk);
                        (phil_contrib, phil#pk);
                        (seb_contrib, seb#pk);
                      ]))
            ~ability:
              (Ability.transfer_mutez
                 ~amount:
                   (`Limit
                     (Constant_or_variable.constant (Z.of_int admin_limit)))
                 ~destination:`Anyone);
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    let open Tezai_smartml_generation in
    let open! Construct in
    (* storage: admin; counter; counter_2 *)
    let init = Fmt.str "(Pair %S %d %d)" initial_admin 0 0 in
    let initial_bal = 100. in
    Mockup_client.originate ~name ~path ~init ctxt ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    let check_bal msg bal =
      Scenario.assert_equal ctxt msg Fmt.float bal
        (Mockup_client.tez_balance_of_contract ctxt addr)
    in
    check_bal "initial balanace" initial_bal;
    (* b1 can do admin_transfer *)
    let amt_sent = 500_000 in
    let burn_cap = 10 in
    let params1 = Fmt.str "(Pair %d %S)" amt_sent boot3 in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:admin_transfer ~arg:params1 ~burn_cap:(Int.to_string burn_cap);
    let new_bal = initial_bal -. (Float.of_int amt_sent /. 1_000_000.) in
    check_bal "b1 can do admin transfer" new_bal;
    (* b2 cannot do admin_transfer  *)
    Scenario.assert_failure ctxt "change-admin-too-early" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
          ~entrypoint:admin_transfer ~arg:params1
          ~burn_cap:(Int.to_string burn_cap));
    check_bal "b2 cannot do admin transfer" new_bal;
    (* Cannot do proxy_admin_change with signers Mark & Phil *)
    let new_admin = boot2 in
    let sig_params =
      [ (Variable.Type.address, Variable.Value.address new_admin) ]
    in
    let mark_sig =
      Signing.sign_multisig ctxt mark ~params:sig_params addr
        ~test_counter:(Z.of_int 0)
    in
    let phil_sig =
      Signing.sign_multisig ctxt phil ~params:sig_params addr
        ~test_counter:(Z.of_int 0)
    in
    let seb_sig =
      Signing.sign_multisig ctxt seb ~params:sig_params addr
        ~test_counter:(Z.of_int 0)
    in
    let params2 =
      Fmt.str "(Pair %S {Some %S; Some %S; None})" new_admin mark_sig phil_sig
    in
    Scenario.assert_failure ctxt
      "insufficient-signing-weight (proxy_admin_change)" (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
          ~entrypoint:proxy_admin_change ~arg:params2
          ~burn_cap:(Int.to_string burn_cap));
    (* Can do msig_admin_change (making b2 the admin) with signers Mark & Seb *)
    let params3 =
      Fmt.str "(Pair %S {Some %S; None; Some %S})" new_admin mark_sig seb_sig
    in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:proxy_admin_change ~arg:params3
      ~burn_cap:(Int.to_string burn_cap);
    (* b2 can now do admin_transfer *)
    Mockup_client.call ctxt ~caller:"bootstrap2" ~contract:addr
      ~entrypoint:admin_transfer ~arg:params1 ~burn_cap:(Int.to_string burn_cap);
    let new_bal_2 = new_bal -. (Float.of_int amt_sent /. 1_000_000.) in
    check_bal "b2 can now do admin transfer" new_bal_2;
    (* Another msig_admin_change (making b3 the admin) with all three *)
    (* signers, using '1' as the counter *)
    let new_admin_2 = boot3 in
    let sig_params_2 =
      [ (Variable.Type.address, Variable.Value.address new_admin_2) ]
    in
    let mark_sig_1 =
      Signing.sign_multisig ctxt mark ~params:sig_params_2 addr
        ~test_counter:(Z.of_int 1)
    in
    let phil_sig_1 =
      Signing.sign_multisig ctxt phil ~params:sig_params_2 addr
        ~test_counter:(Z.of_int 1)
    in
    let seb_sig_1 =
      Signing.sign_multisig ctxt seb ~params:sig_params_2 addr
        ~test_counter:(Z.of_int 1)
    in
    let params4 =
      Fmt.str "(Pair %S {Some %S; Some %S; Some %S})" new_admin_2 mark_sig_1
        phil_sig_1 seb_sig_1
    in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:proxy_admin_change ~arg:params4
      ~burn_cap:(Int.to_string burn_cap);
    (* b3 can now do admin_transfer *)
    let params5 = Fmt.str "(Pair %d %S)" amt_sent boot2 in
    Mockup_client.call ctxt ~caller:new_admin_2 ~contract:addr
      ~entrypoint:admin_transfer ~arg:params5 ~burn_cap:(Int.to_string burn_cap);
    let new_bal_3 = new_bal_2 -. (Float.of_int amt_sent /. 1_000_000.) in
    check_bal "b3 can now do admin transfer" new_bal_3;
    (* signed_transfer entrypoint -- an example of including 2 parameters in the signature: *)
    (* Back to zero for the counter since this is a new entrypoint! *)
    (* Cannot do signed_transfer with signers Mark & Phil *)
    let sig_params_3 =
      [
        (Variable.Type.nat, Variable.Value.nat (Z.of_int amt_sent));
        (Variable.Type.address, Variable.Value.address boot3);
      ]
    in
    let mark_sig_3 =
      Signing.sign_multisig ctxt mark ~params:sig_params_3 addr
        ~test_counter:(Z.of_int 0)
    in
    let phil_sig_3 =
      Signing.sign_multisig ctxt phil ~params:sig_params_3 addr
        ~test_counter:(Z.of_int 0)
    in
    let seb_sig_3 =
      Signing.sign_multisig ctxt seb ~params:sig_params_3 addr
        ~test_counter:(Z.of_int 0)
    in
    let params6 =
      Fmt.str "(Pair %d (Pair %S {Some %S; Some %S; None}))" amt_sent boot3
        mark_sig_3 phil_sig_3
    in
    Scenario.assert_failure ctxt "insufficient-signing-weight (transfer)"
      (fun () ->
        Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
          ~entrypoint:signed_transfer ~arg:params6
          ~burn_cap:(Int.to_string burn_cap));
    (* Can do signed_transfer with signers Mark & Seb *)
    let params7 =
      Fmt.str "(Pair %d (Pair %S {Some %S; None; Some %S}))" amt_sent boot3
        mark_sig_3 seb_sig_3
    in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:signed_transfer ~arg:params7
      ~burn_cap:(Int.to_string burn_cap);
    ()

  let _just_one_updated_variable =
    let name = "just-one-updated-variable" in
    Scenario.add name @@ fun ctxt ->
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make "aaaa" ~control:Control.anyone
            ~ability:
              (Ability.Sequence
                 [
                   Ability.(update_variables [ ("rrr", Variable.Type.address) ]);
                 ]);
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    let open Tezai_smartml_generation in
    let open! Construct in
    let init = Fmt.str "(Pair %S Unit)" boot1 in
    Mockup_client.originate ~name ~path ~init ctxt ~amount:"100"

  let _transfer_sequence =
    let name = "transfer_sequence" in
    Scenario.add name @@ fun ctxt ->
    let two_transfers = "two_transfers" in
    let admin = "admin" in
    let admin_limit = 42_042_042 in
    let path =
      Contracts.compile_to_file ctxt
        [
          default_transfer;
          Entrypoint.make two_transfers
            ~control:(Control.sender_is (Constant_or_variable.variable admin))
            ~ability:
              (Ability.Sequence
                 [
                   Ability.(
                     transfer_mutez
                       ~amount:
                         (`Limit
                           (Constant_or_variable.constant (Z.of_int admin_limit)))
                       ~destination:`Anyone);
                   Ability.(
                     transfer_mutez
                       ~amount:
                         (`Limit
                           (Constant_or_variable.constant (Z.of_int admin_limit)))
                       ~destination:`Anyone);
                 ]);
        ]
    in
    Mockup_client.typecheck ctxt ~path;
    let initial_bal = 100. in
    let init =
      (* bootstrap1 address *)
      Fmt.str "(Pair %S Unit)" boot1
    in
    Mockup_client.originate ~name ~path ctxt ~init ~amount:"100";
    let addr = Mockup_client.address_of_contract_name ctxt name in
    let check_bal msg bal =
      Scenario.assert_equal ctxt msg Fmt.float bal
        (Mockup_client.tez_balance_of_contract ctxt addr)
    in
    check_bal "initial balanace" initial_bal;

    (* verify duplicate fields from seq of transfers have the correct *)
    (* unique names  *)
    let target_str : string =
      "Entrypoint two_transfers: (pair (mutez %amount) (pair (mutez %amount_2) \
       (pair (address %destination) (address %destination_2))))"
    in
    let single_spaces str =
      let foldf : string * bool -> char -> string * bool =
       fun (s, in_space) ch ->
        match (ch, in_space) with
        | ' ', _ -> (s, true)
        | x, true -> (String.of_char x ^ " " ^ s, false)
        | x, false -> (String.of_char x ^ s, false)
      in
      fst (String.fold (String.rev str) ~init:("", false) ~f:foldf)
    in
    let result =
      Mockup_client.contract_entrypoint ctxt ~entrypoint:two_transfers
        ~contract:addr
    in
    let cleaned = single_spaces result in
    Scenario.assert_condition ctxt "entrypoint types do not match"
      (String.equal cleaned target_str);

    (* execute a sequence of two transfers *)
    let amt_sent_1 = 10 in
    let amt_sent_2 = 21 in
    let burn_cap = 10 in
    let params1 =
      Fmt.str "(Pair %d (Pair %d (Pair %S %S)))" amt_sent_1 amt_sent_2 boot2
        boot3
    in
    Mockup_client.call ctxt ~caller:"bootstrap1" ~contract:addr
      ~entrypoint:two_transfers ~arg:params1 ~burn_cap:(Int.to_string burn_cap);
    let new_bal =
      initial_bal
      -. ((Float.of_int amt_sent_1 +. Float.of_int amt_sent_2) /. 1_000_000.)
    in
    check_bal "new balanace" new_bal;
    ()
end

let command () =
  let open Cmdliner in
  let open Term in
  let term =
    let actions = [ ("list", `List); ("run", `Run); ("mockup", `Mockup) ] in
    const (fun root_path action filters more_args ->
        let ctxt =
          let mockup =
            Mockup_client.make ~base_dir:Path.(root_path // "mockup-client") ()
          in
          object
            method mockup_client = mockup
            method file_io = Io.File.default ()
          end
        in
        (* dbgf "Mockup root path: %S" root_path ; *)
        begin
          match action with
          | `List -> List.iter (Scenario.All.names ctxt) ~f:(Fmt.pr "%s\n%!")
          | `Run ->
              let filter =
                match filters with
                | [] -> None
                | l ->
                    let res = List.map ~f:Re.Posix.compile_pat l in
                    Some
                      (fun scen ->
                        List.exists res ~f:(fun re ->
                            Re.execp re (Scenario.name scen)))
              in
              Mockup_client.init ctxt;
              Scenario.All.run ctxt ~root:root_path ?filter
          | `Mockup ->
              let res = Mockup_client.command ctxt more_args in
              Fmt.epr "Mockup-command: %s\n- out: %s\n- err: %s\n%!"
                (if res#success then "SUCCESS" else "FAILURE")
                res#outfile res#errfile;
              List.iter res#out ~f:(Fmt.pr "%s\n%!");
              List.iter res#err ~f:(Fmt.epr "%s\n%!");
              ()
        end;
        ())
    $ Arg.(
        value
          (opt string "/tmp/babuso-mockup-tests/"
             (info [ "root-path" ] ~doc:"The root path for all the test data.")))
    $ Arg.(
        let doc =
          Fmt.str "Action to do (one of: %s)."
            (String.concat ~sep:", " (List.map actions ~f:fst))
        in
        value (pos 0 (enum actions) `Run (info [] ~doc)))
    $ Arg.(
        value
          (opt_all string []
             (info [ "filter" ]
                ~doc:
                  "Filter the tests with a POSIX regexp, option can be passed \
                   many times with “or” semantics.")))
    $ Arg.(value (pos_right 0 string [] (info [] ~doc:"Additional arguments")))
  in
  let doc = "Run smart-contract tests using the mockup mode of octez-client." in
  let command_name = "run-mockup-tests" in
  let man : Manpage.block list =
    let pf fmt = Fmt.kstr (fun p -> `P p) fmt in
    let pre l =
      `Pre (String.concat ~sep:"\n" (List.map ~f:(Fmt.str "  %s") l))
    in
    [
      pf "These are bunch of smart-contract tests:";
      pre (List.map (Scenario.All.names ()) ~f:(Fmt.str "* `%s`."));
      pf "Run them all:";
      pre
        [
          "export PATH=/path/to/octez/client:\\$PATH        # it needs \
           octez-client";
          "alias bb='dune exec babuso-bin/babuso.exe'     # or use ./please.sh \
           bb";
          Fmt.str "bb %s" command_name;
        ];
      pf "Get the list of tests";
      pre [ Fmt.str "bb %s list" command_name ];
      pf "Run only some of the tests using POSIX regular expressions:";
      pre [ Fmt.str "bb %s  --filter ^dead --filter ^boom" command_name ];
      pf "Run extra tests that fail in order to test the test-framework:";
      pre [ Fmt.str "test_the_mockup_test=yes bb %s" command_name ];
      pf "Run mockup commands in the same environment as the latest test run:";
      pre [ Fmt.str "bb %s mockup list known contracts" command_name ];
      pf "This is implemented in `%s`." Caml.__FILE__;
    ]
  in
  (term, Cmd.info command_name ~doc ~man)
