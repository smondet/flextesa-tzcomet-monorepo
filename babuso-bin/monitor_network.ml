open Import
open Babuso_lib
open Wallet_data
module Node = Tezos_node

type t = {
  mutable nodes : (Node.t * Node.Status.t) List.t;
  mutable network : Network.t option;
}

let create () = { nodes = []; network = None }
let _get ctxt : t = ctxt#network_information

let for_protocol_message ctxt =
  let self = _get ctxt in
  Network_information.make ~nodes:self.nodes ?network:self.network ()

module Low_level = struct
  let get_all_node_statuses ctxt nodes =
    let open Lwt in
    let exception Rpc_timed_out of string in
    let exception Rpc_failed of string * exn in
    let get_node_status node =
      let rpc meth path =
        Lwt.catch
          (fun () ->
            Http_client.Low_level.call_lwt ctxt ~timeout:(`Set 3.)
              ~headers:[ ("Content-type", "application/json") ]
              ~meth
              Path.(Node.base_url node // path))
          (function
            | Http_client.Timeout _ -> raise (Rpc_timed_out path)
            | exn -> raise (Rpc_failed (path, exn)))
      in
      let version = ref None in
      let health = ref `Unknown in
      let problems = ref [] in
      let head = ref None in
      let chain_id = ref None in
      Lwt.catch
        (fun () ->
          rpc `Get "/version" >>= fun v ->
          version := Some v;
          rpc `Get "/chains/main/blocks/head" >>= fun metadata ->
          let json = Json.of_string metadata in
          let level =
            Json.Q.(json |> field ~k:"header" |> int_field ~k:"level")
          in
          let hash = Json.Q.(json |> string_field ~k:"hash") in
          let head_chain_id = Json.Q.(json |> string_field ~k:"chain_id") in
          head := Some (level, hash);
          chain_id := Some head_chain_id;
          health := `Fine;
          return_unit)
        (function
          | Rpc_timed_out path when not (String.equal path "/version") ->
              (* Timed out but not on the first RPC: *)
              health := `Injured;
              problems := `Timeouts_observed :: !problems;
              return_unit
          | _exn ->
              health := `Dead;
              return_unit)
      >>= fun () ->
      let status =
        Node.Status.make ?head:!head ?chain_id:!chain_id ~health:!health
          ?version:!version ()
      in
      return (node, status)
    in
    Lwt_list.map_p get_node_status nodes >>= fun nodes_and_statuses ->
    let best_level, consensual_chain_id =
      (* For now the chain-id is the one of the Fine-node with the
         highest level: *)
      List.fold ~init:(0, None) nodes_and_statuses ~f:(fun cur -> function
        | _, { chain_id = Some ci; head = Some (l, _); health = `Fine; _ } -> (
            match cur with
            | clvl, Some _ when l <= clvl -> cur
            | _ -> (l, Some ci))
        | _, _ -> cur)
    in
    let nodes_and_statuses_fixed =
      let open Tezos_node.Status in
      let update_status_for_chain_id = function
        | { chain_id = Some ci; _ } as s -> (
            match consensual_chain_id with
            | None -> add_problem s `No_chain_id_consensus
            | Some c ->
                if String.equal ci c then s
                else add_problem s (`Wrong_chain_id (ci, c)))
        | s -> s
      in
      let update_status_for_level = function
        | { head = Some (l, _); _ } as s when l + 5 < best_level ->
            add_problem s `Head_is_behind
        | s -> s
      in
      List.map nodes_and_statuses ~f:(fun (n, s) ->
          (n, s |> update_status_for_level |> update_status_for_chain_id))
    in
    Lwt.return nodes_and_statuses_fixed
end

let all_nodes ctxt =
  let self = _get ctxt in
  self.nodes

let fine_nodes ctxt =
  List.filter (all_nodes ctxt) ~f:(function
    | _, { health = `Fine; _ } -> true
    | _ -> false)

let one_node ctxt =
  match fine_nodes ctxt with
  | [] ->
      Failure.raise
        Docpp.(
          text "Could not find a node that is in fine health!"
          +++ parentheses
                (textf "Out of %d nodes" (all_nodes ctxt |> List.length)))
  | more -> List.random_element_exn more

let start_update_loop ~get_configured_nodes ctxt =
  let self = _get ctxt in
  let rec loop () =
    let configured_nodes = get_configured_nodes () in
    let statuses =
      Threads.lwt_run ctxt (fun () ->
          Low_level.get_all_node_statuses ctxt configured_nodes)
    in
    self.nodes <- statuses;
    let chain_id =
      try
        let _, { Node.Status.chain_id; _ } = one_node ctxt in
        chain_id
      with _ -> None
    in
    self.network <- Option.map ~f:Network.of_chain_id chain_id;
    Events.add ctxt
      [
        Protocol.Message.Down.fresh_network_information
          (for_protocol_message ctxt);
      ];
    Unix.sleepf 10.;
    loop ()
  in
  Threads.spawn_unit ctxt loop;
  ()
