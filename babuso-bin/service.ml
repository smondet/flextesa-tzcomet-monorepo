open! Import
open Babuso_lib
open Wallet_data
open Babuso_error

let index _ =
  let mime_type = ("Content-Type", "text/html") in
  Httpd.Response.make_string ~headers:[ mime_type ] (Ok Data.index_html)

let main_js _ =
  let mime_type = ("Content-Type", "text/javascript") in
  Httpd.Response.make_string ~headers:[ mime_type ] (Ok Data.main_js)

let bootstrap_css ctxt =
  let mime_type = ("Content-Type", "text/css") in
  let css = Backend_state.theme ctxt |> Ui_theme.css in
  Httpd.Response.make_string ~headers:[ mime_type ] (Ok css)

let originate_multisig_account _ctxt account_id ~add_operation ~gas_wallet
    ~threshold ~keys =
  let operation =
    let id = Fresh_id.make () in
    let initialization =
      Variable.
        [
          ("counter", Value.nat Z.zero);
          ("threshold", Value.nat threshold);
          ("keys", Value.key_list keys);
        ]
    in
    let specification =
      Smart_contract.generic_multisig
        ~variables:(Variable.Record.comb initialization)
    in
    let order =
      Operation.Specification.origination ~specification
        ~initialization:(Variable.Map.of_list initialization)
        ~account:account_id ~gas_wallet
    in
    let last_update = Unix.gettimeofday () in
    Operation.make ~id ~order ~last_update ~status:Operation.Status.ordered ()
  in
  add_operation operation;
  let kind =
    Account.Status.contract_to_originate ~operation:(Operation.id operation)
      ~contract:None
  in
  kind

let originate_custom_smart_contract _ctxt account_id ~gas_wallet ~entrypoints
    ~initialization ~add_operation =
  let operation =
    let id = Fresh_id.make () in
    let specification =
      let can_receive_funds =
        List.exists entrypoints ~f:(function
          | { Entrypoint.control = Anyone; ability = Do_nothing; _ } -> true
          | _ -> false)
      in
      Smart_contract.make ~can_receive_funds entrypoints
        ~variables:
          (Variable.Record.comb
             (* We assume initialization is already properly sorted: *)
             initialization)
    in
    let order =
      Operation.Specification.origination ~specification ~initialization
        ~account:account_id ~gas_wallet
    in
    let last_update = Unix.gettimeofday () in
    Operation.make ~id ~order ~last_update ~status:Operation.Status.ordered ()
  in
  add_operation operation;
  let kind =
    Account.Status.contract_to_originate ~operation:(Operation.id operation)
      ~contract:None
  in
  kind

let unexpected_protocol_exn e =
  dbgf "***** Babuso.Service.ui_protocol - unexpected error: e %a" Exn.pp e;
  Babuso_exn
    (mk_tagged_error ~tag:"Babuso.Service.ui_protocol (unexpected error)"
       (Unknown_error (Exn.to_string e)))

let handle_octez_cmdline_err ctxt e typ failure_identifier =
  match e with
  | Babuso_exn (Command_line_error cle, tag) ->
      let primary_cause =
        Octez_error_type.parse_octez_err typ failure_identifier cle.std_err
      in
      Events.new_async_failure ctxt primary_cause;
      let new_cle = { cle with cmd_type = typ } in
      let octez_err =
        Octez_client_error { cmd_line_err = new_cle; primary_cause }
      in
      let retagged = add_tag (octez_err, tag) "Babuso.Service.ui_protocol" in
      Babuso_exn retagged
  | e -> unexpected_protocol_exn e

let make_blob_for_signature ctxt ~target_contract_id ~entrypoint ~parameters =
  let account = Backend_state.Accounts.get_by_id ctxt target_contract_id in
  match account.state with
  | Account.Status.Key_pair _ | Account.Status.Friend _ | Account.Status.Draft _
  | Account.Status.Contract_to_originate _
  | Account.Status.Contract_failed_to_originate _ ->
      Fmt.failwith "%S is not a proper contract" target_contract_id
  | Account.Status.Originated_contract
      { contract = Generic_multisig { variables }; address } ->
      let full_thing_to_pack =
        let payload =
          match entrypoint with
          | "main/lambda" ->
              Operation_forge.Contract_call.Generic_multisig.lambda_payload ctxt
                ~parameters
          | "main/update_keys" ->
              Operation_forge.Contract_call.Generic_multisig.update_keys_payload
                ctxt ~parameters
          | other ->
              Fmt.failwith "Entrypoint %S not found for %s" other
                target_contract_id
        in
        let chain_id =
          Backend_state.RPC.getf ctxt "/chains/main/chain_id" |> Json.Q.string
        in
        Operation_forge.Contract_call.Generic_multisig.thing_to_pack_and_sign
          ~chain_id ~parameters ~variables ~address payload
      in
      let blob =
        Octez_node.pack_data ctxt
          (Backend_state.RPC.a_node ctxt)
          ~data:full_thing_to_pack
          ~of_type:
            (Michelson.C.concrete
               "(pair (pair chain_id address) (pair nat (or (lambda unit (list \
                operation))(pair nat (list key)) )))")
      in
      blob
  | Account.Status.Originated_contract _ ->
      Fmt.failwith "%S is not a supported contract yet" target_contract_id

let order_of_draft_operation ctxt draft =
  let entrypoint_parameters =
    match
      Drafting.Render.sample_env @@ fun () ->
      Validate.Operation.draft
        ~rates:(Backend_state.exchange_rates ctxt)
        ~with_signatures:Validate.Operation.With_signatures.not_yet draft
    with
    | Ok
        (Transfer
          { source = _; destination = _; amount = _; entrypoint; parameters })
      ->
        Some (entrypoint, parameters)
    | Ok _ -> None
    | Error _ -> None
  in
  let with_signatures =
    match Operation.Draft.destination_address draft with
    | None -> Validate.Operation.With_signatures.none_required
    | Some address -> (
        match Backend_state.Accounts.find_by_address ctxt address with
        | None -> Validate.Operation.With_signatures.none_required
        | Some acc -> (
            match (Account.multisig_parameters acc, entrypoint_parameters) with
            | _, Some ("default", _) ->
                Validate.Operation.With_signatures.none_required
            | params, Some (_entrypoint, parameters) ->
                (* let blob =
                   make_blob_for_signature ctxt ~target_contract_id:acc.id
                     ~entrypoint ~parameters in *)
                let hash =
                  Validate.Operation.With_signatures.make_hash
                    ~destination:address ~parameters ~counter:params#counter
                in
                Validate.Operation.With_signatures.multisig
                  ~public_keys:params#keys ~threshold:params#threshold ~hash
            | _ | (exception _) ->
                Validate.Operation.With_signatures.none_required))
  in
  Drafting.Render.sample_env (fun () ->
      Validate.Operation.draft ~with_signatures draft
        ~rates:(Backend_state.exchange_rates ctxt))
  |> Validate.prosaic_validation_exn

let account_kind_of_draft ctxt ~add_operation ~id content =
  let validated =
    Drafting.Render.sample_env (fun () ->
        let address_of_account_id id =
          let acc = Backend_state.Accounts.get_by_id ctxt id in
          match Account.get_address acc with
          | Some s -> s
          | None ->
              Failure.raise_textf "Account %S does not have an address."
                (Account.display_name acc)
        in
        let public_key_of_account_id id =
          let acc = Backend_state.Accounts.get_by_id ctxt id in
          match Account.get_public_key acc with
          | Some s -> s
          | None ->
              Failure.raise_textf "Account %S does not have a public key."
                (Account.display_name acc)
        in
        Validate.Account.draft
          ~rates:(Backend_state.exchange_rates ctxt)
          ~address_of_account_id ~public_key_of_account_id content)
    |> Validate.prosaic_validation_exn
  in
  match validated with
  | `Uri uri -> (
      match uri with
      | `Friend_pk public_key ->
          let public_key_hash =
            let open Tezai_base58_digest.Identifier.Generic_signer in
            Public_key.of_base58 public_key
            |> Public_key_hash.of_public_key |> Public_key_hash.to_base58
          in
          Account.Status.friend (Friend.make ~public_key ~public_key_hash ())
      | `Kt1 address | `Friend_tz address ->
          Indexer.identify_address ctxt ~address
      | `Unencrypted uri | `Ledger uri ->
          let backend = Signer.process_secret_key_uri ctxt ~account_id:id uri in
          Account.Status.key_pair backend)
  | `Generic_multisig gm ->
      originate_multisig_account ctxt id ~gas_wallet:gm#gas_wallet
        ~threshold:gm#threshold ~keys:gm#keys ~add_operation
  | `Custom_contract cc ->
      originate_custom_smart_contract ctxt id ~gas_wallet:cc#gas_wallet
        ~entrypoints:cc#entrypoints ~initialization:cc#initialization
        ~add_operation

let mime_type = ("Content-Type", "text/plain")

let headers =
  [
    mime_type;
    (* https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Cache-Control *)
    ("Cache-Control", "no-store");
  ]

let respond_down_message m =
  let open Protocol in
  let body = Message.Down.(m |> sexp_of_t) |> Sexp.to_string_mach in
  Httpd.Response.make_string ~headers (Ok body)

let respond_done () = respond_down_message Protocol.Message.Down.Done

let do_simulation ctxt order =
  let node = Backend_state.RPC.a_node ctxt in
  let operation, _ = Update_loop.make_tezos_operation_from_order ctxt order in
  let batch =
    Node_interaction.prepare_operation_simulation ctxt node operation
  in
  let simulation, forges = Node_interaction.simulate_batch ctxt node batch in
  let batch = Node_interaction.set_fees ctxt batch simulation forges in
  let simulation =
    if Simulation_result.all_applied simulation then
      fst (Node_interaction.simulate_batch ctxt node batch)
    else simulation
  in
  respond_down_message
    (Protocol.Message.Down.simulation_result simulation batch)

let ui_protocol ctxt ~body =
  let open Protocol in
  match Sexplib.Sexp.of_string body |> Message.Up.t_of_sexp with
  | Get_configuration ->
      respond_down_message
        (Message.Down.configuration (Backend_state.configuration ctxt))
  | Update_configuration new_config ->
      Backend_state.update_configuration ctxt new_config;
      respond_done ()
  | Get_network_information ->
      respond_down_message
        (Message.Down.network_information
           (Monitor_network.for_protocol_message ctxt))
  | List_connected_ledgers ->
      let parsed = Octez_client.list_connected_ledgers ctxt in
      respond_down_message Message.Down.(Ledgers parsed)
  | All_accounts ->
      respond_down_message
        Message.Down.(Account_list (Backend_state.Accounts.all ctxt))
  | All_operations ->
      respond_down_message
        (Message.Down.operation_list (Backend_state.Operations.all ctxt))
  | Get_events since ->
      dbgf "Get_events %f called!" since;
      let waited_on_events = Events.get ctxt ~since in
      let events, ts =
        let ts = ref Float.min_value in
        let evs =
          List.map waited_on_events ~f:(fun { timestamp; content } ->
              ts := Float.max timestamp !ts;
              content)
        in
        (List.dedup_and_sort ~compare:Poly.compare evs, !ts)
      in
      let msg = Message.Down.events events ts in
      dbgf "Get_events %f woken up with %a!" since Sexp.pp
        (Message.Down.sexp_of_t msg);
      respond_down_message msg
  | Get_exchange_rates ->
      let er = Backend_state.Exchange_rates.get_latest ctxt in
      respond_down_message (Message.Down.exchange_rates er)
  | Delete_account id ->
      Backend_state.Accounts.remove_if_exists ctxt id;
      respond_done ()
  | Create_generic_multisig_account
      { name; keys; threshold; gas_wallet; comments } ->
      let account_id = Fresh_id.make () in
      let kind =
        originate_multisig_account ctxt account_id ~gas_wallet
          ~threshold:(Z.of_int threshold) ~keys
          ~add_operation:(Backend_state.Operations.add ctxt)
      in
      Backend_state.Accounts.add ctxt
        (Account.make ?comments ~id:account_id ~display_name:name
           ~balance:Z.zero kind);
      respond_down_message Message.Down.Done
  | Create_smart_contract
      { name; gas_wallet; entrypoints; initialization; comments } ->
      let account_id = Fresh_id.make () in
      let kind =
        originate_custom_smart_contract ctxt account_id ~gas_wallet ~entrypoints
          ~initialization
          ~add_operation:(Backend_state.Operations.add ctxt)
      in
      Backend_state.Accounts.add ctxt
        (Account.make ~id:account_id ~display_name:name ~balance:Z.zero
           ?comments kind);
      respond_down_message Message.Down.Done
  | Order_operation { specification; comments } ->
      let operation =
        let id = Fresh_id.make () in
        let last_update = Unix.gettimeofday () in
        Operation.make ?comments ~id ~last_update ~order:specification
          ~status:Operation.Status.ordered ()
      in
      Backend_state.Operations.add ctxt operation;
      respond_down_message Message.Down.Done
  | Make_blob_for_signature { target_contract_id; entrypoint; parameters } ->
      let blob =
        make_blob_for_signature ctxt ~target_contract_id ~entrypoint ~parameters
      in
      respond_down_message (Message.Down.blob blob)
  | Sign_blob { account; blob } ->
      (* let bytes_hex = Bytes_rep.to_zero_x blob in *)
      dbgp
        Docpp.(
          text "Sign_blob"
          ++ itemize
               [
                 text "Account:" +++ verbatim account;
                 text "Blob" +++ Bytes_rep.to_pp blob;
               ]);
      let account = Backend_state.Accounts.get_by_id ctxt account in
      dbgp
        Docpp.(
          text "Sign_blob: got account"
          ++ itemize [ text "Account:" +++ sexpable Account.sexp_of_t account ]);
      let signature = Signer.sign_bytes ctxt account blob in
      dbgp
        Docpp.(
          text "Sign_blob: got signature"
          ++ itemize
               [
                 text "Account:" +++ sexpable Account.sexp_of_t account;
                 text "Sig:" +++ verbatim signature;
               ]);
      respond_down_message (Message.Down.signature signature)
  | Check_signature { public_key; blob; signature } -> (
      dbgp
        Docpp.(
          text "Check_signature"
          ++ itemize
               [
                 text "PK:" +++ verbatim public_key;
                 text "Blob" +++ Bytes_rep.to_pp blob;
                 text "Sig" ++ verbatim signature;
               ]);
      match Signer.check_signature ctxt ~public_key ~blob ~signature with
      | `Checks_out -> respond_down_message Message.Down.Done
      | `No msg -> Failure.raise_textf "Signature check failed: %s" msg)
  | Import_account { name; kind; comments } -> (
      match kind with
      | `Address address -> (
          let do_import_address () =
            let balance = Indexer.balance ctxt ~address in
            let kind = Indexer.identify_address ctxt ~address in
            let id = Fresh_id.make () in
            Backend_state.Accounts.add ctxt
              (Account.make ~id ~display_name:name ~balance ?comments kind);
            respond_down_message Message.Down.Done
          in
          try do_import_address ()
          with e ->
            raise
              (handle_octez_cmdline_err ctxt e Import_account
                 "Import_account (via address"))
      | (`Ledger_uri _ | `Unencrypted_uri _) as kind -> (
          let do_import_uri () =
            let id = Fresh_id.make () in
            let uri =
              match kind with `Ledger_uri uri | `Unencrypted_uri uri -> uri
            in
            let lines =
              Octez_client.run_command ctxt
                [ "import"; "secret"; "key"; id; uri; "--force" ]
            in
            dbgf "Import result: %a" Fmt.Dump.(list string) lines;
            let lines =
              Octez_client.run_command ctxt [ "show"; "address"; id ]
            in
            dbgf "Show result: %a" Fmt.Dump.(list string) lines;
            let public_key_hash, public_key =
              match List.map lines ~f:(fun l -> String.split ~on:' ' l) with
              | [ [ "Hash:"; h ]; [ "Public"; "Key:"; p ] ] -> (h, p)
              | _ ->
                  let msg =
                    Fmt.str "Failed to parse public-key(-hash): %a"
                      Fmt.Dump.(list string)
                      lines
                  in
                  raise
                    (Babuso_exn
                       (General_error msg, Some "Babuso.Service.ui_protocol"))
            in
            let backend =
              Key_pair.make ~public_key ~public_key_hash
                (match kind with
                | `Ledger_uri uri -> Ledger { uri }
                | `Unencrypted_uri uri -> Unencrypted { private_uri = uri })
            in
            Backend_state.Accounts.add ctxt
              (Account.make ~id ~display_name:name ?comments
                 (Account.Status.key_pair backend));
            respond_down_message Message.Down.Done
          in
          try do_import_uri ()
          with e ->
            raise
              (handle_octez_cmdline_err ctxt e Import_account
                 "Import_account (via URI)")))
  | Save_account_draft { id; display_name; content; comments; glorify = false }
    ->
      Backend_state.Accounts.replace_draft ctxt
        (Account.make ~id ~display_name
           (Account.Status.draft content)
           ?comments);
      respond_down_message Message.Down.Done
  | Save_operation_draft op ->
      Backend_state.Operations.replace_draft ctxt
        { op with Operation.last_update = Unix.gettimeofday () };
      respond_down_message Message.Down.Done
  | Delete_operation_draft { id } ->
      Backend_state.Operations.remove ctxt id;
      respond_down_message Message.Down.Done
  | Run_operation_draft_simulation { draft } ->
      let order = order_of_draft_operation ctxt draft in
      do_simulation ctxt order
  | Run_account_draft_simulation { draft } ->
      let id = Fresh_id.make () in
      let ops = ref [] in
      let _kind =
        account_kind_of_draft ctxt ~id draft ~add_operation:(fun op ->
            ops := op :: !ops)
      in
      let ops_to_simulate = List.rev !ops in
      let order =
        match ops_to_simulate with
        | [ op ] -> op.order
        | [] ->
            Failure.raise
              Docpp.(
                text
                  "Account draft did not result in any operation to simulate.")
        | more ->
            Failure.raise
              Docpp.(
                text
                  "Account draft requires more than one operation, this is not \
                   implemented yet."
                +++ itemize (List.map ~f:(sexpable Operation.sexp_of_t) more))
      in
      do_simulation ctxt order
  | Glorify_operation_draft op ->
      let open Operation in
      let order =
        let open Specification in
        match op.order with
        | Origination _ | Transfer _ ->
            Failure.raise
              Docpp.(
                textf "Operation %S cannot be glorified, it's not a draft:"
                  op.id
                +++ sexpable Operation.sexp_of_t op)
        | Draft draft -> order_of_draft_operation ctxt draft
      in
      let operation =
        {
          op with
          order;
          last_update = Unix.gettimeofday ();
          status = Status.ordered;
        }
      in
      Backend_state.Operations.modify ctxt operation;
      respond_down_message Message.Down.Done
  | Save_account_draft { id; display_name; content; comments; glorify = true }
    ->
      Threads.spawn_unit ctxt (fun () ->
          let alert_id =
            Events.add_alert ctxt
              (Protocol.Message.Down.Alert.account_is_being_created ~id
                 ~display_name)
          in
          Exn.protect
            ~finally:(fun () -> Events.remove_alert ctxt alert_id)
            ~f:(fun () ->
              try
                let kind =
                  account_kind_of_draft ctxt ~id content
                    ~add_operation:(Backend_state.Operations.add ctxt)
                in
                Backend_state.Accounts.remove_if_exists ctxt id;
                Backend_state.Accounts.add ctxt
                  (Account.make ~id ~display_name ?comments kind)
              with e -> Events.new_async_failure ctxt (Exn.to_string e)));
      respond_down_message Message.Down.Done
  | Update_comments { id; comments } ->
      let what =
        try
          let (_ : Account.t) = Backend_state.Accounts.get_by_id ctxt id in
          `Account
        with _ ->
          let (op : Operation.t) = Backend_state.Operations.get_by_id ctxt id in
          `Operation op
      in
      begin
        match what with
        | `Account ->
            Backend_state.Accounts.modify_by_id ctxt id (fun acc ->
                { acc with Account.comments })
        | `Operation op ->
            Backend_state.Operations.modify ctxt { op with Operation.comments }
      end;
      respond_down_message Message.Down.Done
  | Update_display_name { id; name } ->
      Backend_state.Accounts.modify_by_id ctxt id (fun acc ->
          { acc with Account.display_name = name });
      respond_down_message Message.Down.Done
  | Generate_key_pair { kind = `Unencrypted; curve } ->
      let kp = Signer.generate_key_pair ctxt ~curve in
      respond_down_message
        (Message.Down.key_pair ~public_key:kp#pk ~public_key_hash:kp#pkh
           ~secret_key_uri:kp#sk)
  | Analyze_uri_or_address uri_or_address ->
      let valid, facts =
        match
          Drafting.Render.sample_env (fun () ->
              Validate.uri_or_address uri_or_address)
        with
        | Error _ -> (false, [])
        | Ok v -> (true, Indexer.analyze_uri_result ctxt v)
      in
      let analysis = Uri_analysis.make uri_or_address ~facts ~valid in
      respond_down_message (Message.Down.uri_or_address_analysis analysis)
  | Guess_michelson_expression_type expression ->
      respond_down_message
        (Message.Down.michelson_typing
           (Indexer.guess_michelson_expression_type ctxt expression))
  | exception e ->
      Failure.raise ~code:500
        Docpp.(Fmt.kstr text "Error parsing message: %a" Exn.pp e)
