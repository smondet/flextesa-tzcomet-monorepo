open! Import
open Babuso_lib
open Wallet_data
open Tezos_node

let rpc ctxt self ?(meth = `Get) path : Json.t =
  let url = Path.concat self.base_url path in
  Http_client.Json_web_api.call ctxt url ~meth

let rpcf ctxt ?meth self fmt = Fmt.kstr (rpc ctxt ?meth self) fmt

let get_block ctxt self spec =
  rpcf ctxt self "/chains/main/blocks/%s"
    (match spec with
    | `Head -> "head"
    | `Head_minus n -> Fmt.str "head~%d" n
    | `Hash h -> h
    | `Level i -> Fmt.str "%d" i)

let get_revealed_key ctxt self ~address =
  let json =
    rpcf ctxt self "/chains/main/blocks/head/context/contracts/%s/manager_key"
      address
  in
  match json with
  | `Null -> None
  | `String s -> Some s
  | other ->
      Failure.raise
        Docpp.(
          textf "Getting revealed key for %S, wrong JSON:" address
          +++ Json.to_pp other)

let get_counter ctxt self ~address =
  let json =
    rpcf ctxt self "/chains/main/blocks/head/context/contracts/%s/counter"
      address
  in
  match json with
  | `String s -> Z.of_string s
  | other ->
      Failure.raise
        Docpp.(
          textf "Getting counter for %S, wrong JSON:" address
          +++ Json.to_pp other)

let get_balance ctxt self ~address =
  let json =
    rpcf ctxt self "/chains/main/blocks/head/context/contracts/%s/balance"
      address
  in
  match json with
  | `String s -> Z.of_string s
  | other ->
      Failure.raise
        Docpp.(
          textf "Getting balance for %S, wrong JSON:" address
          +++ Json.to_pp other)

let simulate_operation ctxt node op_json =
  rpc ctxt node ~meth:(`Post op_json)
    "/chains/main/blocks/head/helpers/scripts/simulate_operation"

let next_protocol ctxt node =
  let json = rpc ctxt node "/chains/main/blocks/head/protocols" in
  Json.Q.(string_field ~k:"next_protocol" json)

let forge_operations ctxt node json =
  let bytes =
    rpc ctxt node ~meth:(`Post json)
      "/chains/main/blocks/head/helpers/forge/operations"
    |> Json.Q.string
  in
  Wallet_data.Bytes_rep.of_hex bytes

let inject_operation ctxt node blob =
  rpc ctxt node
    ~meth:(`Post (Bytes_rep.to_json blob))
    "/injection/operation?chain=main"
  |> Json.Q.string

let get_mempool ctxt node =
  let json = rpc ctxt node "/chains/main/mempool/pending_operations" in
  dbgp
    Docpp.(
      text "Mempool:"
      +++ itemize (List.map (Json.Q.obj json) ~f:(fun (k, _) -> verbatim k)));
  let open Json.Q in
  let make_op_object hash obj =
    let branch = string_field ~k:"branch" obj in
    let error =
      try field ~k:"error" obj |> list |> List.map ~f:Mempool_error.of_json
      with _ -> []
    in
    object
      method hash = hash
      method branch = branch
      method errors = error
    end
  in
  let get_ops json =
    list json
    |> List.map ~f:(fun json ->
           match list json with
           | [ `String hash; obj ] -> make_op_object hash obj
           | _ ->
               Failure.raise Docpp.(text "Parsing mempool:" +++ Json.to_pp json))
  in
  let get_ops_a json =
    list json
    |> List.map ~f:(fun json ->
           let hash = string_field ~k:"hash" json in
           make_op_object hash json)
  in
  let applied = field json ~k:"applied" |> get_ops_a in
  let refused = field json ~k:"refused" |> get_ops in
  let outdated = field json ~k:"outdated" |> get_ops in
  let branch_refused = field json ~k:"branch_refused" |> get_ops in
  let branch_delayed = field json ~k:"branch_delayed" |> get_ops in
  let unprocessed = field json ~k:"unprocessed" |> get_ops in
  object
    method contents =
      [
        (Mempool_section.applied, applied);
        (Mempool_section.refused, refused);
        (Mempool_section.outdated, outdated);
        (Mempool_section.branch_refused, branch_refused);
        (Mempool_section.branch_delayed, branch_delayed);
        (Mempool_section.unprocessed, unprocessed);
      ]

    method applied = applied
    method refused = refused
    method outdated = outdated
    method branch_refused = branch_refused
    method branch_delayed = branch_delayed
    method unprocessed = unprocessed
    method as_json = json
  end

let get_constants ctxt node =
  let json = rpc ctxt node "/chains/main/blocks/head/context/constants" in
  let max_operations_time_to_live =
    Json.Q.(int_field ~k:"max_operations_time_to_live" json)
  in
  object
    method whole_json = json
    method max_operations_time_to_live = max_operations_time_to_live
  end

let pack_data ctxt node ~data ~of_type =
  let input =
    Json.C.obj
      [ ("data", Michelson.to_json data); ("type", Michelson.to_json of_type) ]
  in
  let json =
    rpc ctxt node "/chains/main/blocks/head/helpers/scripts/pack_data"
      ~meth:(`Post input)
  in
  Json.Q.string_field json ~k:"packed" |> Bytes_rep.of_hex
