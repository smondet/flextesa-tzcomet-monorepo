open! Import
open Babuso_lib
open Babuso_error
open Wallet_data

type accounts = Account.t list [@@deriving sexp]
type operations = Operation.t list [@@deriving sexp]

type t = {
  data_dir : string;
  mutable configuration : Wallet_configuration.t;
  mutable accounts : accounts;
  mutable operations : operations;
  main_client : Octez_client.t option;
  mutable update_loop_errors : exn list;
  mutable latest_exchange_rates : Exchange_rates.t option;
  accounts_mutex : Mutex.t;
  operations_mutex : Mutex.t;
}
[@@deriving fields]

type state = t

let make ?main_client ?(accounts = []) ?(operations = []) ~configuration
    data_dir =
  {
    data_dir;
    configuration;
    main_client;
    accounts;
    operations;
    update_loop_errors = [];
    latest_exchange_rates = None;
    accounts_mutex = Mutex.create ();
    operations_mutex = Mutex.create ();
  }

let get ctxt : t = ctxt#state
let gettify f ctxt = f (get ctxt)
let configuration ctxt = (get ctxt).configuration
let exchange_rates ctxt = gettify latest_exchange_rates ctxt

let exchange_rates_allowed_age ctxt =
  (configuration ctxt).exchange_rates_allowed_age

let exchange_rates_uri ctxt = (configuration ctxt).exchange_rates_uri
let theme ctxt = (configuration ctxt).ui_options.theme

module Read = struct
  let sexp ctxt data_dir subpath =
    let path = Path.(data_dir // subpath) in
    if Io.File.file_exists ctxt path then
      match Io.File.read_lines ctxt path with
      | more_or_less ->
          Some (Sexplib.Sexp.of_string (String.concat ~sep:"\n" more_or_less))
      | exception e ->
          raise
            (Babuso_exn
               ( Io_error
                   {
                     msg =
                       Fmt.str "path: %s, subpath: %s, exception: %s" path
                         subpath (Exn.to_string e);
                   },
                 Some "Babuso.State.Read.sexp" ))
    else None

  let one_line ctxt data_dir subpath =
    let path = Path.(data_dir // subpath) in
    if Io.File.file_exists ctxt path then
      match Io.File.read_lines ctxt path with
      | [ one ] -> Some one
      | more_or_less ->
          let msg =
            Fmt.str "State: wrong contents at key %S: %a" subpath
              Fmt.Dump.(list string)
              more_or_less
          in
          raise
            (Babuso_exn
               ( General_error
                   (Fmt.str "path:%s, subpath:%s, msg: %s" path subpath msg),
                 Some "Babuso.State.Read.one_line" ))
      | exception e ->
          raise
            (Babuso_exn
               ( General_error
                   (Fmt.str "path:%s, subpath:%s, exception: %s" path subpath
                      (Exn.to_string e)),
                 Some "Babuso.State.Read.one_line" ))
    else None

  let int ctxt data_dir subpath default =
    match one_line ctxt data_dir subpath with
    | None -> default
    | Some s -> (
        match Int.of_string s with
        | v -> v
        | exception (Failure.F _ as e) -> raise e
        | exception _other ->
            Failure.raise_textf "State: at key %S expecting an integer, not %S"
              subpath s)

  let with_of_sexp ctxt data_dir subpath of_sexp default =
    match sexp ctxt data_dir subpath with
    | None -> default
    | Some s -> of_sexp s
end

module Write = struct
  let one_line ctxt data_dir subpath value =
    let path = Path.(data_dir // subpath) in
    Io.File.make_path ctxt (Path.dirname path);
    Io.File.write_lines ctxt path [ value ];
    ()

  let sexp ctxt data_dir subpath value to_sexp =
    one_line ctxt data_dir subpath
      (value |> to_sexp |> Sexplib.Sexp.to_string_hum)
end

module Directory_key_values (P : sig
  type t [@@deriving sexp]

  val subpath : string
  val id : t -> string
  val all : < state : state ; .. > -> t list
  val mutex : < state : state ; .. > -> Mutex.t
end) =
struct
  open P

  let subpath = subpath

  let protect ctxt ~f =
    let mx = mutex ctxt in
    Mutex.lock mx;
    Exn.protect ~f ~finally:(fun () -> Mutex.unlock mx)

  let save_one ctxt data_dir acc =
    Write.sexp ctxt data_dir (Path.concat subpath (id acc)) acc sexp_of_t;
    ()

  let load_one ctxt data_dir path =
    match Option.map ~f:t_of_sexp (Read.sexp ctxt data_dir path) with
    | Some s -> s
    | None ->
        raise
          (Babuso_exn
             ( General_error
                 (Fmt.str
                    "Error trying to load the value - data_dir:%s, path:%s"
                    data_dir path),
               Some "Babuso.State.Directory_key_values.load_one" ))

  let remove_one ctxt id =
    let state = get ctxt in
    Unix.unlink Path.(state.data_dir // subpath // id)

  let save_all ctxt data_dir all =
    List.iter all ~f:(fun acc -> save_one ctxt data_dir acc);
    ()

  let load_all ctxt data_dir =
    let l = ref [] in
    let d = Path.(data_dir // subpath) in
    match Caml.Sys.is_directory d with
    | true ->
        Array.iter (Caml.Sys.readdir d) ~f:(fun sub ->
            let p = Path.(subpath // sub) in
            if Caml.Sys.file_exists Path.(data_dir // p) then
              l := load_one ctxt data_dir p :: !l);
        !l
    | false ->
        let msg =
          Fmt.str "Problem: %S should be a directory or not exist at all" d
        in
        raise
          (Babuso_exn
             ( Io_error { msg },
               Some "Babuso.State.Directory_key_values.load_all" ))
    | exception _ -> []

  let add_protected ctxt v ~add_local =
    protect ctxt ~f:(fun () ->
        let state = get ctxt in
        match
          List.find (all ctxt) ~f:(fun acc -> String.equal (id acc) (id v))
        with
        | Some acc ->
            let msg =
              Fmt.str "Duplicate %s ID: %a %a" subpath Sexp.pp (sexp_of_t acc)
                Sexp.pp (sexp_of_t v)
            in
            raise
              (Babuso_exn
                 ( General_error msg,
                   Some "Babuso.State.Directory_key_values.add" ))
        | None ->
            add_local ctxt v;
            save_one ctxt state.data_dir v;
            (* save ctxt state ; *)
            (* event_added ctxt v ; *)
            ())

  let remove_protected ctxt id ~remove_local =
    protect ctxt ~f:(fun () ->
        remove_local ctxt id;
        remove_one ctxt id)
end

module Accounts = struct
  let all ctxt =
    let state = get ctxt in
    state.accounts

  module Dir = Directory_key_values (struct
    let subpath = "account"

    include Account

    let all = all
    let mutex ctxt = (get ctxt).accounts_mutex
  end)

  let get_by_id ctxt aid =
    let state = get ctxt in
    match
      List.find state.accounts ~f:String.(fun acc -> Account.id acc = aid)
    with
    | Some s -> s
    | None -> Fmt.failwith "Account not found %s" aid

  let add ctxt acc =
    Dir.add_protected ctxt acc ~add_local:(fun ctxt acc ->
        let state = get ctxt in
        state.accounts <- acc :: state.accounts;
        Events.account_added ctxt acc;
        ())

  let remove_if_exists ctxt id =
    let state = get ctxt in
    if List.exists state.accounts ~f:(fun acc -> String.equal acc.id id) then
      Dir.remove_protected ctxt id ~remove_local:(fun ctxt id ->
          let state = get ctxt in
          state.accounts <-
            List.filter state.accounts ~f:(fun acc ->
                String.equal acc.id id |> not);
          (* save ctxt state ; *)
          Events.account_removed ctxt id;
          ())

  let replace_draft ctxt draft_account =
    let open Account in
    remove_if_exists ctxt draft_account.id;
    add ctxt draft_account;
    ()

  let modify_by_id ctxt acc_id f =
    let self = get ctxt in
    Dir.protect ctxt ~f:(fun () ->
        let open Account in
        let found = ref false in
        self.accounts <-
          List.map self.accounts ~f:(fun a ->
              if String.equal a.id acc_id then (
                let acc = f a in
                found := true;
                if Account.equal a acc then a
                else (
                  Dir.save_one ctxt self.data_dir acc;
                  Events.account_changed ctxt acc.id;
                  acc))
              else a);
        if not !found then
          Failure.raise Docpp.(textf "Account %S not found." acc_id);
        ())

  let load_all = Dir.load_all

  let find_by_address ctxt addr =
    let state = get ctxt in
    List.find state.accounts ~f:(fun acc ->
        Option.equal String.equal (Account.get_address acc) (Some addr))
end

module Operations = struct
  let all ctxt =
    let state = get ctxt in
    state.operations

  module Dir = Directory_key_values (struct
    let subpath = "operation"

    include Operation

    let all = all
    let mutex ctxt = (get ctxt).operations_mutex
  end)

  let get_by_id ctxt aid =
    let state = get ctxt in
    match
      List.find state.operations ~f:String.(fun acc -> Operation.id acc = aid)
    with
    | Some s -> s
    | None -> Fmt.failwith "Operation not found %s" aid

  let add ctxt acc =
    Dir.add_protected ctxt acc ~add_local:(fun ctxt acc ->
        let state = get ctxt in
        state.operations <- acc :: state.operations;
        Events.operation_added ctxt acc;
        ())

  let remove ctxt id =
    Dir.remove_protected ctxt id ~remove_local:(fun ctxt id ->
        let state = get ctxt in
        state.operations <-
          List.filter state.operations ~f:(fun acc ->
              String.equal acc.id id |> not);
        (* save ctxt state ; *)
        Events.operation_removed ctxt id;
        ())

  let modify ctxt acc =
    Dir.protect ctxt ~f:(fun () ->
        let self = get ctxt in
        let open Operation in
        self.operations <-
          List.map self.operations ~f:(fun a ->
              if String.equal a.id acc.id then (
                if Operation.equal a acc then a
                else
                  let updated =
                    { acc with last_update = Unix.gettimeofday () }
                  in
                  Dir.save_one ctxt self.data_dir updated;
                  Events.operation_changed ctxt updated.id;
                  updated)
              else a));
    ()

  let replace_draft ctxt draft_op =
    let open Operation in
    let self = get ctxt in
    if List.exists self.operations ~f:(fun op -> String.equal draft_op.id op.id)
    then remove ctxt draft_op.id;
    add ctxt draft_op;
    ()

  let load_all = Dir.load_all
end

module Michelson_code = struct
  let as_file ctxt ~code =
    let state = get ctxt in
    let dir = Path.(state.data_dir // "michelson-contracts") in
    let path =
      Path.(dir // Fmt.str "%s.tz" Caml.Digest.(string code |> to_hex))
    in
    Io.File.make_path ctxt dir;
    Io.File.write_lines ctxt path [ code ];
    path
end

type full_context =
  < clock : Io.Clock.t
  ; events : Events.t
  ; file_io : Io.File.file_io
  ; http_client : Http_client.t
  ; network_information : Monitor_network.t
  ; node_interaction : Node_interaction.t
  ; octez_client : Octez_client.t
  ; shell : Io.Shell.t
  ; signer : Signer.t
  ; state : t
  ; threads : Threads.t >

let load ctxt data_dir : full_context =
  let open Read in
  let configuration =
    let default = Wallet_configuration.make ~port:8480 ~max_connections:32 () in
    with_of_sexp ctxt data_dir "configuration" Wallet_configuration.t_of_sexp
      default
  in
  let accounts = Accounts.load_all ctxt data_dir in
  let operations = Operations.load_all ctxt data_dir in
  let state = make data_dir ~configuration ~accounts ~operations in
  let shell = Io.Shell.default () in
  let file_io = Io.File.default () in
  let clock = Io.Clock.default () in
  let events = Events.create () in
  let threads = Threads.init () in
  let http_client = Http_client.make () in
  let network_information = Monitor_network.create () in
  let node_interaction = Node_interaction.make () in
  let signer = Signer.make () in
  let octez_client =
    Octez_client.make
      ~executable:(Wallet_configuration.octez_client_executable configuration)
      ~data_dir:Path.(data_dir // "main-octez-client-data")
      ~faking_it:(Wallet_configuration.fake_tezos_client configuration)
      ()
  in
  let ctxt =
    object
      method state = state
      method file_io = file_io
      method shell = shell
      method clock = clock
      method events = events
      method threads = threads
      method http_client = http_client
      method network_information = network_information
      method node_interaction = node_interaction
      method signer = signer
      method octez_client = octez_client
    end
  in
  ctxt

let save_configuration ctxt data_dir configuration =
  Write.sexp ctxt data_dir "configuration" configuration
    Wallet_configuration.sexp_of_t;
  ()

let update_configuration ctxt configuration =
  let self = get ctxt in
  self.configuration <- configuration;
  save_configuration ctxt self.data_dir configuration;
  Events.configuration_update ctxt configuration;
  ()

let _save ctxt (* unused any more *)
    {
      data_dir;
      configuration;
      main_client = _;
      accounts;
      operations;
      update_loop_errors = _;
      latest_exchange_rates = _;
      accounts_mutex = _;
      operations_mutex = _;
    } =
  Io.File.make_path ctxt data_dir;
  save_configuration ctxt data_dir configuration;
  List.iter accounts ~f:(fun acc ->
      Write.sexp ctxt data_dir
        (Fmt.str "account/%s" acc.id)
        acc Account.sexp_of_t);
  List.iter operations ~f:(fun op ->
      Write.sexp ctxt data_dir
        (Fmt.str "operation/%s" op.id)
        op Operation.sexp_of_t);
  (* Option.iter main_client ~f:(fun client ->
      Write.sexp ctxt data_dir "main-client" client Octez_client.sexp_of_t ) ; *)
  ()

module Exchange_rates = struct
  let set ctxt rates =
    Events.new_exchange_rates ctxt rates;
    (get ctxt).latest_exchange_rates <- Some rates;
    ()

  let get_latest ctxt = (get ctxt).latest_exchange_rates

  let timestamp ctxt =
    match (get ctxt).latest_exchange_rates with
    | Some s -> s.Exchange_rates.timestamp
    | None -> Float.min_value
end

module RPC = struct
  let a_node ctxt = Monitor_network.one_node ctxt |> fst
  let getf ctxt path = Fmt.kstr (Octez_node.rpc ctxt (a_node ctxt)) path

  let postf ctxt ~body path =
    Fmt.kstr (Octez_node.rpc ~meth:(`Post body) ctxt (a_node ctxt)) path

  let get_balance ctxt ~address =
    getf ctxt "/chains/main/blocks/head/context/contracts/%s/balance" address
    |> Json.Q.string |> Z.of_string

  let get_manager_key ctxt ~address =
    match
      getf ctxt "/chains/main/blocks/head/context/contracts/%s/manager_key"
        address
    with
    | `Null -> None
    | `String pk -> Some pk
    | other -> Fmt.failwith "get_manager_key: Wrong JSON: %a" Json.pp other

  let storage ctxt ~address =
    let body = Json.C.("unparsing_mode" --> string "Optimized_legacy") in
    postf ctxt ~body
      "/chains/main/blocks/head/context/contracts/%s/storage/normalized" address
    |> Tezai_michelson.Untyped.of_json

  let entrypoints ctxt ~address =
    let json =
      getf ctxt "/chains/main/blocks/head/context/contracts/%s/entrypoints"
        address
    in
    let open Json.Q in
    field json ~k:"entrypoints"
    |> obj
    |> List.map ~f:(fun (k, v) ->
           ( k,
             Tezai_michelson.Untyped.of_json v
             |> Tezai_contract_metadata_manipulation.Micheline_helpers
                .normalize_combs ~primitive:"pair" ))

  let script ctxt ~address =
    let script =
      getf ctxt "/chains/main/blocks/head/context/contracts/%s/script" address
    in
    let code_mich =
      match script with
      | `O (("code", code) :: _) -> Tezai_michelson.Untyped.of_json code
      | _ -> assert false
    in
    let open Tezai_michelson.Untyped_contract in
    let contract_storage_type =
      get_storage_type_exn code_mich
      |> Tezai_contract_metadata_manipulation.Micheline_helpers.normalize_combs
           ~primitive:"pair"
    in
    let contract_parameter_type =
      get_parameter_type_exn code_mich
      |> Tezai_contract_metadata_manipulation.Micheline_helpers.normalize_combs
           ~primitive:"pair"
    in
    let contract_code = get_code_exn code_mich in
    object
      method storage_type = contract_storage_type
      method parameter_type = contract_parameter_type
      method code = contract_code
    end

  let typecheck_data ctxt ~data ~t =
    let body =
      Json.C.(
        ("data" --> Michelson.to_json data) @@@ ("type" --> Michelson.to_json t))
    in
    try
      let (_ : Json.t) =
        postf ctxt ~body
          "/chains/main/blocks/head/helpers/scripts/typecheck_data"
      in
      true
    with _ -> false
end

let record_loop_error ctxt ex =
  let state = get ctxt in
  state.update_loop_errors <- ex :: state.update_loop_errors;
  match ex with
  | Babuso_exn e ->
      dbgp
        Docpp.(
          box ~indent:2
            (text
               ("Babuso LOOOP ERROR:"
               ^ Sexp.to_string (sexp_of_tagged_babuso_error e))))
  | exception e ->
      dbgp Docpp.(box ~indent:2 (text "LOOP ERROR:" +++ Exception.to_pp e))
  | other ->
      dbgp
        Docpp.(
          box ~indent:2 (text "Unknown LOOP ERROR:" +++ Exception.to_pp other))

let data_dir_cmdliner_term () =
  let open Cmdliner in
  Arg.(
    let env = Cmd.Env.info ~doc:"State location path." "BABUSO_ROOT" in
    value
      (opt string
         Path.(Caml.Sys.getenv "HOME" // ".babuso")
         (info [ "state-path" ] ~env ~doc:"Set the State location.")))

(** From the point of view of the “state” the configuration is the data-dir
    “plus” the actual [Configuration.t]. *)
let configuration_cmdliner_term () =
  let open Cmdliner in
  let open Term in
  const (fun data_dir configuration -> (data_dir, configuration))
  $ data_dir_cmdliner_term ()
  $ Configuration.cmdliner_term ()

let loading_cmdliner_term () =
  let open Cmdliner in
  let open Term in
  const (fun data_dir ->
      let ctxt =
        object
          method file_io = Io.File.default ()
        end
      in
      load ctxt data_dir)
  $ data_dir_cmdliner_term ()
