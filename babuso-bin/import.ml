include Babuso_lib.Import
include Babuso_unix

module Debug (P : sig
  val name : string
end) =
struct
  let on =
    ref
      (try String.equal "true" (Fmt.kstr Sys.getenv_exn "%s_debug" P.name)
       with _ -> false)

  let color =
    ref
      (match Fmt.kstr Sys.getenv_exn "%s_debug_color" P.name with
      | "none" -> None
      | color -> Some color
      | exception _ -> Some "33m")

  module O = struct
    let dbg (fmt : Caml.Format.formatter -> unit -> unit) =
      let color_start, color_end =
        match !color with
        | None -> ("", "")
        | Some c -> (Fmt.str "\x1b[%s" c, "\x1b[0m")
      in
      if !on then (
        let b = Buffer.create 512 in
        let formatter = Caml.Format.formatter_of_buffer b in
        Caml.Format.(
          let { max_indent; margin } = pp_get_geometry err_formatter () in
          pp_set_geometry formatter ~max_indent ~margin);
        Caml.Format.fprintf formatter
          "@[@<0>%s@[<hov 2>[%s-debug:%s]%s@ @[%a@]@]@]@?\n%!" color_start
          P.name
          (Date.Local.now () |> Date.to_short_string)
          color_end fmt ();
        let bytes = Buffer.contents_bytes b in
        let (_ : int) =
          (* Unix.write is more atomic. *)
          Unix.write Unix.stderr bytes 0 (Bytes.length bytes)
        in
        ())

    let dbgp pp = dbg (fun ppf () -> Pp.to_fmt ppf pp)
    let dbgf fmt = Fmt.kstr (fun s -> dbgp (Pp.verbatim s)) fmt
  end

  module Timing = struct
    open O

    let measure name f =
      let start = Unix.gettimeofday () in
      dbgp Docpp.(textf "Start %s %f" name start);
      Exn.protect ~f ~finally:(fun () ->
          let ending = Unix.gettimeofday () in
          dbgp Docpp.(textf "End %s %f" name (ending -. start)))
  end
end

module Debug_app = Debug (struct
  let name = "babuso"
end)

include Debug_app.O

module Thread_extra = struct
  module Sys = Caml.Sys

  exception Timeout

  let delayed_fun f x timeout =
    let () =
      Sys.set_signal Sys.sigalrm (Sys.Signal_handle (fun _ -> raise Timeout))
    in
    let alarm secs =
      let (_ : int) = Unix.alarm secs in
      ()
    in
    alarm timeout;
    try
      let r = f x in
      alarm 0;
      r
    with e ->
      alarm 0;
      raise e
end

module Threads = struct
  module Preemptive = struct
    type t = Thread of int [@@deriving sexp, compare, variants, equal, hash]

    let pp ppf = function Thread i -> Fmt.pf ppf "{%d}" i
  end

  type t = {
    all : (Preemptive.t, unit Lwt.t) Hashtbl.t;
    mutable next_id : int;
    end_condition : unit Lwt_condition.t;
    lwt_main : Thread.t;
  }

  let _global : t option ref = ref None
  let _get ctxt : t = ctxt#threads

  let spawn ctxt f =
    let self = _get ctxt in
    Lwt_preemptive.run_in_main
      Lwt.(
        fun () ->
          let key = Preemptive.thread self.next_id in
          let th = Lwt_preemptive.detach f () in
          self.next_id <- self.next_id + 1;
          Hashtbl.add_exn self.all ~key ~data:th;
          return key)

  let spawn_unit ctxt f =
    let (_ : Preemptive.t) = spawn ctxt f in
    ()

  let lwt_run ctxt f =
    let (_ : t) = _get ctxt in
    Lwt_preemptive.run_in_main f

  let init () =
    match !_global with
    | Some t ->
        dbgf "WARNING: called Threads.init more than once";
        t
    | None ->
        Lwt_preemptive.init 2 24 (dbgf "Lwt-preemptive log: %s");
        let end_condition = Lwt_condition.create () in
        let lwt_main () =
          let open Lwt in
          let rec loop c =
            Lwt_unix.sleep 1. >>= fun () ->
            (* dbgf "lwt_main heartbeat %d" c ; *)
            loop (c + 1)
          in
          let main () =
            Lwt.pick
              [
                loop 0;
                begin
                  Lwt_condition.wait end_condition >>= fun () ->
                  dbgf "lwt_main woken up by condition";
                  return ()
                end;
              ]
          in
          Lwt_main.run (main ())
        in
        let (thr : Thread.t) = Thread.create lwt_main () in
        let self =
          {
            all = Hashtbl.create (module Preemptive);
            next_id = 0;
            end_condition;
            lwt_main = thr;
          }
        in
        _global := Some self;
        self

  (* let wait = Thread.join *)

  let kill ctxt =
    let self = _get ctxt in
    Hashtbl.iteri self.all ~f:(fun ~key ~data ->
        match Lwt.state data with
        | Lwt.Return _ | Lwt.Fail _ -> ()
        | Lwt.Sleep -> dbgf "Thread %a is not done." Preemptive.pp key);
    Lwt_condition.broadcast self.end_condition ()
end

module Thread = struct
  let create _ _ = `Use_threads_dot_spawn
end
