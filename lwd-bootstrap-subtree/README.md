# lwd-bootstrap

> _Small wrapper around Tyxml-Lwd and Twitter's Bootstrap_

Library providing “an HTML monoid” with Bootstrap constructs.

This project is mostly developed in a monorepo at <https://gitlab.com/oxheadalpha/flextesa-tzcomet-monorepo/>.

## Install

```sh
opam exec -- opam install --with-test --with-doc lwd-bootstrap.opam
```

