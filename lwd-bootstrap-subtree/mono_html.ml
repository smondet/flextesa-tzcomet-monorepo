module H5 = Tyxml_lwd.Html
module List = ListLabels

type 'a t = 'a H5.elt (* list Lwd.t *)

let t (s : string) : _ t = H5.(txt (Lwd.pure s))
let empty () = Lwd.pure Lwd_seq.empty
let ( % ) a b : _ t = Lwd.map2 ~f:Lwd_seq.concat a b
let ( %% ) a b : _ t = a % t " " % b
let singletize f ?a x = f ?a [ x ]

let list = function
  | [] -> empty ()
  | one :: more -> List.fold_left more ~init:one ~f:( % )

let parens c = t "(" % c % t ")"

module H = struct
  let p ?a l = singletize H5.p ?a l
  let i ?a l = singletize H5.i ?a l
  let b ?a l = singletize H5.b ?a l
  let em ?a l = singletize H5.em ?a l
  let code ?a l = singletize H5.code ?a l
  let button ?a l = singletize H5.button ?a l
  let span ?a l = singletize H5.span ?a l
  let sub ?a l = singletize H5.sub ?a l
  let sup ?a l = singletize H5.sup ?a l
  let small ?a l = singletize H5.small ?a l
  let strong ?a l = singletize H5.strong ?a l
  let abbr ?a l = singletize H5.abbr ?a l
  let a ?a l = singletize H5.a ?a l
  let div ?a l = singletize H5.div ?a l
  let pre ?a l = singletize H5.pre ?a l
  let h1 ?a l = singletize H5.h1 ?a l
  let h2 ?a l = singletize H5.h2 ?a l
  let h3 ?a l = singletize H5.h3 ?a l
  let h4 ?a l = singletize H5.h4 ?a l
  let h5 ?a l = singletize H5.h5 ?a l
  let h6 ?a l = singletize H5.h6 ?a l
  let tr ?a l = singletize H5.tr ?a l
  let td ?a l = singletize H5.td ?a l
  let th ?a l = singletize H5.th ?a l
  let blockquote ?a l = singletize H5.blockquote ?a l
  let nav ?a l = singletize H5.nav ?a l
end

let hr = H5.hr
let br = H5.br

include H

let classes l = H5.a_class (Lwd.pure l)
let style s = H5.a_style (Lwd.pure s)
let it s = i (t s)
let bt s = b (t s)
let ct s = code (t s)

let link ~target ?(a = []) content =
  H.a ~a:(H5.a_href (Lwd.pure target) :: a) content

let url ?a t u = link ?a ~target:u (t u)
let abbreviation title c = H5.abbr ~a:[ H5.a_title (Lwd.pure title) ] [ c ]

let make_action action =
  Tyxml_lwd.Lwdom.attr (fun _ ->
      action ();
      false)

let abbreviation_with_toggle ?state title ~abbreviated ~expanded =
  let state =
    match state with Some s -> s | None -> Reactive.var `Abbreviated
  in
  Reactive.bind_var state ~f:(function
    | `Abbreviated ->
        H5.abbr
          ~a:
            [
              H5.a_title (Lwd.pure title);
              H5.a_onclick
                (make_action (fun () -> Reactive.set state `Expanded));
            ]
          [ abbreviated ]
    | `Expanded ->
        H5.span
          ~a:
            [
              H5.a_title (Lwd.pure title);
              H5.a_onclick
                (make_action (fun () -> Reactive.set state `Abbreviated));
            ]
          [ expanded ])

let button ?(a = []) ~action k =
  H.button ~a:(H5.a_onclick (make_action action) :: a) k

let onclick_action action =
  H5.a_onclick
    (Tyxml_lwd.Lwdom.attr (fun _ ->
         action ();
         true))

let itemize ?(numbered = false) ?a_ul ?a_li l =
  (if not numbered then H5.ul else H5.ol)
    ?a:a_ul
    (List.map l ~f:(fun item -> H5.li ?a:a_li [ item ]))

let details ~summary content =
  H5.details (singletize H5.summary summary) [ content ]

let input_bidirectional ?(a = []) bidi =
  H5.input
    ~a:
      (a
      @ [
          H5.a_value (Reactive.Bidirectional.get bidi);
          H5.a_oninput
            (Tyxml_lwd.Lwdom.attr
               Js_of_ocaml.(
                 fun ev ->
                   Js.Opt.iter ev##.target (fun input ->
                       Js.Opt.iter (Dom_html.CoerceTo.input input) (fun input ->
                           let v = input##.value |> Js.to_string in
                           Reactive.Bidirectional.set bidi v));
                   true));
        ])
    ()

module Unsafe = struct
  let html_string : element_name:string -> string -> 'a t =
   fun ~element_name s ->
    let open Js_of_ocaml in
    let dom_node : Tyxml_lwd.raw_node =
      let domelt : Js_of_ocaml.Dom_html.element Js_of_ocaml.Js.t =
        Dom_html.document##createElement (Js.string element_name)
      in
      domelt##.innerHTML := Js.string s;
      (domelt :> Tyxml_lwd.raw_node)
    in
    Tyxml_lwd.Lwdom.elt dom_node |> H5.tot

  let span_string = html_string ~element_name:"span"
  let div_string = html_string ~element_name:"div"
end
