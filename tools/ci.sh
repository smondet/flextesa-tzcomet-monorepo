#! /bin/sh

set -e


say () { printf "[ci.sh:$(date "+%Y%m%d-%H%M%S")]: $@\n" >&2 ; } 

usage () {
cat >&2 <<EOF
$0 <cmd>

See real usage in '.gitlab-ci.yml'.

Locally one can try:

    testing=true sh tools/ci.sh ensure_image
    testing=true sh tools/ci.sh run_all

You may need this once in a while, because we assume the CI does it by itself:

    docker pull ocaml/opam:alpine-3.15-ocaml-4.13

The current image which may or may not have been built by the CI yet would be:

    $(CI_REGISTRY_IMAGE=registry.gitlab.com/oxheadalpha/flextesa-tzcomet-monorepo: build_image_uri)

EOF
}

if [ "$testing" = "true" ] ; then
    say "Testing mode"
    export CI_REGISTRY_IMAGE=local-babuso
fi

docker_login () {
    docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
}

base_image=ocaml/opam:alpine-3.15-ocaml-4.13

interesting_files="
./switch.opam
flextesa/src/scripts/get-octez-static-binaries.sh
flextesa/src/scripts/get-zcash-params.sh
"

interesting_files_basenames () {
    for f in $interesting_files ; do printf "./%s " $(basename $f) ; done
}

build_dockerfile () {
    cat <<EOF
FROM $base_image
# The default is still 2.0:
RUN sudo cp /usr/bin/opam-2.1 /usr/bin/opam
RUN sudo apk update
RUN sudo apk add geckodriver --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted
# We avoid a weirdness with libssl1 Vs openssl-dev-1.1.1n-r0
RUN sudo apk add openssl-dev curl-dev firefox
ADD --chown=opam:opam $(interesting_files_basenames) ./
RUN opam switch import ./switch.opam
RUN sudo sh ./get-octez-static-binaries.sh /usr/bin
RUN sudo sh ./get-zcash-params.sh /usr/share/zcash-params
EOF

}

build_dependencies_hash=$({ \
 build_dockerfile ; cat ./switch.opam ; \
 cat flextesa/src/scripts/get-octez-static-binaries.sh ; echo "$base_image" ; } \
 | md5sum | cut -d' ' -f 1)

build_image_uri () {
    printf "%s" "${CI_REGISTRY_IMAGE}:${build_dependencies_hash}-build"
}



ensure_image () {
    say "Ensuring $(build_image_uri)"
    docker pull  $(build_image_uri) || (
        say "Need to build it :("
        tmpdir=$(mktemp -d)
        say "Getting $interesting_files"
        cp $interesting_files "$tmpdir/"
        cd "$tmpdir"
        build_dockerfile > ./Dockerfile
        find .
        docker build . -t $(build_image_uri)
        if [ "$testing" = "true" ] ; then
            say "Testing mode: not pushing $(build_image_uri)"
        else
            docker push $(build_image_uri)
        fi
    )
}

run_all_local () {
    say "Building:"
    ./please.sh build
    say "Unit tests:"
    ./please.sh test
    say "Linting:"
    ./please.sh lint
    git diff --exit-code
    say "Integration tests:"
    octez_binaries=/usr/bin/ ./please.sh longtests
}
run_all_inside_docker () {
    git config --global --add safe.directory /data
    git status
    run_all_local
}
run_all () {
    docker run -v "$PWD:/data" --workdir /data $(build_image_uri) \
           sh tools/ci.sh run_all_inside_docker
}

{ if [ "$1" = "" ] ; then usage ; else "$@" ; fi ; }
