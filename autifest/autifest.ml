open! Base

let default_dot_ocamlformat =
  {|version=0.24.1
profile=default
exp-grouping=preserve
parse-docstrings
|}

module Path = struct
  include Caml.Filename

  let ( // ) = concat
end

module Metadata = struct
  type dep = {
    name : string; [@main]
    version : [ `Equal of string | `At_least of string ] option;
    with_test : bool; [@default false]
  }
  [@@deriving sexp, fields, make]

  type t = {
    synopsis : string option;
    description : string option;
    authors : string list;
    maintainers : string list;
    license : string;
    version : string option;
    depends : dep list;
    source : [ `Gitlab of string ] option;
  }
  [@@deriving sexp, fields, make]

  let to_dune_project_blurb self =
    let b = ref [] in
    let line l = b := l :: !b in
    let opt o f = Option.iter o ~f in
    let string k s = Fmt.kstr line "(%s %S)" k s in
    let opt_string o k = opt o (string k) in
    let opt_string_list l k =
      match l with
      | [] -> ()
      | l ->
          Fmt.kstr line "(%s" k;
          List.iter l ~f:(fun s -> Fmt.kstr line "  %S" s);
          Fmt.kstr line ")"
      (* Fmt.kstr line "(%s %a)" k Fmt.(list ~sep:(any " ") Dump.string) l *)
    in
    opt_string_list self.authors "authors";
    opt_string_list self.maintainers "maintainers";
    opt_string self.synopsis "synopsis";
    opt_string self.description "description";
    opt_string self.version "version";
    opt self.source (function `Gitlab gl ->
        Fmt.kstr (string "homepage") "https://gitlab.com/%s" gl;
        Fmt.kstr (string "bug_reports") "https://gitlab.com/%s/-/issues" gl);
    Fmt.kstr line "(license %S)" self.license;
    line "(depends";
    List.iter self.depends ~f:(fun { name; version; with_test } ->
        let ope, clo =
          match (version, with_test) with
          | None, false -> ("", "")
          | _ -> ("(", ")")
        in
        Fmt.kstr line "  %s%s%s%s%s" ope name
          (Option.value_map version ~default:"" ~f:(function
            | `Equal v -> Fmt.str " (= %S)" v
            | `At_least v -> Fmt.str " (>= %S)" v))
          (if with_test then " :with-test" else "")
          clo);
    line ")";
    List.rev !b
end

module Dune_project = struct
  type t = Basic_library of { libraries : string list } | Custom
  [@@deriving sexp, variants]

  let apply proj ~path ~name ~metadata =
    let dune_project =
      [
        "(lang dune 2.9)";
        "(generate_opam_files true)";
        Fmt.str "(name %s)" name;
        Fmt.str "(package";
        Fmt.str "  (name %s)" name;
      ]
      @ (Metadata.to_dune_project_blurb metadata |> List.map ~f:(Fmt.str "  %s"))
      @ [ ")" ]
    in
    Stdio.Out_channel.write_lines Path.(path // "dune-project") dune_project;
    begin
      match proj with
      | Custom -> ()
      | Basic_library { libraries } ->
          let dune_file =
            Fmt.str
              {|;; This a generated dune file

(library
 (name %s)
 (public_name %s)
 (inline_tests)
 (preprocess
  (pps ppx_inline_test ppx_expect))
 (libraries %s))
|}
              (String.map name ~f:(function '-' -> '_' | c -> c))
              name
              (String.concat ~sep:" " libraries)
          in
          Stdio.Out_channel.write_all Path.(path // "dune") ~data:dune_file
    end;
    if
      Fmt.kstr Caml.Sys.command "dune build %s"
        Path.(path // Fmt.str "%s.opam" name |> quote)
      = 0
    then ()
    else Fmt.failwith "Building opam file failed!";
    ()
end

module Project = struct
  type t = {
    name : string;
    path : string;
    ocamlformat : [ `Default | `No ];
    dune_project : Dune_project.t option;
    metadata : Metadata.t;
  }
  [@@deriving sexp, fields]

  let make ?dune_project ?(ocamlformat = `Default) ~metadata ?path name =
    let path = Option.value path ~default:name in
    { name; path; ocamlformat; dune_project; metadata }

  let apply p =
    begin
      match p.ocamlformat with
      | `No -> ()
      | `Default ->
          Stdio.Out_channel.write_all
            Path.(p.path // ".ocamlformat")
            ~data:default_dot_ocamlformat
    end;
    Option.iter p.dune_project
      ~f:(Dune_project.apply ~path:p.path ~name:p.name ~metadata:p.metadata);
    ()
end

module Repository = struct
  type task =
    | Gitlab_ci of (string * (path:string -> local:bool -> string list)) list
    | License of [ `MIT ]
    | Readme of string list
  [@@deriving sexp, variants]

  type t = {
    location : [ `Gitlab of string ];
    subtree : string;
    tasks : task list;
  }
  [@@deriving sexp, fields, make]

  let gitlab ?(tasks = []) ?subtree name =
    let subtree =
      match subtree with Some s -> s | None -> Path.basename name
    in
    make ~location:(`Gitlab name) ~tasks () ~subtree

  module Build_instructions = struct
    type pin = { package : string; local_path : string; remote_uri : string }
    [@@deriving sexp, fields, make]

    type t =
      | Opam_install_files of { opam_files : string list; pins : pin list }
    [@@deriving sexp, variants]

    let to_commands ?(local = false) ?(map_paths = Fn.id) = function
      | Opam_install_files { opam_files; pins } ->
          List.map pins ~f:(fun { package; local_path; remote_uri } ->
              Fmt.str "opam pin add -n %s %s" package
                (if local then local_path else remote_uri))
          @ [
              Fmt.str "opam exec -- opam install --with-test --with-doc %s"
                (String.concat ~sep:" " (List.map ~f:map_paths opam_files));
            ]
  end

  module Gitlab_ci_job = struct
    type action = Build of Build_instructions.t [@@deriving sexp, variants]

    type t = {
      name : string;
      image : string;
      setup : string list;
      actions : action list;
    }
    [@@deriving sexp, fields, make]

    let ocaml_job ?(version = `V413) name actions =
      let image, setup =
        match version with
        | `V413 ->
            ( "ocaml/opam:alpine-3.15-ocaml-4.13",
              [ "sudo cp /usr/bin/opam-2.1 /usr/bin/opam"; "sudo apk update" ]
            )
      in
      make ~name ~image ~setup ~actions ()

    let to_task job_list =
      gitlab_ci
        (List.map job_list ~f:(fun { name; image; setup; actions } ->
             ( name,
               fun ~path ~local ->
                 let intro =
                   [ Fmt.str "  image: %s" image; "  script:" ]
                   @ List.map setup ~f:(Fmt.str "    - %s")
                 in
                 let script =
                   List.concat_map actions ~f:(function Build build ->
                       Build_instructions.to_commands ~local
                         ~map_paths:(Path.concat path) build)
                 in
                 intro @ List.map script ~f:(Fmt.str "    - %s") )))
  end

  let apply self =
    let open Stdio in
    List.iter self.tasks ~f:(function
      | License `MIT ->
          let lines = In_channel.read_lines "tzcomet/LICENSE" in
          Out_channel.write_lines Path.(self.subtree // "LICENSE") lines
      | Readme lines ->
          Out_channel.write_lines Path.(self.subtree // "README.md") lines
      | Gitlab_ci jobs ->
          let open Stdio.Out_channel in
          let here = ref (Stdio.In_channel.read_lines ".gitlab-ci.yml") in
          with_file
            Path.(self.subtree // ".gitlab-ci.yml")
            ~f:(fun outchan ->
              List.iter jobs ~f:(fun (name, make_content) ->
                  let here_name = Fmt.str "%s-%s" self.subtree name in
                  let writing = ref false in
                  let written = ref false in
                  let local_content =
                    Fmt.str "%s: # Generated job" here_name
                    :: make_content
                         ~path:(Fmt.str "./%s/" self.subtree)
                         ~local:true
                    @ [ "" ]
                  in
                  here :=
                    List.concat_map !here ~f:(fun l ->
                        if !written then [ l ]
                        else if !writing then
                          if String.equal "" l then (
                            written := true;
                            local_content)
                          else []
                        else if String.is_prefix l ~prefix:here_name then (
                          writing := true;
                          [])
                        else [ l ]);
                  if not !written then here := !here @ [ "" ] @ local_content;
                  output_lines outchan
                    ((Fmt.str "%s:" name :: make_content ~local:false ~path:"./")
                    @ [ "" ])));
          write_lines ".gitlab-ci.yml" !here)
end

module All_projects = struct
  let _all : Project.t list ref = ref []

  let add dune_project ?ocamlformat ?path name metadata =
    _all :=
      Project.make ?ocamlformat name ?path ?dune_project ~metadata :: !_all

  let add_custom = add (Some Dune_project.custom)

  let add_basic_library ~dune_library_deps =
    add (Some (Dune_project.basic_library ~libraries:dune_library_deps))

  let iter f = List.iter !_all ~f
end

module All_repositories = struct
  let _all : Repository.t list ref = ref []
  let add r = _all := r :: !_all
  let apply () = List.iter !_all ~f:Repository.apply
end

let () =
  let metadata =
    let people =
      [
        "Seb Mondet <seb@mondet.org>";
        "Mark Nichols <marklnichols@gmail.com>";
        "Phil Saxton <phillipsaxton@gmail.com>";
      ]
    in
    let more_people =
      people @ [ "David Turner <david.turner@oxheadalpha.com>" ]
    in
    Metadata.make ~license:"MIT" ~authors:more_people ~maintainers:people
  in
  let dep = Metadata.make_dep in
  let standard_small_library ?subtree ?(loc = `Gitlab "oxheadalpha") ~deps
      ?(local_deps = []) ~synopsis ~description name () =
    let subtree = match subtree with None -> name | Some s -> s in
    let source, remote_uri =
      match loc with
      | `Gitlab org ->
          ( `Gitlab (Fmt.str "%s/%s" org name),
            Fmt.str "https://gitlab.com/%s/%s.git" org name )
    in
    let metadata =
      let depends = deps @ List.map local_deps ~f:(fun o -> o#dep) in
      metadata ~synopsis ~description ~depends ~source ()
    in
    let build_instructions =
      let pins = List.map local_deps ~f:(fun o -> o#pin) in
      Repository.Build_instructions.opam_install_files ~pins
        ~opam_files:[ Fmt.str "%s.opam" name ]
    in
    let ci =
      Repository.Gitlab_ci_job.(ocaml_job "build" [ build build_instructions ])
    in
    let readme_md =
      let uri_here =
        "https://gitlab.com/oxheadalpha/flextesa-tzcomet-monorepo/"
      in
      [
        Fmt.str "# %s" name;
        "";
        Fmt.str "> _%s_" synopsis;
        "";
        description;
        "";
        Fmt.str "This project is mostly developed in a monorepo at <%s>."
          uri_here;
        "";
        "## Install";
        "";
        "```sh";
      ]
      @ Repository.Build_instructions.to_commands build_instructions
      @ [ "```"; "" ]
    in
    let tasks =
      Repository.
        [ license `MIT; Gitlab_ci_job.to_task [ ci ]; readme readme_md ]
    in
    All_projects.add_custom ~path:subtree name ~ocamlformat:`Default metadata;
    All_repositories.add (Repository.gitlab ~subtree name ~tasks);
    let self_pin =
      Repository.Build_instructions.make_pin ~package:name ~local_path:subtree
        ~remote_uri
    in
    let self_dep = dep name in
    object
      method pin = self_pin
      method dep = self_dep
    end
  in
  let open All_projects in
  let ppx_test_deps =
    [
      dep "ppx_expect" ~version:(`At_least "0.14.0");
      dep "ppx_inline_test" ~version:(`At_least "0.14.0");
    ]
  in
  let current_tezos_micheline = dep "tezos-micheline" ~version:(`Equal "8.3") in
  let _tezai_michelson =
    standard_small_library "tezai-michelson" ~subtree:"tezai-michelson-subtree"
      ~synopsis:"(Untyped) Michelson and Micheline helpers"
      ~description:
        "Library providing a few modules to manipulate Michelson expressions. \
         It uses `tezos-micheline` and it Js-of-ocaml-friendly."
      ~deps:([ current_tezos_micheline; dep "fmt" ] @ ppx_test_deps)
      ()
  in
  let _tezai_base58_digest =
    (* TODO: for real *)
    let name = "tezai-base58-digest" in
    standard_small_library name
      ~synopsis:"Tezos Base58 hash-encodings based on Digestif"
      ~description:
        "Tezos Base58 hash-encodings, including common prefixes, based on the \
         Digestif library (Js-of-OCaml compliant)."
      ~deps:
        (dep "digestif" ~version:(`At_least "0.9.0")
        :: dep "zarith" :: ppx_test_deps)
      ()
  in
  let _tezai_contract_metadata =
    standard_small_library "tezai-contract-metadata"
      ~subtree:"tezai-contract-metadata-subtree"
      ~synopsis:"OCaml Definitions for TZIP-016/012/021 metadata specifications"
      ~description:
        "This is a library which provides data-encoding definitions for Tezos \
         smart contract metadata standards."
      ~local_deps:[ _tezai_base58_digest; _tezai_michelson ]
      ~deps:([ current_tezos_micheline; dep "fmt" ] @ ppx_test_deps)
      ()
  in
  let _tezai_contract_metadata_manipulation =
    standard_small_library "tezai-contract-metadata-manipulation"
      ~synopsis:"Helper functions for TZIP-016/012/021 metadata"
      ~description:
        "This is a library which provides extra helper functions for types \
         defined in tezai-contract-metadata."
      ~local_deps:
        [ _tezai_base58_digest; _tezai_michelson; _tezai_contract_metadata ]
      ~deps:
        ([ current_tezos_micheline; dep "re"; dep "base"; dep "fmt" ]
        @ ppx_test_deps)
      ()
  in
  let _lwd_bootstrap =
    standard_small_library "lwd-bootstrap" ~subtree:"lwd-bootstrap-subtree"
      ~deps:[ dep "tyxml-lwd"; dep "lwt" ]
      ~synopsis:"Small wrapper around Tyxml-Lwd and Twitter's Bootstrap"
      ~description:
        "Library providing “an HTML monoid” with Bootstrap constructs." ()
  in
  let _lwd_bootstrap_generator =
    standard_small_library "lwd-bootstrap-generator"
      ~deps:[ dep "fmt"; dep "base"; dep "base64"; dep "stdio" ]
      ~synopsis:"Generator for HTML files using lwd-bootstrap"
      ~description:
        "Application for generating `index.html` files with your build system."
      ()
  in
  add_custom "autifest"
    (metadata ~depends:[ dep "fmt"; dep "base"; dep "stdio" ] ());
  ()

let () =
  match Caml.Sys.argv.(1) with
  | "apply" ->
      All_projects.iter Project.apply;
      All_repositories.apply ();
      Fmt.epr "autifest: Done, don't forget to `dune build @fmt`\n%!"
  | (exception _) | _ ->
      Fmt.epr "usage: autifest.exe {apply}!\n%!";
      Caml.exit 2
