open Tezos_micheline

type t = (int, string) Micheline.node

let of_canonical_micheline m = Micheline.(root m)
let to_canonical_micheline m = Micheline.strip_locations m

let of_micheline_node ?(make_location = fun _ -> 0) node =
  Micheline.map_node make_location (fun x -> x) node

module C = struct
  let int i = Micheline.(Int (0, i))
  let inti i = Micheline.(Int (0, Z.of_int i))
  let string s = Micheline.(String (0, s))
  let bytes s = Micheline.(Bytes (0, s))

  let prim ?(annotations = []) s l =
    let ann = annotations in
    Micheline.(Prim (0, s, l, ann))

  let seq l = Micheline.(Seq (0, l))
  let t_or l = prim "or" l
  let t_unit = prim "unit" []
  let t_pair l = prim "pair" l
  let t_nat = prim "nat" []
  let t_string = prim "string" []
  let t_mutez = prim "mutez" []
  let t_operation = prim "operation" []
  let t_option o = prim "option" [ o ]
  let t_list o = prim "list" [ o ]
  let t_lambda f t = prim "lambda" [ f; t ]
  let t_key_hash = prim "key_hash" []
  let t_key = prim "key" []
  let t_address = prim "address" []
  let e_unit = prim "Unit" []
  let e_none = prim "None" []
  let e_some v = prim "Some" [ v ]
  let seq_map l ~f = seq (List.map f l)
  let e_right e = prim "Right" [ e ]
  let e_left e = prim "Left" [ e ]
  let e_pair a b = prim "Pair" [ a; b ]
  let push t v = prim "PUSH" [ t; v ]
  let fail_with s = [ push t_string (string s); prim "FAILWITH" [] ]

  let assert_some =
    prim "IF_NONE" [ seq (fail_with "none"); seq [ prim "RENAME" [] ] ]

  let t_entrypoints l =
    t_or
      (ListLabels.map l ~f:(function
        | k, Micheline.Prim (l, t, arg, ann) ->
            Micheline.Prim (l, t, arg, Fmt.str "%%%s" k :: ann)
        | _, _ -> Fmt.failwith "t_entrypoints: not a primitive!"))

  let script ~parameter ~storage code =
    seq
      [
        prim "parameter" [ parameter ];
        prim "storage" [ storage ];
        prim "code" [ seq code ];
      ]

  let concrete s =
    match Micheline_parser.tokenize s with
    | tokens, [] -> (
        match Micheline_parser.parse_expression ~check:false tokens with
        | node, [] ->
            of_micheline_node
              ~make_location:(fun loc -> loc.Micheline_parser.start.byte)
              node
        | _, errs ->
            Fmt.failwith "Untyped.C.concrete %s: %a" s
              Tezos_error_monad.Error_monad.pp_print_error errs)
    | _, errs ->
        Fmt.failwith "Untyped.C.concrete %s: %a" s
          Tezos_error_monad.Error_monad.pp_print_error errs
end

module M = struct
  include Micheline
end

let pp ppf m =
  Fmt.pf ppf "%a" Micheline_printer.print_expr
    (Micheline_printer.printable Base.Fn.id (to_canonical_micheline m))

(** See src/proto_alpha/lib_protocol/michelson_v1_primitives.ml
And you can use {|
     '<,'>s/ [ITKH]_\(\w*\)/"\1"/
|}
 *)
let primitives =
  [
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("parameter", "parameter");
    ("storage", "storage");
    ("code", "code");
    ("False", "False");
    ("Elt", "Elt");
    ("Left", "Left");
    ("None", "None");
    ("Pair", "Pair");
    ("Right", "Right");
    ("Some", "Some");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("True", "True");
    ("Unit", "Unit");
    ("PACK", "PACK");
    ("UNPACK", "UNPACK");
    ("BLAKE2B", "BLAKE2B");
    ("SHA256", "SHA256");
    ("SHA512", "SHA512");
    ("ABS", "ABS");
    ("ADD", "ADD");
    ("AMOUNT", "AMOUNT");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("AND", "AND");
    ("BALANCE", "BALANCE");
    ("CAR", "CAR");
    ("CDR", "CDR");
    ("CHECK_SIGNATURE", "CHECK_SIGNATURE");
    ("COMPARE", "COMPARE");
    ("CONCAT", "CONCAT");
    ("CONS", "CONS");
    ("CREATE_ACCOUNT", "CREATE_ACCOUNT");
    ("CREATE_CONTRACT", "CREATE_CONTRACT");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("IMPLICIT_ACCOUNT", "IMPLICIT_ACCOUNT");
    ("DIP", "DIP");
    ("DROP", "DROP");
    ("DUP", "DUP");
    ("EDIV", "EDIV");
    ("EMPTY_MAP", "EMPTY_MAP");
    ("EMPTY_SET", "EMPTY_SET");
    ("EQ", "EQ");
    ("EXEC", "EXEC");
    ("FAILWITH", "FAILWITH");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("GE", "GE");
    ("GET", "GET");
    ("GT", "GT");
    ("HASH_KEY", "HASH_KEY");
    ("IF", "IF");
    ("IF_CONS", "IF_CONS");
    ("IF_LEFT", "IF_LEFT");
    ("IF_NONE", "IF_NONE");
    ("INT", "INT");
    ("LAMBDA", "LAMBDA");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("LE", "LE");
    ("LEFT", "LEFT");
    ("LOOP", "LOOP");
    ("LSL", "LSL");
    ("LSR", "LSR");
    ("LT", "LT");
    ("MAP", "MAP");
    ("MEM", "MEM");
    ("MUL", "MUL");
    ("NEG", "NEG");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("NEQ", "NEQ");
    ("NIL", "NIL");
    ("NONE", "NONE");
    ("NOT", "NOT");
    ("NOW", "NOW");
    ("OR", "OR");
    ("PAIR", "PAIR");
    ("PUSH", "PUSH");
    ("RIGHT", "RIGHT");
    ("SIZE", "SIZE");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("SOME", "SOME");
    ("SOURCE", "SOURCE");
    ("SENDER", "SENDER");
    ("SELF", "SELF");
    ("STEPS_TO_QUOTA", "STEPS_TO_QUOTA");
    ("SUB", "SUB");
    ("SWAP", "SWAP");
    ("TRANSFER_TOKENS", "TRANSFER_TOKENS");
    ("SET_DELEGATE", "SET_DELEGATE");
    ("UNIT", "UNIT");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("UPDATE", "UPDATE");
    ("XOR", "XOR");
    ("ITER", "ITER");
    ("LOOP_LEFT", "LOOP_LEFT");
    ("ADDRESS", "ADDRESS");
    ("CONTRACT", "CONTRACT");
    ("ISNAT", "ISNAT");
    ("CAST", "CAST");
    ("RENAME", "RENAME");
    ("bool", "bool");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("contract", "contract");
    ("int", "int");
    ("key", "key");
    ("key_hash", "key_hash");
    ("lambda", "lambda");
    ("list", "list");
    ("map", "map");
    ("big_map", "big_map");
    ("nat", "nat");
    ("option", "option");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("or", "or");
    ("pair", "pair");
    ("set", "set");
    ("signature", "signature");
    ("string", "string");
    ("bytes", "bytes");
    ("mutez", "mutez");
    ("timestamp", "timestamp");
    ("unit", "unit");
    ("operation", "operation");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, "FOR" BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("address", "address");
    (* Alpha_002 addition *)
    ("SLICE", "SLICE");
    (* Alpha_005 addition *)
    ("DIG", "DIG");
    ("DUG", "DUG");
    ("EMPTY_BIG_MAP", "EMPTY_BIG_MAP");
    ("APPLY", "APPLY");
    ("chain_id", "chain_id");
    ("CHAIN_ID", "CHAIN_ID")
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
    (* Alpha_008 addition *);
    ("LEVEL", "LEVEL");
    ("SELF_ADDRESS", "SELF_ADDRESS");
    ("never", "never");
    ("NEVER", "NEVER");
    ("UNPAIR", "UNPAIR");
    ("VOTING_POWER", "VOTING_POWER");
    ("TOTAL_VOTING_POWER", "TOTAL_VOTING_POWER");
    ("KECCAK", "KECCAK");
    ("SHA3", "SHA3");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
    (* Alpha_008 addition *)
    ("PAIRING_CHECK", "PAIRING_CHECK");
    ("bls12_381_g1", "bls12_381_g1");
    ("bls12_381_g2", "bls12_381_g2");
    ("bls12_381_fr", "bls12_381_fr");
    ("sapling_state", "sapling_state");
    ("sapling_transaction", "sapling_transaction");
    ("SAPLING_EMPTY_STATE", "SAPLING_EMPTY_STATE");
    ("SAPLING_VERIFY_UPDATE", "SAPLING_VERIFY_UPDATE");
    ("ticket", "ticket");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
    (* Alpha_008 addition *)
    ("TICKET", "TICKET");
    ("READ_TICKET", "READ_TICKET");
    ("SPLIT_TICKET", "SPLIT_TICKET");
    ("JOIN_TICKETS", "JOIN_TICKETS");
    ("GET_AND_UPDATE", "GET_AND_UPDATE");
    (* Alpha_011 addition *)
    ("chest", "chest");
    ("chest_key", "chest_key");
    ("OPEN_CHEST", "OPEN_CHEST");
    (* /!\ NEW INSTRUCTIONS MUST BE ADDED AT THE END OF THE STRING_ENUM, FOR BACKWARD COMPATIBILITY OF THE ENCODING. *)
    ("VIEW", "VIEW");
    ("view", "view");
    ("constant", "constant");
    (* Alpha_012 addition *)
    ("SUB_MUTEZ", "SUB_MUTEZ")
    (* New instructions must be added here, for backward compatibility of the encoding. *)
    (* Keep the comment above at the end of the list *);
  ]

let expr_encoding =
  Micheline.canonical_encoding_v1 ~variant:"michelson_v1"
    (* Data_encoding.Encoding.string *)
    (let open Data_encoding in
    def "michelson.v1.primitives" @@ string_enum primitives)

let encoding =
  let open Data_encoding in
  conv to_canonical_micheline of_canonical_micheline expr_encoding

let of_json (json : Ezjsonm.value) = Data_encoding.Json.destruct encoding json
let to_json mich : Ezjsonm.value = Data_encoding.Json.construct encoding mich
