# tezai-michelson

> _(Untyped) Michelson and Micheline helpers_

Library providing a few modules to manipulate Michelson expressions. It uses `tezos-micheline` and it Js-of-ocaml-friendly.

This project is mostly developed in a monorepo at <https://gitlab.com/oxheadalpha/flextesa-tzcomet-monorepo/>.

## Install

```sh
opam exec -- opam install --with-test --with-doc tezai-michelson.opam
```

