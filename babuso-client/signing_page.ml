open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

module Examples = struct
  let comment s =
    String.split s ~on:'\n'
    |> List.map ~f:(Fmt.str "# %s\n")
    |> String.concat ~sep:""

  let all =
    [
      ("0x05 valid", "0x050707002a0033");
      ( "0x05 valid, multiline",
        "0x05 # <- This is the watermark\n\
         0707\n\
         002a # The 42\n\
         0033 # The fifty-one\n" );
      ("hex invalid", "0707002a003");
      ("0x05 invalid", "0x050707002a00");
      ("garbage", "garbage");
      ( "Valid with guesses",
        comment
          "This contains Mainnet's chain-id,\n\
           Alice's address, and her public key:"
        ^ "0x0507070a000000047a06a77007070a00000016\n\
           00006b82198cb179e8306c1bedd08f12dc863f32\n\
           88860a0000002100d670f72efd9475b62275fae7\n\
           73eb5f5eb1fea4f2a0880e6d21983273bf95a0af\n"
        ^ comment
            {sh|Made with:
 $ octez-client hash data '(Pair "NetXdQprcVkpaWU" "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn")' of type '(pair chain_id (pair address key))'
...
Script-expression-ID-Hash: expru8Z7Embnz3GbD2jRwcdWjavD145wR5SoycbeNaZsd4EG2m7xdr
Raw Script-expression-ID-Hash: 0x5101bb5f54ae42dad5572a013e5db80eae8f6b331a30c11e54b44552e76a996f
Ledger Blake2b hash: 6TDZCq5g4BVH5gby3xz3j491ozmz6wb2xXpBwCLsvYJr
Raw Sha256 hash: 0x011371b0f092c1d67f5214b887af023dfbbe8d47476af2502f0e8ceab7bd8987
Raw Sha512 hash: 0x20eecc4f288f5338da2aa014e244cb0d535f316c4a6cf7608f116e2fb781c1ee31e796647d9335343ff0d666b67970c5ada93cae89da8d631064784d7a47dbd6
Gas remaining: 1039662.302 units remaining
|sh}
      );
      ( "With simple lambda guess",
        comment "This was made with the generic-multisig GUI:"
        ^ "0x05070707070a000000040e1415fa0a00000016018832fe43\n\
           bbe277f09be86660b17c3da0bcf574df000707000005050200\n\
           0000060320053d036d" );
      ( "With transfer inside a lambda",
        comment "This was made with the generic-multisig GUI:"
        ^ "0x05070707070a000000040e1415fa0a00000016018832fe43bbe277f09b\n\
           e86660b17c3da0bcf574df00070700000505020000005d0320053d036d02\n\
           000000500743036e0a0000001600006b82198cb179e8306c1bedd08f12dc\n\
           863f3288860555036c072f020000000f0743036801000000046e6f6e6503\n\
           27020000000203580743036a0080897a0743036c030b034d031b" );
      ("With min-lambda: unit→nat", "0x0502000000080320074303620000");
    ]
end

module Bytes_parsing = struct
  type pos = { line : int; column : int option } [@@deriving make, fields]

  type error =
    | Illegal_character of { c : char; pos : pos }
    | Odd_length of { length : int; pos : pos }
  [@@deriving variants]

  module Guess = struct
    module Michelson_fact = struct
      type t =
        | Bytes_are_address of { bytes : Bytes.t; address : Address.t }
        | Bytes_are_chain_id of { bytes : Bytes.t; network : Network.t }
        | Bytes_are_public_key of { bytes : Bytes.t; key : Public_key.t }
        | Could_be_a_lambda of {
            expression : Michelson.t;
            typing : Michelson_type.t Option.t Wip.t Reactive.var;
          }
      [@@deriving variants]
    end

    type t =
      | Michelson_expression of {
          with_05 : bool;
          expression : Michelson.t;
          facts : Michelson_fact.t list;
        }
    [@@deriving variants]
  end

  module Attempt = struct
    type t = Bytes_rep.t -> Guess.t

    let michelson_expression ctxt : t =
     fun bytes ->
      let bin = Bytes_rep.to_binary bytes in
      let with_05, bin =
        match String.chop_prefix bin ~prefix:"\x05" with
        | Some b -> (true, b)
        | None -> (false, bin)
      in
      let expression =
        Data_encoding.Binary.of_bytes_exn Michelson.expr_encoding
          (Bytes.of_string bin)
        |> Michelson.of_canonical_micheline
      in
      let facts = ref [] in
      let may_add_fact f = try facts := f () :: !facts with _ -> () in
      let rec visit ?(on_prim = fun _ ~children:_ ~annotations:_ -> ())
          ?(on_seq = fun _ -> ()) ?(on_bytes = fun _ -> ()) =
        let open Michelson.M in
        let continue l = List.iter l ~f:(visit ~on_prim ~on_seq ~on_bytes) in
        function
        | Int _ -> ()
        | String _ -> ()
        | Bytes (_, b) -> on_bytes b
        | Prim (_, p, children, annotations) ->
            on_prim p ~children ~annotations;
            continue children
        | Seq (_, nodes) ->
            on_seq nodes;
            continue nodes
      in
      let on_bytes b =
        may_add_fact (fun () ->
            let address =
              Tezai_base58_digest.Identifier.Address.(of_bytes b |> to_base58)
            in
            Guess.Michelson_fact.bytes_are_address ~bytes:b ~address);
        may_add_fact (fun () ->
            let key =
              Tezai_base58_digest.Identifier.Generic_signer.Public_key.(
                of_bytes b |> to_base58)
            in
            Guess.Michelson_fact.bytes_are_public_key ~bytes:b ~key);
        may_add_fact (fun () ->
            let chain_id =
              Tezai_base58_digest.Identifier.Chain_id.(
                encode (Bytes.to_string b))
            in
            match
              List.mem
                (Web_app_state.Network_info.known_chain_ids ctxt)
                chain_id ~equal:String.equal
            with
            | true ->
                Guess.Michelson_fact.bytes_are_chain_id ~bytes:b
                  ~network:(Network.of_chain_id chain_id)
            | false -> assert false);
        ()
      in
      let on_seq nodes =
        let lambdaish_prims = ref [] in
        List.iter nodes
          ~f:
            (visit ~on_prim:(fun p ~children:_ ~annotations:_ ->
                 if
                   List.mem
                     [
                       "DROP";
                       "NIL";
                       "DUP";
                       "PUSH";
                       "PAIR";
                       "DIG";
                       "TRANSFER_TOKENS";
                     ]
                     p ~equal:String.equal
                 then lambdaish_prims := p :: !lambdaish_prims));
        if List.length !lambdaish_prims >= 2 then
          may_add_fact (fun () ->
              Guess.Michelson_fact.could_be_a_lambda
                ~expression:(Michelson.C.seq nodes) ~typing:(Wip.idle_var ()))
      in
      visit expression ~on_seq ~on_bytes;
      Guess.michelson_expression ~with_05 ~expression ~facts:(List.rev !facts)
  end

  let attempts ctxt = [ Attempt.michelson_expression ctxt ]

  let make_hashes bytes =
    let bytes = Bytes_rep.to_binary bytes in
    let hex s =
      let (`Hex x) = Hex.of_string s in
      x
    in
    [
      `Ledger_blake2b_base58
        Tezai_base58_digest.(
          Crypto_hash.String.blake2b ~size:32 bytes
          |> Raw.String.No_checksum.to_base58);
      `Sha256_hex (hex (Tezai_base58_digest.Crypto_hash.String.sha256 bytes));
      `Sha512_hex (hex (Tezai_base58_digest.Crypto_hash.String.sha512 bytes));
      `Script_expr_base58
        Tezai_base58_digest.Identifier.Script_expr_hash.(
          hash_string (* B58_hashes.b58_script_id_hash *) bytes |> encode);
    ]

  let all_guesses ctxt bytes =
    List.filter_map (attempts ctxt) ~f:(fun f ->
        try Some (f bytes) with _ -> None)

  let validate_input ctxt text =
    let exception Nope of error in
    let nope v = raise (Nope v) in
    begin
      match Signing_request.t_of_sexp (Sexplib.Sexp.of_string text) with
      | req ->
          let bytes = Signing_request.blob req in
          let hashes = make_hashes bytes in
          let guesses = all_guesses ctxt bytes in
          let res =
            object
              method bytes = bytes
              method signing_request = Some req
              method guesses = guesses
              method hashes = hashes
            end
          in
          Ok res
      | exception _ -> begin
          try
            let lines = String.split text ~on:'\n' in
            let clean =
              List.mapi lines ~f:(fun line content ->
                  match String.split content ~on:'#' with
                  | one :: _ ->
                      let col = ref 0 in
                      let nozerox =
                        match String.chop_prefix one ~prefix:"0x" with
                        | Some chopped -> chopped
                        | None ->
                            col := 2;
                            one
                      in
                      let filtered =
                        String.filter nozerox ~f:(fun c ->
                            Int.incr col;
                            let pos = make_pos ~line ~column:!col () in
                            match c with
                            | ' ' | '\t' -> false
                            | '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' -> true
                            | c -> nope (illegal_character ~c ~pos))
                      in
                      let length = String.length filtered in
                      if length % 2 = 1 then
                        nope (odd_length ~length ~pos:(make_pos ~line ()));
                      filtered
                  | [] -> assert false)
            in
            let cleaned = String.concat ~sep:"" clean in
            let bytes = Bytes_rep.of_hex cleaned in
            let guesses = all_guesses ctxt bytes in
            let hashes = make_hashes bytes in
            let res =
              object
                method bytes = bytes
                method signing_request = None
                method guesses = guesses
                method hashes = hashes
              end
            in
            Ok res
          with Nope error -> Error error
        end
    end
end

let display_guess ctxt guess =
  let open Mono_html in
  let open Bytes_parsing.Guess in
  match guess with
  | Michelson_expression { with_05; expression; facts } -> (
      let fact =
        let byteseem bytes =
          t "The bytes"
          %% Fmt.kstr ct "%s"
               Bytes_rep.(of_binary_bytes bytes |> to_zero_x_summary ~limit:60)
          %% t "seem to be"
        in
        let open Bytes_parsing.Guess.Michelson_fact in
        function
        | Bytes_are_address { bytes; address } ->
            byteseem bytes %% t "the address:"
            %% Custom_widget.account_or_address_link
                 ~add_details:[ `Address_short 12 ]
                 ctxt (`Address address)
        | Bytes_are_chain_id { bytes; network } ->
            byteseem bytes
            %% t "the chain-id for the network:"
            %% Custom_widget.network_link ctxt network
        | Bytes_are_public_key { bytes; key } ->
            byteseem bytes %% t "the public key:"
            %% Custom_widget.account_or_address_link
                 ~add_details:[ `Public_key 18 ]
                 ctxt (`Public_key key)
        | Could_be_a_lambda { expression; typing } ->
            t "The sub-expression"
            %% Custom_widget.big_michelson ctxt expression ~label:(t "Show")
                 ~before_button:(fun () ->
                   ct
                     (Language.string_summary
                        (Michelson.to_concrete expression)))
            %% t "looks like it could be a lambda"
            % Reactive.bind_var typing ~f:(function
                | Wip.Idle ->
                    Server_communication.call_with_var typing
                      (Protocol.Message.Up.guess_michelson_expression_type
                         expression)
                      Protocol.Message.Down.(
                        function
                        | Michelson_typing opt -> opt
                        | _ -> Fmt.failwith "Wrong down message");
                    empty ()
                | Wip.WIP -> t "…" %% Bootstrap.spinner (t "WIP")
                | Wip.Failure s ->
                    Bootstrap.alert ~kind:`Danger (Fmt.kstr ct "FAILURE: %s" s)
                | Wip.Success None ->
                    t ", but we" %% bt "could not"
                    %% t "find how to type-check it."
                | Wip.Success (Some ty) -> (
                    t ", and it does type-check as"
                    %%
                    match ty with
                    | Michelson_type.Lambda_unit_operations ->
                        t "a lambda from" %% ct "unit" %% t "to"
                        %% ct "operation-list"
                        % it
                            ", which is the typical multisig-instructions type."
                    ))
      in
      t "💡 This a valid Michelson expression"
      %% parens
           ((if with_05 then t "including" else bt "Without")
           %% t "the" %% ct "0x05" %% t "watermark")
      % Custom_widget.code_block [ Michelson.to_concrete expression ]
      %
      match facts with
      | [] -> empty ()
      | more -> itemize (List.map ~f:fact more))

let render ctxt =
  let open Mono_html in
  let account = Web_app_state.Signing_page.account ctxt in
  let bytes = Web_app_state.Signing_page.input ctxt in
  let sig_wip = Reactive.var Wip.Idle in
  let validation =
    Reactive.(map (get bytes)) ~f:(function
      | "" -> `Empty
      | input_content ->
          `Parsed (Bytes_parsing.validate_input ctxt input_content))
  in
  let controls =
    bt "Account used to sign:"
    % Custom_widget.Form.choose_an_account ctxt
        (Reactive.Bidirectional.of_var account)
        ~placeholder:(it "Signing Account Required")
        ~filter:Account.can_sign
    %% Reactive.(bind (validation ** get account)) ~f:(fun (valid, acc) ->
           let disabled, emoji, action =
             match (valid, acc) with
             | `Empty, _ | `Parsed (Error _), _ | _, "" -> (true, "✋", Fn.id)
             | `Parsed (Ok parsed), _ ->
                 let action () =
                   Wip.lwt_async sig_wip
                     ~f:
                       Lwt.(
                         fun () ->
                           Server_communication.call
                             (Protocol.Message.Up.sign_blob
                                ~account:(Reactive.peek account)
                                ~blob:parsed#bytes)
                           >>= function
                           | Protocol.Message.Down.Signature blob -> return blob
                           | _ -> assert false)
                 in
                 (false, "✍", action)
           in
           Custom_widget.inline_button ~disabled
             ~a:[ style "min-width: 12em; margin-left: 2em" ]
             (Fmt.kstr t "Sign Now %s" emoji)
             ~action)
  in
  let editor_viewer () =
    let open Custom_widget.Editor in
    let editor =
      Reactive.bind_var sig_wip ~f:(fun wip ->
          let disabled = not (Wip.is_idle wip) in
          low_level_editor ctxt ~disabled ~rows:(Reactive.pure 20)
            (Reactive.Bidirectional.of_var bytes))
    in
    let viewer =
      Reactive.bind_var bytes ~f:(function
        | "" ->
            Bootstrap.p_lead
              (Bootstrap.color `Secondary
                 (it "Please enter something in the Editor."))
        | input_content -> begin
            match Bytes_parsing.validate_input ctxt input_content with
            | Error err ->
                Bootstrap.alert ~kind:`Danger
                  (let open Bytes_parsing in
                  let showpos { line; column } =
                    Fmt.kstr t "At line %d%s:" line
                      (Option.value_map column ~default:""
                         ~f:(Fmt.str " (column: %d)"))
                  in
                  bt "Error:"
                  %%
                  match err with
                  | Illegal_character { c; pos } ->
                      showpos pos %% Fmt.kstr t "Wrong character %C" c
                  | Odd_length { length; pos } ->
                      showpos pos
                      %% Fmt.kstr t "Wrong number of characters %d" length)
            | Ok parsed ->
                begin
                  match parsed#signing_request with
                  | None -> empty ()
                  | Some req ->
                      h3 (t "Signing Request")
                      %% p
                           (t "This request is asking for a signature from"
                           %% Custom_widget.account_or_address_link ctxt
                                ~shorten_public_keys:None
                                (`Public_key (Signing_request.public_key req))
                           % t ".")
                      % list
                          (List.map
                             (Signing_request.metadata req)
                             ~f:
                               begin
                                 function
                                 | `From_address addr ->
                                     div
                                       (bt "From:"
                                       %% Custom_widget.account_or_address_link
                                            ctxt ~shorten_addresses:None
                                            (`Address addr))
                                 | `Comment (key, prose) ->
                                     div (bt "Comment" %% ct key % t ":")
                                     % div
                                         ~a:
                                           [
                                             H5.a_id
                                               (Fmt.kstr Reactive.pure
                                                  "request-comment-%s" key);
                                           ]
                                         (Custom_widget.Human_prose_widget
                                          .option_div ctxt (Some prose))
                                 | `Operation_draft _ ->
                                     div (bt "Operation_draft: TODO")
                               end)
                end
                % begin
                    h3 (t "Guessing Game")
                    %% p
                         (t
                            "This is what we can guess from the bytes \
                             themselves:")
                    %% list
                         (match parsed#guesses with
                         | [] ->
                             [
                               it
                                 "Could not find any insight into those \
                                  bytes … 🤷";
                             ]
                         | more ->
                             List.map more ~f:(fun guess ->
                                 div (display_guess ctxt guess)))
                  end
                % h3 (t "Hashes")
                % list
                    (let hash name formatted =
                       let input_id = "input-" ^ formatted in
                       let as_input =
                         let v = Reactive.var formatted in
                         Bootstrap.Input.bidirectional
                           (Reactive.Bidirectional.of_var v)
                           ~a:
                             [
                               H5.a_disabled () (* ; style "min-width: 30em" *);
                               H5.a_id (Reactive.pure input_id);
                             ]
                       in
                       let open Bootstrap in
                       div_row (div_col 12 (t "🔪" %% b name))
                       % div_row
                           (div_col 10 as_input
                           % div_col 2
                               (Custom_widget.copy_to_clipboard_button ctxt
                                  ~min_width:"5em" ~input_id))
                     in
                     List.map parsed#hashes ~f:(function
                       | `Ledger_blake2b_base58 h ->
                           hash
                             (t "Ledger Nano S/X" %% it "“Sign Hash”"
                             %% t "display, a.k.a. Blake2b+Base58")
                             h
                       | `Sha256_hex h ->
                           hash (t "SHA256 hash, in hexadecimal") h
                       | `Sha512_hex h ->
                           hash (t "SHA512 hash, in hexadecimal") h
                       | `Script_expr_base58 h ->
                           hash
                             (t
                                "Script-ID-Hash, in Base58, useful for RPC \
                                 big-map access")
                             h))
                % h3 (t "“Hex Dump” 🖳")
                % Custom_widget.code_block [ Bytes_rep.hex_dump parsed#bytes ]
          end)
    in
    (* Bootstrap.alert ~kind:`Primary controls
       % *)
    Custom_widget.Debug.(
      bloc ctxt
        (List.map Examples.all ~f:(fun (n, v) ->
             Custom_widget.inline_button (t n) ~action:(fun () ->
                 Reactive.set bytes v))
        |> list))
    % side_by_side ctxt ~editor ~viewer ~editor_tab_title:"Input"
        ~viewer_tab_title:"Parsing Result"
  in
  let clear_button () =
    Custom_widget.inline_button (t "Clear All") ~action:(fun () ->
        Reactive.set sig_wip Wip.idle;
        Reactive.set bytes "";
        Reactive.set account "")
  in
  let signature_result signature =
    let sig_input_id = "sig-result-input" in
    let as_input =
      let v = Reactive.var signature in
      Bootstrap.Input.bidirectional ~cols:8 ~inline_block:true
        (Reactive.Bidirectional.of_var v)
        ~a:[ H5.a_id (Reactive.pure sig_input_id) ]
    in
    Bootstrap.alert ~kind:`Success
      (bt "Signature:" %% as_input
      % Custom_widget.copy_to_clipboard_button ctxt ~input_id:sig_input_id
      % Custom_widget.inline_button (t "Back") ~action:(fun () ->
            Reactive.set sig_wip Wip.idle)
      % clear_button ())
  in
  Custom_widget.wip_div sig_wip
    ~with_spinner:(fun spin ->
      Bootstrap.alert ~kind:`Warning (it "Making signature …" %% spin))
    ~success:signature_result
    ~idle:(fun () ->
      Bootstrap.alert ~kind:`Primary (controls %% clear_button ()))
  % editor_viewer ()
