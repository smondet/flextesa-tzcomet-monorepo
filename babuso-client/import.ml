include Babuso_lib.Import

module Context = struct
  type 'a t = 'a constraint 'a = < .. >
end

module Browser_window = struct
  module Reactive = Lwd_bootstrap.Reactive

  type width_state = [ `Thin | `Wide ]
  type t = { width : width_state option Reactive.var }

  open Js_of_ocaml

  let create ?(threshold = 992) () =
    let find_out () =
      let w = Dom_html.window##.innerWidth in
      if w >= threshold then Some `Wide else Some `Thin
    in
    let width = Reactive.var (find_out ()) in
    Dom_html.window##.onresize :=
      Dom_html.handler (fun _ ->
          let current = Reactive.peek width in
          let new_one = find_out () in
          if Poly.(current <> new_one) then Reactive.set width new_one;
          Js._true);
    { width }

  let get (c : < window : t ; .. > Context.t) = c#window
  let width c = (get c).width |> Reactive.get
end

module Wip = struct
  type 'a t = Idle | Success of 'a | Failure of string | WIP
  [@@deriving sexp, variants, equal, compare]

  open Lwd_bootstrap

  let idle_var () = Reactive.var Idle
  let is_wip = function WIP -> true | _ -> false
  let is_idle = function Idle -> true | _ -> false

  let lwt_async var ~f =
    Reactive.set var wip;
    Lwt.async
      Lwt.(
        fun () ->
          catch
            (fun () ->
              f () >>= fun res ->
              Reactive.set var (success res);
              return_unit)
            (fun exn ->
              let str = match exn with e -> Fmt.str "%a" Exn.pp e in
              Reactive.set var (failure str);
              return_unit))
end

module Date_js = struct
  type t = Js_of_ocaml.Js.date Js_of_ocaml.Js.t

  let of_float seconds : t =
    new%js Js_of_ocaml.Js.date_fromTimeValue (1000. *. seconds)

  let to_iso_string (self : t) = self##toISOString |> Js_of_ocaml__Js.to_string
end

(** A wrapper around Lwd_bootstrap.Boostrap.Collapse to have a consistent style
    for all “show low-level details” buttons. *)
module Low_level_collapse = struct
  open Lwd_bootstrap
  open Bootstrap

  type t = Bootstrapped of Collapse.t

  let make () = Bootstrapped (Collapse.make ())

  let button ?(style = Lwd.pure "width: 12em; font-size: 85%") ~content_of_state
      (Bootstrapped clps) =
    Collapse.make_button clps ~kind:`Info ~style
      (Reactive.bind (Collapse.collapsed_state clps) ~f:content_of_state)

  let div ?a (Bootstrapped clps) content = Collapse.make_div ?a clps content
  let collapsed_state (Bootstrapped c) = Collapse.collapsed_state c
end

module Local_storage : sig
  type t

  val create : unit -> t
  val get : < storage : t ; .. > Context.t -> t
  val available : < storage : t ; .. > Context.t -> bool
  val read_file : < storage : t ; .. > Context.t -> string -> string option

  val write_file :
    < storage : t ; .. > Context.t -> string -> content:string -> unit

  val remove_file : < storage : t ; .. > Context.t -> string -> unit
end = struct
  open Js_of_ocaml

  type t = Js_of_ocaml.Dom_html.storage Js_of_ocaml.Js.t option

  let create () : t =
    Js.Optdef.case
      Dom_html.window##.localStorage
      (fun () ->
        dbgf "Local_storage: nope";
        None)
      (fun x ->
        dbgf "Local_storage: YES length: %d" x##.length;
        Some x)

  let get (c : < storage : t ; .. > Context.t) = c#storage
  let available c = get c |> Option.is_some

  let read_file ctxt path =
    get ctxt
    |> Option.bind ~f:(fun sto ->
           Js.Opt.to_option (sto##getItem (Js.string path))
           |> Option.map ~f:Js.to_string)

  let write_file ctxt path ~content =
    get ctxt
    |> Option.iter ~f:(fun sto ->
           sto##setItem (Js.string path) (Js.string content))

  let remove_file ctxt path =
    get ctxt |> Option.iter ~f:(fun sto -> sto##removeItem (Js.string path))
end
