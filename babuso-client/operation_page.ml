open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

let display_specification ctxt op =
  let open Mono_html in
  let open Operation.Specification in
  match Operation.order op with
  | Draft draft ->
      Fmt.kstr t "%s draft"
        (match draft with
        | Simple_transfer _ -> "Transfer"
        | Call_foreign_contract_entrypoint _ | Call_custom_contract_entrypoint _
        | Call_generic_multisig_main _ | Call_generic_multisig_update_keys _ ->
            "Contract call")
      %% t "→"
      %% Operation_display.draft_resume_discard ctxt op.id
  | Origination
      { account; specification = _; initialization = _; gas_wallet = _ } ->
      t "Origination of the smart-contract account"
      %% Custom_widget.account_or_address_link ctxt (`Id account)
  | Transfer { source = _; destination = _; amount = _; entrypoint; parameters }
    -> (
      match (entrypoint, parameters) with
      | "default", [] -> t "Simple ꜩ transfer"
      | _, _ -> (
          Fmt.kstr t "Call entrypoint"
          %% b (ct entrypoint)
          %
          match parameters with
          | [] -> t " (without parameters)"
          | more ->
              t " with parameters:"
              % itemize
                  (List.map more ~f:(fun (k, v) ->
                       ct k %% t "→" %% Custom_widget.variable_value ctxt v))))

let display_status ctxt op_status =
  let open Mono_html in
  let open Operation.Status in
  let colort kind text = Bootstrap.color kind (bt text) in
  let tezos_op = function
    | None -> empty ()
    | Some { operation_hash; base_block_hash = _; inclusion_block_hashes } ->
        div
          (t "Tezos operation:"
          %% Custom_widget.operation_hash ?shorten:None ctxt operation_hash
          %
          match inclusion_block_hashes with
          | [] -> t ": Not included in a block yet."
          | [ (one, final) ] ->
              t ": included in"
              %% Custom_widget.block_hash ~shorten:None ctxt one
              %% parens (if final then it "final" else it "not final")
              % t "."
          | more ->
              t ": included in %d blocks:"
              %% (Language.oxfordize_list more
                    ~map:(fun (j, _) -> Custom_widget.block_hash ctxt j)
                    ~sep:(fun () -> t ", ")
                    ~last_sep:(fun () -> t ", and")
                 |> list)
              % t " … a Tenderbake rarity.")
  in
  let mempool_section_name =
    let open Mempool_section in
    function
    | Applied -> "Applied"
    | Refused -> "Refused"
    | Outdated -> "Outdated"
    | Branch_refused -> "Branch-refused"
    | Branch_delayed -> "Branch-delayed"
    | Unprocessed -> "Unprocessed"
  in
  let mempool_errors_parens =
    let error =
      let open Mempool_error in
      function
      | Unknown json ->
          ct
            Json.Q.(
              try string_field json ~k:"id" with _ -> "CompletelyUnknown")
    in
    function
    | [] -> empty ()
    | [ one ] -> parens (t "Error:" %% error one)
    | more ->
        parens
          (t "Errors:"
          %% list
               (Language.oxfordize_list more ~map:error
                  ~sep:(fun () -> t ", ")
                  ~last_sep:(fun () -> t ", and ")))
  in
  let mempool ~mempool_errors =
    let open Mempool_section in
    let errors = mempool_errors_parens mempool_errors in
    function
    | None -> t "Looking for it …" %% errors
    | Some Applied -> t "Applied in the mempool." %% errors
    | Some other ->
        Fmt.kstr t "In the mempool's “%s” section." (mempool_section_name other)
        %% errors
  in
  let operation_error =
    let open Operation.Error in
    function
    | Stuck_in_the_mempool (section, mempool_errors, expiredness) -> (
        let errors = mempool_errors_parens mempool_errors in
        t "Operation seems stuck in the mempool"
        %% parens (it (mempool_section_name section) %% t "section" %% errors)
        %
        match expiredness with
        | `Expired ->
            t "and it has expired, it cannot be included in a block any more."
        | `Non_expired -> t ", but it has not expired yet, you should wait.")
    | Operation_lost ->
        t
          "The operation is now lost (likely garbage-collected from the \
           mempool)."
    | Simulation_failure { which; simulation; batch = _ } ->
        Fmt.kstr t "The operation's %s simulation failed."
          (match which with
          | `Computing_fees -> "preliminary"
          | `Final -> "final")
        %% itemize
             (List.map
                (Simulation_result.all_errors simulation)
                ~f:(Custom_widget.tezos_operation_error ctxt))
    | Unknown sexp ->
        t "The operation failed with an unknown error, please report:"
        %% Custom_widget.Debug.sexpable Fn.id sexp
  in
  match op_status with
  | Paused -> colort `Secondary "Edition by user."
  | Ordered -> colort `Warning "Ordered"
  | Work_in_progress { op; simulation_result; mempool_section; mempool_errors }
  (*  as status *) ->
      let show_json =
        match simulation_result with
        | None -> empty ()
        | Some sr ->
            Custom_widget.Global_modal.(
              raw_json (Simulation_result.raw sr |> Json.to_string_minify)
              |> as_button ~label:(t "Show Raw JSON"))
      in
      colort `Warning "Work in progress"
      %% div (mempool mempool_section ~mempool_errors)
      %% tezos_op op %% show_json
      %% Custom_widget.Debug.(
           bloc ctxt
             (match simulation_result with
             | None -> bt "Not available"
             | Some sr ->
                 t "Operation Simulations:"
                 %% sexpable
                      (List.sexp_of_t
                         Simulation_result.Operation_result.sexp_of_t)
                      (Simulation_result.operations sr))
           (* % bloc ctxt
               (t "full:" %% sexpable Operation.Status.sexp_of_t status) *))
  | Operation.Status.Success { op } ->
      colort `Success "Succeeded 🎉"
      %% tezos_op (Some op)
      %% Custom_widget.Debug.(
           bloc ctxt (t "tezos-op:" %% sexpable sexp_of_tezos_operation op))
  | Operation.Status.Failed err ->
      colort `Danger "Failed 😿"
      %% operation_error err
      %% Custom_widget.Debug.(
           expandable_sexpable ctxt (t "See the error")
             Operation.Error.sexp_of_t err)

let whole_status_and_history ~history_collapser ctxt op =
  let open Mono_html in
  let history =
    let open Bootstrap.Collapse in
    fixed_width_reactive_button_with_div_below ~kind:`Info history_collapser
      ~width:"10em"
      ~button:(function true -> t "Show History" | false -> t "Hide History")
      (fun () ->
        list
          (List.map (Operation.status_history op) ~f:(fun (date, opst) ->
               let date_string = Date_js.(of_float date |> to_iso_string) in
               div
                 (ct date_string %% display_status ctxt opst)
                 ~a:[ style "border: 1px solid #888" ])))
  in
  display_status ctxt (Operation.status op) %% history

let render ctxt ~id =
  let open Mono_html in
  let history_collapser = Bootstrap.Collapse.make () in
  let whole_table op =
    let open Operation in
    let row k v = Bootstrap.Table.row [ bt k; v ] in
    let or_empty o f = match o with None -> empty () | Some s -> f s in
    Custom_widget.make_table ctxt
      (row "Last Update"
         (let date_string =
            Date_js.(of_float op.last_update |> to_iso_string)
          in
          ct date_string)
      % row "Type" (display_specification ctxt op)
      % or_empty (get_source_account_id op) (fun id ->
            row "Source" (Custom_widget.account_or_address_link ctxt (`Id id)))
      % or_empty (get_destination_address op) (fun a ->
            row "Destination"
              (Custom_widget.account_or_address_link ctxt (`Address a)))
      % or_empty (get_amount op) (fun amount ->
            row "Amount"
              (if Z.equal Z.zero amount then t "0 (zero)"
              else
                Custom_widget.Balance.(
                  multi ctxt amount ~with_style:(adaptive_inline_style amount))))
      % (if Operation.is_draft op then empty ()
        else row "Status" (whole_status_and_history ctxt ~history_collapser op))
      % row "User-comments"
          (Custom_widget.Human_prose_widget.editable ctxt op.comments ~id:op.id)
      )
  in
  Web_app_state.bind_operation ctxt id ~f:(fun op ->
      Custom_widget.div_title
        (t "Operation" %% Bootstrap.monospace (Fmt.kstr t "%s" op.id))
      % whole_table op
      %% Custom_widget.Debug.(
           bloc ctxt
             (Operation_display.short ctxt op % sexpable Operation.sexp_of_t op)))
