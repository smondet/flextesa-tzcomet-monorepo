open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

let show_comments_summary ?wrap acc =
  Custom_widget.Human_prose_widget.option_summary ?wrap (Account.comments acc)

let accounts_table ctxt ~accounts ~fields =
  let open Mono_html in
  let open Wallet_configuration.Accounts_table_field_name in
  let field_title = function
    | Name { with_type } ->
        Fmt.kstr t "%sName" (match with_type with `No -> "" | _ -> "Type/")
    | Address _ -> t "Address"
    | Balance { convert } ->
        Fmt.kstr t "Balance%s"
          (Option.value_map convert ~default:"" ~f:(fun c ->
               Fmt.str " (%s)" (Currency.to_symbol c)))
    | Comments_summary -> t "Comments"
    | Quick_actions -> t "🛠"
  in
  let account_type_text acc =
    let open Account in
    match acc.state with
    | Status.Originated_contract _ -> "Contract"
    | Key_pair _ -> "Key-pair"
    | Friend _ -> "Friend"
    | Contract_to_originate _ -> "WIP-Contract"
    | Contract_failed_to_originate _ -> "Failed-Contract"
    | Draft _ -> "Draft"
  in
  let account_type_icon acc =
    let m s =
      span
        ~a:
          [
            style "width: 1.7em; display: inline-block";
            H5.a_title (Reactive.pure (account_type_text acc));
          ]
        (t s)
    in
    let open Account in
    match acc.state with
    | Status.Originated_contract _ -> m "📰"
    | Key_pair _ -> m "🔑"
    | Friend _ -> m "🙋"
    | Contract_to_originate _ -> m "🚧"
    | Contract_failed_to_originate _ -> m "🕱"
    | Draft _ -> m "🏗"
  in
  let delete_button acc =
    let cross = Custom_widget.icon_with_hover "❌" "Delete Account" in
    Custom_widget.delete_account_button ~kind:`Secondary ctxt acc cross
  in
  let title_cell = Custom_widget.div_title (t "Accounts") in
  Custom_widget.make_table ctxt
    ~header_row:
      (title_cell
      ::
      (match fields with [] -> [] | _ :: more -> List.map more ~f:field_title))
    (list
       (List.map accounts ~f:(function
         | { Account.state = Account.Status.Draft _; _ } -> empty ()
         | acc ->
             let align_right = div ~a:[ style "text-align: right" ] in
             List.map fields
               ~f:
                 Account.(
                   function
                   | Name { with_type } ->
                       (match with_type with
                       | `No -> empty ()
                       | `Text -> bt (account_type_text acc) % t ": "
                       | `Icon -> account_type_icon acc % t " ")
                       % Custom_widget.account_link ctxt ~id:acc.id
                           (bt acc.display_name)
                   | Quick_actions -> delete_button acc
                   | Address { shorten; links } ->
                       Custom_widget.address ctxt acc ?shorten ~links
                   | Balance { convert = None } ->
                       align_right (Custom_widget.Balance.of_account_tez acc)
                   | Balance { convert = Some currency } ->
                       align_right
                         (Custom_widget.Balance.of_account_converted ctxt acc
                            currency)
                   | Comments_summary -> show_comments_summary acc)
             |> Bootstrap.Table.row)))

let accounts_section ctxt =
  let open! Mono_html in
  Reactive.bind
    Reactive.(
      Web_app_state.accounts ctxt ** Web_app_state.Configuration.get ctxt)
    ~f:(fun (l, conf) ->
      let fields =
        match conf with
        | Some s -> s.ui_options.accounts_table_fields
        | None -> Wallet_configuration.Accounts_table_field_name.default_list
      in
      accounts_table ctxt ~accounts:l ~fields)

let render ctxt =
  let open Mono_html in
  accounts_section ctxt
  % Custom_widget.make_table ctxt
      Operations_table.(
        let should_show = function `Empty -> true | `Not_empty -> false in
        draft_operations_section ctxt ~continue_with:(fun recent_status ->
            let show_field_titles = should_show recent_status in
            recent_operations_section ~show_field_titles ctxt))
