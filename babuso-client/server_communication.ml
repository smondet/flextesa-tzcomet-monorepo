open! Import
open Babuso_lib

let with_timeout timeout ~f =
  let open Lwt.Infix in
  Lwt.pick
    [
      f ();
      ( Js_of_ocaml_lwt.Lwt_js.sleep timeout >>= fun () ->
        Fmt.kstr Lwt.fail_with "Timeout: %f" timeout );
    ]

let counter = ref 0

let call ?(timeout = 10.) message =
  let open Lwt.Infix in
  let open Js_of_ocaml_lwt in
  let body_sexp = Protocol.Message.Up.sexp_of_t message in
  let body = Sexp.to_string_mach body_sexp in
  with_timeout timeout
    ~f:
      XmlHttpRequest.(
        fun () ->
          Int.incr counter;
          perform_raw_url ~contents:(`String body)
            (Fmt.str "ui-protocol?c=%d"
               !counter (* This seems to be a hack against caches? *))
            ~override_method:`POST
          >>= fun frame ->
          dbgf
            "Server_communication.call - query#%d %s -> code: %d, content: %s"
            !counter body frame.code frame.content;
          match frame.code with
          | 200 ->
              Lwt.return
                (Protocol.Message.Down.t_of_sexp
                   (Sexplib.Sexp.of_string frame.content))
          | _other -> Fmt.kstr Lwt.fail_with "%s" frame.content)

(* TODO: Babuso errors will be raised instead of string exceptions *)
(* | _other -> *)
(*   try *)
(*     let sexp = sexp_of_string frame.content in *)
(*     let bab_err = Babuso_error.tagged_babuso_error_of_sexp sexp in *)
(*     Lwt.fail (Babuso_error.Babuso_exn bab_err) *)
(*   with *)
(*   | e -> Fmt.kstr Lwt.fail_with "Error converting to a Babuso_error: %a, %s" *)
(*          Exn.pp e frame.content) *)

let call_with_units message ~wip ~success ~failure get_result =
  let open Babuso_error in
  let open Lwt.Infix in
  wip ();
  Lwt.async (fun () ->
      Lwt.catch
        (fun () ->
          call message >>= fun down ->
          let v = get_result down in
          success v;
          Lwt.return_unit)
        (fun exn ->
          let str =
            match exn with
            | Babuso_exn (e, z) ->
                tagged_error_pp
                  (Babuso_error.add_tag (e, z)
                     "Server_communication.call_with_units")
            | e -> Fmt.str "%a" Exn.pp e
          in
          failure str;
          Lwt.return_unit))

(* TODO: the values passed to failure will be Babuso_errors instead of strings  *)
(* match exn with *)
(* | Babuso_exn (e, z) -> *)
(*     let retagged =add_tag (e, z) "Server_communication.call_with_units" in *)
(*     failure retagged; *)
(*     Lwt.return_unit *)
(* | e -> *)
(*     let bab_err = mk_tagged_error (Unknown_error (Fmt.str "%s" (Exn.to_string e))) *)
(*           ~tag:"Server_communication.call_with_units" in *)
(*     failure bab_err; *)
(*     Lwt.return_unit )) *)

let call_with_var var ?catch_failure message get_result =
  let open Lwd_bootstrap in
  let open Wip in
  let failure =
    match catch_failure with
    | None -> fun s -> Reactive.set var (Failure s)
    | Some f -> f
  in
  call_with_units message get_result
    ~wip:(fun () -> Reactive.set var WIP)
    ~success:(fun v -> Reactive.set var (Success v))
    ~failure

let list_connected_ledgers var =
  let open Protocol.Message in
  call_with_var var Up.List_connected_ledgers (function
    | Down.Ledgers sl -> sl
    | _ -> Fmt.failwith "Wrong Response")

let call_with_var_unit var ?catch_failure msg =
  let open Protocol.Message in
  call_with_var var msg ?catch_failure (function
    | Down.Done -> ()
    | _ -> Fmt.failwith "Wrong Response")

let import_account var ?comments name kind =
  let open Protocol.Message in
  call_with_var_unit var Up.(Import_account { name; kind; comments })

let delete_account var ~id =
  let open Protocol.Message in
  call_with_var_unit var Up.(Delete_account id)

let get_configuration var =
  let open Protocol.Message in
  call_with_var var Up.Get_configuration (function
    | Down.Configuration sl -> sl
    | _ -> Fmt.failwith "Wrong Response")
