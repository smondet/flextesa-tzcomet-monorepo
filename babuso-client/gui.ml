open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

let test_page ctxt =
  let open Mono_html in
  h2 (t "Test Page") %% Old_interface.render ctxt

let navigation_menu ctxt =
  let open Mono_html in
  let branding () = bt "Babuso💰" in
  let history ~max_length () =
    Web_app_state.Current_page.navigation_history ctxt
    |> Reactive.bind ~f:(fun histo ->
           let first, histo =
             if List.length histo <= max_length then ([], histo)
             else ([ t "..." ], List.take histo max_length)
           in
           let item ?(make_link = true) page =
             let make c =
               if make_link then
                 a
                   ~a:
                     [
                       H5.a_href (Reactive.pure "#");
                       H5.a_onclick
                         Reactive.(
                           pure
                             (Some
                                (fun _ ->
                                  Web_app_state.Current_page.change_to ctxt page;
                                  false)));
                     ]
                   c
               else a c
             in
             let open Web_app_state.Page in
             match page with
             | Overview -> make (t "Overview")
             | Operations -> make (t "Operations")
             | Account { id } ->
                 Web_app_state.bind_account_opt ctxt id ~f:(function
                   | None -> a (it "Deleted Account")
                   | Some acc ->
                       make
                         (t "Account"
                         %% Custom_widget.account_short_display ctxt acc))
             | Operation { id } ->
                 Web_app_state.bind_operation ctxt id ~f:(fun op ->
                     make (Fmt.kstr t "Op %s…" (String.subo op.id ~len:8)))
             | Signing_interface -> make (t "Signing")
             | Settings -> make (t "Settings")
             | Tests -> make (t "Tests🤡")
             | Draft_account { id = None } -> make (t "New Account")
             | Draft_account { id = Some id } ->
                 Web_app_state.bind_account_opt ctxt id ~f:(function
                   | None -> a (it "Deleted Draft")
                   | Some acc ->
                       make
                         (t "Draft"
                         %% Custom_widget.account_short_display ctxt acc))
             | Draft_operation { id = None } -> make (t "New Operation")
             | Draft_operation { id = Some id } ->
                 (* Web_app_state.bind_operation ctxt id ~f:(fun op -> *)
                 Web_app_state.bind_operation_opt ctxt id ~f:(function
                   | None -> a (it "Deleted Operation Draft")
                   | Some _ ->
                       make
                         (Fmt.kstr t "Draft operation %s…"
                            (String.sub id ~pos:0 ~len:6)))
             (* make (Fmt.kstr t "Draft Account %s…" (String.subo id ~len:4)) *)
           in
           match histo with
           | current :: more ->
               Bootstrap.Breadcrumb.of_stack ~first_empty:true
                 ~a_ol:[ style "padding: 0" ]
                 (item ~make_link:false current)
                 (List.map ~f:(item ~make_link:true) more @ first)
               |> div ~a:[ style "font-size: 85%" ]
           | [] -> Bootstrap.label `Danger (t "NO HISTORY???"))
  in
  let all_items =
    Reactive.map (Web_app_state.Current_page.navigables ctxt)
      ~f:(fun navigables ->
        let of_page (p, current) =
          Bootstrap.Navigation_bar.item
            (span
               ~a:[ style "text-decoration: underline" ]
               (bt
                  Web_app_state.Page.(
                    match p with
                    | Overview -> "Overview"
                    | Signing_interface -> "Signing"
                    | Settings -> "Settings"
                    | Tests -> "🚧Tests🚧"
                    | Operations -> "Operations"
                    | Draft_operation { id = None } -> "New Operation"
                    | Draft_account { id = None } -> "New Account"
                    | non_nav ->
                        let s = to_string non_nav in
                        Web_app_state.Local_failures.add_message ctxt
                          (Fmt.str
                             "BUG: Non-navigable %S shows up in navigation??" s);
                        s)))
            ~active:(Reactive.pure (not current))
            ~action:(fun () -> Web_app_state.Current_page.change_to ctxt p)
            ~fragment:(Reactive.pure "")
        in
        List.map ~f:of_page navigables)
  in
  let right_side =
    let exchange_rates =
      Reactive.bind_var (Web_app_state.exchange_rates ctxt) ~f:(function
        | None -> t "No exchange rates"
        | Some exch ->
            let mutez = Z.of_int 1_000_000 in
            let with_style =
              Custom_widget.Balance.adaptive_inline_style mutez
            in
            let bal cur =
              Custom_widget.Balance.converted ctxt mutez cur ~with_style
            in
            let currencies = [ `EUR; `BTC; `ETH ] in
            let all =
              List.fold ~init:(bal `USD) currencies ~f:(fun p v ->
                  p %% t "|" %% bal v)
            in
            let hover_thing =
              Fmt.kstr Reactive.pure "Updated on %s"
                Date_js.(of_float exch.timestamp |> to_iso_string)
            in
            div ~a:[] (small ~a:[ H5.a_title hover_thing ] all))
    in
    let basic_network_info =
      Reactive.bind (Web_app_state.Network_info.reactive ctxt) ~f:(function
        | None -> t "No network information"
        | Some info ->
            let network =
              match Network_information.network info with
              | None -> it "????"
              | Some n -> Custom_widget.network_link ctxt n
            in
            let head =
              match Network_information.fine_nodes info with
              | [] -> Bootstrap.color `Danger (t "No good node!")
              | (l, h, _) :: more ->
                  let level, hash =
                    List.fold more ~init:(l, h) ~f:(fun (l, h) (ll, hh, _) ->
                        if ll > l then (ll, hh) else (l, h))
                  in
                  t "@" %% Fmt.kstr bt "%d" level % t "/"
                  % Custom_widget.block_hash ctxt hash
            in
            network %% head)
    in
    div (exchange_rates %% small basic_network_info)
  in
  Reactive.bind (Browser_window.width ctxt) ~f:(function
    | Some `Wide ->
        Reactive.bind all_items
          ~f:
            Bootstrap.Navigation_bar.(
              fun all ->
                let brand = Bootstrap.label `Dark (branding ()) in
                make ~brand all ~right_side)
        %% history ~max_length:7 ()
    | None | Some `Thin ->
        let nav_thing =
          Reactive.bind all_items
            ~f:
              Bootstrap.Navigation_bar.(
                fun all -> make ~right_side ~brand:(empty ()) all)
        in
        let on_top =
          div
            ~a:
              [
                style
                  "z-index: 2; position: absolute; text-align: center; \
                   font-size: 200%; pointer-events: none; width: 100%; color: \
                   #004";
                classes [ "mx-auto" ];
              ]
            (branding ())
        in
        on_top % nav_thing %% history ~max_length:4 ())

module Operations_page = struct
  let render ctxt = Operations_table.whole_table ctxt
end

let alerts_section ctxt =
  let open Mono_html in
  let show_box kind content = Bootstrap.alert ~kind content in
  let show_alert alert =
    let open Protocol.Message.Down.Alert in
    show_box `Warning
      (a ~a:H5.[ a_id (Lwd.pure alert.id) ] (t "")
      %
      match alert.content with
      | Ledger_wants_human_intervention { ledger_id; ledger_name; reason } ->
          t "✋ Ledger"
          %% Fmt.kstr ct "%s (%s)" ledger_name ledger_id
          %% t "requires human intervention:"
          %% it reason
      | Account_is_being_created { id; display_name } ->
          Bootstrap.spinner (t "WIP")
          %% t "👷 Account" %% bt display_name
          %% parens (ct id)
          %% t "is being created.")
  in
  let show_local_error
      Web_app_state.Local_failure.{ id; date; repeats; content = Message msg } =
    show_box `Danger
      (a ~a:H5.[ a_id (Lwd.pure id) ] (t "")
      % bt "GUI-Error"
      %% parens
           (ct Date_js.(to_iso_string (of_float date))
           %
           match repeats with
           | [] -> empty ()
           | more -> Fmt.kstr ct " × %d" (List.length more))
      % t ":" %% t msg
      %% Custom_widget.inline_button (t "Discard") ~action:(fun () ->
             Web_app_state.Local_failures.remove ctxt id))
  in
  Reactive.bind (Web_app_state.Alerts.currently_on ctxt) ~f:(function
    | [] -> empty ()
    | als -> list (List.map als ~f:show_alert))
  % Reactive.bind_var (Web_app_state.failures ctxt) ~f:(function
      | [] -> empty ()
      | more -> list (List.map ~f:show_local_error more))

let render ctxt =
  let open Mono_html in
  let tests_page = lazy (test_page ctxt) in
  let settings_page = lazy (Settings_page.render ctxt) in
  let overview_page = lazy (Overview_page.render ctxt) in
  (* let editor = lazy (Editor.page state) in
     let settings = lazy (Settings_page.render state) in
     let about = lazy (about_page state) in *)
  Bootstrap.container ~suffix:"-fluid"
    ~a:[ H5.a_id (Reactive.pure "Babuso-top") ]
    (navigation_menu ctxt % alerts_section ctxt
    % Reactive.bind
        (Web_app_state.Current_page.get ctxt)
        ~f:
          Web_app_state.Page.(
            function
            | `Changing_to p ->
                Lwt.async
                  Lwt.Infix.(
                    fun () ->
                      Js_of_ocaml_lwt.Lwt_js.yield () >>= fun () ->
                      Web_app_state.Current_page.set ctxt (`Page p);
                      Lwt.return_unit);
                div (Bootstrap.spinner (t "WIP"))
            | `Page Overview -> Lazy.force overview_page
            | `Page Operations -> Operations_page.render ctxt
            | `Page Signing_interface -> Signing_page.render ctxt
            | `Page Settings -> Lazy.force settings_page
            | `Page (Account { id }) -> Account_page.render ctxt ~account_id:id
            | `Page (Operation { id }) -> Operation_page.render ctxt ~id
            | `Page (Draft_account { id }) ->
                Draft_account_page.render ctxt ?account_id:id ()
            | `Page (Draft_operation { id }) ->
                Draft_operation_page.render ctxt ?id ()
            | `Page Tests -> Lazy.force tests_page))
