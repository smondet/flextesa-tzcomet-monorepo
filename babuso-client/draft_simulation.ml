open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

type t = {
  v : (Simulation_result.t * Tezos_batch.t) Wip.t Reactive.var;
  up_message : unit -> Protocol.Message.Up.t;
}

let make ~up_message () = { v = Reactive.var Wip.idle; up_message }

let rec display_operation_result ctxt opres =
  let open Mono_html in
  let open Simulation_result.Operation_result in
  let {
    status;
    errors;
    balance_updates;
    consumed_milligas;
    storage_size;
    allocated_destination_contract;
    paid_storage_size_diff;
    originated_contracts;
    internal_operations;
  } =
    opres
  in
  let successful =
    match status with `Applied -> true | `Backtracked | `Failed -> false
  in
  let of_list ~f = function [] -> [] | more -> [ f more ] in
  let of_list_head head ~f l =
    of_list l ~f:(fun l -> head %% list (List.map ~f:(fun x -> div (f x)) l))
  in
  let of_option_head head ~f = function
    | None -> []
    | Some o -> [ head %% f o ]
  in
  let errors =
    of_list_head (t "Errors:") errors
      ~f:(Custom_widget.tezos_operation_error ctxt)
  in
  let internals =
    of_list_head (t "Internal Operations:") internal_operations ~f:(fun op ->
        display_operation_result ctxt op)
  in
  let data =
    (if successful then
     let always =
       [
         (match consumed_milligas with
         | None -> empty ()
         | Some z ->
             t "Consumed gas units:"
             %% Fmt.kstr ct "%s.%03d"
                  Z.(to_string (div z (of_int 1_000)))
                  Z.(rem z (of_int 1_000) |> to_int));
       ]
     in
     let balance_updates =
       of_list_head (t "Balance Updates:") balance_updates ~f:(function
         | Simulation_result.Balance_update.Account { address; change } ->
             t "Account"
             %% Custom_widget.account_or_address_link ctxt (`Address address)
             % t ":"
             % (if Z.sign change < 0 then t "➖" else t "➕")
             %% Custom_widget.Balance.inline_tez change
         | Burn { category; change } ->
             t "Burning"
             %% Custom_widget.Balance.inline_tez change
             %% parens (Fmt.kstr it "“%s”" category))
     in
     let storage_size =
       of_option_head (t "Storage Used:") storage_size ~f:(fun z ->
           Fmt.kstr ct "%s B" (Z.to_string z))
     in
     let storage_diff =
       of_option_head (t "Storage “Diff” Paid:") paid_storage_size_diff
         ~f:(fun z -> Fmt.kstr ct "%s B" (Z.to_string z))
     in
     let contract_addresses =
       of_list_head
         (t "Originated Contracts (fake addresses):")
         originated_contracts ~f:ct
     in
     always @ balance_updates @ storage_size @ storage_diff
     @ (match allocated_destination_contract with
       | None | Some false -> []
       | Some true -> [ t "Destination account was allocated." ])
     @ contract_addresses
    else [])
    @ internals @ errors
  in
  let div_a =
    [ style "border-left: 2px solid #888; padding-left: 5px; margin-top: 5px" ]
  in
  div ~a:div_a (list (List.map data ~f:div))

let display_simulation_with_batch ctxt simu batch =
  let open Mono_html in
  let open Custom_widget in
  let tezos_operation op =
    let open Tezos_operation in
    match op with
    | Transfer { source; amount; destination; entrypoint; parameters } ->
        t "Transfer" %% Balance.inline_tez amount %% t "from"
        %% account_or_address_link ctxt (`Id source.id)
        %% t "to"
        %% account_or_address_link ctxt (`Address destination)
        %% t "at entrypoint" %% ct entrypoint %% t "and"
        %% (match parameters with
           | None -> it "no parameters"
           | Some m -> t "parameters:" %% big_michelson ctxt m)
        % t "."
    | Origination { source; balance; code; storage_initialization } ->
        t "Contract origination with balance"
        %% Balance.inline_tez balance %% t "by"
        %% account_or_address_link ctxt (`Id source.id)
        %% t "which runs" %% big_michelson ctxt code %% t "initialized with"
        %% big_michelson ctxt storage_initialization
        % t "."
    | Reveal { account } ->
        t "Reveal the public key of"
        %% account_or_address_link ctxt (`Id account.id)
        % t "."
  in
  let op_number = List.length (Tezos_batch.operations batch) in
  let operations =
    let count = ref 0 in
    try
      List.map2_exn (Tezos_batch.operations batch)
        (Simulation_result.operations simu) ~f:(fun op sim ->
          Int.incr count;
          let open Simulation_result.Operation_result in
          let tzop = Tezos_batch.specification op in
          let div_a =
            [
              style
                "border-left: 2px solid #888; padding-left: 5px; margin-top: \
                 5px";
            ]
          in
          let successful =
            match sim.status with
            | `Applied -> true
            | `Backtracked | `Failed -> false
          in
          let data =
            [
              t "Fee to the baker:" %% Balance.inline_tez (Tezos_batch.fee op);
              t "Gas-Limit:" %% ct (Z.to_string (Tezos_batch.gas_limit op));
              t "Signature-counter:"
              %% ct (Z.to_string (Tezos_batch.counter op));
              t "Storage-Limit:"
              %% ct (Z.to_string (Tezos_batch.storage_limit op));
              t "Operation-results:" %% display_operation_result ctxt sim;
            ]
          in
          div ~a:div_a
            (Fmt.kstr bt "Tezos-Operation #%d is" !count
            %% (if successful then Bootstrap.color `Success (t "Successful")
               else Bootstrap.color `Danger (t "Failed"))
            % div (tezos_operation tzop)
            % itemize data))
    with _ ->
      [
        Bootstrap.alert ~kind:`Danger
          (t "Error while displaying simulation, batch length mismatch.");
      ]
  in
  let show_json =
    Custom_widget.Global_modal.(
      raw_json (Simulation_result.raw simu |> Json.to_string_minify)
      |> as_button ~label:(t "Show Raw JSON"))
  in
  Fmt.kstr bt "Batch containing %d Tezos-operation%s based on branch" op_number
    (if op_number = 1 then "" else "s")
  %% block_hash ctxt (Tezos_batch.branch batch)
  %% bt "and protocol"
  %% protocol ctxt (Tezos_batch.protocol batch)
  % list operations % show_json
  % Debug.(
      bloc ctxt
        (div (bt "Simulation" %% sexpable Simulation_result.sexp_of_t simu)
        % div (bt "Batch" %% sexpable Tezos_batch.sexp_of_t batch)))

let render ctxt self =
  let open Mono_html in
  Reactive.bind_var self.v ~f:(function
    | Wip.Idle -> empty ()
    | Wip.Success (simu, batch) ->
        h4 (t "Simulation Result")
        % div (display_simulation_with_batch ctxt simu batch)
    | Wip.Failure s ->
        Bootstrap.alert ~kind:`Danger (Fmt.kstr ct "Simulation failed: %s" s)
    | Wip.WIP -> Bootstrap.spinner (t "WIP"))

let start_simulation_action self () =
  Server_communication.call_with_var self.v (self.up_message ())
    Protocol.Message.Down.(
      function
      | Simulation_result (r, b) -> (r, b)
      | _ -> Fmt.failwith "Did not get the right message back")

let make_button _ctxt self ~client_validation content =
  Reactive.(bind (client_validation ** get self.v)) ~f:(fun (res, sim) ->
      let btn disabled = Custom_widget.inline_button ~disabled content in
      match (res, sim) with
      | Ok _, Wip.WIP -> btn true ~action:Fn.id
      | Ok _, _ -> btn false ~action:(start_simulation_action self)
      | _ -> btn true ~action:Fn.id)
