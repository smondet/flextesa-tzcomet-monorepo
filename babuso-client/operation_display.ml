open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap
open Custom_widget

let draft_resume_discard ctxt id =
  let open Mono_html in
  inline_button (t "Resume") ~action:(fun () ->
      Web_app_state.Current_page.change_to ctxt
        (Web_app_state.Page.draft_operation ~id:(Some id));
      ())
  % t " / "
  % inline_button (t "Discard") ~action:(fun () ->
        Web_app_state.call_server_async ctxt
          (Protocol.Message.Up.delete_operation_draft ~id);
        ())

let short ?(draft_links = true) ctxt op =
  let open Operation in
  let open Mono_html in
  let make_link c =
    inline_button c ~action:(fun () ->
        Web_app_state.Current_page.change_to ctxt
          (Web_app_state.Page.operation ~id:op.id))
  in
  make_link (t "👀")
  %%
  match op.order with
  | Specification.Draft draft ->
      let or_empty field f =
        Reactive.bind_var field ~f:(function
          | "" ->
              span
                ~a:[ style "border: solid 1px #99999944; padding: 1px" ]
                (it "???")
          | more -> f more)
      in
      t "Draft"
      %% (match draft with
         | Draft.Simple_transfer { source_account; destination_address; amount }
           ->
             t "Transfer" %% or_empty amount ct %% t "from"
             %% or_empty source_account (fun id ->
                    account_or_address_link ctxt (`Id id))
             %% t "to"
             %% or_empty destination_address (fun addr ->
                    account_or_address_link ctxt (`Address addr))
         | Call_custom_contract_entrypoint { contract_address; entrypoint; _ }
         | Call_foreign_contract_entrypoint { contract_address; entrypoint; _ }
           ->
             t "Call entrypoint" %% ct entrypoint %% t "of contract"
             %% account_or_address_link ctxt (`Address contract_address)
         | Call_generic_multisig_update_keys { contract_address; _ } ->
             t "Update of the multisig parameters for"
             %% account_or_address_link ctxt (`Address contract_address)
         | Call_generic_multisig_main { contract_address; _ } ->
             t "Call the multisig"
             %% account_or_address_link ctxt (`Address contract_address))
      %% if draft_links then draft_resume_discard ctxt op.id else empty ()
  | Specification.Origination
      { account; specification; initialization = _; gas_wallet } ->
      Fmt.kstr t "Origination of %s contract"
        (match specification with
        | Custom _ -> "custom"
        | Generic_multisig _ -> "generic-multisig"
        | Foreign _ -> "foreign")
      %% account_or_address_link ctxt (`Id account)
      %% t "by"
      %% account_or_address_link ctxt (`Id gas_wallet)
  | Specification.Transfer
      { source; destination; amount; entrypoint; parameters } ->
      (match parameters with
      | [] -> t "Pure transfer of"
      | _ -> t "Call" %% ct entrypoint %% t "with amount")
      %% Fmt.kstr t "%s ꜩ:" (Mutez.to_tez_string amount)
      %% account_or_address_link ctxt (`Id source)
      %% bt "→"
      %% account_or_address_link ctxt (`Address destination)

module Internal = struct
  open Mono_html

  let dest ctxt destination_address =
    Reactive.bind_var destination_address ~f:(fun destination ->
        account_or_address_link ctxt (`Address destination))

  let account_var ctxt =
    Reactive.bind_var ~f:(fun source ->
        account_or_address_link ctxt (`Id source))

  let pubkey ctxt pk =
    account_or_address_link ctxt (`Public_key pk)
      ~add_details:[ `Public_key 14 ]

  let amount_var _ctxt = Reactive.bind_var ~f:(Fmt.kstr bt "%s")

  let lambda_ops_draft ctxt =
    let open Lambda_unit_operations.Draft in
    fun (Structured lambda_draft as ld) ->
      Debug.(bloc ctxt (sexpable Lambda_unit_operations.Draft.sexp_of_t ld))
      % Reactive.bind (Reactive.Table.list_document lambda_draft) ~f:(function
          | [] -> bt "Empty lambda 🤷"
          | more ->
              list
                (List.map more ~f:(fun subop ->
                     div
                     @@ t "⏵ "
                        %
                        match subop with
                        | Transfer { destination; amount } ->
                            t "Transfer" %% amount_var ctxt amount %% t "to"
                            %% dest ctxt destination
                        | Delegation { delegate } ->
                            Reactive.bind_var delegate ~f:(function
                              | "" -> t "Undelegate the contract"
                              | del ->
                                  t "Delegate the contract to"
                                  %% account_or_address_link ctxt (`Address del))
                        | Fa2_transfer _ -> bt "TODO")))
end

let specification ctxt op =
  let open Operation in
  let open Specification in
  let open Mono_html in
  let open Internal in
  let show_draft =
    let open Draft in
    let param_draft param =
      let open Operation.Draft.Parameter_draft in
      ct param.name % t " →"
      %% (match param.v with
         | Address addr -> dest ctxt addr
         | Amount am -> amount_var ctxt am
         | Of_type (t, v) ->
             Reactive.bind_var v ~f:(fun s ->
                 Fmt.kstr ct "%s : %a" s Sexp.pp_hum (Variable.Type.sexp_of_t t)))
      % Debug.(bloc ctxt (sexpable sexp_of_t param))
    in
    function
    | Simple_transfer { source_account; destination_address; amount } ->
        t "Transfer" %% amount_var ctxt amount %% t "from"
        %% account_var ctxt source_account
        %% bt "to"
        %% dest ctxt destination_address
    | Call_generic_multisig_update_keys
        { contract_address; gas_wallet; threshold; public_keys; signatures = _ }
      ->
        t "Reconfigure the public-keys and threshold of the multisig"
        %% account_or_address_link ctxt (`Address contract_address)
        % t " using:"
        % itemize
            [
              t "Threshold:" %% Reactive.bind_var threshold ~f:ct;
              t "Public keys:"
              %% Reactive.bind_var public_keys ~f:(fun l ->
                     itemize (List.map l ~f:(pubkey ctxt)));
              t "Gas-account:" %% account_var ctxt gas_wallet;
            ]
    | Call_generic_multisig_main
        { contract_address; gas_wallet; lambda; signatures = _ } ->
        t "Send instrucitons to the multisig"
        %% account_or_address_link ctxt (`Address contract_address)
        % t " using:"
        % itemize
            [
              t "Instructions:"
              %% Reactive.bind_var lambda ~f:(lambda_ops_draft ctxt);
              t "Gas-account:" %% account_var ctxt gas_wallet;
            ]
    | Call_custom_contract_entrypoint
        { contract_address; entrypoint; gas_wallet; parameters } ->
        t "Call entrypoint" %% ct entrypoint %% t "of contract"
        %% account_or_address_link ctxt (`Address contract_address)
        %% t "using:"
        % itemize
            (List.map parameters ~f:param_draft
            @ [ t "Gas-account:" %% account_var ctxt gas_wallet ])
    | Call_foreign_contract_entrypoint
        { contract_address; entrypoint; gas_wallet; parameters } ->
        t "Call entrypoint" %% ct entrypoint %% t "of contract"
        %% account_or_address_link ctxt (`Address contract_address)
        %% t "using:"
        % itemize
            [
              t "Parameter:" %% itemize (List.map parameters ~f:param_draft);
              t "Gas-account:" %% account_var ctxt gas_wallet;
            ]
  in
  match op.order with
  | Draft draft -> show_draft draft
  | Origination { account; specification; initialization = _; gas_wallet } ->
      Fmt.kstr t "Origination of %s contract"
        (match specification with
        | Custom _ -> "custom"
        | Generic_multisig _ -> "generic-multisig"
        | Foreign _ -> "foreign")
      %% account_or_address_link ctxt (`Id account)
      %% t "by"
      %% account_or_address_link ctxt (`Id gas_wallet)
  | Transfer { source; destination; amount; entrypoint; parameters } ->
      t "Transfer:"
      % itemize
          [
            t "Source:" %% account_or_address_link ctxt (`Id source);
            t "Destination:"
            %% account_or_address_link ctxt (`Address destination);
            t "Amount:" %% Balance.inline_tez amount;
            t "Entrypoint:" %% ct entrypoint;
            (t "Parameters:"
            %
            match parameters with
            | [] -> t "None"
            | more ->
                itemize
                  (List.map more ~f:(fun (k, v) ->
                       b (ct k)
                       % t " ="
                       %% (Variable.Value.to_michelson v |> big_michelson ctxt)))
            );
          ]
