open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

let validate_whole_draft ~with_signatures ctxt draft =
  let rates = Web_app_state.exchange_rates ctxt |> Reactive.peek in
  Drafting.Render.env (fun () ->
      Validate.Operation.draft draft ~rates ~with_signatures
      (*   |> Drafting.Render.Env.ignore_result *))

let draft_ready_for_sigs ctxt ~multisig draft_var =
  let rates = Web_app_state.exchange_rates ctxt |> Reactive.peek in
  Reactive.(bind (get draft_var)) ~f:(fun draft ->
      Reactive.map
        (Drafting.Render.env (fun () ->
             Validate.Operation.draft ~rates
               ~with_signatures:Validate.Operation.With_signatures.not_yet
               (* ~before_signatures:true *) draft))
        ~f:(function
          | Ok
              {
                v =
                  Operation.Specification.Transfer
                    {
                      source = _;
                      destination;
                      amount = _;
                      entrypoint;
                      parameters;
                    };
                _;
              } ->
              let hash =
                Validate.Operation.With_signatures.make_hash ~destination
                  ~parameters ~counter:multisig#counter
              in
              Some (hash, entrypoint, parameters)
          | Ok _ -> None
          | Error _ -> None))

let with_multisig_contract ctxt contract_address ~error ~f =
  Web_app_state.bind_account_by_address ctxt contract_address ~f:(function
    | None ->
        Web_app_state.Local_failures.add_message ctxt
          (Fmt.str "Unknown Contract: %s" contract_address);
        error ()
    | Some destination_account -> begin
        match Account.multisig_parameters destination_account with
        | params -> f destination_account params
        | exception _ ->
            Web_app_state.Local_failures.add_message ctxt
              (Fmt.str "Contract %s is not a generic multisig" contract_address);
            error ()
      end)

let with_custom_contract_entrypoint ctxt ~contract_address ~entrypoint f =
  let open Mono_html in
  Web_app_state.bind_account_by_address ctxt contract_address ~f:(function
    | None -> Bootstrap.alert ~kind:`Danger (t "Unknown Contract")
    | Some destination_account -> begin
        match
          Account.custom_contract_entrypoint ~entrypoint destination_account
        with
        | params -> f destination_account params
        | exception e ->
            Bootstrap.alert ~kind:`Danger
              (Custom_widget.account_or_address_link ctxt
                 (`Address contract_address)
              %% t "→"
              %%
              match e with
              | Failure s -> t s
              | other -> ct @@ Exn.to_string other)
      end)

let with_foreign_contract_entrypoint ctxt ~contract_address ~entrypoint f =
  let open Mono_html in
  Web_app_state.bind_account_by_address ctxt contract_address ~f:(function
    | None -> Bootstrap.alert ~kind:`Danger (t "Unknown Contract")
    | Some destination_account -> begin
        match
          Account.foreign_contract_entrypoint ~entrypoint destination_account
        with
        | params -> f destination_account params
        | exception e ->
            Bootstrap.alert ~kind:`Danger
              (Custom_widget.account_or_address_link ctxt
                 (`Address contract_address)
              %% t "→"
              %%
              match e with
              | Failure s -> t s
              | other -> ct @@ Exn.to_string other)
      end)

let message_about_sigs params ~ready =
  let open Mono_html in
  let threshold_one = Z.(equal one) params#threshold in
  let keys_nb = List.length params#keys in
  let keys_one = keys_nb = 1 in
  (if ready then t "This operation requires"
  else t "Once ready, this operation will require")
  %% (if keys_one then empty () else t "at least")
  %% (if threshold_one then bt "one" %% t "signature"
     else Fmt.kstr bt "%a" Z.pp_print params#threshold %% t "signatures")
  %%
  if keys_one then t "from the configured key."
  else if Z.(equal (of_int keys_nb)) params#threshold then
    t "from each of the the configured keys"
  else
    t "from keys among the" %% Fmt.kstr bt "%d" keys_nb %% t "configured"
    % if ready then t ":" else t "."

let signatures_form ctxt ~parameters ~signatures ~multisig ~target_contract_id
    ~hash ~ep_name =
  let open Mono_html in
  let blob = Reactive.var None in
  Lwt.async
    Lwt.(
      fun () ->
        Server_communication.call
          (Protocol.Message.Up.make_blob_for_signature ~target_contract_id
             ~entrypoint:ep_name ~parameters)
        >>= function
        | Protocol.Message.Down.Blob b ->
            Reactive.set blob (Some b);
            return_unit
        | _ -> assert false);
  div (message_about_sigs multisig ~ready:true)
  % Reactive.bind_var blob ~f:(function
      | None -> Bootstrap.spinner (t "WIP")
      | Some bytes ->
          let show_bytes b =
            let zerox = Bytes_rep.to_hex b in
            if String.length zerox < 100 then ct zerox
            else
              let buf = Buffer.create 1024 in
              Caml.StringLabels.iteri zerox ~f:(fun ith elt ->
                  if ith <> 0 && Int.(ith % 8 = 0) then Buffer.add_char buf ' ';
                  if ith <> 0 && Int.(ith % 80 = 0) then
                    Buffer.add_char buf '\n';
                  Buffer.add_char buf elt);
              Custom_widget.code_block [ Buffer.contents buf ]
          in
          let as_0x_id = "bytes-0x" in
          let as_0x =
            let v = Reactive.var (Bytes_rep.to_zero_x bytes) in
            Bootstrap.Input.bidirectional ~cols:4 ~inline_block:true
              (Reactive.Bidirectional.of_var v)
              ~a:[ H5.a_id (Reactive.pure as_0x_id) ]
          in
          div (t "Those are the bytes to sign:" %% show_bytes bytes)
          % div
              (t "As" %% ct "0x" %% t "syntax:" %% as_0x
              % Custom_widget.copy_to_clipboard_button ctxt ~input_id:as_0x_id))
  %% Custom_widget.Form.list_of_optional_signatures ctxt
       ~public_keys:multisig#keys ~signatures ~hash ~blob
       ~sign_blob:
         Lwt.(
           fun account blob ->
             Server_communication.call
               (Protocol.Message.Up.sign_blob ~account:account.Account.id ~blob)
             >>= function
             | Protocol.Message.Down.Signature blob -> return blob
             | _ -> assert false)

let subsection text = Mono_html.h4 text

let signatures_section ctxt ~draft_var ~signatures ~multisig ~target_contract_id
    =
  let open Mono_html in
  subsection (t "Signatures")
  %% Reactive.bind (draft_ready_for_sigs ctxt ~multisig draft_var) ~f:(function
       | Some (hash, ep_name, parameters) ->
           signatures_form ctxt ~parameters ~signatures ~multisig
             ~target_contract_id ~hash ~ep_name
       | None -> div (message_about_sigs multisig ~ready:false))

let show_operation_summary ctxt op ~comments_var ~draft_var ~specification =
  let open Operation in
  let open Mono_html in
  let fully_realized =
    Reactive.(map (get draft_var ** get comments_var))
      ~f:(fun (draft, comments) ->
        { op with order = Specification.Draft draft; comments })
  in
  Custom_widget.Debug.(
    bloc ctxt
      (Reactive.(bind fully_realized) ~f:(fun op ->
           bt "Fully Realized:" %% sexpable sexp_of_t op)))
  % Bootstrap.alert ~kind:`Success
      (Reactive.(bind fully_realized) ~f:(fun op ->
           h4 (t "Operation Summary")
           % Operation_display.specification ctxt op
           % h4 (t "Resulting Tezos Operation")
           % Operation_display.specification ctxt
               { op with order = specification }
           % Custom_widget.Human_prose_widget.option_div
               ~before_some:(fun () -> h4 (t "Comments"))
               ctxt op.comments))

let list_of_parameters ctxt parameters =
  let open Mono_html in
  itemize
    (List.map parameters ~f:(fun parameter ->
         let open Variable in
         let open Operation.Draft.Parameter_draft in
         b (ct parameter.name)
         % t ":"
         %%
         match parameter.v with
         | Address v ->
             Custom_widget.Form.destination_address ctxt
               (Reactive.Bidirectional.of_var v)
         | Amount v ->
             Custom_widget.Form.amount ctxt (Reactive.Bidirectional.of_var v)
         | Of_type (Type.Time_span, sv) ->
             it "An amount of time:"
             %% Custom_widget.Form.time_span ctxt
                  (Reactive.Bidirectional.of_var sv)
         | Of_type (Type.Nat, sv) ->
             it "A natural number:"
             %% Bootstrap.Input.bidirectional
                  ~placeholder:(Reactive.return "42") ~inline_block:true ~cols:2
                  (Reactive.Bidirectional.of_var sv)
         | Of_type (Type.Arbitrary mich, sv) ->
             it "Arbitrary Michelson of type"
             %% Custom_widget.michelson ctxt
                  (Michelson_expression.to_michelson mich)
             % t ":"
             %% Custom_widget.Editor.low_level_editor ctxt
                  ~rows:(Reactive.return 3)
                  (Reactive.Bidirectional.of_var sv)
         | Of_type (ty, s) ->
             it "Unhandled type (Enter S-Expression):"
             %% Custom_widget.Debug.sexpable Type.sexp_of_t ty
             %% Custom_widget.Editor.low_level_editor ctxt
                  ~rows:(Reactive.return 3)
                  (Reactive.Bidirectional.of_var s)))

let edit_operation_draft_form ctxt op comments_var ~draft =
  let open Operation in
  let open Mono_html in
  let draft_var = Reactive.var draft in
  let page_title =
    let open Draft in
    let highlight c = H5.mark ~a:[ style "border: 1px solid #bbb" ] [ c ] in
    match draft with
    | Simple_transfer _ -> subsection (t "Simple ꜩ Transfer")
    | Call_generic_multisig_update_keys { contract_address; _ } ->
        subsection
          (t "Reconfiguring The Generic Multisig"
          %% highlight
               (Custom_widget.account_or_address_link ctxt
                  (`Address contract_address)))
    | Call_generic_multisig_main { contract_address; _ } ->
        subsection
          (t "Sending Instructions To The Generic Multisig"
          %% highlight
               (Custom_widget.account_or_address_link ctxt
                  (`Address contract_address)))
    | Call_custom_contract_entrypoint { contract_address; entrypoint; _ }
    | Call_foreign_contract_entrypoint { contract_address; entrypoint; _ } ->
        subsection
          (t "Calling"
          %% highlight (ct entrypoint)
          %% t "of"
          %% highlight
               (Custom_widget.account_or_address_link ctxt
                  (`Address contract_address)))
  in
  let main_form =
    let open Draft in
    match draft with
    | Simple_transfer { source_account; destination_address; amount } ->
        List.map ~f:div
          [
            Custom_widget.Form.choose_an_account ctxt
              (Reactive.Bidirectional.of_var source_account)
              ~placeholder:(t "Source of the operation.");
            Custom_widget.Form.destination_address ctxt
              (Reactive.Bidirectional.of_var destination_address);
            Custom_widget.Form.amount ctxt
              (Reactive.Bidirectional.of_var amount);
          ]
        |> list
    | Call_generic_multisig_main
        { contract_address = _; gas_wallet; lambda; signatures = _ } ->
        List.map ~f:div
          [
            Custom_widget.Form.gas_wallet ctxt
              (Reactive.Bidirectional.of_var gas_wallet)
              ~for_reason:(t "Gas-Wallet / Operator");
            bt "Prepare a list of instructions for the multisig account:"
            %% Custom_widget.Form.lambda_unit_operations ctxt lambda;
          ]
        |> list
    | Call_generic_multisig_update_keys
        {
          contract_address = _;
          gas_wallet;
          threshold;
          public_keys;
          signatures = _;
        } ->
        List.map ~f:div
          [
            Custom_widget.Form.gas_wallet ctxt
              (Reactive.Bidirectional.of_var gas_wallet)
              ~for_reason:(t "Gas-Wallet / Operator");
            Custom_widget.Form.collection_of_public_keys ctxt
              ~prompt:(fun () -> t "Update the list of public keys:")
              ~public_keys;
            Custom_widget.Form.multisig_threshold_input ctxt
              ~prompt:(fun () ->
                t "Update the number of signatures required (“threshold”):")
              ~threshold:(Reactive.Bidirectional.of_var threshold)
              ~public_keys:(Reactive.get public_keys);
          ]
        |> list
    | Call_custom_contract_entrypoint
        { contract_address; entrypoint; gas_wallet; parameters } ->
        Custom_widget.Form.gas_wallet ctxt
          (Reactive.Bidirectional.of_var gas_wallet)
          ~for_reason:(t "Gas-Wallet / Operator")
        %% with_custom_contract_entrypoint ctxt ~contract_address ~entrypoint
             (fun destination_account entrypoint_parameters ->
               list_of_parameters ctxt parameters
               % Custom_widget.Debug.(
                   bloc ctxt
                     (bt "Account:" %% ct destination_account.id % br ()
                    %% bt "Variables:"
                     %% sexpable Variable.Record.sexp_of_t
                          entrypoint_parameters#variables)))
    | Call_foreign_contract_entrypoint
        { contract_address; entrypoint; gas_wallet; parameters } ->
        Custom_widget.Form.gas_wallet ctxt
          (Reactive.Bidirectional.of_var gas_wallet)
          ~for_reason:(t "Gas-Wallet / Operator")
        %% with_foreign_contract_entrypoint ctxt ~contract_address ~entrypoint
             (fun _destination_account entrypoint_parameters ->
               div
                 (t "Please enter a value of type"
                 %% Custom_widget.michelson ctxt
                      entrypoint_parameters#entrypoint_type
                 % t ":"
                 % list_of_parameters ctxt parameters
                   (* % Custom_widget.Editor.side_by_side
                       ~editor_tab_title:"Edit Michelson Parameter"
                       ~viewer_tab_title:"Michelson Verification" ctxt
                       ~editor:
                         (Custom_widget.Editor.low_level_editor ctxt
                            (Reactive.Bidirectional.of_var parameter) )
                       ~viewer:
                         (Reactive.bind_var parameter ~f:(fun param ->
                              match
                                Tezai_michelson.Concrete_syntax.parse_exn param
                                  ~check_primitives:true ~check_indentation:false
                              with
                              | v -> begin
                                (* Tezai_michelson.Untyped. *)
                                try
                                  let vn =
                                    Tezai_contract_metadata_manipulation
                                    .Micheline_helpers
                                    .normalize_combs ~primitive:"Pair" v in
                                  let r =
                                    Variable.Record
                                    .of_normalized_michelson_type_and_expression
                                      ~t:entrypoint_parameters#entrypoint_type
                                      ~v:vn in
                                  t "TODO:"
                                  %% Custom_widget.Debug.sexpable
                                       Variable.Record.sexp_of_t r
                                with e ->
                                  Custom_widget.ui_errorf ctxt
                                    "Failed to parse: %a" Exn.pp e
                              end
                              | exception e ->
                                  Custom_widget.ui_errorf ctxt
                                    "Failed to parse: %a" Exn.pp e ) ) *)))
  in
  let markdown_comments =
    Custom_widget.Editor.side_by_side_human_prose_editor ctxt comments_var
  in
  let simulation =
    Draft_simulation.make () ~up_message:(fun () ->
        let draft = Reactive.peek draft_var in
        Protocol.Message.Up.run_operation_draft_simulation ~draft)
  in
  let with_signatures =
    match draft with
    | Draft.Simple_transfer _ | Call_custom_contract_entrypoint _
    | Call_foreign_contract_entrypoint _ ->
        Reactive.return Validate.Operation.With_signatures.none_required
    | Call_generic_multisig_main { contract_address; _ }
    | Call_generic_multisig_update_keys { contract_address; _ } ->
        with_multisig_contract ctxt contract_address
          ~error:(fun () ->
            Reactive.pure Validate.Operation.With_signatures.not_yet)
          ~f:(fun _ multisig ->
            Reactive.map (draft_ready_for_sigs ctxt ~multisig draft_var)
              ~f:(function
              | None -> Validate.Operation.With_signatures.not_yet
              | Some (hash, _, _) ->
                  Validate.Operation.With_signatures.multisig
                    ~public_keys:multisig#keys ~threshold:multisig#threshold
                    ~hash))
  in
  let client_validated =
    Reactive.(
      bind
        (get draft_var ** with_signatures)
        ~f:(fun (draft, with_signatures) ->
          validate_whole_draft ctxt draft ~with_signatures))
  in
  let discard () =
    Custom_widget.inline_button (t "🗙 Cancel/Discard") ~action:(fun () ->
        Web_app_state.call_server_async ctxt
          (Protocol.Message.Up.delete_operation_draft ~id:op.id);
        Web_app_state.Current_page.change_to ctxt Web_app_state.Page.overview;
        ())
  in
  let valid_for_signatures =
    let with_signatures =
      match draft with
      | Draft.Simple_transfer _ | Call_custom_contract_entrypoint _
      | Call_foreign_contract_entrypoint _ ->
          Validate.Operation.With_signatures.none_required
      | Call_generic_multisig_main _ | Call_generic_multisig_update_keys _ ->
          Validate.Operation.With_signatures.not_yet
    in
    Reactive.(
      bind (get draft_var) ~f:(fun draft ->
          validate_whole_draft ctxt draft ~with_signatures
          |> Reactive.map ~f:(function Ok _ -> true | Error _ -> false)))
  in
  let valid_for_simulation_or_order =
    Reactive.(
      bind
        (get draft_var ** with_signatures)
        ~f:(fun (draft, with_signatures) ->
          validate_whole_draft ctxt draft ~with_signatures
          |> Reactive.map ~f:(function
               | Ok _ ->
                   true
                   && not
                        Validate.Operation.With_signatures.(
                          equal Not_yet with_signatures)
               | Error _ -> false)))
  in
  let signing_form =
    match draft with
    | Draft.Simple_transfer _ | Call_custom_contract_entrypoint _
    | Call_foreign_contract_entrypoint _ ->
        Bootstrap.alert ~kind:`Danger (t "You shouldn't be here")
    | Call_generic_multisig_main { contract_address; signatures; _ }
    | Call_generic_multisig_update_keys { contract_address; signatures; _ } ->
        with_multisig_contract ctxt contract_address
          ~error:(fun () ->
            Bootstrap.alert ~kind:`Danger (t "CANNOT SIGN STUFF!"))
          ~f:(fun destination_account multisig ->
            signatures_section ctxt ~draft_var ~signatures ~multisig
              ~target_contract_id:destination_account.id)
  in
  let edit_stage =
    Custom_widget.Staged_form.item ~button_title:(t "Edit")
      (main_form %% subsection (t "Comments") %% markdown_comments)
  in
  let sign_stage () =
    Custom_widget.Staged_form.item
      ~button_title:
        (Reactive.bind valid_for_simulation_or_order ~f:(function
          | true -> t "Signatures Ready ✅"
          | false -> t "Add Signatures ✍"))
      ~button_active:valid_for_signatures signing_form
  in
  let simulation_stage =
    Custom_widget.Staged_form.item ~button_title:(t "Run Simulation")
      ~load_action:(Draft_simulation.start_simulation_action simulation)
      (Draft_simulation.render ctxt simulation
      % Draft_simulation.make_button ctxt simulation
          ~client_validation:client_validated (t "Run Simulation Again"))
      ~button_active:valid_for_simulation_or_order
  in
  let order_stage =
    Custom_widget.Staged_form.item ~button_active:valid_for_simulation_or_order
      ~button_title:(t "Submit Operation")
      (Reactive.bind client_validated ~f:(function
        | Ok { v = specification; _ } ->
            let are_you_sure_banner =
              Custom_widget.are_you_sure_and_submit_banner ~banner_kind:`Danger
                ctxt ~button_label:(t "Order Operation")
                ~question:(bt "Are you sure you want to submit the order?")
                ~action:(fun () ->
                  let op =
                    {
                      op with
                      order = Specification.draft (Reactive.peek draft_var);
                      comments = Reactive.peek comments_var;
                    }
                  in
                  Web_app_state.call_server_async ctxt
                    (Protocol.Message.Up.glorify_operation_draft op);
                  Web_app_state.Current_page.change_to ctxt
                    Web_app_state.Page.overview)
            in
            show_operation_summary ctxt op ~comments_var ~draft_var
              ~specification
            %% are_you_sure_banner
        | Error _ ->
            Custom_widget.ui_errorf ctxt "BUG: this should not show up!"))
  in
  let form_stages =
    [ edit_stage ]
    @ (match draft with
      | Draft.Simple_transfer _ -> []
      | Call_custom_contract_entrypoint _ -> []
      | Call_foreign_contract_entrypoint _ -> []
      | Call_generic_multisig_main _ | Call_generic_multisig_update_keys _ ->
          [ sign_stage () ])
    @ [ simulation_stage; order_stage ]
  in
  page_title
  % Custom_widget.show_validation_errors_and_warnings ctxt client_validated
  % Custom_widget.Staged_form.render ctxt form_stages
      ~before_buttons:(discard () % bt " | ")

let render ctxt ?id () =
  let open Mono_html in
  match id with
  | None ->
      let current_drafts =
        let op_table = Web_app_state.operations ctxt in
        Reactive.Table.sort_extract_transform op_table
          ~compare:Web_app_state.Operation_row.younger_first
          ~extract:(Array.filter ~f:Web_app_state.Operation_row.is_draft)
          ~transform:(fun op ->
            Web_app_state.bind_operation ctxt op.id ~f:(fun op ->
                Operation_display.short ctxt op))
          ~reduce:(fun a b -> div a % div b)
      in
      let actions =
        let open Custom_widget.List_of_actions in
        render
          [
            item (t "Start a simple transfer") ~action:(fun () ->
                Web_app_state.new_operation_draft ctxt
                  (Operation.Draft.create_simple_transfer ()));
          ]
      in
      h3 (t "Order an Operation") % actions % current_drafts
  | Some id ->
      Web_app_state.bind_dynamic_operation ctxt id ~f:(fun op ->
          let open Operation in
          let the_form =
            Web_app_state.Dynamic_operation.operation op |> fun o ->
            match o.order with
            | Specification.Draft draft ->
                edit_operation_draft_form ctxt o
                  (Web_app_state.Dynamic_operation.comments_var op)
                  ~draft
            | _ ->
                Bootstrap.alert ~kind:`Danger
                  (t "Operation" %% ct o.id
                  %% t
                       "has already been ordered, it cannot be edited as a \
                        draft.")
          in
          the_form
          %% Custom_widget.Debug.(
               bloc ctxt
                 (t "Currently saved:"
                 %% sexpable Web_app_state.Dynamic_operation.sexp_of_t op)))
