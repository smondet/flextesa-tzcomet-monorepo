open Import
open Babuso_lib
open Wallet_data
open Lwd_bootstrap

module Page = struct
  type t =
    | Overview
    | Account of { id : string }
    | Operation of { id : string }
    | Settings
    | Tests
    | Operations
    | Draft_account of { id : string option }
    | Draft_operation of { id : string option }
    | Signing_interface
  [@@deriving variants, sexp, compare, equal]

  let to_string = Variants.to_name

  let navigables_in_order ~testing =
    [
      Overview; draft_account ~id:None; Operations; Signing_interface; Settings;
    ]
    @ if testing then [ Tests ] else []
end

module Local_failure = struct
  type content = Message of string [@@deriving variants, sexp, compare, equal]

  type t = {
    id : string;
    date : float;
    repeats : float list;
    content : content; [@main]
  }
  [@@deriving fields, make, sexp, compare, equal]
end

module Operation_row = struct
  type t = { id : string; last_update : float; draft : bool }
  [@@deriving fields, make, sexp, compare, equal, hash]

  let of_operation ({ Operation.id; last_update; _ } as op) =
    make ~id ~last_update ~draft:(Operation.is_draft op)

  let younger_first oa ob =
    match Float.compare ob.last_update oa.last_update with
    | 0 -> String.compare oa.id ob.id
    | n -> n

  let is_draft { draft; _ } = draft
end

module Dynamic_account = struct
  open Drafting

  type t = {
    saved : string Reference.t;
    v : Account.t; [@main]
    display_name_var : string Reference.t;
    comments_var : Human_prose.t option Reference.t;
  }
  [@@deriving fields, make, sexp, equal]

  let _serialize_account a = Account.sexp_of_t a |> Sexp.to_string_mach

  let of_account a =
    make
      ~saved:(_serialize_account a |> Reference.create)
      a
      ~display_name_var:(Reference.create a.Account.display_name)
      ~comments_var:(Reference.create a.Account.comments)

  let v (_ : t) = `No_please_use_account

  let account dynacc =
    {
      dynacc.v with
      Account.display_name = Reactive.peek dynacc.display_name_var;
      comments = Reactive.peek dynacc.comments_var;
    }

  let update_saved dynacc =
    Reactive.set (saved dynacc) (account dynacc |> _serialize_account)

  (* let saved_account a = Sexplib.Sexp.of_string a.saved |> Account.t_of_sexp *)
  let id a = a.v |> Account.id

  let has_changed_upstream dynacc newacc =
    let as_full_account = account dynacc in
    (not (Account.is_draft as_full_account && Account.is_draft newacc))
    && not
         (String.equal
            (_serialize_account as_full_account)
            (_serialize_account newacc))

  let has_changes self =
    not
      (String.equal
         (account self |> _serialize_account)
         (saved self |> Reference.get))
end

module Dynamic_operation = struct
  open Drafting

  type t = {
    v : Operation.t; [@main]
    saved : string Reference.t;
    comments_var : Human_prose.t option Reference.t;
  }
  [@@deriving fields, make, sexp, equal]

  let _serialize_op a = Operation.sexp_of_t a |> Sexp.to_string_mach

  let of_operation op =
    make
      ~saved:(_serialize_op op |> Reference.create)
      op
      ~comments_var:(Reference.create op.Operation.comments)

  let v (_ : t) = `No_please_use_operation
  let operation dyn = { dyn.v with comments = Reactive.peek dyn.comments_var }

  let operation_document dyn =
    Reactive.bind_var dyn.comments_var ~f:(fun comments ->
        Reactive.return { dyn.v with comments })

  let update_saved dyn =
    Reactive.set (saved dyn) (operation dyn |> _serialize_op)

  let has_changed_upstream dyn newop =
    let as_full = operation dyn in
    (not (Operation.is_draft as_full && Operation.is_draft newop))
    && not (String.equal (_serialize_op as_full) (_serialize_op newop))

  let has_changes self =
    not
      (String.equal
         (operation self |> _serialize_op)
         (saved self |> Reference.get))

  (* let saved_account a = Sexplib.Sexp.of_string a.saved |> Account.t_of_sexp *)
  let id a = a.v |> Operation.id
end

module Locally_stored_state = struct
  type t = {
    page : Page.t;
    signing_page_input : string;
    signing_page_account : string;
  }
  [@@deriving fields, sexp, make, equal]

  let file_path = "babuso-local-state"

  let default =
    make ~page:Page.overview ~signing_page_input:"" ~signing_page_account:""

  let load ctxt =
    Local_storage.read_file ctxt file_path
    |> Option.bind ~f:(fun s ->
           try Some (t_of_sexp (Sexplib.Sexp.of_string s)) with _ -> None)
    |> Option.value ~default

  let save ctxt v =
    Local_storage.write_file ctxt file_path
      ~content:(sexp_of_t v |> Sexp.to_string_mach)
end

type t = {
  page_history : Page.t list Reactive.var;
  current_page : [ `Page of Page.t | `Changing_to of Page.t ] Reactive.var;
  configuration : Wallet_configuration.t option Reactive.var;
  accounts : Dynamic_account.t Reactive.Table.t;
  operations : Operation_row.t Reactive.Table.t;
  operations_contents :
    (Operation.id, Dynamic_operation.t option Reactive.var) Hashtbl.t;
  exchange_rates : Exchange_rates.t option Reactive.var;
  network_information : Network_information.t option Reactive.var;
  failures : Local_failure.t list Reactive.var;
  alerts : Protocol.Message.Down.Alert.t list Reactive.var;
  low_resolution_date_10s : float Reactive.var;
  low_resolution_date_5m : float Reactive.var;
  signing_page_input : string Reactive.var;
  signing_page_account : string Reactive.var;
}
[@@deriving fields]

let make ~locally_stored () =
  let { Locally_stored_state.page; signing_page_input; signing_page_account } =
    locally_stored
  in
  {
    page_history = Reactive.var [];
    current_page = Reactive.var (`Changing_to page);
    configuration = Reactive.var None;
    accounts = Reactive.Table.make ();
    operations = Reactive.Table.make ();
    operations_contents = Hashtbl.create (module String);
    failures = Reactive.var [];
    exchange_rates = Reactive.var None;
    network_information = Reactive.var None;
    alerts = Reactive.var [];
    low_resolution_date_10s = Reactive.var (Unix.gettimeofday ());
    low_resolution_date_5m = Reactive.var (Unix.gettimeofday ());
    signing_page_input = Reactive.var signing_page_input;
    signing_page_account = Reactive.var signing_page_account;
  }

let get ctxt : t = ctxt#state
let state = get

module Local_failures = struct
  let get_var ctxt = (get ctxt).failures

  let fresh ?repeats c =
    Local_failure.make ~id:(Fresh_id.make ()) ?repeats
      ~date:(Unix.gettimeofday ()) c

  let add_message ctxt s =
    let open Local_failure in
    let cur = get_var ctxt in
    let peeked = Reactive.peek cur in
    let content = Local_failure.message s in
    match List.find peeked ~f:(fun lf -> equal_content lf.content content) with
    | Some previous ->
        let new_one =
          fresh content ~repeats:(previous.date :: previous.repeats)
        in
        Reactive.set cur
          (new_one
          :: List.filter peeked ~f:(fun lf ->
                 not (String.equal lf.id previous.id)))
    | None ->
        let new_one = fresh content in
        Reactive.set cur (new_one :: peeked)

  let remove ctxt id =
    let cur = get_var ctxt in
    Reactive.set cur
      (List.filter (Reactive.peek cur) ~f:(fun lf ->
           not (String.equal lf.id id)))
end

(** [action_not_implemented ctxt thing] is a [unit -> unit] function that works
    nicely with the [~action] parameter of HTML buttons and adds an asynchronous
    failure. *)
let action_not_implemented ctxt thing () =
  dbgf "Action %S not implemented" thing;
  Fmt.kstr (Local_failures.add_message ctxt) "%S is not implemented yet …" thing

(** {!Server_communication.call_with_var_unit} wired with
    {!Local_failures.add_message} and an optional `Wip.t` variable. *)
let call_server_async ?(wip_var = Wip.idle_var ()) ctxt up =
  Server_communication.call_with_var_unit wip_var up
    ~catch_failure:(Local_failures.add_message ctxt)

module Configuration = struct
  let get ctxt =
    let self = state ctxt in
    Reactive.get self.configuration

  let peek ctxt =
    let self = state ctxt in
    Reactive.peek self.configuration

  let set ctxt conf =
    let self = state ctxt in
    dbgf "setting the configuration";
    Reactive.set self.configuration (Some conf)

  let unset ctxt =
    let self = state ctxt in
    Reactive.set self.configuration None

  let ensure_initialized ctxt =
    let self = state ctxt in
    if Option.is_none (Reactive.peek self.configuration) then begin
      let open Protocol.Message in
      let open Lwt.Infix in
      dbgf "getting the configuration";
      Server_communication.call Up.get_configuration >>= function
      | Down.Configuration conf ->
          set ctxt conf;
          Lwt.return_unit
      | other ->
          Fmt.kstr
            (Local_failures.add_message ctxt)
            "Wrong return: %a" Sexp.pp_hum (Down.sexp_of_t other);
          Lwt.return_unit
    end
    else Lwt.return_unit

  let debug ctxt =
    let open Wallet_configuration in
    Reactive.map (get ctxt) ~f:(function
      | None -> false
      | Some { ui_options = { debug; _ }; _ } -> debug)

  let compact ctxt =
    let open Wallet_configuration in
    Reactive.map (get ctxt) ~f:(function
      | None -> false
      | Some { ui_options = { compact; _ }; _ } -> compact)

  let operations_per_page ctxt =
    let open Wallet_configuration in
    Reactive.map (get ctxt) ~f:(function
      | None -> 6
      | Some { ui_options = { operations_per_page; _ }; _ } ->
          operations_per_page)

  let modification_action ctxt f =
    match peek ctxt with
    | None ->
        Local_failures.add_message ctxt
          "Cannot modify configuration, it is currently not available on the \
           client."
    | Some conf ->
        unset ctxt;
        let conf = f conf in
        call_server_async ctxt (Protocol.Message.Up.update_configuration conf)
end

module Current_page = struct
  let set ctxt p =
    let self = state ctxt in
    Reactive.set self.current_page p

  let get ctxt =
    let self = state ctxt in
    Reactive.get self.current_page

  let navigables ctxt =
    let self = state ctxt in
    let open Reactive in
    Configuration.debug ctxt ** get self.current_page
    |> Reactive.map ~f:(fun (testing, current) ->
           List.map (Page.navigables_in_order ~testing) ~f:(fun page ->
               ( page,
                 match current with
                 | `Changing_to _ -> false
                 | `Page current -> Page.equal page current )))

  let peek_page ctxt =
    let self = state ctxt in
    match Reactive.peek self.current_page with `Changing_to p | `Page p -> p

  let change_to ctxt page =
    let self = state ctxt in
    let p = peek_page ctxt in
    Reactive.set self.page_history
      (List.take (p :: Reactive.peek self.page_history) 10);
    Reactive.set self.current_page (`Changing_to page)

  let navigation_history ctxt =
    let self = state ctxt in
    Reactive.get self.page_history
    |> Reactive.map ~f:(fun h -> peek_page ctxt :: h)
end

let locally_storable ctxt : Locally_stored_state.t =
  let state = state ctxt in
  Locally_stored_state.make
    ~page:(Current_page.peek_page ctxt)
    ~signing_page_input:(Reactive.peek state.signing_page_input)
    ~signing_page_account:(Reactive.peek state.signing_page_account)

module Signing_page = struct
  let input ctxt = (get ctxt).signing_page_input
  let account ctxt = (get ctxt).signing_page_account
end

let dynamic_accounts ctxt = (get ctxt).accounts

let accounts ctxt =
  dynamic_accounts ctxt |> Reactive.Table.list_document
  |> Reactive.map ~f:(List.map ~f:Dynamic_account.account)

let bind_account_opt ctxt id ~f =
  Reactive.bind (accounts ctxt) ~f:(fun accs ->
      f (List.find accs ~f:(fun acc -> String.equal (Account.id acc) id)))

let bind_account ctxt id ~f =
  bind_account_opt ctxt id ~f:(function
    | Some s -> f s
    | None ->
        let open Mono_html in
        Bootstrap.alert ~kind:`Danger
          (Fmt.kstr t "BUG: account %S not found" id))

let bind_account_by_address ctxt addr ~f =
  Reactive.bind (accounts ctxt) ~f:(fun l ->
      f
        (List.find l ~f:(fun acc ->
             Option.equal String.equal (Account.get_address acc) (Some addr))))

let bind_account_by_public_key ctxt pk ~f =
  Reactive.bind (accounts ctxt) ~f:(fun l ->
      f
        (List.find l ~f:(fun acc ->
             Option.equal String.equal (Account.get_public_key acc) (Some pk))))

let operations ctxt = (get ctxt).operations
let operations_contents ctxt = (get ctxt).operations_contents
let failures ctxt = (get ctxt).failures
let exchange_rates ctxt = (get ctxt).exchange_rates

let peek_operation ctxt id =
  match Hashtbl.find (operations_contents ctxt) id with
  | None ->
      Local_failures.add_message ctxt (Fmt.str "Operation %s not found :(" id);
      Fmt.failwith "Operation not found: %s" id
  | Some b -> begin
      match Reactive.peek b with
      | None ->
          Local_failures.add_message ctxt
            (Fmt.str "Operation %s not found :,(" id);
          Fmt.failwith "Operation not found: %s" id
      | Some x -> Dynamic_operation.operation x
    end

let bind_operation_opt ctxt id ~f =
  let dynopt =
    Reactive.Table.find_map (operations ctxt) ~f:(fun op ->
        if String.(Operation_row.id op = id) then
          Hashtbl.find (operations_contents ctxt) id
        else None)
  in
  Reactive.bind dynopt ~f

let rec bind_dynamic_operation ctxt id ~f =
  match Hashtbl.find (operations_contents ctxt) id with
  | None ->
      (* Ask the backend to retrieve it *)
      (* Local_failures.add_message ctxt (Fmt.str "Operation %s not found!" id) ; *)
      Hashtbl.update (operations_contents ctxt) id ~f:(function
        | None -> Reactive.var None
        | Some v -> v);
      bind_dynamic_operation ctxt id ~f
  | Some b ->
      Reactive.bind_var b ~f:(function
        | None -> Mono_html.empty ()
        | Some x -> f x)

let rec bind_operation ctxt id ~f =
  match Hashtbl.find (operations_contents ctxt) id with
  | None ->
      (* Ask the backend to retrieve it *)
      (* Local_failures.add_message ctxt (Fmt.str "Operation %s not found!" id) ; *)
      Hashtbl.update (operations_contents ctxt) id ~f:(function
        | None -> Reactive.var None
        | Some v -> v);
      bind_operation ctxt id ~f
  | Some b ->
      Reactive.bind_var b ~f:(function
        | None -> Mono_html.empty ()
        | Some x -> Reactive.bind (Dynamic_operation.operation_document x) ~f)

module Alerts = struct
  let get_var ctxt = (get ctxt).alerts
  let currently_on ctxt = get_var ctxt |> Reactive.get

  let add ctxt alert =
    let v = (get ctxt).alerts in
    Reactive.set v (alert :: Reactive.peek v);
    ()

  let remove ctxt alert_id =
    let v = (get ctxt).alerts in
    Reactive.set v
      (List.filter ~f:String.(fun al -> al.id <> alert_id) (Reactive.peek v));
    ()
end

module Network_info = struct
  let reactive ctxt = (get ctxt).network_information |> Reactive.get

  let public_network ctxt =
    Reactive.map (reactive ctxt) ~f:(function
      | None -> None
      | Some i -> (
          match Network_information.network i with
          | Some Mainnet -> Some `Mainnet
          | Some Kathmandunet -> Some `Kathmandunet
          | Some Ghostnet -> Some `Ghostnet
          | Some (Sandbox _) | None -> None))

  let known_chain_ids ctxt =
    let sandboxes =
      match (get ctxt).network_information |> Reactive.peek with
      | None -> []
      | Some { network = Some (Network.Sandbox sbox); _ } -> [ sbox ]
      | Some _ -> []
    in
    sandboxes @ Network.all_public_chain_ids
end

let get_low_resolution_date ctxt = function
  | `Seconds_10 -> (get ctxt).low_resolution_date_10s |> Reactive.get
  | `Minutes_5 -> (get ctxt).low_resolution_date_5m |> Reactive.get

module Operations = struct
  let merge_with ?(clear = false) ctxt l =
    let op_table = operations ctxt in
    assert clear;
    if clear then begin
      Reactive.Table.clear op_table;
      ()
    end;
    List.iter l ~f:(fun op ->
        Hashtbl.update (operations_contents ctxt) op.Operation.id ~f:(function
          | None -> Reactive.var (Some (Dynamic_operation.of_operation op))
          | Some opv -> begin
              match Reactive.peek opv with
              | None ->
                  Reactive.set opv (Some (Dynamic_operation.of_operation op));
                  opv
              | Some v when Dynamic_operation.has_changed_upstream v op ->
                  Reactive.set opv (Some (Dynamic_operation.of_operation op));
                  opv
              | Some v ->
                  Dynamic_operation.update_saved v;
                  opv
            end);
        Reactive.Table.append op_table (Operation_row.of_operation op);
        ());
    ()
end

let new_operation_draft ctxt kind =
  let open Operation in
  let op = new_draft kind in
  call_server_async ctxt (Protocol.Message.Up.save_operation_draft op);
  Current_page.change_to ctxt (Page.draft_operation ~id:(Some op.id))

let get_all_the_things ctxt =
  let open Protocol.Message in
  let open Lwt.Infix in
  Configuration.ensure_initialized ctxt >>= fun () ->
  (Server_communication.call Up.All_accounts >>= function
   | Down.Account_list l ->
       let accarr = Array.of_list_map l ~f:Option.some in
       Reactive.Table.iter_rows (dynamic_accounts ctxt) ~f:(fun row ->
           match Reactive.Table.get row with
           | None -> ()
           | Some dynacc -> (
               let id = Dynamic_account.id dynacc in
               match
                 Array.findi accarr ~f:(fun _ -> function
                   | None -> false
                   | Some acc -> String.equal (Account.id acc) id)
               with
               | Some (_, None) -> ()
               | Some (i, Some newacc)
                 when Dynamic_account.has_changed_upstream dynacc newacc ->
                   Reactive.Table.set row (Dynamic_account.of_account newacc);
                   accarr.(i) <- None
               | Some (i, Some _) (* not changed *) ->
                   Dynamic_account.update_saved dynacc;
                   accarr.(i) <- None
               | None -> Reactive.Table.remove row));
       Array.iter accarr ~f:(function
         | None -> ()
         | Some newacc ->
             Reactive.Table.append (dynamic_accounts ctxt)
               (Dynamic_account.of_account newacc));
       (* Reactive.Table.clear (dynamic_accounts ctxt) ;
          List.iter l ~f:(fun acc ->
              Reactive.Table.append (dynamic_accounts ctxt)
                (Dynamic_account.of_account acc) ) ; *)
       Lwt.return_unit
   | other ->
       Fmt.kstr
         (Local_failures.add_message ctxt)
         "Wrong return: %a" Sexp.pp_hum (Down.sexp_of_t other);
       Lwt.return_unit)
  >>= fun () ->
  Server_communication.call Up.all_operations >>= function
  | Down.Operation_list l ->
      Operations.merge_with ~clear:true ctxt l;
      Lwt.return_unit
  | other ->
      Fmt.kstr
        (Local_failures.add_message ctxt)
        "Wrong return: %a" Sexp.pp_hum (Down.sexp_of_t other);
      Lwt.return_unit

let save_draft_account ctxt account_id =
  let open Lwt.Infix in
  (* dbgp Docpp.(dbgprompt +++ textf "account draft %s" account_id) ; *)
  Reactive.Table.Lwt.find (dynamic_accounts ctxt)
    ~f:String.(fun a -> Lwt.return (Dynamic_account.id a = account_id))
  >>= function
  | None ->
      Local_failures.add_message ctxt
        (Fmt.str "Saving draft error: %s not found" account_id);
      Lwt.return_unit
  | Some
      ({
         v = { state = Account.Status.Draft draft; _ } as acc;
         saved = _;
         display_name_var;
         comments_var;
       } as dynacc)
    when Dynamic_account.has_changes dynacc ->
      Server_communication.call
        (Protocol.Message.Up.save_account_draft ~id:acc.id
           ~display_name:(Reactive.peek display_name_var)
           ~comments:(Reactive.peek comments_var)
           ~glorify:false ~content:draft)
      >>= fun _ -> Lwt.return_unit
  | _ -> Lwt.return_unit

let save_draft_operation ctxt op_id =
  let open Lwt.Infix in
  Hashtbl.find (operations_contents ctxt) op_id |> Option.bind ~f:Reactive.peek
  |> function
  | None ->
      Local_failures.add_message ctxt
        (Fmt.str "Saving draft error: %s not found" op_id);
      Lwt.return_unit
  | Some
      ({
         Dynamic_operation.v =
           { order = Operation.Specification.Draft _draft; _ };
         _;
       } as dyn)
    when Dynamic_operation.has_changes dyn ->
      Server_communication.call
        (Protocol.Message.Up.save_operation_draft
           (Dynamic_operation.operation dyn))
      >>= fun _ -> Lwt.return_unit
  | _ -> Lwt.return_unit

let init ctxt =
  let open Lwt.Infix in
  let open Protocol.Message in
  let backoff = ref 1. in
  let events_since = ref Float.min_value in
  let record_failuref fmt =
    Fmt.kstr
      (fun s ->
        (Local_failures.add_message ctxt) (Fmt.str "Event loop error: %s" s);
        backoff := !backoff *. 2.;
        Js_of_ocaml_lwt.Lwt_js.sleep !backoff)
      fmt
  in
  let rec loop () =
    Lwt.catch
      (fun () ->
        dbgf "Getting events";
        Server_communication.call ~timeout:10_000. (Up.Get_events !events_since)
        >>= function
        | Down.Events (events, date) ->
            events_since := date;
            dbgf "Received events: %f %a" date
              Fmt.(Dump.list Sexp.pp)
              (List.map ~f:Down.sexp_of_event events);
            backoff := 1.;
            let reloaded_all_once = ref false in
            Lwt_list.iter_s
              (function
                | Down.Please_just_reload_all reason ->
                    dbgf "Asked to reload all: %s -> %s" reason
                      (if !reloaded_all_once then "NO" else "OK");
                    if !reloaded_all_once then Lwt.return_unit
                    else
                      get_all_the_things ctxt >>= fun () ->
                      reloaded_all_once := true;
                      Lwt.return_unit
                | Down.New_exchange_rates rates ->
                    Reactive.set (get ctxt).exchange_rates (Some rates);
                    Lwt.return_unit
                | Down.Fresh_network_information info ->
                    Reactive.set (get ctxt).network_information (Some info);
                    Lwt.return_unit
                | Down.Alert_on alert ->
                    Alerts.add ctxt alert;
                    Lwt.return_unit
                | Down.Alert_off { id } ->
                    Alerts.remove ctxt id;
                    Lwt.return_unit
                | Down.Configuration_update conf ->
                    Configuration.set ctxt conf;
                    Lwt.return_unit
                | Down.Async_failure_event failure ->
                    Local_failures.add_message ctxt failure;
                    Lwt.return_unit)
              events
            >>= fun () -> loop ()
        | other ->
            record_failuref "wrong server message %a" Sexp.pp
              (Down.sexp_of_t other)
            >>= fun () -> loop ())
      (fun exn ->
        record_failuref "comminucation error %a" Exn.pp exn >>= fun () ->
        loop ())
  in
  let rec quick_loop () =
    let now = Unix.gettimeofday () in
    Reactive.set (get ctxt).low_resolution_date_10s now;
    if
      Float.(Reactive.peek (get ctxt).low_resolution_date_5m + (60. * 5.) > now)
    then Reactive.set (get ctxt).low_resolution_date_5m now
      (* Operations.update_aging ctxt *);
    Js_of_ocaml_lwt.Lwt_js.sleep 10. >>= fun () -> quick_loop ()
  in
  Lwt.async quick_loop;
  let rec very_quick_loop () =
    let dbgprompt = Docpp.(text "✍ draft_saving_loop ➰➰➰") in
    dbgp Docpp.(dbgprompt +++ text "starts");
    begin
      begin
        match Reactive.peek (get ctxt).current_page with
        | `Page (Draft_account { id = Some account_id }) ->
            save_draft_account ctxt account_id
        | `Page (Draft_operation { id = Some op_id }) ->
            save_draft_operation ctxt op_id
        | _ -> Lwt.return_unit
      end
      >>= fun () ->
      Locally_stored_state.save ctxt (locally_storable ctxt);
      Js_of_ocaml_lwt.Lwt_js.sleep 1. >>= fun () -> very_quick_loop ()
    end
  in
  Lwt.async very_quick_loop;
  Lwt.async (fun () ->
      (Server_communication.call Up.get_exchange_rates >>= function
       | Down.Exchange_rates (Some er) ->
           Reactive.set (get ctxt).exchange_rates (Some er);
           Lwt.return_unit
       | _ -> Lwt.return_unit)
      >>= fun () ->
      get_all_the_things ctxt >>= fun () -> loop ())
