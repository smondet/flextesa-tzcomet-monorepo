open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap
open! Custom_widget

module Table_field = struct
  type t = Status | Summary | Comment_summary | Last_update
  [@@deriving sexp, compare, equal, variants]

  let default = [ summary; status; comment_summary; last_update ]
end

let render_rows ctxt ?(continue_with = fun _ -> Mono_html.empty ())
    ?(with_header = true) ?(show_field_titles = true)
    ?(fields = Table_field.default) table ~title_text ~filter =
  let open Mono_html in
  let field_title f =
    let open Table_field in
    match f with
    | Status -> t "Status"
    | Summary -> t "Summary"
    | Comment_summary -> t "Comments"
    | Last_update -> t "Last Updated"
  in
  let operation_row op =
    let open Table_field in
    let open Operation in
    let kind =
      match op.status with
      | Status.Paused | Status.Ordered -> `Primary
      | Status.Work_in_progress _ -> `Warning
      | Status.Success _ -> `Success
      | Status.Failed _ -> `Danger
    in
    let spec_summary = Operation_display.short ctxt op in
    Bootstrap.Table.row
      ~a_tr:[ classes [ Bootstrap.Label_kind.fill_div_class kind ] ]
    @@ List.map fields ~f:(function
         | Status -> begin
             match op.status with
             | Status.Paused -> t "Paused-draft ⏸"
             | Status.Ordered -> t "Ordered ✍"
             | Status.Work_in_progress _ -> t "Work-in-Progress 👷"
             | Status.Success _ -> t "Done ✅"
             | Status.Failed _ -> t "Failed 😿"
           end
         | Comment_summary -> Human_prose_widget.option_summary op.comments
         | Last_update ->
             let time_info =
               let open Float in
               Reactive.bind
                 (Web_app_state.get_low_resolution_date ctxt `Seconds_10)
                 ~f:(fun now ->
                   match now - op.last_update with
                   | s when s < 5. -> t "Seconds ago"
                   | s when s < 60. * 55. ->
                       let mins = to_int (s / 60. |> round_up) in
                       Fmt.kstr t "About %d minute%s ago" mins
                         Int.(if mins = 1 then "" else "s")
                   | s when s < 60. * 60. *. 10. ->
                       let hours = to_int (s / (60. * 60.) |> round_up) in
                       Fmt.kstr t "About %d hour%s ago" hours
                         Int.(if hours = 1 then "" else "s")
                   | _ ->
                       let date_string =
                         (new%js Js_of_ocaml.Js.date_fromTimeValue
                            (1000. *. op.last_update))##toISOString
                         |> Js_of_ocaml__Js.to_string
                       in
                       Fmt.kstr t "On %s" date_string)
             in
             i time_info
         | Summary -> spec_summary)
  in
  let extract ~page_length page array =
    let more a =
      let pos = page * page_length in
      let len = min page_length (Array.length a - pos) in
      try Array.sub a ~pos ~len
      with _ ->
        dbgf "sub: %d %d (%d)" pos len (Array.length a);
        a
    in
    Array.filter ~f:filter array |> more
  in
  let page_var = Reactive.var 0 in
  Reactive.bind
    Reactive.(
      Table.count table filter ** get page_var
      ** Web_app_state.Configuration.operations_per_page ctxt)
    ~f:(fun (table_length, (page, page_length)) ->
      let rows =
        match table_length with
        | 0 ->
            Debug.or_empty ctxt (fun () ->
                Bootstrap.Table.row
                  [ Debug.bloc ctxt (Fmt.kstr bt "Empty %s" title_text) ])
        | _ ->
            let all_columns () =
              H5.a_colspan (Reactive.pure (List.length fields))
            in
            let first_row =
              let title_cell = div_title (t title_text) in
              match (with_header, show_field_titles) with
              | false, _ -> empty ()
              | true, false -> tr (th ~a:[ all_columns () ] title_cell)
              | true, true ->
                  Bootstrap.Table.header_row
                    (title_cell :: List.map (List.tl_exn fields) ~f:field_title)
            in
            let last_row =
              if table_length > page_length then
                tr
                  (td
                     ~a:[ all_columns (); style "font-size: 90%" ]
                     (let nb_pages = (table_length / page_length) + 1 in
                      let dbg =
                        Fmt.kstr (Debug.text ctxt)
                          "table-length: %d, page-length: %d, pages: %d"
                          table_length page_length nb_pages
                      in
                      bt "Page:"
                      %% list
                           (Language.oxfordize_list ~map:Fn.id
                              ~sep:(fun () -> t ", ")
                              ~last_sep:(fun () -> t ", ")
                              (List.init nb_pages ~f:(fun pnb ->
                                   let show =
                                     Fmt.kstr t "%d–%d"
                                       ((pnb * page_length) + 1)
                                       (min
                                          ((pnb + 1) * page_length)
                                          table_length)
                                   in
                                   if pnb = page then show
                                   else
                                     action_link ctxt show ~action:(fun () ->
                                         Reactive.set page_var pnb))))
                      % dbg))
              else empty ()
            in
            first_row
            % Reactive.Table.sort_extract_transform table
                ~compare:Web_app_state.Operation_row.younger_first
                ~extract:(extract ~page_length page)
                ~transform:(fun op ->
                  Web_app_state.bind_operation ctxt ~f:operation_row op.id)
                ~reduce:(fun a b -> a % b)
            % last_row
      in
      rows % continue_with (if table_length > 0 then `Not_empty else `Empty))

let recent { Web_app_state.Operation_row.last_update; _ } =
  Float.(last_update + (5. * 60.) > Unix.gettimeofday ())

let recent_operations_section ?with_header ?(title_text = "Recent Operations")
    ?(extra_filter = fun _ -> true) ?show_field_titles ?continue_with ctxt =
  let is_not_draft ({ Web_app_state.Operation_row.draft; _ } as op) =
    recent op && (not draft) && extra_filter op
  in
  let table = Web_app_state.operations ctxt in
  render_rows ctxt ?with_header table ~filter:is_not_draft ?continue_with
    ~title_text ?show_field_titles

let old_operations_section ?with_header ?(title_text = "Old Operations")
    ?(extra_filter = fun _ -> true) ?show_field_titles ctxt =
  let filter op =
    (not (Web_app_state.Operation_row.is_draft op))
    && (not (recent op))
    && extra_filter op
  in
  let table = Web_app_state.operations ctxt in
  render_rows ctxt table ~filter ?with_header ?show_field_titles ~title_text

let draft_operations_section ?with_header ?(title_text = "Operation Drafts")
    ?(extra_filter = fun _ -> true) ?show_field_titles ?continue_with ctxt =
  let filter ({ Web_app_state.Operation_row.draft; _ } as op) =
    draft && extra_filter op
  in
  let table = Web_app_state.operations ctxt in
  render_rows ?continue_with ?show_field_titles ?with_header ctxt table ~filter
    ~title_text

let whole_table ctxt =
  (* let open Mono_html in *)
  let should_show = function `Empty -> true | `Not_empty -> false in
  make_table ctxt
    (draft_operations_section ctxt ~continue_with:(fun recent_status ->
         let show_field_titles = should_show recent_status in
         recent_operations_section ~show_field_titles ctxt
           ~continue_with:(fun draft_status ->
             let show_field_titles =
               show_field_titles && should_show draft_status
             in
             old_operations_section ctxt ~show_field_titles)))

let table_for_account ctxt acc =
  (* let open Mono_html in *)
  let should_show = function `Empty -> true | `Not_empty -> false in
  let extra_filter op =
    let acc_id = Account.id acc in
    let acc_addr = Account.get_address acc in
    let op =
      Web_app_state.peek_operation ctxt op.Web_app_state.Operation_row.id
    in
    let open Operation in
    match op.order with
    | Specification.Origination { account; gas_wallet; _ } ->
        String.equal account acc_id || String.equal gas_wallet acc_id
    | Specification.Transfer { source; destination; _ } ->
        String.equal source acc_id
        || Option.equal String.equal (Some destination) acc_addr
    | Specification.Draft _ ->
        let source, address =
          (get_source_account_id op, get_destination_address op)
        in
        Option.equal String.equal source (Some acc_id)
        || Option.is_some acc_addr
           && Option.equal String.equal address acc_addr
  in
  let title_text = "Operations" in
  let open Mono_html in
  make_table ctxt
    (draft_operations_section ctxt ~title_text ~extra_filter
       ~continue_with:(fun draft_status ->
         let show_field_titles = should_show draft_status in
         recent_operations_section ~title_text ~extra_filter ~show_field_titles
           ~with_header:show_field_titles ctxt
           ~continue_with:(fun recent_status ->
             let show_field_titles =
               show_field_titles && should_show recent_status
             in
             (if show_field_titles then empty ()
             else
               tr
                 ~a:
                   [
                     style
                       "height: 5px; padding: 0; margin: 0; background-color: \
                        #aaa";
                   ]
                 (td
                    ~a:
                      [
                        style "padding: 0; margin: 0";
                        H5.a_colspan (Reactive.pure 4);
                      ]
                    (empty ())))
             % old_operations_section ~title_text ctxt ~extra_filter
                 ~with_header:show_field_titles ~show_field_titles)))
