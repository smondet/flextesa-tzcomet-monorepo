open Import

let lwd_onload _ =
  let open Tyxml_lwd in
  let open Js_of_ocaml in
  let base_div = Dom_html.getElementById "attach-ui" in
  base_div##.innerHTML := Js.string "";
  Lwt.ignore_result
    (let local_storage = Local_storage.create () in
     let locally_stored =
       let storage_context =
         object
           method storage = local_storage
         end
       in
       Web_app_state.Locally_stored_state.load storage_context
     in
     let state =
       let window = Browser_window.create () in
       let state = Web_app_state.make ~locally_stored () in
       object
         method window = window
         method state = state
         method storage = local_storage
       end
     in
     Web_app_state.init state;
     let doc = Gui.render state in
     let root = Lwd.observe doc in
     Lwd.set_on_invalidate root (fun _ ->
         let _frame =
           Dom_html.window##requestAnimationFrame
             (Js.wrap_callback (fun _ ->
                  while Lwd.is_damaged root do
                    let _sample = Lwd.quick_sample root in
                    ()
                  done))
         in
         ());
     List.iter ~f:(Dom.appendChild base_div)
       (Lwd_seq.to_list (Lwd.quick_sample root) : _ node list :> raw_node list);
     Lwt.return_unit);
  Js._false

let () =
  debug_formatter := (* Looks better on the JS console: *) Fmt.stdout;
  dbgf "Hello Main!";
  let open Js_of_ocaml in
  (Lwt.async_exception_hook := fun e -> dbgf "Async Exn: %s" (Exn.to_string e));
  Dom_html.window##.onload := Dom_html.handler lwd_onload
