open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

let validate_whole_draft ctxt draft =
  let rates = Web_app_state.exchange_rates ctxt |> Reactive.peek in
  Drafting.Render.env (fun () ->
      Validate.Account.draft ~rates
        ~address_of_account_id:(fun id -> "tz123FakeAddressOf-" ^ id)
        ~public_key_of_account_id:(fun id -> "edpkFakePublicKeyOf-" ^ id)
        draft
      (* |> Drafting.Render.Env.ignore_result *))

let save_draft ?(glorify = false) ?name ?comments ctxt ~id content =
  let display_name =
    match name with
    | Some s -> s
    | None -> (
        match content with
        | Account.Draft.Generic_multisig _ ->
            Fmt.str "MultiSig %s" (String.sub id ~pos:0 ~len:4)
        | Account.Draft.Custom_contract _ ->
            Fmt.str "Custom-contract %s" (String.sub id ~pos:0 ~len:4)
        | From_address_or_uri _ ->
            Fmt.str "Address-URI %s" (String.sub id ~pos:0 ~len:4))
  in
  Web_app_state.call_server_async ctxt
    (Protocol.Message.Up.save_account_draft ~id ~display_name ~comments ~glorify
       ~content)

let get_variables ~capabilities ~roles ~timers ~spending_limits ~teams =
  Drafting.Render.env (fun () ->
      Validate.Account.variables_of_custom_contract ~capabilities ~roles ~timers
        ~spending_limits ~teams)
  |> Reactive.map ~f:(function
       | Error _ -> []
       | Ok { Drafting.Render.v = l; _ } -> l)

let collection_like_roles ctxt ~fresh ~name col ~f =
  let open Mono_html in
  Reactive.Table.concat_map col ~map:(fun row v ->
      div ~a:[ Custom_widget.Debug.boxify_style ctxt ~level:1 "" ] (f row v))
  % Custom_widget.inline_button (Fmt.kstr t "New %s ➕" name) ~action:(fun () ->
        Drafting.Collection.append col (fresh ()))

let remove_row ?(content = Mono_html.t "❌") row =
  Custom_widget.inline_button content ~action:(fun () ->
      Reactive.Table.remove row)

let custom_contract_subform ctxt ~gas_wallet ~capabilities
    ~has_default_do_nothing ~roles ~timers ~spending_limits ~teams =
  let open Mono_html in
  let open Account in
  let open Draft in
  let toolkit =
    let module TK = Custom_widget.Toolkit_automaton in
    let items =
      [
        TK.item ~button_name:(t "Add administrator pattern") ~form:(fun ~idle ->
            let base_name = Reactive.var "administrator" in
            div (bt "Adding an admin …")
            % t "Base-name:"
            %% Bootstrap.Input.bidirectional
                 (Reactive.Bidirectional.of_var base_name)
            %% Custom_widget.inline_button (t "Go!") ~action:(fun () ->
                   let base = Reactive.peek base_name in
                   let role = Role.create_empty () in
                   Reactive.set (Role.public_name role) base;
                   let cap = Capability.create_empty () in
                   let cap_name = Fmt.str "update%s" (String.capitalize base) in
                   Reactive.set (Capability.name cap) cap_name;
                   Reactive.set (Capability.control cap)
                     (Some
                        (Capability.Control.sender_is
                           (Reactive.var (Some (Role.id role)))));
                   Reactive.Table.append (Capability.actions cap)
                     (Reactive.var
                        (Capability.Action.set_variable
                           (Reactive.var (Some (Role.id role)))));
                   Reactive.Table.append roles role;
                   Reactive.Table.append capabilities cap;
                   idle
                     [
                       t "Created role" %% ct base
                       %% parens (it "not initialized")
                       % t ".";
                       t "Created capability" %% ct cap_name
                       %% t "to allow the administrator"
                       %% ct base %% t "to update themselves.";
                     ]));
        TK.item ~button_name:(t "Add dead-human-switch pattern")
          ~form:(fun ~idle ->
            let weeks = Reactive.var 8 in
            div (bt "Adding a dead-human-switch …")
            % div
                (t "Wait"
                % Bootstrap.Input.range ~init_value:3 ~cols:3 ~inline_block:true
                    ~min:1 ~max:100 ~step:1. ~action:(Reactive.set weeks) None
                % Reactive.bind_var weeks ~f:(fun m -> b (Fmt.kstr ct "%d" m))
                %% t "weeks before unlocking.")
            %% Custom_widget.inline_button (t "Go!") ~action:(fun () ->
                   let beneficiary_name = "heir" in
                   let role = Role.create_empty () in
                   Reactive.set (Role.public_name role) beneficiary_name;
                   let timer_name = Fmt.str "%sUnlockTimer" beneficiary_name in
                   let timer_weeks = Reactive.peek weeks in
                   let timer = Timer.create_empty () in
                   Reactive.set (Timer.public_name timer) timer_name;
                   Reactive.set
                     (Timer.initial_value timer)
                     (Fmt.str "%d weeks" timer_weeks);
                   let update = Capability.create_empty () in
                   let update_cap_name =
                     Fmt.str "configure%sDeadHumanSwitch"
                       (String.capitalize beneficiary_name)
                   in
                   Reactive.set (Capability.name update) update_cap_name;
                   Reactive.set
                     (Capability.control update)
                     (Some (Capability.Control.sender_is (Reactive.var None)));
                   Reactive.Table.append
                     (Capability.actions update)
                     (Reactive.var
                        (Capability.Action.set_variable
                           (Reactive.var (Some (Role.id role)))));
                   Reactive.Table.append
                     (Capability.actions update)
                     (Reactive.var
                        (Capability.Action.set_variable
                           (Reactive.var (Some (Timer.id timer)))));
                   let example = Capability.create_empty () in
                   let example_cap_name =
                     Fmt.str "example4DHS%s"
                       (String.capitalize beneficiary_name)
                   in
                   Reactive.set (Capability.name example) example_cap_name;
                   let example_ors = Reactive.Table.make () in
                   let example_ands = Reactive.Table.make () in
                   Reactive.Table.append example_ors
                     (Reactive.var
                        (Some (Capability.Control.sender_is (Reactive.var None))));
                   Reactive.Table.append example_ands
                     (Reactive.var
                        (Some
                           (Capability.Control.sender_is
                              (Reactive.var (Some (Role.id role))))));
                   Reactive.Table.append example_ands
                     (Reactive.var
                        (Some
                           (Capability.Control.timer_expires
                              (Reactive.var (Some (Timer.id timer))))));
                   Reactive.Table.append example_ors
                     (Reactive.var
                        (Some (Capability.Control.and_ example_ands)));
                   Reactive.set
                     (Capability.control example)
                     (Some (Capability.Control.or_ example_ors));
                   Reactive.Table.append
                     (Capability.actions example)
                     (Reactive.var
                        (Capability.Action.reset_timer
                           (Reactive.var (Some (Timer.id timer)))));
                   Reactive.Table.append roles role;
                   Reactive.Table.append timers timer;
                   Reactive.Table.append capabilities update;
                   Reactive.Table.append capabilities example;
                   idle
                     [
                       t "Beneficiary role" %% ct beneficiary_name
                       %% t "was added (not initialized).";
                       t "A timer" %% ct timer_name
                       %% t "was created to count down to the deadline."
                       %% t "It is initialized at"
                       %% Fmt.kstr ct "%d" timer_weeks
                       %% t "weeks.";
                       t "A capability" %% ct update_cap_name
                       %% t
                            "was added to reconfigure the daed-human-switch: \
                             the value of the beneficiary and the unlocking \
                             timer (access control needs to be filled-in).";
                       t "A capability" %% ct example_cap_name
                       %% t
                            "was added as an example of use of the \
                             dead-human-switch.";
                     ]));
      ]
    in
    TK.box_with_title items
  in
  let internal_id id = Fmt.kstr ct "%a" Internal_id.pp id in
  let role_name role =
    let open Role in
    Reactive.bind_var role.public_name ~f:(function
      | "" -> Fmt.kstr it "Unnamed role" %% internal_id role.id
      | s -> ct s)
  in
  let timer_name timer =
    let open Timer in
    Reactive.bind_var timer.public_name ~f:(function
      | "" -> Fmt.kstr it "Unnamed timer" %% internal_id timer.id
      | s -> ct s)
  in
  let team_name team =
    let open Team in
    Reactive.bind_var team.public_name ~f:(function
      | "" -> Fmt.kstr it "Unnamed team" %% internal_id team.id
      | s -> ct s)
  in
  let pick_a_role ~empty variable =
    Reactive.(Table.list_document roles ** get variable)
    |> Reactive.bind ~f:(fun (roles, current) ->
           let open Role in
           let label =
             match current with
             | None -> empty
             | Some curr -> begin
                 match
                   List.find roles ~f:(fun r -> Role_id.equal r.id curr)
                 with
                 | None -> empty
                 | Some r -> role_name r
               end
           in
           Bootstrap.Dropdown_menu.button label
             (List.map roles ~f:(fun role ->
                  Bootstrap.Dropdown_menu.item (role_name role)
                    ~action:(fun () -> Reactive.set variable (Some role.id)))))
  in
  let pick_a_team ~empty variable =
    Reactive.(Table.list_document teams ** get variable)
    |> Reactive.bind ~f:(fun (teams, current) ->
           let open Team in
           let label =
             match current with
             | None -> empty
             | Some curr -> begin
                 match
                   List.find teams ~f:(fun r -> Team_id.equal r.id curr)
                 with
                 | None -> empty
                 | Some r -> team_name r
               end
           in
           Bootstrap.Dropdown_menu.button label
             (List.map teams ~f:(fun team ->
                  Bootstrap.Dropdown_menu.item (team_name team)
                    ~action:(fun () -> Reactive.set variable (Some team.id)))))
  in
  [
    toolkit;
    Custom_widget.Form.gas_wallet ctxt
      (Reactive.Bidirectional.of_var gas_wallet)
      ~for_reason:(t "for the Origination");
    Reactive.bind_var has_default_do_nothing ~f:(fun current ->
        Bootstrap.Input.check_box ~init_checked:current
          (Some (t "Contract can receive funds in the standard Tezos way."))
          ~action:(fun c -> Reactive.set has_default_do_nothing c));
    h4 (t "Roles")
    % p
        (t
           "People, job-titles, or (sub-)organizations, etc. The names\n\
            are indirections, you may change the value later by creating an \
            entrypoint for that purpose.")
    % collection_like_roles ctxt roles ~name:"Role" ~fresh:Role.create_empty
        ~f:(fun row role ->
          let open Role in
          div
            (bt "Public Name:"
            %% Bootstrap.Input.bidirectional ~inline_block:true ~cols:2
                 (Reactive.Bidirectional.of_var role.public_name)
            %% bt "Initial Account:"
            %% Custom_widget.Form.choose_an_account ctxt
                 (Reactive.Bidirectional.of_var role.initial_value)
                 ~placeholder:(t "Choose account.")
            %% remove_row row));
    h4 (t "Teams")
    % p
        (t
           "A team is a particular sub-organization of Roles; it used to \
            protect capabilities by “weighted-multisig” access rights. For now\n\
            teams are only capable of signing offline (key-pairs).")
    % collection_like_roles ctxt teams ~name:"Team" ~fresh:Team.create_empty
        ~f:(fun row team ->
          let open Team in
          div
            (bt "Public Name:"
            %% Bootstrap.Input.bidirectional ~inline_block:true ~cols:2
                 (Reactive.Bidirectional.of_var team.public_name)
            %% Reactive.bind_var team.initial_value
                 ~f:
                   Team_draft.(
                     function
                     | Percentages { keys } ->
                         collection_like_roles ctxt keys ~name:"Role"
                           ~fresh:(fun () ->
                             (Reactive.var None, Reactive.var 0))
                           ~f:(fun row (roleopt, portion) ->
                             Reactive.bind_var portion ~f:(fun init_value ->
                                 Bootstrap.Input.range ~inline_block:true
                                   ~cols:6 None ~min:0 ~max:100 ~init_value
                                   ~action:(Reactive.set portion)
                                 %% div
                                      ~a:
                                        [
                                          style
                                            "display: inline-block; width: 5em";
                                        ]
                                      (Fmt.kstr ct "%02d%%" init_value))
                             %% pick_a_role roleopt ~empty:(t "Choose a user")
                             %% remove_row row))
            %% remove_row row));
    h4 (t "Timers")
    % p
        (t
           "A timer is a way counting down time from the last time an event \
            happened. Usually this is useful to make a so-called \
            “dead-man-switch.”")
    % collection_like_roles ctxt timers ~name:"Timer" ~fresh:Timer.create_empty
        ~f:(fun row role ->
          let open Timer in
          div
            (bt "Public Name:"
            %% Bootstrap.Input.bidirectional ~inline_block:true ~cols:2
                 (Reactive.Bidirectional.of_var role.public_name)
            %% bt "Initial Value:"
            %% Custom_widget.Form.time_span ctxt
                 (Reactive.Bidirectional.of_var role.initial_value)
            %% remove_row row));
    h4 (t "Capabilities")
    % p
        (t
           "List of things the contract can be asked to do each guarded by \
            some form of access control.")
    % collection_like_roles ctxt capabilities ~name:"Capability"
        ~fresh:Capability.create_empty (* Timer.create_empty *)
        ~f:(fun
             (* row role ->
                % Custom_widget.inline_button (t "➕") ~action:(fun () ->
                      Drafting.Collection.append capabilities ( ()) )
                % Reactive.Table.concat_map capabilities ~map:(fun *) ep_row
             ep
           ->
          let pick_a_timer ~empty variable =
            Reactive.(Table.list_document timers ** get variable)
            |> Reactive.bind ~f:(fun (timers, current) ->
                   let open Timer in
                   let label =
                     match current with
                     | None -> empty
                     | Some curr -> begin
                         match
                           List.find timers ~f:(fun r ->
                               Timer_id.equal r.id curr)
                         with
                         | None -> empty
                         | Some r -> timer_name r
                       end
                   in
                   Bootstrap.Dropdown_menu.button label
                     (List.map timers ~f:(fun timer ->
                          Bootstrap.Dropdown_menu.item (timer_name timer)
                            ~action:(fun () ->
                              Reactive.set variable (Some timer.id)))))
          in
          let open Capability in
          let rec control depth control_v =
            let go_deeper = control (depth + 1) in
            Reactive.bind_var control_v ~f:(fun ctrlopt ->
                let label =
                  match ctrlopt with
                  | None -> t "Choose, please."
                  | Some c -> t (Capability.Control.Variants.to_name c)
                in
                let list_of_controls prompt controls =
                  Custom_widget.inline_button (t "➕") ~action:(fun () ->
                      Drafting.Collection.append controls
                        (Drafting.Reference.create None))
                  %% b prompt
                  % Reactive.bind (Reactive.Table.list_document controls)
                      ~f:(fun controls_l ->
                        itemize
                          ~a_ul:
                            [
                              Custom_widget.Debug.boxify_style
                                ~level:(2 + depth) ctxt "margin-left: 40px";
                            ]
                          (List.map controls_l ~f:go_deeper))
                in
                Bootstrap.Dropdown_menu.button label
                  [
                    Bootstrap.Dropdown_menu.item (t "Anyone") ~action:(fun () ->
                        Reactive.set control_v (Some Capability.Control.anyone));
                    Bootstrap.Dropdown_menu.item (t "Sender is …")
                      ~action:(fun () ->
                        Reactive.set control_v
                          (Some
                             (Capability.Control.sender_is
                                (Drafting.Reference.create None))));
                    Bootstrap.Dropdown_menu.item (t "Timer expires …")
                      ~action:(fun () ->
                        Reactive.set control_v
                          (Some
                             (Capability.Control.timer_expires
                                (Drafting.Reference.create None))));
                    Bootstrap.Dropdown_menu.item (t "Team agrees …")
                      ~action:(fun () ->
                        Reactive.set control_v
                          (Some
                             (Capability.Control.team_acknowledges
                                (Drafting.Reference.create None))));
                    Bootstrap.Dropdown_menu.item (t "Any-of / OR-condition …")
                      ~action:(fun () ->
                        Reactive.set control_v
                          (Some
                             (Capability.Control.or_
                                (Drafting.Collection.create ()))));
                    Bootstrap.Dropdown_menu.item (t "All-of / AND-condition …")
                      ~action:(fun () ->
                        Reactive.set control_v
                          (Some
                             (Capability.Control.and_
                                (Drafting.Collection.create ()))));
                  ]
                %
                match ctrlopt with
                | None | Some Control.Anyone -> empty ()
                | Some (Control.Sender_is sender) ->
                    pick_a_role sender ~empty:(t "Pick a role")
                | Some (Control.Timer_expires timer) ->
                    pick_a_timer timer ~empty:(t "Pick a timer")
                | Some (Control.Team_acknowledges team) ->
                    pick_a_team team ~empty:(t "Pick a team")
                | Some (Control.Or controls) ->
                    list_of_controls (t "Any of the following") controls
                | Some (Control.And controls) ->
                    list_of_controls (t "All of the following") controls)
          in
          let actions action_collection =
            let add_button =
              Custom_widget.inline_button (t "Add Action ➕") ~action:(fun () ->
                  Drafting.Collection.append action_collection
                    (Drafting.Reference.create Action.do_nothing))
            in
            (* % Reactive.bind (Reactive.Table.list_document action_collection) *)
            let actions =
              Reactive.Table.concat_map action_collection
                ~map:(fun row action ->
                  (* ~f:(fun actions ->
                     itemize
                       (List.mapi actions ~f:(fun ith  action -> *)
                  let action_name = function
                    | Action.Do_nothing -> bt "Do nothing."
                    | Action.Transfer _ -> t "Transfer"
                    | Action.Reset_timer _ -> t "Reset timer"
                    | Action.Set_delegate -> t "Set Delegate"
                    | Action.Set_variable _ -> t "Set parameter"
                  in
                  let dropdown =
                    let label = Reactive.bind_var action ~f:action_name in
                    List.map
                      ~f:(fun act ->
                        Bootstrap.Dropdown_menu.item (action_name act)
                          ~action:(fun () -> Reactive.set action act))
                      [
                        Action.do_nothing;
                        Action.set_delegate;
                        Action.empty_transfer ();
                        Action.set_variable (Reactive.var None);
                        Action.reset_timer (Reactive.var None);
                      ]
                    |> Bootstrap.Dropdown_menu.button label
                  in
                  div
                    ~a:[ Custom_widget.Debug.boxify_style ~level:2 ctxt "" ]
                    (dropdown
                    %% Reactive.bind_var action ~f:(function
                         | Action.Do_nothing | Action.Set_delegate -> empty ()
                         | Action.Reset_timer timer ->
                             pick_a_timer timer
                               ~empty:(t "Pick a timer to reset.")
                         | Action.Set_variable v ->
                             Reactive.bind
                               (get_variables ~capabilities ~roles ~timers
                                  ~spending_limits ~teams) ~f:(fun l ->
                                 let label =
                                   Reactive.bind_var v ~f:(function
                                     | None -> it "Choose a variable"
                                     | Some n -> (
                                         match
                                           List.Assoc.find l n
                                             ~equal:Internal_id.equal
                                         with
                                         | Some (n, _) -> ct n
                                         | None -> it "Choose a variable, again"
                                         ))
                                 in
                                 List.map l ~f:(fun (id, (name, _)) ->
                                     Bootstrap.Dropdown_menu.item (ct name)
                                       ~action:(fun () ->
                                         Reactive.set v (Some id)))
                                 |> Bootstrap.Dropdown_menu.button label)
                         | Action.Transfer { amount_limit; destination } ->
                             Reactive.bind_var amount_limit ~f:(function
                               | None ->
                                   Custom_widget.inline_button
                                     (t "Add a spending limit")
                                     ~action:(fun () ->
                                       let fresh =
                                         Spending_limit.create_empty ()
                                       in
                                       Reactive.Table.append spending_limits
                                         fresh;
                                       Reactive.set amount_limit (Some fresh.id))
                               | Some current ->
                                   Reactive.(
                                     Table.list_document spending_limits)
                                   |> Reactive.bind ~f:(fun limits ->
                                          let open Spending_limit in
                                          match
                                            List.find limits ~f:(fun l ->
                                                Spending_limit_id.equal l.id
                                                  current)
                                          with
                                          | Some sl ->
                                              div
                                                ~a:
                                                  [
                                                    Custom_widget.Debug
                                                    .boxify_style ~level:3 ctxt
                                                      "";
                                                  ]
                                                (t "Spending limit name:"
                                                % Bootstrap.Input.bidirectional
                                                    (Reactive.Bidirectional
                                                     .of_var sl.public_name)
                                                    ~inline_block:true
                                                    ~placeholder:
                                                      (Reactive.pure
                                                         "Public name for the \
                                                          limit")
                                                % br ()
                                                % Custom_widget.Form.amount ctxt
                                                    ~prompt:(fun () ->
                                                      t
                                                        "Initial spending \
                                                         limit:")
                                                    (Reactive.Bidirectional
                                                     .of_var sl.initial_value))
                                          | None ->
                                              Bootstrap.alert ~kind:`Danger
                                                (t "Unknown limit:"
                                                %% Fmt.kstr ct "%a"
                                                     Spending_limit_id.pp
                                                     current)))
                             %% div
                                  ~a:
                                    [
                                      Custom_widget.Debug.boxify_style ~level:3
                                        ctxt "";
                                    ]
                                  (t "Impose a destination:"
                                  %% pick_a_role destination ~empty:(t "Anyone")
                                  ))
                    %% Custom_widget.inline_button (t "❌") ~action:(fun () ->
                           Reactive.Table.remove row)))
            in
            actions % add_button
          in
          Bootstrap.bordered ~kind:`Info
            ~a:[ style "padding: 3px" ]
            (div
               (Bootstrap.Input.bidirectional ~inline_block:true ~cols:3
                  ~placeholder:(Reactive.pure "entrypoint_name")
                  (Reactive.Bidirectional.of_var ep.name)
               %% Custom_widget.inline_button
                    (Bootstrap.color `Danger @@ t "Delete capability")
                    ~action:(fun () -> Reactive.Table.remove ep_row))
            %% div (h5 (t "Control:") %% control 0 ep.control)
            %% div (h5 (t "Actions:") %% actions ep.actions)));
  ]

let import_ledger_tool_ui ctxt ~return =
  let open Mono_html in
  let ledgers = Reactive.var (Wip.success []) in
  let ledger_choice = Reactive.var None in
  let curve_choice = Reactive.var None in
  let path_choice = Reactive.Table.make () in
  Reactive.Table.append path_choice (Reactive.var "0h");
  Reactive.Table.append path_choice (Reactive.var "0h");
  let show_ledger =
    Ledger_nano.Device.(
      fun ({ animals; wallet_app_version } as self) ->
        Bootstrap.color `Primary (b (ct animals))
        %% (match wallet_app_version with
           | None -> bt "(Unknown version)"
           | Some v -> t "(Tezos Wallet" %% ct v % t ")")
        %% Reactive.bind_var ledger_choice ~f:(function
             | Some s when equal s self -> t "👈"
             | Some _ | None ->
                 Custom_widget.inline_button (t "Choose") ~action:(fun () ->
                     Reactive.set ledger_choice (Some self)))
        %% Reactive.bind (Web_app_state.accounts ctxt) ~f:(fun accs ->
               let which_use_this_ledger =
                 let prefix = "ledger://" ^ animals in
                 List.filter_map accs ~f:(fun acc ->
                     match Account.get_secret_key acc with
                     | Some (Key_pair.Ledger { uri })
                       when String.is_prefix uri ~prefix ->
                         Some (acc, uri)
                     | _ -> None)
               in
               match which_use_this_ledger with
               | [] -> empty ()
               | more ->
                   div
                     (t "Already used by:"
                     % itemize
                         (List.map more ~f:(fun (acc, uri) ->
                              Custom_widget.account_or_address_link ctxt
                                (`Account acc)
                                ~add_details:[ `Address_short 12; `Balance ]
                              % t ":" %% ct uri)))))
  in
  let list_connected_ledgers_button ~disabled () =
    Custom_widget.inline_button ~disabled
      (t "Check for connected ledgers again") ~action:(fun () ->
        Server_communication.list_connected_ledgers ledgers)
  in
  let validate_derivation_path_item x =
    try
      let int s =
        let (_ : int32) = Int32.of_string s in
        true
      in
      match String.split ~on:'h' x with
      | [ one ] -> int one
      | [ one; "" ] -> int one
      | _ -> false
    with _ -> false
  in
  let resulting_uri () =
    Reactive.(
      let path_lwd =
        Table.map_reduce
          (fun _ v -> map (get v) ~f:(fun x -> [ x ]))
          (pure [], fun a b -> map (a ** b) ~f:(fun (a, b) -> a @ b))
          path_choice
        |> join
      in
      map (get ledger_choice ** get curve_choice ** path_lwd))
      ~f:(fun (ledger, (curve, path)) ->
        ( Fmt.str "ledger://%s/%s/%s"
            (match ledger with
            | None -> "… - … - … - …"
            | Some { animals; _ } -> animals)
            (Option.value_map curve ~default:"<curve>"
               ~f:Ledger_nano.Curve_derivation.to_path)
            (String.concat ~sep:"/" path),
          Option.is_some ledger && Option.is_some curve
          && List.for_all path ~f:validate_derivation_path_item ))
  in
  let choose_curve () =
    let show = function
      | None -> t "Please choose a derivation curve."
      | Some c -> Fmt.kstr ct "/%s" (Ledger_nano.Curve_derivation.to_path c)
    in
    let title = Reactive.bind_var curve_choice ~f:show in
    Bootstrap.Dropdown_menu.(
      button title
        (header (it "Choose Curve/Derivation Method")
        :: List.map Ledger_nano.Curve_derivation.all ~f:(fun v ->
               item (show (Some v)) ~action:(fun () ->
                   Reactive.set curve_choice (Some v)))))
  in
  let choose_device () =
    Reactive.bind_var ledgers ~f:(function
      | Wip.WIP -> list_connected_ledgers_button ~disabled:true ()
      | _ -> list_connected_ledgers_button ~disabled:false ())
    %% Custom_widget.wip_div ledgers
         ~with_spinner:(fun spinn -> it "Checking for ledgers …" %% spinn)
         ~error:(fun s -> bt "Error:" %% t s)
         ~success:(function
           | [] -> it "No available ledgers" |> div
           | ledgers -> itemize (List.map ~f:show_ledger ledgers))
  in
  let choose_path () =
    t "Detivation path:"
    % Reactive.Table.concat_map path_choice ~map:(fun row content ->
          Bootstrap.color `Info
            (span ~a:[ style "font-weight: bold; font-size: 160%" ] (t "  /"))
          % Bootstrap.Input.bidirectional ~inline_block:true
              ~a:[ style "max-width: 4em" ]
              (Reactive.Bidirectional.of_var content)
          % Custom_widget.inline_button (t "╳") ~action:(fun () ->
                Reactive.Table.remove row))
    %% Custom_widget.inline_button (t "➕") ~action:(fun () ->
           Reactive.Table.append path_choice (Reactive.var "0h"))
  in
  let brute_force_ui () =
    let form_state = Reactive.var `Button in
    Reactive.(bind (get form_state ** get ledger_choice))
      ~f:(fun (state, ledger) ->
        let disabled = Option.is_none ledger in
        match state with
        | `Button ->
            Custom_widget.inline_button ~disabled
              (t "Brute-force addresses and their balances (Experimental ☢)")
              ~action:(fun () -> Reactive.set form_state `Configuration)
        | `Configuration ->
            let path_components = Reactive.var 2 in
            let attempts_per_path_component = Reactive.var 3 in
            let only_hardenned = Reactive.var false in
            let make_uris ~path_components ~attempts_per_path_component
                ~only_hardenned =
              try
                let base =
                  match ledger with
                  | Some { animals; _ } -> animals
                  | None -> "weird-bug-nasty-bug"
                in
                let u u =
                  List.map [ "ed25519"; "secp256k1"; "P-256" ] ~f:(fun curve ->
                      Fmt.str "ledger://%s/%s/%s" base curve
                        (String.concat ~sep:"/" u))
                in
                let attempts =
                  List.init attempts_per_path_component ~f:(fun i ->
                      if only_hardenned then [ Fmt.str "%dh" i ]
                      else [ Fmt.str "%d" i; Fmt.str "%dh" i ])
                  |> List.concat
                in
                let templates =
                  List.init (path_components + 1) ~f:(fun length ->
                      List.init length ~f:(fun _ -> ()))
                in
                let paths =
                  List.concat_map templates ~f:(fun template ->
                      List.fold template ~init:[ [] ] ~f:(fun prev () ->
                          List.cartesian_product prev attempts
                          |> List.map ~f:(fun (a, b) -> b :: a)))
                in
                if List.length paths > 400 then
                  (* We protect against some asynchronous
                     Stack-overflow from the browser: *)
                  assert false;
                Ok (List.concat_map paths ~f:u)
              with _ -> Error "Too many results …"
            in
            let uris_doc =
              Reactive.(
                map
                  (get path_components
                  ** get attempts_per_path_component
                  ** get only_hardenned))
                ~f:(fun
                     ( path_components,
                       (attempts_per_path_component, only_hardenned) )
                   ->
                  make_uris ~path_components ~attempts_per_path_component
                    ~only_hardenned)
            in
            div
              (t "Number of path components:"
              %% Bootstrap.Input.range ~init_value:2 ~cols:3 ~inline_block:true
                   ~min:0 ~max:10 ~step:1.
                   ~action:(Reactive.set path_components)
                   None
              %% t "→"
              %% Reactive.bind_var path_components ~f:(Fmt.kstr ct "%d"))
            % div
                (t "Attempts per path component:"
                %% Bootstrap.Input.range ~init_value:3 ~cols:3
                     ~inline_block:true ~min:1 ~max:10 ~step:1.
                     ~action:(Reactive.set attempts_per_path_component)
                     None
                %% t "→"
                %% Reactive.bind_var attempts_per_path_component
                     ~f:(Fmt.kstr ct "%d"))
            % div
                (Bootstrap.Input.check_box ~init_checked:false
                   ~action:(fun v -> Reactive.set only_hardenned v)
                   (Some (t "Use only hardened path components.")))
            % div
                (t "Would try:"
                %% Reactive.bind uris_doc ~f:(function
                     | Ok l ->
                         Fmt.kstr bt "%d" (List.length l)
                         %% Fmt.kstr t "ledger URI%s:"
                              (match l with [ _ ] -> "" | _ -> "s")
                         % Bootstrap.bordered ~kind:`Info
                             ~a:
                               [ style "max-height: 400px; overflow-y: scroll" ]
                             (itemize (List.map l ~f:ct))
                     | Error e ->
                         Bootstrap.alert ~kind:`Danger (bt "Error:" %% it e)))
            % Reactive.bind uris_doc ~f:(fun res ->
                  let disabled = disabled || Result.is_error res in
                  Custom_widget.inline_button ~disabled (t "Go!")
                    ~action:(fun () ->
                      match res with
                      | Ok uris -> Reactive.set form_state (`Brute_force uris)
                      | Error _ -> ()))
            % Custom_widget.inline_button (t "Cancel") ~action:(fun () ->
                  Reactive.set form_state `Button)
        | `Brute_force uris ->
            let with_wips =
              List.map uris ~f:(fun u -> (u, Reactive.var Wip.Idle))
            in
            Lwt.async
              Lwt.Infix.(
                fun () ->
                  let try_one (u, wip) =
                    Reactive.set wip Wip.WIP;
                    Lwt.catch
                      (fun () ->
                        Server_communication.call ~timeout:5.
                          (Protocol.Message.Up.analyze_uri_or_address u)
                        >>= function
                        | Protocol.Message.Down.Uri_or_address_analysis a ->
                            Reactive.set wip (Wip.success a);
                            Lwt.return_unit
                        | _other -> Fmt.failwith "Wrong down message")
                      (function
                        | Failure s ->
                            Reactive.set wip (Wip.failure s);
                            Lwt.return_unit
                        | e ->
                            Reactive.set wip
                              (Fmt.kstr Wip.failure "%a" Exn.pp e);
                            Lwt.return_unit)
                  in
                  Lwt_list.iter_s try_one with_wips);
            itemize
              (List.map with_wips ~f:(fun (u, wip) ->
                   ct u
                   %% Custom_widget.wip_div wip
                        ~success:(fun { Uri_analysis.facts; _ } ->
                          list
                            (List.map facts ~f:(function
                              | Balance b ->
                                  Custom_widget.Balance.inline_tez b
                                  %% if Z.equal b Z.zero then t "😿" else t "🥳"
                              | Address a ->
                                  t " ➡ "
                                  % Custom_widget.address_with_links
                                      ~links:[ `BCD ] ctxt a
                                  % t ", "
                              | Public_key _ -> empty ())))
                   %% Custom_widget.inline_button (t "Use This One")
                        ~action:(fun () ->
                          return
                            [
                              t "Used" %% ct u
                              %% t "as URI (found by brute-force).";
                            ]
                            (Some u)))))
  in
  Server_communication.list_connected_ledgers ledgers;
  div (choose_device ())
  % div (brute_force_ui ())
  % div (choose_curve ())
  % div (choose_path ())
  % div
      (t "➤"
      %% Reactive.bind (resulting_uri ()) ~f:(fun (uri, valid) ->
             b (ct uri)
             %% Custom_widget.inline_button ~disabled:(not valid) (t "Use")
                  ~action:(fun () ->
                    return [ t "Used" %% ct uri %% t "as URI." ] (Some uri))
             %% Custom_widget.inline_button (t "Cancel") ~action:(fun () ->
                    return [] None)))

let uri_import_debug_block ctxt ~display_name_var ~uri_var =
  let open Mono_html in
  let fake_ledgers =
    [
      ("Valid tz1", "ledger://crouching-tiger-hidden-dragon/ed25519/0h/1h");
      ("Valid tz2", "ledger://crouching-tiger-hidden-dragon/secp256k1/0h/2h");
      ("Valid tz3", "ledger://crouching-tiger-hidden-dragon/P-256/0h/2h");
      ("Valid tz3", "ledger://crouching-tiger-hidden-dragon/P-256/0h/2h");
      ( "Valid tz1 + 5s delay",
        "ledger://crouching-tiger-hidden-dragon/bip25519/5/51h" );
      ( "Fails at import + 5s delay",
        "ledger://crouching-tiger-hidden-dragon/bip25519/5/42h" );
      ( "Fails at 3rd signature + 1s delay",
        "ledger://crouching-tiger-hidden-dragon/bip25519/1/3/420h" );
      ( "Fails to be shown ↓ + 3s delay",
        "ledger://crouching-tiger-hidden-dragon/bip25519/1h/3/4242h" );
    ]
  in
  let friends =
    [
      ("Alice-tz1", "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb");
      ("Alice-PK", "edpkvGfYw3LyB1UcCahKQk4rF2tvbMUk8GFiTuMjL75uGXrpvKXhjn");
      ("Bob-PK", "edpkurPsQ8eUApnLUJ9ZPDvu98E8VNj4KtJa1aZr16Cr5ow5VHKnz4");
      ("Bob-tz1", "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6");
    ]
  in
  let private_keys =
    [
      ( "Alice-SK",
        "unencrypted:edsk3QoqBuvdamxouPhin7swCvkQNgq4jP5KZPbwWNnwdZpSpJiEbq" );
      ( "Bob-SK",
        "unencrypted:edsk3RFfvaFaxbHx8BMtEW1rKQcPtDML3LXjNqMNLCzC3wLC1bWbAt" );
    ]
  in
  let contracts =
    let random =
      Tezai_base58_digest.Identifier.Kt1_address.(
        Fmt.kstr hash_string "hello-%f" (Unix.gettimeofday ()) |> encode)
    in
    [
      ("Ghostnet-misg", "KT1WSUAUrqBaDou2DPkysu4nNQUoUyHgtRKz");
      (Fmt.str "Random-%s" (String.prefix random 8), random);
    ]
  in
  let button_list name_prefix l =
    list
      (List.map l ~f:(fun (name, u) ->
           Custom_widget.inline_button (Fmt.kstr t "Add %s" name)
             ~action:(fun () ->
               Reactive.set display_name_var (Fmt.str "%s %s" name_prefix name);
               Reactive.set uri_var u)))
  in
  Custom_widget.Debug.(
    bloc ctxt
      (div (bt "Fake Ledgers:" %% button_list "FakeLedger" fake_ledgers)
      % div (bt "Friends" %% button_list "Friend" friends)
      % div (bt "Key Pairs" %% button_list "KeyPair" private_keys)
      % div (bt "KT1-Contracts" %% button_list "Ext-KT1" contracts)))

let genrate_key_pair_tool_ui _ctxt ~return =
  let open Mono_html in
  let skuri_var = Reactive.var Wip.Idle in
  let button_of_curve curve c =
    Custom_widget.inline_button c ~action:(fun () ->
        Server_communication.call_with_var skuri_var
          (Protocol.Message.Up.generate_key_pair ~kind:`Unencrypted ~curve)
          (function
          | Protocol.Message.Down.Key_pair
              { public_key_hash; public_key; secret_key_uri } ->
              (public_key_hash, public_key, secret_key_uri)
          | _ -> Fmt.failwith "Wrong down message"))
  in
  Custom_widget.wip_div skuri_var
    ~idle:(fun () ->
      div (b (t "Generate a fresh Key-Pair:"))
      % button_of_curve `Ed25519 (t "ED25519 (a.k.a. tz1, recommended)")
      % button_of_curve `Secp256k1 (t "Secp256k1 (a.k.a. tz2)")
      % button_of_curve `P256 (t "P-256 (a.k.a. tz3)"))
    ~with_spinner:(fun spin -> it "Generating key …" %% spin)
    ~success:(fun (pkh, pk, uri) ->
      div (b (t "Generated Secret Key:") %% ct uri)
      %% it "Corresponds to"
      % itemize [ it "Public-key:" %% ct pk; it "Address:" %% ct pkh ]
      %% Custom_widget.inline_button (t "Use") ~action:(fun () ->
             return [ t "Used" %% ct uri %% t "as URI." ] (Some uri)))
  %% Custom_widget.inline_button (t "Cancel") ~action:(fun () -> return [] None)

let display_uri_analysis ctxt analysis =
  let open Mono_html in
  Custom_widget.Debug.(bloc ctxt (sexpable Uri_analysis.sexp_of_t analysis))
  %
  match analysis.facts with
  | [] -> empty ()
  | more ->
      bt "Interesting facts:"
      % itemize
          (List.map more
             ~f:
               Uri_analysis.Fact.(
                 function
                 | Balance bal ->
                     t "Current balance:"
                     %% Custom_widget.Balance.inline_tez bal
                 | Public_key pk -> t "Corresponding public-key:" %% ct pk
                 | Address address ->
                     t "Corresponding address:"
                     %% Custom_widget.address_with_links ctxt ~links:[ `BCD ]
                          address))

let show_result_summary ctxt ready_result ~simulation =
  let open Mono_html in
  let open Custom_widget in
  let using_gas_wallet wid =
    t "Using gas-account:" %% account_or_address_link ctxt (`Id wid)
  in
  let simulation_summary () =
    let open Draft_simulation in
    Draft_simulation.start_simulation_action simulation ();
    wip_div simulation.v
      ~success:(fun (res, batch) ->
        let ops = Tezos_batch.operations batch in
        Fmt.kstr bt "Tezos Operation%s required"
          (if List.length ops = 1 then "" else "s")
        %% parens
             (if Simulation_result.all_applied res then
              Bootstrap.color `Success (bt "Simulation successful")
             else Bootstrap.color `Danger (bt "Simulation failed"))
        % t ":"
        % itemize
            (List.map ops
               ~f:
                 Tezos_operation.(
                   fun op ->
                     match op.specification with
                     | Transfer _ -> t "TRANSFER???"
                     | Origination { source; _ } ->
                         t "Origination of the contract operated by"
                         %% account_or_address_link ctxt (`Account source)
                         % t "."
                     | Reveal { account } ->
                         t "Revelation of the public-key for account"
                         %% account_or_address_link ctxt (`Account account)
                         %% t "(necessary for signing operations).")))
        (* ~with_spinner:(fun _ -> empty ()) *)
      ~idle:(fun _ -> empty ())
  in
  match ready_result with
  | `Custom_contract cc ->
      let entrypoint ep =
        let open Entrypoint in
        let params =
          div
            (match collect_parameters ep with
            | [] -> it "No parameters."
            | more ->
                it "Parameters:"
                %% itemize
                     (List.map more ~f:(fun (name, ty) ->
                          ct name % t ":" %% wip_sexp Variable.Type.sexp_of_t ty)))
        in
        let ctrl =
          let open Control in
          div
            (it "Access Control:"
            %%
            match ep.control with
            | Anyone -> bt "Anyone can call this entrypoint."
            | other -> wip_sexp sexp_of_t other)
        in
        let act =
          let open Ability in
          div
            (it "Actions:"
            %%
            match ep.ability with
            | Do_nothing -> t "No action, just accepting fund transfers."
            | other -> wip_sexp sexp_of_t other)
        in
        div
          (t "🚪" %% bt ep.name
          %% div
               ~a:
                 [
                   style
                     "border-left: solid 2px #aaa; margin-left: 10px; \
                      padding-left: 4px";
                 ]
               (params % ctrl % act % Debug.(bloc ctxt (sexpable sexp_of_t ep)))
          )
      in
      let initialization =
        list
          (List.map cc#initialization ~f:(fun (name, v) ->
               div
                 (t "📂 "
                 % b (ct name)
                 % t " ←"
                 %% wip_sexp Variable.Value.sexp_of_t v)))
      in
      bt "Creating a Custom Contract"
      % (if cc#can_receive_funds then empty ()
        else Bootstrap.color `Danger (bt "… which cannot receive funds!"))
      % itemize
          [
            t "Entrypoints:" %% list (List.map ~f:entrypoint cc#entrypoints);
            t "Storage Initialization:" %% initialization;
            using_gas_wallet cc#gas_wallet;
          ]
      %% simulation_summary ()
  | `Generic_multisig gm ->
      bt "Formally Verified Generic Multisig"
      %% itemize
           [
             t "Signers:"
             %% list
                  (List.map gm#keys ~f:(fun pk ->
                       div
                         (t "✍ "
                         % account_or_address_link ctxt (`Public_key pk)
                             ~add_details:[ `Public_key 14 ])));
             t "Signature threshold:"
             %% Fmt.kstr bt "%a" Z.pp_print gm#threshold;
             using_gas_wallet gm#gas_wallet;
           ]
      %% simulation_summary ()
  | `Uri uri_parsed ->
      bt "Import URI"
      % div
          (match uri_parsed with
          | `Friend_pk pk ->
              t "Friend public-key:"
              %% account_or_address_link ctxt (`Public_key pk)
          | `Friend_tz tz ->
              t "Friend address:" %% account_or_address_link ctxt (`Address tz)
          | `Kt1 kt1 ->
              t "Existing contract address:"
              %% account_or_address_link ctxt (`Address kt1)
          | `Ledger uri -> t "Legder secret-key:" %% ct uri
          | `Unencrypted uri ->
              t "Unencrypted secret-key:" %% secret_key ctxt uri)

let draft_edition_ui ctxt dynacc draft =
  let open Mono_html in
  let open Account in
  let acc, display_name_var, comments_var =
    Web_app_state.Dynamic_account.
      (account dynacc, display_name_var dynacc, comments_var dynacc)
  in
  let draft_var = Reactive.var draft in
  let extra_analysis = Reactive.var Wip.Idle in
  let show_extra_analysis extra_analysis =
    Custom_widget.wip_div extra_analysis ~success:(display_uri_analysis ctxt)
  in
  let rows =
    let backend_rows =
      let open Account.Draft in
      match draft with
      | Generic_multisig { gas_wallet; threshold; public_keys } ->
          [
            Custom_widget.Form.gas_wallet ctxt
              (Reactive.Bidirectional.of_var gas_wallet)
              ~for_reason:(t "for the Origination");
            Custom_widget.Form.collection_of_public_keys ctxt ~public_keys;
            Custom_widget.Form.multisig_threshold_input ctxt
              ~threshold:(Reactive.Bidirectional.of_var threshold)
              ~public_keys:(Reactive.get public_keys);
          ]
      | Custom_contract
          {
            gas_wallet;
            capabilities;
            has_default_do_nothing;
            roles;
            timers;
            spending_limits;
            teams;
          } ->
          custom_contract_subform ctxt ~gas_wallet ~capabilities
            ~has_default_do_nothing ~roles ~timers ~spending_limits ~teams
      | From_address_or_uri uri ->
          let uri_bidi = Reactive.Bidirectional.of_var uri in
          let toolkit =
            let module TK = Custom_widget.Toolkit_automaton in
            let items =
              [
                TK.item ~button_name:(t "Generate a fresh key-pair")
                  ~form:(fun ~idle ->
                    genrate_key_pair_tool_ui ctxt ~return:(fun logs uris ->
                        Option.iter ~f:(Reactive.set uri) uris;
                        idle logs));
                TK.item ~button_name:(t "Import a Ledger Nano S or Nano X")
                  ~form:(fun ~idle ->
                    import_ledger_tool_ui ctxt ~return:(fun logs uris ->
                        Option.iter ~f:(Reactive.set uri) uris;
                        idle logs));
              ]
            in
            TK.box_with_title items
          in
          let validate_and_maybe_analyze uri =
            Reactive.map (Validate.uri_or_address uri) ~f:(fun res ->
                begin
                  match (res, Reactive.peek extra_analysis) with
                  | Error _, Wip.WIP -> ()
                  | Error _, _ -> Reactive.set extra_analysis Wip.Idle
                  | Ok _, _ ->
                      Server_communication.call_with_var extra_analysis
                        (Protocol.Message.Up.analyze_uri_or_address uri)
                        (function
                        | Protocol.Message.Down.Uri_or_address_analysis a -> a
                        | _ -> Fmt.failwith "wrong down message")
                end;
                res)
          in
          [
            uri_import_debug_block ctxt ~display_name_var ~uri_var:uri
            % toolkit
            % Bootstrap.Input.bidirectional
                ~label:(t "Enter Address or URI:")
                ~help:
                  (t "A" %% ct "KT1/tz123"
                  %% t
                       "address, a public key, or a secret key URI \
                        (unencrypted or ledger)."
                  %% Reactive.Bidirectional.bind uri_bidi ~f:(fun uri ->
                         Custom_widget.render_validation ctxt
                           (validate_and_maybe_analyze uri) ~ok:(function
                           | `Friend_tz _ -> t "A friends' tz123 address."
                           | `Friend_pk _ -> t "A friends' public-key."
                           | `Kt1 _ -> t "A KT1 smart contract."
                           | `Unencrypted _ -> t "An unencrypted key-pair."
                           | `Ledger _ -> t "A Ledger™-based key-pair.")))
                (* ~a:[H5.a_size (Reactive.pure 50)] *)
                uri_bidi
            % show_extra_analysis extra_analysis;
          ]
    in
    let markdown_comments =
      Custom_widget.Editor.side_by_side_human_prose_editor ctxt comments_var
    in
    [
      Bootstrap.Input.bidirectional ~label:(t "Display Name:")
        ~help:(t "A recognizable name for yourself.")
        (* ~a:[H5.a_size (Reactive.pure 50)] *)
        (Reactive.Bidirectional.of_var display_name_var);
    ]
    @ backend_rows
    @ [ t "Markdown Comments:" % markdown_comments ]
  in
  let simulation =
    Draft_simulation.make () ~up_message:(fun () ->
        let draft = Reactive.peek draft_var in
        Protocol.Message.Up.run_account_draft_simulation ~draft)
  in
  let client_validated =
    Reactive.(bind (get draft_var) ~f:(validate_whole_draft ctxt))
  in
  let discard () =
    Custom_widget.inline_button (t "Cancel/Discard") ~action:(fun () ->
        Web_app_state.call_server_async ctxt
          (Protocol.Message.Up.delete_account acc.id);
        Web_app_state.Current_page.change_to ctxt
          (Web_app_state.Page.draft_account ~id:None);
        ())
  in
  let title =
    h4
      (t "Editing account"
      %% Bootstrap.color `Primary
           (Reactive.bind_var display_name_var ~f:(function
             | "" -> Bootstrap.monospace (t acc.id)
             | other -> Bootstrap.monospace (t other))))
  in
  let edit_stage =
    let row content =
      div ~a:[ Custom_widget.Debug.boxify_style ctxt "" ] content
    in
    Custom_widget.Staged_form.item ~button_title:(t "Edit")
      (list (List.map rows ~f:row))
  in
  let simulation_stage () =
    Custom_widget.Staged_form.item ~button_title:(t "Run Simulation")
      ~load_action:(Draft_simulation.start_simulation_action simulation)
      (Draft_simulation.render ctxt simulation
      % Draft_simulation.make_button ctxt simulation
          ~client_validation:client_validated (t "Run Simulation Again"))
      ~button_active:(Reactive.map ~f:Result.is_ok client_validated)
  in
  let order_stage =
    Custom_widget.Staged_form.item
      ~button_active:(Reactive.map ~f:Result.is_ok client_validated)
      ~button_title:(t "Add/Create Account")
      (Reactive.bind client_validated ~f:(function
        | Error _ -> Custom_widget.ui_errorf ctxt "This should not be available"
        | Ok { v = ready_result; _ } ->
            let are_you_sure_banner =
              Custom_widget.are_you_sure_and_submit_banner ctxt
                ~button_label:(t "Add/Make Account")
                ~question:(bt "Are you sure you want to create this account?")
                ~action:(fun () ->
                  let draft = Reactive.peek draft_var in
                  save_draft ctxt ~glorify:true ~id:acc.id draft
                    ~name:(Reactive.peek display_name_var)
                    ?comments:(Reactive.peek comments_var);
                  Web_app_state.Current_page.change_to ctxt
                    Web_app_state.Page.overview)
            in
            Bootstrap.alert ~kind:`Success
              (h4 (t "Account Creation Summary")
              % show_result_summary ctxt ready_result ~simulation
              % show_extra_analysis extra_analysis)
            %% are_you_sure_banner))
  in
  let form_stages =
    [ edit_stage ]
    @ (match draft with
      | Draft.From_address_or_uri _ -> []
      | Draft.Generic_multisig _ | Draft.Custom_contract _ ->
          [ simulation_stage () ])
    @ [ order_stage ]
  in
  title
  % Custom_widget.show_validation_errors_and_warnings ctxt client_validated
  % Custom_widget.Staged_form.render ctxt form_stages
      ~before_buttons:(discard () % bt " | ")
  % Custom_widget.Debug.(
      bloc ctxt
        (pre
           (Reactive.bind_var
              ~f:(fun s -> Sexplib.Sexp.of_string s |> Sexp.to_string_hum |> ct)
              (Web_app_state.Dynamic_account.saved dynacc))
        % hr ()
        % Reactive.bind client_validated ~f:(function
            | Ok { v; warnings = _ } ->
                t "👍"
                % div
                    (match v with
                    | `Custom_contract cc ->
                        itemize
                          [
                            t "Gas:" %% ct cc#gas_wallet;
                            Fmt.kstr t "can receive %b" cc#can_receive_funds;
                            t "Entrypoints:"
                            % itemize
                                (List.map cc#entrypoints
                                   ~f:(sexpable Entrypoint.sexp_of_t));
                            t "Initialization:"
                            %% sexpable Variable.Map.sexp_of_t cc#initialization;
                          ]
                    | `Generic_multisig _ | `Uri _ -> t "Not rendered")
            | Error _ -> bt "Does not validate")))

let render ctxt ?account_id () =
  let open Mono_html in
  let start_draft kind () =
    let id = Fresh_id.make () in
    save_draft ~id ctxt (kind ());
    Web_app_state.Current_page.change_to ctxt
      (Web_app_state.Page.draft_account ~id:(Some id));
    ()
  in
  match account_id with
  | None ->
      h3 (t "Create/Import a New Account")
      % Custom_widget.List_of_actions.(
          render
            [
              item
                (t "Originate a new Generic Multisig")
                ~action:(start_draft Account.Draft.create_generic_multisig);
              item
                (t "Originate a custom contract")
                ~action:(start_draft Account.Draft.create_custom_contract);
              item
                (t "Import an addess or URI")
                ~action:(start_draft Account.Draft.create_from_address_or_uri);
            ])
      % Reactive.bind (Web_app_state.accounts ctxt) ~f:(fun accounts ->
            List.fold accounts ~init:(empty ()) ~f:(fun prev acc ->
                match acc.state with
                | Draft draft ->
                    let name =
                      match acc.display_name with
                      | "" -> String.sub ~pos:0 ~len:6 acc.id ^ "…"
                      | n -> n
                    in
                    let name_html =
                      a
                        ~a:[ H5.a_id (Reactive.pure acc.id) ]
                        (Fmt.kstr bt "Draft “%s”" name)
                      %% parens
                           (match draft with
                           | Account.Draft.From_address_or_uri _ ->
                               it "URI/Address Import"
                           | Generic_multisig _ -> it "Generic Multisig"
                           | Custom_contract _ -> it "Custom-contract")
                    in
                    prev
                    % div
                        (Custom_widget.inline_button (t "Resume")
                           ~action:(fun () ->
                             Web_app_state.Current_page.change_to ctxt
                               (Web_app_state.Page.draft_account
                                  ~id:(Some acc.id));
                             ())
                        %% t "/"
                        %% Custom_widget.inline_button (t "Discard")
                             ~action:(fun () ->
                               Web_app_state.call_server_async ctxt
                                 (Protocol.Message.Up.delete_account acc.id);
                               ())
                        %% name_html
                        %% Overview_page.show_comments_summary acc
                             ~wrap:(fun c -> t ":" %% c))
                | _ -> prev))
  | Some account_id ->
      h2 (t "Account Creation/Import")
      % Reactive.bind
          (Web_app_state.dynamic_accounts ctxt
          |> Reactive.Table.find_map ~f:(fun dyn ->
                 if
                   String.equal
                     (Web_app_state.Dynamic_account.id dyn)
                     account_id
                 then Some dyn
                 else None))
          ~f:(function
            (* fun accounts ->
               List.find_map accounts ~f:(fun dyn ->
                   if
                     String.equal
                       (Web_app_state.Dynamic_account.id dyn)
                       account_id
                   then Some dyn
                   else None )
               |> *)
            | Some ({ v = { state = Account.Status.Draft draft; _ }; _ } as acc)
              ->
                draft_edition_ui ctxt acc draft
            | None | Some _ ->
                Bootstrap.alert ~kind:`Danger
                  (Fmt.kstr bt "Account %s is not a draft ⇒ BUG Found"
                     account_id))
