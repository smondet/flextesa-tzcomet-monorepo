open! Import
open! Babuso_lib
open! Wallet_data
open! Lwd_bootstrap

type style =
  | Mono_right_aligned of { decimals : int }
  | Inline of { decimals : int }
[@@deriving variants, sexp, equal, compare]

(** The “default” style is the one for the overview table; just for historical
    reasons. *)
let default_style = mono_right_aligned ~decimals:6

let adaptive_inline_style mutez =
  let decimals = if Z.gt mutez (Z.of_int 1_000) then 3 else 6 in
  inline ~decimals

let float_to_string ?(force_right_space = true) ?(decimals = 6) ?unit bal =
  let jane_st =
    Float.to_string_hum ~delimiter:',' ~decimals bal ~strip_zero:true
  in
  let buf = Buffer.create 12 in
  let int_part s =
    String.iter s ~f:(function
      | ',' -> Buffer.add_string buf " "
      | other -> Buffer.add_char buf other)
  in
  begin
    match String.lsplit2 jane_st ~on:'.' with
    | None (* integer *) ->
        int_part jane_st;
        if force_right_space then (
          Buffer.add_char buf '.';
          for _ = 1 to decimals do
            Buffer.add_string buf " "
          done)
    | Some (i, d) ->
        int_part i;
        Buffer.add_char buf '.';
        Buffer.add_string buf d;
        if force_right_space then
          for _ = 1 to decimals - String.length d do
            Buffer.add_string buf " "
          done
  end;
  Option.iter unit ~f:(fun u ->
      Buffer.add_string buf " ";
      Buffer.add_string buf u);
  Buffer.contents buf

let tez ?(with_style = default_style) mutez =
  let open Mono_html in
  match with_style with
  | Mono_right_aligned { decimals } ->
      ct
        (float_to_string ~unit:"ꜩ"
           (Mutez.to_float mutez /. 1_000_000.)
           ~decimals ~force_right_space:true)
  | Inline { decimals } ->
      t
        (float_to_string ~unit:"ꜩ"
           (Mutez.to_float mutez /. 1_000_000.)
           ~decimals ~force_right_space:false)

let inline_tez mutez =
  let with_style = adaptive_inline_style mutez in
  tez ~with_style mutez

let of_account_tez ?(with_style = default_style) acc =
  let open Mono_html in
  match acc.Account.balance with None -> it "🤷" | Some i -> tez ~with_style i

let converted ?(with_style = default_style) ctxt bal currency =
  let open Mono_html in
  Reactive.bind_var (Web_app_state.exchange_rates ctxt) ~f:(fun exch ->
      match exch with
      | None -> it "🤷"
      | Some ex -> (
          let factor, unit = Currency.to_unit currency in
          let converted =
            let open Float in
            let base_tz = Z.to_float bal / 1_000_000. in
            factor * base_tz * Exchange_rates.get_currency ex currency
          in
          match with_style with
          | Mono_right_aligned { decimals } ->
              ct
                (float_to_string ~force_right_space:true ~unit converted
                   ~decimals)
          | Inline { decimals } ->
              t
                (float_to_string ~force_right_space:false ~unit converted
                   ~decimals)))

let of_account_converted ?(with_style = default_style) ctxt acc currency =
  let open Mono_html in
  Reactive.bind_var (Web_app_state.exchange_rates ctxt) ~f:(fun exch ->
      match (Account.balance acc, exch) with
      | None, _ | _, None -> it "🤷"
      | Some bal, Some ex -> (
          let factor, unit = Currency.to_unit currency in
          let converted =
            let open Float in
            let base_tz = Z.to_float bal / 1_000_000. in
            factor * base_tz * Exchange_rates.get_currency ex currency
          in
          match with_style with
          | Mono_right_aligned { decimals } ->
              ct
                (float_to_string ~force_right_space:true ~unit converted
                   ~decimals)
          | Inline { decimals } ->
              t
                (float_to_string ~force_right_space:false ~unit converted
                   ~decimals)))

let multi ?(with_style = default_style)
    ?(currencies = [ `USD; `EUR; `BTC; `ETH ]) ctxt mutez =
  let open Mono_html in
  let bal cur = converted ctxt mutez cur ~with_style in
  tez mutez ~with_style
  %%
  match currencies with
  | [] -> empty ()
  | one :: more ->
      parens
        (List.fold more ~init:(bal one) ~f:(fun prev cur ->
             prev % t " ≅ " % bal cur))
