module W = Webdriver_cohttp_lwt_unix

module Failure : sig
  type t

  exception F of { str : string; retry : bool }

  val failure : string -> bool -> exn
  val raise : string -> bool -> 'a
end

module Unit_of_retry : sig
  type attribute_asserter = string -> string -> bool

  type attribute_assert_data = {
    name : string;
    value : string;
    msg : string;
    asserter : attribute_asserter;
  }
  [@@deriving make]

  module Find : sig
    type find_one = {
      from : W.elt option;
      usin : W.using;
      search_str : string;
      attrib_assert_data : attribute_assert_data option;
    }
    [@@deriving make]

    type find_multi = {
      from : W.elt option;
      usin : W.using;
      search_str : string;
      selector_text : string;
    }
    [@@deriving make]

    type simple_find = Find_one of find_one | Find_multi of find_multi
    [@@deriving variants]

    type t =
      | Single_element of simple_find
      | Nested_elements of simple_find list
    [@@deriving variants]
  end

  type command = Click | Send_key_list of { keys : string }
  [@@deriving variants]

  type unit_of_work_assert = W.elt -> bool

  type t = {
    unit_name : string;
    find_query : Find.t;
    pre_command_wait : float option;
    commands : command list;
    uow_assert : unit_of_work_assert option;
  }
  [@@deriving make]

  val assert_condition : string -> bool -> unit
  val assert_equal : attribute_asserter
  val assert_starts_with : attribute_asserter
  val send_backspaces_cmd : unit -> command
  val babuso_top : string
end

val sleep : float -> unit W.cmd
val do_with_retry : Unit_of_retry.t -> W.elt W.cmd
