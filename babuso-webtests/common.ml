open Webdriver_cohttp_lwt_unix
open Infix
open! Babuso_lib.Import
module W = Webdriver_cohttp_lwt_unix

module Failure = struct
  type t

  exception F of { str : string; retry : bool }

  let failure str retry = F { str; retry }
  let raise str retry = raise (failure str retry)

  let () =
    Stdlib.Printexc.register_printer (function
      | F { str; retry } ->
          let retry_str =
            if retry then "[retry enabled]" else "[retry disabled]"
          in
          Some (Fmt.str "%s%s" str retry_str)
      | _ -> None)
end

module Unit_of_retry = struct
  type attribute_asserter = string -> string -> bool

  type attribute_assert_data = {
    name : string;
    value : string;
    msg : string;
    asserter : attribute_asserter;
  }
  [@@deriving make]

  module Find = struct
    type find_one = {
      from : elt option;
      usin : using;
      search_str : string;
      attrib_assert_data : attribute_assert_data option;
    }
    [@@deriving make]

    type find_multi = {
      from : elt option;
      usin : using;
      search_str : string;
      selector_text : string;
    }
    [@@deriving make]

    type simple_find = Find_one of find_one | Find_multi of find_multi
    [@@deriving variants]

    type t =
      | Single_element of simple_find
      | Nested_elements of simple_find list
    [@@deriving variants]
  end

  type command = Click | Send_key_list of { keys : string }
  [@@deriving variants]

  type unit_of_work_assert = W.elt -> bool

  type t = {
    unit_name : string;
    find_query : Find.t;
    pre_command_wait : float option;
    commands : command list;
    uow_assert : unit_of_work_assert option;
  }
  [@@deriving make]

  let assert_condition msg = function
    | true -> ()
    | false ->
        let open Failure in
        raise (Fmt.str "Assertion failed: %s" msg) true

  let assert_equal value expected =
    let b = String.equal value expected in
    if not b then
      Fmt.epr "assert_equal - %s does not equal %s\n%!" value expected;
    b

  let assert_starts_with value expected_prefix =
    let b = Caml.String.starts_with ~prefix:expected_prefix value in
    if not b then
      Fmt.epr "assert_starts_with - %s does not start with %s\n%!" value
        expected_prefix;
    b

  let send_backspaces_cmd () =
    let rec f k = function 0 -> "" | n -> k ^ f k (n - 1) in
    let backspaces = f Key.backspace 32 in
    send_key_list ~keys:backspaces

  let babuso_top = "Babuso-top"
end

let sleep dt =
  Fmt.pr ".%!";
  lift (Lwt_unix.sleep dt)

let do_with_retry (u : Unit_of_retry.t) =
  let open Unit_of_retry in
  let open Failure in
  let max_retries = 10 in
  let rec retry count =
    W.Error.catch
      (fun () ->
        (* from_param is used when fq.from is None *)
        let process_find ?from_param fq =
          let open Find in
          match fq with
          | Find_one fo -> (
              let open Find in
              let the_from =
                match fo.from with None -> from_param | Some fr -> Some fr
              in
              let* elt = find_first ?from:the_from fo.usin fo.search_str in
              match fo.attrib_assert_data with
              | None -> return elt
              | Some aa ->
                  let* attrib = attribute elt aa.name in
                  let b = aa.asserter attrib aa.value in
                  let () = assert_condition aa.msg b in
                  return elt)
          | Find_multi fm -> (
              let open Find in
              Fmt.epr "%s\n%!" "Find_multi:...";
              let the_from =
                match fm.from with None -> from_param | Some fr -> Some fr
              in
              let* elts = find_all ?from:the_from fm.usin fm.search_str in
              Fmt.epr "Find_multi - searching %d elements for: %s\n%!"
                (List.length elts) fm.selector_text;
              let foldf : elt -> elt option cmd -> elt option cmd =
               fun el acc_cmd ->
                let* acc = acc_cmd in
                let* txt = text el in
                if String.equal txt fm.selector_text then return (Some el)
                else return acc
              in
              let* match_opt =
                List.fold_right elts ~f:foldf ~init:(return None)
              in
              match match_opt with
              | None ->
                  W.Error.fail
                    (failure
                       (Fmt.str
                          "find_multi - the selector text was not found: \
                           \"%s\" "
                          fm.selector_text)
                       true)
              | Some x -> return x)
        in
        let find_q = u.find_query in
        let* find_result_elt =
          match find_q with
          | Single_element fq ->
              let* result_elt = process_find fq in
              return result_elt
          | Nested_elements [] ->
              W.Error.fail
                (failure "Empty Nested_find list - nothing to find." true)
          | Nested_elements xs ->
              let rec nested fqs prev_element =
                match fqs with
                | [] ->
                    return (Option.value_exn prev_element)
                    (* should never be None *)
                | y :: ys ->
                    let* elt = process_find ?from_param:prev_element y in
                    nested ys (Some elt)
              in
              let* z = nested xs None in
              return z
        in
        let wait_amt =
          match u.pre_command_wait with None -> 1.0 | Some w -> w
        in
        let rec do_cmds cmds =
          match cmds with
          | [] -> return ()
          | c :: cs ->
              let* () = sleep wait_amt in
              let* () =
                match c with
                | Click -> W.click find_result_elt
                | Send_key_list sks -> send_keys find_result_elt sks.keys
              in
              do_cmds cs
        in
        let* () = do_cmds (u.commands : command list) in
        Fmt.pr "\"%s\" - success (%d retries)\n%!" u.unit_name count;
        return find_result_elt)
      ~errors:[ `no_such_element; `stale_element_reference ]
      (fun e ->
        let open Failure in
        let* () = sleep 0.1 in
        match e.error with
        | `stale_element_reference ->
            if count + 1 >= max_retries then (
              Fmt.epr
                "[FAIL] inner loop - 'stale element reference' - Failing after \
                 max retries\n\
                 %!";
              W.Error.fail (failure (W.Error.to_string e) true))
            else retry (count + 1)
        | `no_such_element ->
            if count + 1 >= max_retries then (
              Fmt.epr
                "[FAIL] inner loop - 'no such element' - Failing after max \
                 retries\n\
                 %!";
              W.Error.fail (failure (W.Error.to_string e) true))
            else retry (count + 1)
        | _ ->
            if count + 1 >= max_retries then (
              Fmt.epr
                "[FAIL] inner loop - unexpected error - Failing after max \
                 retries\n\
                 %!";
              W.Error.fail (failure (W.Error.to_string e) true))
            else retry (count + 1))
  in
  retry 0
