open Webdriver_cohttp_lwt_unix
open Infix
open Common
open! Babuso_lib.Import

let find_navbar () =
  let open Unit_of_retry in
  let open Find in
  let attrib_assert_data =
    make_attribute_assert_data ~name:"class" ~value:"navbar-toggler bg-light"
      ~msg:"navbar class name does not match" ~asserter:assert_equal
  in
  let find_query =
    Single_element
      (Find_one
         (make_find_one ~usin:`xpath ~search_str:"/html/body/div/div/nav/button"
            ~attrib_assert_data ()))
  in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"find navbar" ~find_query ~commands:[] ()
  in
  do_with_retry retry_unit

let click_new_account () =
  let open Unit_of_retry in
  let open Find in
  let find_query =
    Single_element
      (Find_one
         (make_find_one ~usin:`xpath
            ~search_str:"/html/body/div/div/nav/div[1]/ul/li[2]/a" ()))
  in
  let commands = [ Click ] in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"click new account" ~find_query ~commands ()
  in
  do_with_retry retry_unit

let click_address_or_uri () =
  let open Unit_of_retry in
  let open Find in
  let attrib_assert_data =
    make_attribute_assert_data ~name:"class"
      ~value:"btn btn-outline-info btn-sm"
      ~msg:"Address or URI button - class name does not match"
      ~asserter:assert_equal
  in
  let find_query =
    Single_element
      (Find_one
         (make_find_one ~usin:`xpath
            ~search_str:"/html/body/div/div/div[4]/button" ~attrib_assert_data
            ()))
  in
  let commands = [ Click ] in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"click address or uri" ~find_query ~commands
      ()
  in
  do_with_retry retry_unit

let new_acct_name = "Webdriver Alice-tz1"

let enter_display_name () =
  let open Unit_of_retry in
  let open Find in
  let att_assert_data_1 =
    make_attribute_assert_data ~name:"class" ~value:"form-group"
      ~msg:"Display name form group - form class name does not match"
      ~asserter:assert_equal
  in
  let find_query_1 =
    Find_one
      (make_find_one ~usin:`xpath
         ~search_str:"/html/body/div/div/div[4]/div[1]/div"
         ~attrib_assert_data:att_assert_data_1 ())
  in
  let att_assert_data_2 =
    make_attribute_assert_data ~name:"id" ~value:"input-item-"
      ~msg:"Display name form group -- input control id prefix does not match"
      ~asserter:assert_starts_with
  in
  let find_query_2 =
    Find_one
      (make_find_one ~usin:`tag_name ~search_str:"input"
         ~attrib_assert_data:att_assert_data_2 ())
  in
  let nested_find = Nested_elements [ find_query_1; find_query_2 ] in
  let bs = send_backspaces_cmd () in
  let commands = [ bs; send_key_list ~keys:(new_acct_name ^ Key.enter) ] in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"enter display name" ~find_query:nested_find
      ~commands ()
  in
  do_with_retry retry_unit

let enter_uri () =
  let open Unit_of_retry in
  let open Find in
  let att_assert_data_1 =
    make_attribute_assert_data ~name:"class" ~value:"form-group"
      ~msg:"uri form group - form class name does not match"
      ~asserter:assert_equal
  in
  let find_query_1 =
    Find_one
      (make_find_one ~usin:`xpath
         ~search_str:"/html/body/div/div/div[4]/div[2]/div[3]"
         ~attrib_assert_data:att_assert_data_1 ())
  in
  let att_assert_data_2 =
    make_attribute_assert_data ~name:"id" ~value:"input-item-"
      ~msg:"uri form group -- input control id prefix does not match"
      ~asserter:assert_starts_with
  in
  let find_query_2 =
    Find_one
      (make_find_one ~usin:`tag_name ~search_str:"input"
         ~attrib_assert_data:att_assert_data_2 ())
  in
  let nested_find = Nested_elements [ find_query_1; find_query_2 ] in
  let bs = send_backspaces_cmd () in
  let commands =
    [
      bs;
      send_key_list ~keys:("tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjb" ^ Key.enter);
    ]
  in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"enter form uri" ~find_query:nested_find
      ~commands ()
  in
  do_with_retry retry_unit

(* TODO: This may not be needed... *)
let find_babuso_top () =
  let open Unit_of_retry in
  let open Find in
  let attrib_assert_data =
    make_attribute_assert_data ~name:"id" ~value:babuso_top
      ~msg:"new_account - the id in the topmost element does not match"
      ~asserter:assert_equal
  in
  let find_query =
    Single_element
      (Find_one
         (make_find_one ~usin:`xpath ~search_str:"//*[@id=\"Babuso-top\"]"
            ~attrib_assert_data ()))
  in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"find babuso top" ~find_query ~commands:[] ()
  in
  do_with_retry retry_unit

let click_add_create () =
  let open Unit_of_retry in
  let open Find in
  let find_query =
    Single_element
      (Find_multi
         (make_find_multi ~usin:`xpath
            ~search_str:"/html/body/div/div/*/button[3]"
            ~selector_text:"Add/Create Account" ()))
  in
  let commands = [ Click ] in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"click add/create button" ~find_query
      ~commands ()
  in
  do_with_retry retry_unit

let find_acct_creation_summary () =
  let open Unit_of_retry in
  let open Find in
  let find_query =
    Single_element
      (Find_multi
         (make_find_multi ~usin:`xpath
            ~search_str:"/html/body/div/div/*/div[1]/h4"
            ~selector_text:"Account Creation Summary" ()))
  in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"find account creation summary" ~find_query
      ~commands:[] ()
  in
  do_with_retry retry_unit

let click_add_create_ack () =
  let open Unit_of_retry in
  let open Find in
  let find_query =
    Single_element
      (Find_multi
         (make_find_multi ~usin:`xpath
            ~search_str:"/html/body/div/div/*/div[2]/p/button"
            ~selector_text:"Add/Make Account" ()))
  in
  let commands = [ Click ] in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"click add/create button acknowledgement"
      ~find_query ~commands ()
  in
  do_with_retry retry_unit

let find_acct_in_table () =
  let open Unit_of_retry in
  let open Find in
  let find_query =
    Single_element
      (Find_multi
         (make_find_multi ~usin:`xpath
            ~search_str:"/html/body/div/div/table[1]/tbody/*/td[1]/a/b"
            ~selector_text:new_acct_name ()))
  in
  let retry_unit =
    Unit_of_retry.make ~unit_name:"find account in table" ~find_query
      ~commands:[] ()
  in
  do_with_retry retry_unit

let run_test ~babuso_port () =
  let* () = Fmt.kstr goto "localhost:%d" babuso_port in
  let* _ = find_navbar () in
  let* _ = click_new_account () in
  let* _ = click_address_or_uri () in
  let* _ = enter_display_name () in
  let* _ = enter_uri () in
  let* _ = click_add_create () in
  let* _ = find_acct_creation_summary () in
  let* _ = click_add_create_ack () in
  let* _ = find_acct_in_table () in
  return ()

(* let host = "http://127.0.0.1:4444" *)
let () =
  match Caml.Sys.argv |> Array.to_list with
  | [ _; "--babuso-port"; babport; "--geckodriver-host"; host ] ->
      (* try Lwt_main.run (run ~host Capabilities.firefox test) *)
      let test = run_test ~babuso_port:(Int.of_string babport) () in
      Lwt_main.run (run ~host Capabilities.firefox_headless test);
      Fmt.pr "Ok!\n%!";
      ()
  | other -> Fmt.failwith "Wrong command-line: %a" Fmt.Dump.(list string) other
